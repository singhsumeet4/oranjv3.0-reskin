module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '../Oranj',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            '../dist/scripts/vendor.js',
            '../dist/scripts/ui.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'vendors/*.js',
            'app/common/app.module.js',
            'app/common/**/*.module.js',
            'js/*.js',
            'services/*.js',
            'app/**/*.js',
            'app/**/*.html',
            '../tests/unit-tests/**/*.js'
        ],


        // list of files to exclude
        exclude: [],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'app/**/*.html': ['ng-html2js'],
            'app/**/*.js': ['coverage']
        },

        ngHtml2JsPreprocessor: {
            // strip this from the file path
            //  stripPrefix: 'www/',
            moduleName: 'unitTestTemplates'
        },

        coverageReporter: {
            type: 'lcov',
            dir: '../tests/coverage/',
            file: 'coverage.txt',
            check: {
                global: {
                  //  statements: 80,
                  //  lines: 80,
                   // functions: 80,
                  //  branches: 0
                }
            }
        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'coverage' ],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
       // singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity
    })
}
