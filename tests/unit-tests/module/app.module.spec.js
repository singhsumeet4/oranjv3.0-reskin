describe('Main application module should be loaded', function() {
    
    beforeEach(module('app'));
    
    it('app module should be defined', function() {
        expect(angular.module('app')).toBeDefined();
    })
})