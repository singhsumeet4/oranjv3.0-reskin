module.exports = function() {
    var client = 'Oranj',
        clientApp = './Oranj/app'
        dist = 'dist',
        tmp = '.tmp',
        docs = 'documentation',
        landing = 'landing';
    var config = {
        client: client,
        clientApp: clientApp,
        dist: dist,
        tmp: tmp,
        index: client + "/index.html",
        alljs: [
            client + "/app/**/*.js",
            './*.js'
        ],
        assets: [
            client + "/app/**/*.html",
            client + "/bower_components/font-awesome/css/*",
            client + "/bower_components/font-awesome/fonts/*",
            client + "/bower_components/weather-icons/css/*",
            client + "/bower_components/weather-icons/font/*",
            client + "/bower_components/weather-icons/fonts/*",
            client + "/bower_components/material-design-iconic-font/dist/**/*",
            client + "/fonts/**/*",
            client + "/i18n/**/*",
            client + "/images/**/*",
            client + "/styles/loader.css",
            client + "/styles/ui/images/*",
            client + "/favicon.ico"
        ],
        less: [],
        sass: [
            client + "/styles/**/*.scss"
        ],
        js: [
            clientApp + "/**/*.module.js",
            clientApp + "/**/*.js",
            '!' + clientApp + "/**/*.spec.js"
        ],
        allToClean: [
            tmp,
            ".DS_Store",
            ".sass-cache",
            "node_modules",
            ".git",
            client + "/bower_components",
            docs + "/jade",
            docs + "/layout.html",
            landing + "/jade",
            landing + "/bower_components",
            "readme.md"
        ],
        allTemplatesHtml: clientApp+'/**/*.html',
        apiUrl: 'http://oranjadvisor.dev30sing.oranjsites.com',
        //apiUrl: 'http://localhost:8080',
        socketUrl: 'http://ionic.dev30sing.oranjsites.com',
        wsSocketUrl: 'ws://ionic.dev30sing.oranjsites.com',
        apiFirm: 'nij'
    };

    return config;
};
