## Oranj Reskin Project ##

##### Running the project in your local.

* Once you have a clone of the project in your local, follow the below steps:
	* Open terminal/cmd, and navigate to the folder were you cloned the repo.
	* Run the below commands:
		* npm install
		* bower install
	* Open gulp.config.js,
		Here find the property ** apiUrl **. You can set to the host name of any server whoz API you want to use.
	    
	    Find the Property named ** apiFirm **. You can set the property to whichever firm you want to use for application.
	* Run ** gulp serve **    	


##### Coding Standards & Architecture 

** Folder Structure to follow **

```
├── root/
│   ├── Readme.md
│   ├── gulpfile.js
│   ├── gulp.config.js
│   ├── Oranj (Parent Application folder)
│   │   ├── app (Javascript & template folder)
│   │   |   ├── advisor (Advisor Features)
│   │   │   ├── client  (Client Features)
│   │   │   |    ├── goals  (goal feature)
│   │   │   |    |     ├── goals.module.js  (goal feature main module )
│   │   │   |    |     ├── goals.dashboard.controller.js  (goal feature, dashboard screen controller)
│   │   │   |    |     ├── goals.data.service.js  (goal feature, dashboard screen controller)
│   │   │   |    |     ├── html  (All templates related to goal feature go here)
│   │   │   |    |     |    ├── goal.dashboard.html  (All templates related to goal feature go here)
│   │   │   |    ├── vault  (vault feature)
│   │   │   ├── common  (Application level Features)
│   │   ├── bower_components (Third Party Liberaries)
│   │   ├── images (Images)
│   │   ├── fonts  (Font files)
│   │   ├── styles (CSS files)
│   │   ├── index.html (Main index.html)
│   │   ├── favicon.ico (Fav icon for website)
│   ├── tests (Parent Application folder)
│   |   ├── coverage (Coverage Reporter Results)
│   |   ├── results (Unit test case results)
│   |   ├── client (Client Features)
│   │   │   |    ├── goals  (goal feature)
│   │   │   |    |     ├── goals.module.spec.js  (goal feature main module )
│   |   ├── advisor (Advisor Features)
│   |   ├── common (common features)
│   |   ├── unit.karma.conf.js (Karma Config File)
```
** Naming Conventions & Coding Practices **

* JS files
	* All features inside a particular feature should have there own ** module.js ** file.
	* Any modules name should be the format ** api.<feature_name> OR api.client.<feature_name> **
	* All modules present inside the common folder become dependencies to the ** app (app.module.js) ** module. All modules defined inside user folder (client/advisor) become dependencies to the ** api.client (client.module.js)/ api.advisor (advisor.module.js) ** module.
	* All files inside a feature should have the fetaure name prefixed to the file name. For Ex: 	  goal.module.js, goal.dashboard.controller.js
	* All components should be defined in there own file. ***** No two components within the same file.***
	* **Keep direct use of jQuery to minimum. Write directives to get access to elements. Do not use jQuery selectors in controllers.**
* Test case files
	* All test case files will follow the same rules as above.
	* All test case files should have .spec.js at the end of the file. For Ex: goal.dashboard.controller.spec.js
	* The folder structure for test case remains the same as inside the app folder.
* HTML Files
	* Template files will have the feature name, followed by a logical view name (Probably the state name), followed by any distinguishing name then .html. For Ex: goals.dahsboard.html or goal.detail.html.

** Configuring States **

* All client/advisor specific states should be child of the "page" state. For Ex: 

  ```
  $stateProvider.state('signin', {
     templateUrl : 'app/common/page/signin.html'
  });
  // User specific states begin here.
                    .state('page', {
                        abstract: true,
                        //controller: 'AppCtrl'
                        templateUrl: 'app/common/page/page.layout.html',
                        url: '/page'
                    })
                    // Client states begin here.
                    .state('page.client', {
                        abstract: true,
                        controller: 'client.main.controller',
                        templateUrl: 'app/client/client.main.html',
                        url: '/client'
                    })


                    //Client goals feature states
                    .state('page.client.goal', {
                        abstract: true,
                        url: '/goals',
                        templateUrl: 'app/client/goals/html/goal.html',
                        controller: 'goalsClientController'
                    })
                    .state('page.client.goal.dashboard', {
                        url: '/dashboard',
                        //templateUrl: 'app/client/goals/goalRetirementMain.html'
                        templateUrl: 'app/client/goals/goalDash.html'
                    })
  ```

##### SERVICES, DIRECTIVE, FACTORIES

* OranjApiService
	- This is a data service wrapping the $http service we use to trigger AJAX calls. Basic functionality includes:
	* Include auth headers to every call.
	* Provide url rewrite with restfull parameters.
	* Provide auth interceptor functionality.
	
	** Usage **
	
	1> Any java API endpoint you want to call from your anywhere should be registered in the api.backend.endpoint.config.js. For Ex:

'login': {
    url: '/oauth/token',
    method: 'POST',
    noFirm: true
}
Here, "login" is the name of the API which would be used while initiating an AJAX call.
the object is a config object which would allow all config properties avaiable via $http service of angularJS.

A separate "noFirm" flag is for API's which do not require the firm name in the url. Default is false.

2> To initiate an ajax call, inject and use the OranjApiService. It is function which takes the following parameters:

OranjApiService(ApiConfigName[,$httpConfigObject, restFulParams])

ApiConfigName : It is the name of the api config that is defined in the api.backend.endpoint.config.js. Here it is 'login'.
$httpConfigObject : It is similar to the config object which you pass to $http service, use it in case you want to override/add to any config params already defined.
restFullParams : it is an object which would help to inject restfull params into the url.
For Ex:

For goals list the url is something like: 
/goal/stats/{userId}

Here userId is a restfull param.

So the config for such an endpoint would be:

'goalStats'; {
	url: '/goal/stats/:userid',
	restfull: true
}

Usage:

OranjApiService('goalStats',null,{userId:112022}).then(successFnctn, errorFnctn);

Resulting AJAX call would be to 
http://oranjadvisor.dev30sing.oranjsites.com:8080/oranj/nij/goal/stats/112022

Simple and easy, without the need to concatenate strings.
		 
	 
    
** More to be Added... **




 