(function () {


    angular.module('app.api')
        .value('apiEndpoints', {
            'login': {
                url: '/oauth/token',
                method: 'POST',
                noFirm: true
            },
            'forgotPassword': {
                url: '/password/recovery/email',
                method: 'POST'
            },
            'getUserInfo': {
                url: '/user-info'
            },
            'getUserRole': {
                url: '/user-info/roles'
            },
            'getGoalStats': {
                url: '/goal/list'
            },
            'getNetworthTotal': {
                url: '/networth/snapshot/total'
            },
            'getNetworthPercent': {
                url: '/networth/difference/percent?startDateString=:startDate&endDateString=:endDate',
                restfull: true
            },
            'getGoalTotal': {
                url: '/goal/snapshot/percentage-completion'
            },
            'getGoalPercent': {
                url: '/goal/range/percentage-completion?startDateString=:startDate&endDateString=:endDate',
                restfull: true
            },
            'getLiquidityTotal': {
                url: '/liquidity/snapshot/total'
            },
            'getLiquidityPercent': {
                url: '/liquidity/difference/percent?startDateString=:startDate&endDateString=:endDate',
                restfull: true
            },
            'getUserProfileData': {
                url: '/profile/getProfileAndAvatar?userId=:userid',
                restfull: true
            },
            'getAdvisorProfileData': {
                url: '/profile/getAdvisorProfileInfo'
            },
            'getuserAdvisorInfo': {
                url: '/user/userAdvisor'
            },
            'getVaultFolderListAll': {
                url: '/vault/user/folder'
            },
            'getVaultFileListAll': {
                url: '/vault/user/file'
            },
            'getVaultRecentList': {
                url: '/vault/user/files-recent'
            },
            'getVaultFolderData': {
                url: '/vault/user/file?folder=:folder',
                restfull: true
            },
            'getVaultFilePath': {
                url: '/vault/file/:id',
                restfull: true
            },
            'getVaultStarFolder': {
                url: '/vault/user/folder?starred=true'
            },
            'getVaultStarFile': {
                url: '/vault/user/files?starred=true'
            },
            'getVaultSearchList': {
                url: '/vault/search',
                method: 'POST',
                restfull: true
            },
            'vaultDeleteFolder': {
                url: '/vault/delete/folder',
                method: 'POST',
                restfull: true
            },
            'vaultDeleteFile': {
                url: '/vault/delete/file/:id',
                method: 'DELETE',
                restfull: true
            },
            'updateVault': {
                url: '/vault/update',
                method: 'PUT',
            },
            'vaultCreateFolder': {
                url: '/vault/folder/add',
                method: 'POST',
            },
            'vaultRenameFolder': {
                url: '/vault/user/rename-folder',
                method: 'POST',
            },
            'vaultUploadRoot': {
                url: '/vault/file-upload',
                method: 'POST',
                restfull: true
            },
            'vaultUploadFolder': {
                url: '/vault/file-upload?folder=:folder',
                method: 'POST',
                restfull: true
            },
            'downloadFrequency': {
                url: '/vault/file/downloadCount/:id',
                method: 'POST',
                restfull: true
            },
            'getRiskListForInsurance': {
                url: '/risk/list?groupByType=true'
            },
            'getSettings': {
                url: '/user/email/turned-on-off/UserEmailSettings'
            },
            'getAssets': {
                url: '/networth/user/assets?version=1'
            },
            'getLiabilities': {
                url: '/networth/user/liabilities?version=1'
            },
            'getNotifications': {
                url: '/alert/list?pageIndex=:pageNum&pageSize=:pageSize',
                restfull: true
            },
            'getAccountMainHeader': {
                url: '/account/header',
                restfull: true
            },
            'getListOfAccount': {
                url: '/user/accounts'
            },
            'getTranscations': {
                url: '/transactions/list',
                method: 'POST',
                restfull: true
            },
            'getAccountMainHistoryChart': {
                //url: '/chart-of-flows/listAccounts/:startDate/:endDate',
                url: '/user/accounts/chart/:startDateString/:endDateString',
                restfull: true
            },
            'getAccountMainHistoryChartAdvisor': {
                //url: '/chart-of-flows/listAccounts/:startDate/:endDate',
                url: '/user/accounts/chart/:startDateString/:endDateString?userId=:userId',
                restfull: true
            },
            'goalsList': {
                url: '/goal-account/by-account/count/:userId',
                restfull: true
            },
            'goalsAdd': {
                url: '/goal/add',
                method: 'POST',
                restfull: true
            },
            'goalDelete': {
                url: '/goal/:goalId',
                method: 'DELETE',
                restfull: true
            },
            'getInvestmentPerformaceChart': {
                url: '/investment/performanceChart/:startDate/:endDate',
                restfull: true
            },
            'getInvestmentHeader': {
                url: '/investment/account/header',
                restfull: true
            },
            'getInvestmentTotal': {
                url: '/investment/snapshot/total'
            },
            'getInvestmentPercent': {
                url: '/investment/difference/percent?startDateString=:startDate&endDateString=:endDate',
                restfull: true
            },
            'getInvestmentAccounts': {
                url: '/investment/accounts?version=1',
                restfull: true
            },
            'getInvestmentMainAssetChart': {
                url: "/investment/assets-pie-chart",
                restfull: true
            },
            'getInvestementAccountHoldingsYodlee': {
                url: '/investment/holdings/by-account?yodleeItemAccountId=:yodleeItemAccountId',
                restfull: true
            },
            'getInvestmentHoldingsAssetChart': {
                url: "/investment/assets-pie-chart/by-account/:yodleeItemAccountId",
                restfull: true
            },
            'getInvestmentHoldingsPercent': {
                url: '/investment/performance-chart/by-account/:yodleeItemAccountId/:startDate/:endDate',
                restfull: true
            },
            'getcollegeListByState': {
                url: '/college/list/:state',
                restfull: true
            },
            'getDependents': {
                url: '/user/dependents',
                restfull: true
            },
            'getAccountSpecificGoal': {
                url: '/goal-account/details/:yodleeItemAccountId',
                restfull: true
            },
            'getTransactionHistoryChart': {
                url: '/user/accounts/chart/by-account/:yodleeItemAccountId/:startDateString/:endDateString',
                restfull: true
            },
            'updateProfileInfo': {
                url: '/profile/updateInfo?userId=:userid',
                method: 'PUT',
                restfull: true
            },
            'updateAvatar': {
                url: '/profile/avatar/upload?userId=:userid',
                method: 'POST',
                restfull: true
            },
            'deleteAvatar': {
                url: '/profile/avatar/delete?userId=:userid',
                method: 'DELETE',
                restfull: true
            },
            'updateAdvisorAvatar': {
                url: '/advisor-portal/home/advisor/avatar/upload',
                method: 'POST',
                restfull: true
            },
            'changePass': {
                url: '/user/password/change',
                method: 'PUT',
                restfull: true
            },
            'updateSettings': {
                url: '/user/email/turned-on-off/UserEmailSettings',
                method: 'POST',
                restfull: true
            },
            'getAdv': {
                url: '/firm-info/basic-info/adv-file',
            },
            'getGoalAccountsClient': {
                url: '/goal-account/list/:id',
                restfull: true
            },
            'getGoalContributionHistory': {
                url: '/goal/contribution/history?' +
                'startDate=:startDateParam' +
                '&endDate=:endDateParam' +
                '&goalId=:goalId',
                restfull: true
            },
            'getfeedActivity': {
                url: '/advisor-client/life-feed-activity?userId=:userid&pageNumber=:pageNum&pageSize=:pageSize&filter=:filter&search=:search',
                restfull: true
            },
            'getQuestions': {
                url: '/admin/question/list',
            },
            'getMatchedPortfolio': {
                url: '/virtualwizard/portfolio/matched-list',
                method: 'POST',
                restfull: true
            },
            'updateAdvisorProfile': {
                url: '/advisor-portal/home/advisor/save',
                method: 'POST',
                restfull: true
            },
            'getFirmDisclaimer': {
                url: '/firm-info/disclaimer'
            },
            'getAllClientList': {
                url: '/advisor-portal/home/client-summary/?version=1'
            },
            'createContact': {
                url: '/welcomewizard/contact/add?' +
                'userType=:type',
                method: 'POST',
                restfull: true
            },
            'sendCredentialsToClient': {
                url: '/advisor-portal/home/credentials/send',
                method: 'POST',
                restfull: true
            },
            'getFinancialAggregationSite': {
                url: '/aggregation-site/get-all-sites',
                restfull: true
            },
            'getAggregationSiteLoginForm': {
                url: '/aggregation-site/site-login-form/:siteId',
                restfull: true
            },
            'getClientNetworthLiablity': {
                url: '/networth/asset/liability'
            },
            'getClientNetworthFinancial': {
                url: '/networth/financial'
            },
            'getInvestmentPercentWithoutDate': {
                url: '/investment/difference/percent'
            },
            'getNetworthHistory': {
                url: '/networth/history?' +
                'startDateString=:startDate' +
                '&endDateString=:endDate',
                restfull: true
            },
            'getLiquidityAccounts': {
                url: '/liquidity/liquidity-accounts?version=1'
            },
            'getInsuranceList': {
                url: '/risk/list'
            },
            'getAllInvestmentAccountHoldingsYodlee': {
                url: '/investment/holdings/by-account'
            },
            'getAllInvestmentAccountHoldingsQuovo': {
                url: '/investment/holdings/by-account?version=1'
            },
            'disclaimerCount': {
                url: '/user/disclaimer',
                method: 'POST',
            },
            'getUserVaultFolderListAll': {
                url: '/vault/user/folder?' +
                'userId=:userId' +
                '&shared=:shared',
                restfull: true
            },
            'getUserVaultFileListAll': {
                url: '/vault/user/file?' +
                'userId=:userId' +
                '&shared=:shared',
                restfull: true
            },
            'getAccountMainHeaderAdvisor': {
                url: '/account/header?userId=:userId',
                restfull: true
            },
            'getListOfAccountAdvisor': {
                url: '/user/accounts?userId=:userId',
                restfull: true
            },
            'getTranscationsAdvisor': {
                url: '/transactions/list?' +
                'userId=:userId',
                method: 'POST',
                restfull: true
            },
            'getInvestmentHeaderAdvisor': {
                url: '/investment/account/header?userId=:userId',
                restfull: true
            },
            'getInvestmentAccountsAdvisor': {
                url: '/investment/accounts?version=1&&userId=:userId',
                restfull: true
            },
            'getInvestmentMainAssetChartAdvisor': {
                url: "/investment/assets-pie-chart?userId=:userId",
                restfull: true
            },
            'getInvestementAccountHoldingsYodleeAdvisor': {
                url: '/investment/holdings/by-account?yodleeItemAccountId=:yodleeItemAccountId&&userId=:userId',
                restfull: true
            },
            'getInvestementAccountHoldingsQuovoAdvisor': {
                url: '/investment/holdings/by-account?yodleeItemAccountId=:yodleeItemAccountId&version=1&&userId=:userId',
                restfull: true
            },
            'getInvestmentHoldingsAssetChartAdvisor': {
                url: "/investment/assets-pie-chart?accountId=:yodleeItemAccountId&&userId=:userId",
                restfull: true
            },
            'getInvestmentHoldingsPercentAdvsior': {
                url: '/investment/performance-chart/by-account/:yodleeItemAccountId/:startDate/:endDate&&userId=:userId',
                restfull: true
            },
            'getAccountSpecificHeaderAdvisor': {
                url: '/account/header?userId=:userId',
                restfull: true
            },
            'getInvestmentPerformaceChartAdvsior': {
                url: '/investment/performanceChart/:startDate/:endDate?userId=:userId',
                restfull: true
            },
            'getUserVaultFolderData': {
                url: '/vault/user/file?' +
                'userId=:userId' +
                '&folder=:folderName' +
                '&shared=:shared',
                restfull: true
            },
            'vaultCreateUserFolder': {
                url: '/vault/folder/add?' +
                'userId=:userId',
                restfull: true,
                method: 'POST'
            },
            'vaultUploadUserRoot': {
                url: '/vault/file-upload?' +
                'userId=:userId',
                method: 'POST',
                restfull: true
            },
            'vaultUploadUserFolder': {
                url: '/vault/file-upload?' +
                'userId=:userId' +
                '&folder=:folder',
                method: 'POST',
                restfull: true
            },
            'getUserVaultFilePath': {
                url: '/vault/file/:id?' +
                'userId=:userId',
                restfull: true
            },
            'downloadUserFrequency': {
                url: '/vault/file/downloadCount/:id' +
                '?userId=:userId',
                method: 'POST',
                restfull: true
            },
            'getUserVaultSearchList': {
                url: '/vault/search?' +
                'userId=:userId',
                method: 'POST',
                restfull: true
            },
            'getTransactionHistoryChartAdvisor': {
                url: '/user/accounts/chart/by-account/:yodleeItemAccountId/:startDateString/:endDateString?userId=:userId',
                restfull: true
            },
            'vaultDeleteUserFolder': {
                url: '/vault/delete/folder?' +
                'userId=:userId',
                method: 'POST',
                restfull: true
            },
            'vaultDeleteUserFile': {
                url: '/vault/delete/file/:id?' +
                'userId=:userId',
                method: 'DELETE',
                restfull: true
            },
            'getAdvisorClientLastContactInfo': {
                url: '/message/last-contact-between/:chatKey',
                restfull: true
            },
            'getUserLastLogin': {
                url: '/advisor-portal/home/last-login/:userId',
                restfull: 'true'
            },
            'getAdvisorClientNetworth': {
                url : '/networth/?userId=:userId',
                restfull: true
            },
            'getGoalStatsForUser': {
                url: '/goal/list?userId=:userId',
                restfull: true
            },
            'getRiskListForInsuranceForUser': {
                url: '/risk/list?groupByType=true&userId=:userId',
                restfull: true
            },
            'getInvestmentTotalForUser': {
                url: '/investment/snapshot/total?userId=:userId',
                restfull: true
            },
            'getInvestmentPercentWithoutDateForUser': {
                url: '/investment/difference/percent?userId=:userId',
                restfull: true
            },
            'getAllInvestmentAccountHoldingsYodleeAdvisor': {
                url: '/investment/holdings/by-account?userId=:userId',
                restfull: true
            },
            'getAllInvestmentAccountHoldingsQuovoAdvisor': {
                url: '/investment/holdings/by-account?version=1&userId=:userId',
                restfull: true
            },
            'getLiquidityTotalAdvisor': {
                url: '/liquidity/snapshot/total?userId=:userId',
                restfull: true
            },
            'getLiquidityPercentAdvisor': {
                url: '/liquidity/difference/percent?startDateString=:startDate&endDateString=:endDate&userId=:userId',
                restfull: true
            },
            'getNetworthHistoryAdvisor': {
                url: '/networth/history?startDateString=:startDate&endDateString=:endDate&userId=:userId',
                restfull: true
            },
            'getLiquidityAccountsAdvisor': {
                url: '/liquidity/liquidity-accounts?version=1&userId=:userId',
                restfull: true
            },
            'getClientNetworthLiablityAdvisor': {
                url: '/networth/asset/liability?userId=:userId',
                restfull: true
            },
            'getClientNetworthFinancialAdvisor': {
                url: '/networth/financial?userId=:userId',
                restfull: true
            },
            'getRiskListForInsuranceAdvisor': {
                url: '/risk/list?groupByType=true&userId=:userId',
                restfull:true
            },
            'getGoalAccountsClientAdvisor': {
                url: '/goal-account/list/:id?userId=:userId',
                restfull: true
            },
            'getGoalContributionHistoryAdvisor': {
                url: '/goal/contribution/history?' +
                'startDate=:startDateParam' +
                '&endDate=:endDateParam' +
                '&goalId=:goalId' +
                '&userId=:userId',
                restfull: true
            }
        });
})();
