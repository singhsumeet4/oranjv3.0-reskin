(function() {

    function injectRestfullParams(_url_, paramObj) {
        var url = _url_;
        angular.forEach(paramObj, function(value, key) {
            url = url.replace(':'+key, value);
        });

        return url;
    }

    angular.module('app.api')
        .provider('OranjApiService', function() {
            var provider = this,
                host = '/oranj';
            var pathArray = window.location.hostname.split( '.' );
            var firmName = pathArray[0];

            provider.$get = ['$http', '$q','apiEndpoints','authInterceptorService', function($http, $q, apiEndpoints, authInterceptorService) {
                 function apiService(endpointName,_config_, restfullParams) {
                    var config = apiEndpoints[endpointName];

                    if(config === undefined) {
                        throw new Error('API config not found in endpointConfig. Please check api.backend.endpoints.config.js');
                        return ;
                    } else {
                        config = angular.copy(config);
                    }


                    //Inject Restful params here.
                    if(config.restfull) {
                        config.url = injectRestfullParams(config.url, restfullParams);
                    } else {
                        config.url = config.url;
                    }

                    if(config.noFirm) {
                        config.url = host+config.url;

                    } else {
                        config.url = host+'/'+firmName+config.url;
                    }



                    angular.extend(config, _config_);
                    config.method = config.method || 'GET';

                    function success(response) {
                        defer.resolve(response.data);
                    }

                    function error(error) {
                        defer.reject(error);
                    }

                    //Create defer object and supply
                    var defer = $q.defer();

                    $http(config).then(success, error);

                    return defer.promise;
                }


                apiService.host = host;
                apiService.firmname = firmName;
                apiService.authToken = authInterceptorService.getAuthtoken;
                return apiService;
            }];

        })
})();
