(function() {

    angular.module('app.api')
        .service('ApiLoading', function () {
            var nProgress = window.NProgress,
                _callsInProgress = 0, _hasStarted = false;

            nProgress.configure({ trickleRate: 0.01, trickleSpeed: 200, showSpinner: false });

            return {
                start: function() {
                    ++_callsInProgress;
                    if (!_hasStarted) {
                        nProgress.start();
                    } else {

                    }
                },
                stop: function() {
                    --_callsInProgress;
                    if (_callsInProgress <= 0) {
                        nProgress.done();
                    }
                }
            };
        });
})();
