'use strict';

/* Services */
angular.module('app.api')
    .factory('authInterceptorService', ['$q', '$location', 'AUTH_EVENTS', '$rootScope','$cookies','ApiLoading',
        function ($q, $location, AUTH_EVENTS, $rootScope, $cookies, ApiLoading) {

            var authInterceptorServiceFactory = {};
            var auth = $cookies.getObject('auth');
            var token = auth == undefined ? "" : auth.access_token;
            var userCredential = {
                "access_token": 'Bearer ' + token,
                "token_type": "bearer"
            };


            var _request = function (config) {

                ApiLoading.start();
                config.headers = config.headers || {};

                if (token && !config.headers.Authorization) {
                    config.headers.Authorization = config.headers.Authorization || 'Bearer ' + token;
                }
                return config;
            };

            var _response = function (res) {
                ApiLoading.stop();
                if (res.config.url.indexOf('/oauth/token') > -1) {
                    // auth headers from the res
                    if (res.data.access_token) {
                        var now = new Date();
                        now.setTime(now.getTime()+10*1000);
                        console.log(new Date());
                        console.log(now);
                        $cookies.putObject('auth', res.data);
                        token = res.data === undefined ? "" : res.data.access_token;
                        userCredential = {
                            "access_token": 'Bearer ' + token,
                            "token_type": "bearer"
                        };
                    }
                }

                return res;
            };

            var _responseError = function (rejection) {

                //TODO better error handling here.
                ApiLoading.stop();
                $rootScope.$broadcast({
                    400: AUTH_EVENTS.invalidUser,
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized,
                    404: 'loading:hide',
                    500: 'loading:hide',
                    0: AUTH_EVENTS.noService
                }[rejection.status], rejection);

                if (rejection.status === 401) {
                    $location.path = '#/page/signin';
                }


                return $q.reject(rejection);
            };

            authInterceptorServiceFactory.request = _request;
            authInterceptorServiceFactory.responseError = _responseError;
            authInterceptorServiceFactory.response = _response;
            authInterceptorServiceFactory.getAuthtoken = function() {
                return token || '';
            }

            return authInterceptorServiceFactory;
        }]);
