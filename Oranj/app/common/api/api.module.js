(function() {
    angular.module('app.api',[])
        .config(['$httpProvider', function($httpProvider, AUTHORIZATION_KEY) {
            //Removing support for legacy usage of $http. use $http.then instead for $http.success/$http.error
            $httpProvider.useLegacyPromiseExtensions(false);

            //Support for resolving multiple async calls within one digest cycle.
            $httpProvider.useApplyAsync(true);

            // $httpProvider.defaults.headers.common.Authorization = 'Basic ' + btoa(AUTHORIZATION_KEY.client_id + ':' + AUTHORIZATION_KEY.client_secret);

            $httpProvider.interceptors.push('authInterceptorService');
        }]);
})();