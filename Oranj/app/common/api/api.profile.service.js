(function () {
    angular.module('app.api').service('UserProfileService', ['OranjApiService', '$q', '$state',
        function (OranjApiService, $q, $state) {
        var _userProfile = $q.defer(),
            _userRole = $q.defer(),
            _advisorProfile = $q.defer(),
            _userRoleFailed = false,
            _userProfileFetched = false,
            _advisorProfileFetched = false;;

        return {
            fetchUserProfile: function (id) {
                if(!_userProfileFetched) {
                    OranjApiService('getUserInfo').then(function (res) {
                        if (res.status == 'success') {
                           // localStorageService.set("clientInfo", res.data);
                           // localStorageService.set("email", res.data.email);
                            _userProfile.resolve(res.data);
                            _userProfileFetched = true;
                        }
                    }, function () {
                        _userProfileFetched = false;
                    });
                }

                return _userProfile.promise;
            },
            fetchUserRole: function () {
                var userRole = {
                    isClient: false,
                    isAdmin: false,
                    isAdvisor: false
                };


                if (_userRoleFailed) {
                    _userRole = $q.defer();
                }

                OranjApiService('getUserRole').then(function (res) {
                    //localStorageService.set("userType", res.data[0].name);
                    if (res.status === 'error') {
                        $state.go('signin');
                        _userRoleFailed = true;
                        _userRole.reject();
                    } else {
                        switch (res.data[0].name) {
                            case 'ROLE_USER':
                            case 'ROLE_PROSPECT':
                            case 'ROLE_CLIENT':
                                userRole.isClient = true;
                                break;
                            case 'ROLE_ADVISOR':
                                userRole.isAdvisor = true;
                                break;
                            case 'ROLE_ADMIN':
                            case 'ROLE_LITE':
                                userRole.isAdmin = true;
                                break;
                            default:
                                $state.go('signin');
                        }
                        _userRole.resolve(userRole);
                    }
                }, function (response) {
                    _userRole.reject(response);
                    _userRoleFailed = true;
                });

                return _userRole.promise;
            },
            fetchAdvisorProfile: function () {
                if(!_advisorProfileFetched) {
                    OranjApiService('getAdvisorProfileData').then(function (res) {
                        if (res.status == 'success') {

                            _advisorProfile.resolve(res.data);
                            _advisorProfileFetched = true;
                        }
                    }, function () {
                        _advisorProfileFetched = false;
                    });
                }

                return _advisorProfile.promise;
            }
        }
    }]);
})();
