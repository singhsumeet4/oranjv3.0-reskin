(function () {
    'use strict';

    angular.module('app.transferAccount', [])
        .controller('transferCtrl', ['$scope', 'Questionaries', '$log', transferCtrl])
        .controller('transferAccountCtrl', ['$scope', '$rootScope', 'world', 'jQlinechart', transferAccountCtrl])

        //----------------SSN Directive Start ----------------------------

        .directive('validateSsn', function () {
            var SSN_REGEXP = /^(?!000)(?!666)(?!9)\d{3}[- ]?(?!00)\d{2}[- ]?(?!0000)\d{4}$/;
            var ssnPattern = {
                3: '-',
                5: '-'
            };
            return {
                require: 'ngModel',
                link: function (scope, elem, attr, ctrl) {
                    var formatSSN = function () {
                        var sTempString = ctrl.$viewValue;
                        sTempString = sTempString.replace(/\-/g, '');
                        var numbers = sTempString;
                        var temp = '';
                        for (var i = 0; i < numbers.length; i++) {
                            temp += (ssnPattern[i] || '') + numbers[i];
                        }
                        ctrl.$viewValue = temp;

                        scope.$apply(function () {
                            elem.val(ctrl.$viewValue);
                        });

                    };
                    ctrl.$parsers.unshift(function (viewValue) {
                        // test and set the validity after update.
                        var valid = SSN_REGEXP.test(viewValue);
                        ctrl.$setValidity('ssnValid', valid);
                        return viewValue;
                    });
                    // This runs when we update the text field
                    ctrl.$parsers.push(function (viewValue) {

                        var valid = SSN_REGEXP.test(viewValue);
                        ctrl.$setValidity('ssnValid', valid);
                        return viewValue;
                    });
                    elem.bind('blur', formatSSN);

                }
            };
        })

    //----------------SSN Directive End----------------------------

    function transferCtrl($scope, Questionaries, $log) {

        $scope.addDymCLass = function (params) {
            return 'cl' + params;
        }



        //--------------------------------------------------------------------------------
        Chart.defaults.global.responsive = true;
        var recommendedPortfolio = [
            {
                portfolioName: "Moderate",

                profileData: [

                           {
                label: 'US Stock',
                value: 700,
                name: 'US Stock',
                color: '#fdce0b'


            },

            {
                label: 'Non US Stock',
                value: 400,
                name: 'Non US Stock',
                color: '#a3cd39'


            },

            {
                label: 'US Bond',
                value: 100,
                name: 'US Bond',
                color: '#3eb649',

            },

            {
                label: 'Non US Bond',
                value: 80,
                name: 'Non US Bond',
                color: '#23c1e2',

            },
            {
                label: 'Preferred',
                value: 250,
                name: 'Preferred',
                color: '#2e4ea1',

            },

            {
                label: 'Convertible',
                value: 90,
                name: 'Convertible',
                color: '#000285',

            },
            {
                label: 'Cash',
                value: 50,
                name: 'Convertible',
                color: '#fdb45c',

            },
            {
                label: 'Other',
                value: 50,
                name: 'Convertible',
                color: '#f7464a',

            }
                ]
            },
            {
                portfolioName: "Moderate2",

                profileData: [

                    {
                        label: 'US Stock',
                        value: 500,
                        name: 'US Stock',
                        color: '#fdce0b'
                       

                    },

                    {
                        label: 'Non US Stock',
                        value: 300,
                        name: 'Non US Stock',
                        color: '#a3cd39'
                        

                    },

                    {
                        label: 'US Bond',
                        value: 200,
                        name: 'US Bond',
                        color: '#3eb649',

                    },

                    {
                        label: 'Non US Bond',
                        value: 80,
                        name: 'Non US Bond',
                        color: '#23c1e2',

                    }


                ]
            }

        ]

        var doughnutOptionsStep4 = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,

        };





        recommendedPortfolioChart(0);
        //-------------------------------------------------------------------------------------

        var selectedPortfolio = 0;
        $scope.goTo = function () {
            if (selectedPortfolio === 0) {
                recommendedPortfolioChart(1);
                selectedPortfolio = 1;
            }

            else {
                selectedPortfolio = 0;
                recommendedPortfolioChart(0);
            }

        }

        function recommendedPortfolioChart(index) {
            try {
                document.getElementById("matchedportfolioDoughnutStep4").innerHTML = '';
                document.getElementById('my-legendStep4').innerHTML = '';
                var ctxStep4 = document.getElementById("matchedportfolioDoughnutStep4").getContext("2d");
                var myDoughnutStep4 = new Chart(ctxStep4).Doughnut(recommendedPortfolio[index].profileData, doughnutOptionsStep4);
                $scope.nameData = recommendedPortfolio[index].portfolioName;

                console.log($scope.nameData);

                document.getElementById('my-legendStep4').innerHTML = myDoughnutStep4.generateLegend(); // for legend 
            }
            catch (e) {

            }
        }


        // TransferAccountStep4 doughnut chart end


        // TransferAccountStep4 doughnut chart start

        Chart.defaults.global.responsive = true;

         var doughnutDataStep8 = [

            {
                label: 'US Stock',
                value: 700,
                name: 'US Stock',
                color: '#fdce0b'
                

            },

            {
                label: 'Non US Stock',
                value: 400,
                name: 'Non US Stock',
                color: '#a3cd39'
                

            },

            {
                label: 'US Bond',
                value: 100,
                name: 'US Bond',
                color: '#3eb649',

            },

            {
                label: 'Non US Bond',
                value: 80,
                name: 'Non US Bond',
                color: '#23c1e2',

            },
            {
                label: 'Preferred',
                value: 250,
                name: 'Preferred',
                color: '#2e4ea1',

            },

            {
                label: 'Convertible',
                value: 90,
                name: 'Convertible',
                color: '#000285',

            },
            {
                label: 'Cash',
                value: 50,
                name: 'Convertible',
                color: '#fdb45c',

            },
            {
                label: 'Other',
                value: 50,
                name: 'Convertible',
                color: '#f7464a',

            }

        ];

        var doughnutOptionsStep8 = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,

        };
        try {
            var ctxStep8 = document.getElementById("matchedportfolioDoughnutStep8").getContext("2d");
            var myDoughnutStep8 = new Chart(ctxStep8).Doughnut(doughnutDataStep8, doughnutOptionsStep8);

            document.getElementById('my-legendStep8').innerHTML = myDoughnutStep8.generateLegend(); // for legend 
        }
        catch (e) {

        }

        // TransferAccountStep4 doughnut chart end



        // for save data locally start
        $scope.questions = Questionaries.questions;

        $scope.save = function () {
            $log.debug($scope.questions);
            console.log($scope.questions);
            // window.localStorage.set('selectedQues', [$scope.questions]);
            window.sessionStorage.setItem("savselectedQuesed", JSON.stringify($scope.questions));
        }

        $scope.selectedQuestion = JSON.parse(window.sessionStorage.getItem("savselectedQuesed"));

    }



    function transferAccountCtrl($scope, $rootScope, world, jQlinechartFactory) {

        $rootScope.breadcrum = "Account Transfer";

        // Slider
        $scope.color = {
            red: 97,
            green: 211,
            blue: 140
        };
        $scope.rating1 = 3;
        $scope.rating2 = 2;
        $scope.rating3 = 4;
        $scope.disabled1 = 0;
        $scope.disabled2 = 70;

        // Input
        $scope.user = {
            title: 'Developer',
            email: 'ipsum@lorem.com',
            firstName: '',
            lastName: '',
            company: 'Google',
            address: '1600 Amphitheatre Pkwy',
            city: 'Mountain View',
            state: 'CA',
            biography: 'Loves kittens, snowboarding, and can type at 130 WPM.\n\nAnd rumor has it she bouldered up Castle Craig!',
            postalCode: '94043'
        };


        // Select
        $scope.select1 = '1';
        $scope.toppings = [
            { category: 'meat', name: 'Pepperoni' },
            { category: 'meat', name: 'Sausage' },
            { category: 'meat', name: 'Ground Beef' },
            { category: 'meat', name: 'Bacon' },
            { category: 'veg', name: 'Mushrooms' },
            { category: 'veg', name: 'Onion' },
            { category: 'veg', name: 'Green Pepper' },
            { category: 'veg', name: 'Green Olives' }
        ];
        $scope.favoriteTopping = $scope.toppings[0].name

        // Switch
        $scope.switchData = {
            cb1: true,
            cbs: false,
            cb4: true,
            color1: true,
            color2: true,
            color3: true
        };
        $scope.switchOnChange = function (cbState) {
            $scope.message = "The switch is now: " + cbState;
        };



        $scope.step3 = {
            fullname: 'John Christian Mckinley',
            email: 'johnchristian@cogent.com',
            phone: '650-555-1234',
            ssn: '12345-67890',
            annualincome: '$629,000',
            city: 'Mountain View',
            zipcode: '989907',
            spousefullname: 'Jennifer Taylor',
            spouseannualincome: '$507,088',
            childrenfullname: 'Martin Magalona',
            empworkstatus: 'Contractual',
            empoyername: 'Brandon Voyd',
            empoccupation: 'Software Development',
            emptypeofbuss: 'Development Company',
            emplegalstatus: 'Temporary',
            empcitizenship: 'United States of America',
            empcity: 'Mountain View',
            empzipcode: '989907',
            bankname: 'Wells Fargo',
            factitle: 'Nicholas Darwin',
            accountno: '2290656935',
            fundtransferaccountno: '2290978337',
            routingno: '121000358',
            transamount: '$209,000.00',
            acname: 'Marx Consuegra',
            typeofaccount: 'checking',
        };

        $scope.step3.trustee = ('Other Other1 '
        ).split(' ').map(function (trustee) {
            return { abbrev: trustee };
        });

        $scope.step3.country = 'US',
            $scope.count = world.countries;


        $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
            'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
            'WY').split(' ').map(function (state) {
                return { abbrev: state };
            })

        $scope.accounttype = ('checking testing production portfolio').split(' ').map(function (noa) {
            return { abbrev: noa };
        })

        $scope.onChange = function (files) {
            if (files[0] == undefined) return;
            $scope.fileExt = files[0].name.split(".").pop()
        }

        $scope.isImage = function (ext) {
            if (ext) {
                return ext == "jpg" || ext == "jpeg" || ext == "gif" || ext == "png" || ext == 'JPG'
            }
        }


        $scope.Beneficiaries = ('Spouse Data1 Data2').split(' ').map(function (Beneficiary) {
            return { abbrev: Beneficiary };
        })
        $scope.step3.Beneficiary = 'Spouse',


            $scope.BeneficiariesType = ('Contingent Data1 Data2').split(' ').map(function (BeneficiaryType) {
                return { abbrev: BeneficiaryType };
            })
        $scope.step3.BeneficiaryType = 'Contingent',


            $scope.RelationshipType = ("Sibling's Child,Data1,Data2").split(',').map(function (Relationship) {
                return { abbrev: Relationship };
            })
        $scope.step3.Relationship = "Sibling's Child",





            // matchedportfolio doughnut chart start

            Chart.defaults.global.responsive = true;


        var transferAccountDoughnutData = [

            {
                label: 'US Bond',
                value: 100,
                name: 'US Bond',
                 color: '#3eb649',

            },

            {
                label: 'US Stock',
                value: 700,
                name: 'US Stock',
                color: '#fdce0b',

            },

        ];

        var transferAccountDoughnutOptions = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,

        };
        try {
            var ctx2 = document.getElementById("transferAccountDoughnut").getContext("2d");
            var myDoughnut = new Chart(ctx2).Doughnut(transferAccountDoughnutData, transferAccountDoughnutOptions);

            document.getElementById('transferAccount-legend').innerHTML = myDoughnut.generateLegend(); // for legend 
        }
        catch (e) {

        }

        // matchedportfolio doughnut chart end


        // Grid Start
        $scope.leosFundHoldings =
            [
                {
                    "Name": "US Stock",
                    "Symbol": "78.85%",
                    "Shares": "$134,564.67",
                    //  "Test": "(-38.85%)",
                    "Test": -38.85,
                    "PortfolioName": "US Stock",
                    "Percentage": "40%",
                    "Value": "$65,212.71",

                },
                {
                    "Name": "Non US Stock",
                    "Symbol": "0%",
                    "Shares": "  ",
                    // "Test": "(+25%)",
                    "Test": 25,

                    "PortfolioName": "Non US Stock",
                    "Percentage": "25%",
                    "Value": "$40,757.95",

                },
                {
                    "Name": "US Bond",
                    "Symbol": "21.15%",
                    "Shares": "$28,467.11",
                    // "Test": "(-16.15%)",
                    "Test": -16.15,
                    "PortfolioName": "US Bond",
                    "Percentage": "5%",
                    "Value": "$8,151.59",

                },
                {
                    "Name": "Non US Bond",
                    "Symbol": "0%",
                    "Shares": "  ",
                    // "Test": "(+5%)",
                    "Test": 5,
                    "PortfolioName": "Non US Bond",
                    "Percentage": "5%",
                    "Value": "$8,151.59",
                },
                {
                    "Name": "Preferred",
                    "Symbol": "0%",
                    "Shares": "  ",
                    // "Test": "(+20%)",
                    "Test": 20,
                    "PortfolioName": "Preferred",
                    "Percentage": "20%",
                    "Value": "$32,606.36",

                },
                {
                    "Name": "Convertible",
                    "Symbol": "0%",
                    "Shares": "  ",
                    // "Test": "(+5%)",
                    "Test": 5,
                    "PortfolioName": "Convertible",
                    "Percentage": "5%",
                    "Value": "$8,151.59",

                },
                {
                    "Name": "Cash",
                    "Symbol": "0%",
                    "Shares": " ",
                    "Test": 0,
                    "PortfolioName": "Cash",
                    "Percentage": "0%",
                    "Value": "0%",
                }
            ]

        // Grid End


        // performance chart main right start

        var data = {
            labels: ["January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016", "October 2016", "November 2016", "December 2016", "January 2017", "July 2016", "August 2016", "September 2016", "October 2016", "November 2016", "December 2016", "January 2017"],
            datasets: [
                {
                    label: "Retirement",
                    type: 'line',
                    fillColor: "transparent",
                    strokeColor: "#36b8b7",
                    pointColor: "#fff4d3",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "#586fb2",
                    data: [280000, 330000, 315000, 280000, 260000, 275000, 290000, 270000, 298000, 310000, 305000, 320000, 355000, 374000, 390000, 375000, 410000, 435000, 410000, 460000],
                    //    dottedFromLabel: "August 2016"

                },
                {
                    label: "Home",
                    type: 'line',
                    fillColor: "transparent",
                    strokeColor: "#797ba3",
                    pointColor: "#daecd4",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "#586fb2",
                    data: [280000, 270000, 295000, 315000, 305000, 310000, 305000, 326000, 314000, 330000, 310000, 311000, 328000, 318000, 330000, 327000, 350000, 342000, 360000, 352000, 368000],
                    //    dottedFromLabel: "August 2016"

                }
            ]

        };

        var lineoptions = {
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,
            showScale: true,
            multiTooltipTemplate: function (value) { return ' $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            // tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            scaleFontSize: 0
        }

        jQlinechartFactory.jQLineChart("canvas", data, lineoptions);//parameter sequence 1 canvas ID,2 Data to build, 3 options to configure

        // performance chart main right ends


    }


})();