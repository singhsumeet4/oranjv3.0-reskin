(function () {

    'use restrict';

    angular.module('app.directives', [])

        //............directive for phoneNumber..................................

        .directive("phoneformat", function () {
            return {
                restrict: "A",
                require: "ngModel",
                link: function (scope, element, attr, ngModelCtrl) {
                    //Parsing is performed from angular from view to model (e.g. update input box)
                    //Sounds like you just want the number without the hyphens, so take them out with replace
                    var phoneParse = function (value) {
                        var numbers = value && value.replace(/-/g, "").replace(/\(/g, "").replace(/\) /g, "");
                        if (numbers != undefined)
                        numbers= numbers.length > 10 ? numbers.substr(1,10):numbers;
                        if (/^\d{10}$/.test(numbers)) {
                            return numbers;
                        }

                        return undefined;
                    }
                    //Formatting is done from view to model (e.g. when you set $scope.telephone)
                    //Function to insert hyphens if 10 digits were entered.
                    var phoneFormat = function (value) {

                        var numbers = value && value.replace(/-/g, "").replace(/\(/g, "").replace(/\) /g, "");
                          if (numbers != undefined) {
                        	  if(numbers.length != 0) {
                        		  numbers= numbers.length > 10 ? numbers.substr(1,10):numbers;
                        	  }else {
                        		  
                        		  if(ngModelCtrl.$validators.required) {
                        			  ngModelCtrl.$setValidity('required', false);
                        		  }
                        	  }
                          }
                          
                        var matches = numbers && numbers.match(/^(\d{3})(\d{3})(\d{4})$/);

                        if (matches) {
                            scope.PhoneNumError = false;
                            return "" + matches[1] + "-" + matches[2] + "-" + matches[3];

                        }
                        if (matches === null)
                            scope.PhoneNumError = true;

                        if (!(value === undefined))
                            if (!(value == 'undefined'))
                                if (value.length > 0)
                                    scope.PhoneNumError = true;

                        if (value == "")
                            scope.PhoneNumError = false;

                        return undefined;
                    }

                    //Add these functions to the formatter and parser pipelines
                    if(element.val().length > 0) {
	                    ngModelCtrl.$parsers.push(phoneParse);
	                    ngModelCtrl.$formatters.push(phoneFormat);
                    }

                    //Since you want to update the error message on blur, call $setValidity on blur
                    element.bind("blur", function () {
                        var value = phoneFormat(element.val());
                        var isValid = !!value;
                        if (isValid) {
                            ngModelCtrl.$setViewValue(value);
                            ngModelCtrl.$render();
                        }
                        if(element.val().length > 0) {
                        	ngModelCtrl.$setValidity("telephone", isValid);
                        }
                        
                        //call scope.$apply() since blur event happens "outside of angular"
                        scope.$apply();
                    });
                }
            };
        })

    //----------------phoneNumber Directive End----------------------------
        .directive('noCacheSrc', function($window) {
        	  return {
        	    priority: 99,
        	    link: function(scope, element, attrs) {
        	      attrs.$observe('noCacheSrc', function(noCacheSrc) {
        	        noCacheSrc += '?' + (new Date()).getTime();
        	        attrs.$set('src', noCacheSrc);
        	      });
        	    }
        	  }
        	});    

})();