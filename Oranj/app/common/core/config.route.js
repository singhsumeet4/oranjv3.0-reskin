(function () {
    'use strict';

    angular.module('app')
        .config(['$stateProvider', '$urlRouterProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $breadcrumbProvider) {
                var routes, setRoutes;

                routes = [
                    'ui/cards', 'ui/typography', 'ui/buttons', 'ui/icons', 'ui/grids', 'ui/widgets', 'ui/components', 'ui/timeline', 'ui/lists', 'ui/pricing-tables',
                    'map/maps',
                    'table/static', 'table/dynamic', 'table/responsive',
                    'form/elements', 'form/layouts', 'form/validation', 'form/wizard',
                    'chart/echarts', 'chart/echarts-line', 'chart/echarts-bar', 'chart/echarts-pie', 'chart/echarts-scatter', 'chart/echarts-more',
                    'page/404', 'page/500', 'page/blank', 'page/invoice', 'page/lock-screen', 'page/profile', 'page/signup',
                    'app/calendar'];

                setRoutes = function (route) {
                    var config, url;
                    url = '/' + route;
                    config = {
                        url: url,
                        templateUrl: 'app/common/' + route + '.html'
                    };
                    $stateProvider.state(route, config);
                    return $stateProvider;
                };

                routes.forEach(function (route) {
                    return setRoutes(route);
                });


                $urlRouterProvider
                // .when('/', '/page/signin')
                    .otherwise('/signin');

                $stateProvider
                    .state('signin', {
                        url: '/signin',
                        templateUrl: 'app/common/page/html/signin.html',
                        bodyClass: 'body-wide body-auth',
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })

                    .state('forgot-password', {
                        url: '/forgot-password',
                        templateUrl: 'app/common/page/html/forgot-password.html',
                        bodyClass: 'body-wide body-auth',
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })

                    .state('reset-password', {
                        url: '/reset-password',
                        templateUrl: 'app/common/page/html/reset-password.html',
                        bodyClass: 'body-wide body-auth',
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })

                    // User specific states begin here.
                    .state('page', {
                        abstract: true,
                        controller: 'layoutController',
                        templateUrl: 'app/common/page/html/page.layout.html',
                        url: '/page'
                    })

                    // Client states begin here.
                    .state('page.client', {
                        abstract: true,
                        controller: 'client.main.controller',
                        templateUrl: 'app/client/client.main.html',
                        url: '/client'
                    })

                    //Client Dash state
                    .state('page.client.home', {
                        url: '/home',
                        template: '<ui-view></ui-view>',
                        ncyBreadcrumb: {
                            label: 'Home'
                        }
                    })
                    //.state('page.client.home.investment', {
                    //    abstract: true,
                    //    url: '/investments',
                    //    template: '<ui-view></ui-view>',
                    //    ncyBreadcrumb: {
                    //        label: 'Investments'
                    //    }
                    //})

                    .state('page.client.home.networth', {
                        abstract: true,
                        url: '/networth',
                        template: '<ui-view></ui-view>',
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })


                    //.state('page.client.home.accounts', {
                    //    abstract: true,
                    //    url: '/accounts',
                    //    template: '<ui-view></ui-view>',
                    //    ncyBreadcrumb: {
                    //        skip: true
                    //    }
                    //})

                    /* Home summary state */
                    .state('page.client.home.dash', {
                        templateUrl: 'app/client/home/html/clientDash.html',
                        url: '/dash',
                        ncyBreadcrumb: {
                            label: 'Summary'
                        },
                        controller: 'clientHomeDashController',
                        controllerAs: 'clientDashCtrl'
                    })


                    //Client goals feature states
                    .state('page.client.home.goal', {
                        url: '/goals',
                        templateUrl: 'app/client/goals/html/goal.html',
                        controller: 'goalsClientController',
                        controllerAs: 'ctrl',
                        ncyBreadcrumb: {
                            label: 'Goals'
                        }
                    })
                    .state('page.client.home.goal.dashboard', {
                        url: '/dashboard',
                        templateUrl: 'app/client/goals/html/goalDash.html',
                        controller: 'goalDashboardCtrl',
                        controllerAs: 'dashCtrl',
                        ncyBreadcrumb: {
                            label: 'Dashboard'
                        }
                    })
                    .state('page.client.home.goal.id', {
                        url: '/:goalId/:goalName',
                        templateUrl: 'app/client/goals/html/individualGoalDash.html',
                        controller: 'individualGoalCtrl',
                        controllerAs: 'goalCtrl',
                        ncyBreadcrumb: {
                            label: '{{goalCtrl.goalName}}'
                        }
                    })

                    /* Client goal states end */

                    /* Client insurance states*/

                    .state('page.client.home.insurance', {
                        abstract: true,
                        url: '/insurance',
                        template: '<ui-view></ui-view>',
                        controller: 'InsuranceMainCtrl',
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })

                    .state('page.client.home.insurance.dash', {
                        url: '/insurance-home',
                        templateUrl: 'app/client/insurance/html/insurance.html',
                        controller: 'InsuranceCtrl',
                        ncyBreadcrumb: {
                            label: ' Insurance'
                        }
                    })

                    .state('page.client.home.insurance.specificLife', {
                        url: '/Life',
                        templateUrl: 'app/client/insurance/html/insuranceLife.html',
                        controller: 'InsuranceLifeCtrl',
                        ncyBreadcrumb: {
                            label: 'Insurance / Life'
                        }
                    })

                    .state('page.client.home.insurance.specificLTC', {
                        url: '/Long Term Care',
                        templateUrl: 'app/client/insurance/html/insuranceLongTermCare.html',
                        controller: 'InsuranceLTCCtrl',
                        ncyBreadcrumb: {
                            label: 'Insurance / Long Term Care'
                        }
                    })

                    .state('page.client.home.insurance.specificLTD', {
                        url: '/ Long Term Disability',
                        templateUrl: 'app/client/insurance/html/insuranceLongTermDisability.html',
                        controller: 'InsuranceLTDCtrl',
                        ncyBreadcrumb: {
                            label: 'Insurance / Long Term Disability'
                        }
                    })
                    /* Client insurance states end*/

                    /* Client investment states*/

                    .state('page.client.home.investment', {
                        url: '/investment',
                        templateUrl: 'app/client/investment/html/clientInvestment.html',
                        controller: 'clientInvestmentCtrl',
                        ncyBreadcrumb: {
                            label: 'Investment'
                        }
                    })

                    .state('page.client.home.investment.dash', {
                        url: '/dashboard',
                        templateUrl: 'app/client/investment/html/clientInvestmentMain.html',
                        ncyBreadcrumb: {
                            label: 'Dashboard'
                        }
                    })

                    .state('page.client.home.investment.specific', {
                        url: '/:id/:name',
                        templateUrl: 'app/client/investment/html/investment-specific.html',
                        controller: 'clientCtrl',
                        controllerAs: 'investmentSpecificCtrl',
                        ncyBreadcrumb: {
                            label: '{{investmentSpecificCtrl.accountName}}'
                        }
                    })
                    /* Client investment states*/

                    /* Client net worth states*/
                    .state('page.client.home.networth.dash', {
                        url: '/networthMain',
                        templateUrl: 'app/client/networth/html/networth.html',
                        ncyBreadcrumb: {
                            label: 'Networth'
                        }
                    })
                /* Client net worth states end*/

                /* Client add Financial states */

                $stateProvider.state('page.addFinancialConfirm', {
                    url: '/addFinancialConfirm',
                    templateUrl: 'app/client/clientCommon/addFinancialCustom/addFinancialConfirm.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancial', {
                    url: '/addFinancial',
                    templateUrl: 'app/client/clientCommon/addFinancialCustom/addFinancial.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancialType', {
                    url: '/addFinancialType',
                    templateUrl: 'app/client/clientCommon/addFinancialCustom/addFinancialType.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                });

                $stateProvider.state('page.addFinancialCustom', {
                    url: '/addFinancialCustom',
                    templateUrl: 'app/client/clientCommon/addFinancialCustom/addFinancialCustom.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancialProperty', {
                    url: '/addFinancialProperty',
                    templateUrl: 'app/client/clientCommon/addFinancialProperty.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancialLife', {
                    url: '/addFinancialLife',
                    templateUrl: 'app/client/clientCommon/addFinancialInsurance/addFinancialLife.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancialLongTermCare', {
                    url: '/addFinancialLongTermCare',
                    templateUrl: 'app/client/clientCommon/addFinancialInsurance/addFinancialInsuranceLongTermCare.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancialLongTermDisability', {
                    url: '/addFinancialLongTermDisability',
                    templateUrl: 'app/client/clientCommon/addFinancialInsurance/addFinancialInsuranceLongTermDisability.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })
                $stateProvider.state('page.addFinancialInstitution', {
                    url: '/addFinancialInstitution',
                    templateUrl: 'app/client/clientCommon/addFinancialInstitution/addFinancial.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })


                $stateProvider.state('page.addFinancialInstitutionType', {
                    url: '/addFinancialInstitutionType',
                    templateUrl: 'app/client/clientCommon/addFinancialInstitution/addFinancialType.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancialInstitutionTypeIndv', {
                    url: '/addFinancialInstitutionTypeIndv',
                    templateUrl: 'app/client/clientCommon/addFinancialInstitution/addFinancialTypeIndividual.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })

                $stateProvider.state('page.addFinancialInstitutionTypeIndvagg', {
                    url: '/addFinancialInstitutionTypeIndvagg',
                    templateUrl: 'app/client/clientCommon/addFinancialInstitution/addFinancialTypeIndividualagg.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })


                $stateProvider.state('page.addFinancialInstitutionConfirm', {
                    url: '/addFinancialInstitutionConfirm',
                    templateUrl: 'app/client/clientCommon/addFinancialInstitution/addFinancialConfirm.html',
                    ncyBreadcrumb: {
                        label: 'Home / Add a Financial',
                    }
                })
                /* Client add Financial statesEnds */

                /* Settings states starts*/
                    .state('page.client.settings', {
                        abstract: true,
                        url: '/settings',
                        template: '<ui-view></ui-view>'
                    })

                    .state('page.client.settings.privacy', {
                        url: '/privacysettings',
                        templateUrl: 'app/common/settings/html/settings.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Privacy Settings'
                        }
                    })

                    .state('page.client.settings.password', {
                        url: '/password',
                        templateUrl: 'app/common/settings/html/password_settings.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Password Settings'
                        }
                    })

                    .state('page.client.settings.info', {
                        url: '/profileinfo',
                        templateUrl: 'app/common/settings/html/profile_info.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Profile Info'
                        }
                    })

                    .state('page.client.settings.credentials', {
                        url: '/credentials',
                        templateUrl: 'app/common/settings/html/credentials.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Credentials'
                        }
                    })

                    .state('page.profileinfom', {
                        url: '/settings/myprofile/profileinfom',
                        templateUrl: 'app/common/settings/profile_infoInner.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Profile Info'
                        }
                    })
                    /* Settings states starts*/

                    /* Settings states starts*/

                    .state('page.disclaimer', {
                        url: '/disclaimer',
                        templateUrl: 'app/common/disclaimer/html/disclaimer.html',
                        ncyBreadcrumb: {
                            label: 'Disclaimer'
                        }
                    })

                    .state('page.dashboard', {
                        url: '/dashboard',
                        templateUrl: 'app/common/dashboard/dashboard.html'
                    })

                    // .state('page.vault-share', {
                    //     url: '/vault-share',
                    //     templateUrl: 'app/client/vault/vault-share.html',
                    //     controller: 'VaultShareCtrl'
                    // })


                    .state('page.messages', {
                        url: '/message',
                        templateUrl: 'app/common/messages/html/messagesSection.html',
                        ncyBreadcrumb: {
                            label: 'Message'
                        }
                    })


                    // .state('page.client', {
                    //     url: '/client/dash',
                    //     templateUrl: 'app/client/home/clientDash.html'
                    // })

                    .state('page.client.investmentMain', {
                        url: '/InvestmentMain',
                        templateUrl: 'app/client/investment/html/clientInvestmentMain.html',
                        controller: 'clientInvestmentCtrl'
                    })

                    .state('page.networthMain', {
                        url: '/networth/networthMain',
                        templateUrl: 'app/client/networth/html/networth.html'
                    })

                    .state('page.client.feed', {
                        abstract: true,
                        url: '/feed',
                        template: '<ui-view></ui-view>'
                    })

                    .state('page.client.feed.activity', {
                        url: '/feed',
                        templateUrl: 'app/common/feed/html/feed.html',
                        controller: 'feedController',
                        ncyBreadcrumb: {
                            label: 'Feed'
                        }
                    })

                    .state('page.client.feed.systemNotification', {
                        url: '/systemNotifications',
                        templateUrl: 'app/common/feed/html/systemNotifications.html',
                        controller: 'notificationController',
                        ncyBreadcrumb: {
                            label: 'Feed / System Notification'
                        }
                    })

                    .state('page.feedSearch', {
                        url: '/feedSearch',
                        templateUrl: 'app/common/feed/feedSearch.html'
                    })
                    .state('page.feedSearchBlank', {
                        url: '/feedSearchBlank',
                        templateUrl: 'app/common/feed/feedSearchBlank.html'
                    })

                    .state('page.client.vault-home', {
                        url: '/vault-home',
                        templateUrl: 'app/client/vault/html/vault.html',
                        controller: 'VaultCtrl',
                        ncyBreadcrumb: {
                            label: 'Vault'
                        }
                    })
                    .state('page.client.vault-home.dashboard', {
                        url: '/root',
                        templateUrl: 'app/client/vault/html/vault-home.html',
                        controller: 'VaultHomeCtrl',
                        params: {
                            obj: null
                        },
                        ncyBreadcrumb: {
                            label: 'My Vault'
                        }
                    })

                    .state('page.client.vault-home.folder', {
                        url: '/root/:folderName',
                        templateUrl: 'app/client/vault/html/vault-folder.html',
                        controller: 'VaultHomeCtrl',
                        params: {
                            obj: null
                        },
                        controllerAs: 'vaultCtrl',
                        ncyBreadcrumb: {
                            label: '{{vaultCtrl.FolderName}}'
                        }
                    })

                    .state('page.client.vault-home.recent', {
                        url: '/recent',
                        templateUrl: 'app/client/vault/html/vault-recent.html',
                        controller: 'VaultHomeCtrl',
                        params: {
                            obj: null
                        },
                        ncyBreadcrumb: {
                            label: 'Recent'
                        }
                    })

                    .state('page.client.vault-home.search', {
                        url: '/search/:searchString',
                        templateUrl: 'app/client/vault/html/vault-recent.html',
                        controller: 'VaultHomeCtrl',
                        ncyBreadcrumb: {
                            label: 'Search Result'
                        }
                    })

                    .state('page.client.vault-item', {
                        url: '/vault-item',
                        templateUrl: 'app/client/vault/html/vault-item.html',
                        controller: 'VaultItemCtrl'
                    })

                    .state('page.client.insurance', {
                        url: '/insurance',
                        templateUrl: 'app/client/insurance/html/insurance.html',
                        controller: 'InsuranceCtrl'
                    })

                    // .state('page.vault-share', {
                    //     url: '/vault-share',
                    //     templateUrl: 'app/client/vault/vault-share.html',
                    //     controller: 'VaultShareCtrl'
                    // })
                    .state('page.advisor-dash', {
                        url: '/advisor-dash',
                        templateUrl: 'app/advisor/advisorHome.html',

                    })

                    .state('page.investment', {
                        url: '/investment',
                        templateUrl: 'app/client/investment/investment.html'
                    })

                    .state('page.commingSoon', {
                        url: '/coming-soon',
                        templateUrl: 'app/common/commingsoon.html'
                    })

                    //.state('page.client.home.accounts.dash', {
                    //    url: '/Accounts/AccountMain',
                    //    templateUrl: 'app/client/Accounts/html/clientAccountMain.html',
                    //    ncyBreadcrumb: {
                    //        label: 'Accounts'
                    //    }
                    //})

                    //.state('page.client.home.accounts.specific', {
                    //     url: '/Accounts/clientAccountSpecific',
                    //     templateUrl: 'app/client/Accounts/html/clientAccountSpecific.html',
                    //     ncyBreadcrumb: {
                    //         label: 'Accounts'
                    //     }
                    //})

                    /* Client Account states*/

                    .state('page.client.home.accounts', {
                        url: '/accounts',
                        templateUrl: 'app/client/Accounts/html/clientAccount.html',
                        controller: 'clientAccountCtrl',
                        ncyBreadcrumb: {
                            label: 'Accounts'
                        }
                    })

                    .state('page.client.home.accounts.dash', {
                        url: '/dashboard',
                        templateUrl: 'app/client/Accounts/html/clientAccountMain.html',
                        controller: 'clientAccountMainCtrl',
                        ncyBreadcrumb: {
                            label: 'Dashboard'
                        }
                    })

                    .state('page.client.home.accounts.specific', {
                        url: '/:id/:accountName',
                        templateUrl: 'app/client/Accounts/html/clientAccountSpecific.html',
                        controller: 'clientAccountSpecificCtrl',
                        controllerAs: 'clientAccountSpecificCtrl',
                        ncyBreadcrumb: {
                            label: '{{clientAccountSpecificCtrl.accountName}}'
                        }
                    })
                    /* Client account states*/

                    .state('page.client.home.accounts.edit', {
                        url: '/Accounts/clientAccountEdit',
                        templateUrl: 'app/client/Accounts/html/clientAccountEdit.html',
                        ncyBreadcrumb: {
                            label: 'Accounts'
                        }
                    })

                    .state('page.addGoal', {
                        url: '/home/addGoal',
                        templateUrl: 'app/client/home/addGoal.html'
                    })

                    .state('page.addGoalStep1', {
                        url: '/home/addGoalStep1',
                        templateUrl: 'app/client/home/html/addGoalStep1.html',
                        ncyBreadcrumb: {
                            label: 'Add a Goal'
                        }
                    })

                    .state('page.addGoalStep2', {
                        url: '/home/addGoalStep2',
                        templateUrl: 'app/client/home/html/addGoalStep2.html'
                    })

                    .state('page.addGoalStep3', {
                        url: '/home/addGoalStep3',
                        templateUrl: 'app/client/home/html/addGoalStep3.html'
                    })
                    .state('page.addGoalStep', {
                        url: '/home/addGoalStep',
                        templateUrl: 'app/client/home/html/addGoalStep3_empty.html'
                    })

                    .state('page.openaccountstep1', {
                        url: '/home/openaccountstep1',
                        templateUrl: 'app/common/openAccount/html/openAccountstep1.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })

                    .state('page.openaccountstep2', {
                        url: '/home/openaccountstep2',
                        templateUrl: 'app/common/openAccount/html/openAccountstep2.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })

                    .state('page.openaccountstep3', {
                        url: '/home/openaccountstep3',
                        templateUrl: 'app/common/openAccount/html/openAccountstep3.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })
                    .state('page.openaccountstep4', {
                        url: '/home/openaccountstep4',
                        templateUrl: 'app/common/openAccount/html/openAccountstep4.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })
                    .state('page.openaccountstep5', {
                        url: '/home/openaccountstep5',
                        templateUrl: 'app/common/openAccount/html/openAccountstep5.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })
                    .state('page.openaccountstep6', {
                        url: '/home/openaccountstep6',
                        templateUrl: 'app/common/openAccount/html/openAccountstep6.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })
                    .state('page.openaccountstep7', {
                        url: '/home/openaccountstep7',
                        templateUrl: 'app/common/openAccount/html/openAccountstep7.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })
                    .state('page.openaccountstep8', {
                        url: '/home/openaccountstep8',
                        templateUrl: 'app/common/openAccount/html/openAccountstep8.html',
                        ncyBreadcrumb: {
                            label: 'Open an Account'
                        }
                    })

                    .state('page.editgoalstep1', {
                        url: '/home/editgoalstep1',
                        templateUrl: 'app/common/editAccount/editGoalStep1.html'
                    })

                    .state('page.editgoalstep2', {
                        url: '/home/editgoalstep2',
                        templateUrl: 'app/common/editAccount/editGoalStep2.html'
                    })

                    .state('page.editgoalstep3', {
                        url: '/home/editgoalstep3',
                        templateUrl: 'app/common/editAccount/editGoalStep3.html'
                    })

                    .state('page.transferaccountstep1', {
                        url: '/home/transferaccountstep1',
                        templateUrl: 'app/common/transferAccount/transferAccountstep1.html'
                    })
                    .state('page.transferaccountstep2', {
                        url: '/home/transferaccountstep2',
                        templateUrl: 'app/common/transferAccount/transferAccountstep2.html'
                    })
                    .state('page.transferaccountstep3', {
                        url: '/home/transferaccountstep3',
                        templateUrl: 'app/common/transferAccount/transferAccountstep3.html'
                    })
                    .state('page.transferaccountstep4', {
                        url: '/home/transferaccountstep4',
                        templateUrl: 'app/common/transferAccount/transferAccountstep4.html'
                    })
                    .state('page.transferaccountstep5', {
                        url: '/home/transferaccountstep5',
                        templateUrl: 'app/common/transferAccount/transferAccountstep5.html'
                    })
                    .state('page.transferaccountstep6', {
                        url: '/home/transferaccountstep6',
                        templateUrl: 'app/common/transferAccount/transferAccountstep6.html'
                    })
                    .state('page.transferaccountstep7', {
                        url: '/home/transferaccountstep7',
                        templateUrl: 'app/common/transferAccount/transferAccountstep7.html'
                    })
                    .state('page.transferaccountstep8', {
                        url: '/home/transferaccountstep8',
                        templateUrl: 'app/common/transferAccount/transferAccountstep8.html'
                    })
                    .state('page.transferaccountstep9', {
                        url: '/home/transferaccountstep9',
                        templateUrl: 'app/common/transferAccount/transferAccountstep9.html'
                    })

                    .state('page.advisorinfo', {
                        url: '/advisorinfo',
                        templateUrl: 'app/advisor/advisorInfo.html'
                    })
                    // States defined for Goals end here.

                    // advisor states begin here.
                    .state('page.advisor', {
                        abstract: true,
                        // controller: 'advisor.main.controller',
                        templateUrl: 'app/advisor/advisor.main.html',
                        url: '/advisor'
                    })

                    //   .state('page.advisor.home', {
                    //     abstract: true,
                    //     url: '/home',
                    //     template: '<ui-view></ui-view>'
                    // })

                    /* Advisor Home state */
                    .state('page.advisor.home', {
                        templateUrl: 'app/advisor/home/html/advisorHome.html',
                        url: '/dash',
                    })

                    /* advisor admin states starts*/
                    .state('page.advisor.admin', {
                        abstract: true,
                        url: '/admin',
                        template: '<ui-view></ui-view>'
                    })

                    .state('page.advisor.admin.firminfo', {
                        url: '/firminfo',
                        templateUrl: 'app/advisor/admin/html/firmInfo.html'
                    })

                    .state('page.advisor.admin.advisors', {
                        abstract: true,
                        url: '/advisors',
                        template: '<ui-view></ui-view>',

                    })

                    .state('page.advisor.admin.advisors.dash', {
                        url: '/advisors',
                        templateUrl: 'app/advisor/admin/html/advisors.html'
                    })


                    .state('page.advisor.admin.advisors.individual', {
                        url: '/advisorsindividual',
                        templateUrl: 'app/advisor/admin/html/advisorsIndividual.html'
                    })

                    .state('page.advisor.admin.matchedportfolios', {
                        url: '/matchedportfolios',
                        templateUrl: 'app/advisor/admin/html/matchedPortfolio.html'
                    })

                    .state('page.advisor.admin.questionnaire', {
                        url: '/questionnaire',
                        templateUrl: 'app/advisor/admin/html/questionnaire.html'
                    })


                    .state('page.advisor.admin.userdetails', {
                        url: '/userdetails',
                        templateUrl: 'app/advisor/admin/html/userDetails.html'
                    })

                    .state('page.advisor.admin.managedaccounts', {
                        url: '/managedaccounts',
                        templateUrl: 'app/advisor/admin/html/managedAccounts.html'
                    })

                    // advisor info starts

                    .state('page.advisor.advisor', {
                        abstract: true,
                        url: '/main',
                        template: '<ui-view></ui-view>'
                    })

                    .state('page.advisor.advisor.info', {
                        url: '/maininfo',
                        templateUrl: 'app/advisor/advisor/html/advisor_info.html',
                        ncyBreadcrumb: {
                            label: 'Advisor Info'
                        }
                    })

                    // advisor info ends

                    /* advisor Account states*/

                    .state('page.advisor.account', {
                        url: '/accounts',
                        templateUrl: 'app/advisor/Accounts/html/advisorAccount.html',
                        controller: 'advisorAccountCtrl',
                        ncyBreadcrumb: {
                            label: 'Accounts'
                        }
                    })

                    .state('page.advisor.account.dash', {
                        url: '/dashboard',
                        templateUrl: 'app/advisor/Accounts/html/advisorAccountMain.html',
                        controller: 'advisorAccountMainCtrl',
                        ncyBreadcrumb: {
                            label: 'Dashboard'
                        }
                    })

                    .state('page.advisor.account.specific', {
                        url: '/:userId/:id/:accountName',
                        templateUrl: 'app/advisor/Accounts/html/advisorAccountSpecific.html',
                        controller: 'advisorAccountSpecificCtrl',
                        controllerAs: 'advisorAccountSpecificCtrl',
                        ncyBreadcrumb: {
                            label: '{{advisorAccountSpecificCtrl.accountName}}'
                        }
                    })
                    /* advisor account states*/
                    /* advisor investment states*/

                    .state('page.advisor.investment', {
                        url: '/investment',
                        templateUrl: 'app/advisor/investment/html/clientInvestment.html',
                        controller: 'advisorInvestmentMainCtrl',
                        ncyBreadcrumb: {
                            label: 'Investment'
                        }
                    })

                    .state('page.advisor.investment.dash', {
                        url: '/dashboard',
                        templateUrl: 'app/advisor/investment/html/clientInvestmentMain.html',
                        ncyBreadcrumb: {
                            label: 'Dashboard'
                        }
                    })

                    .state('page.advisor.investment.specific', {
                        url: '/:id/:ID/:name',
                        templateUrl: 'app/advisor/investment/html/investment-specific.html',
                        controller: 'advisorInvestmentCtrl',
                        controllerAs: 'investmentSpecificCtrl',
                        ncyBreadcrumb: {
                            label: '{{investmentSpecificCtrl.accountName}}'
                        }
                    })
                    /* advisor investment states*/

                    // advisor settings starts

                    .state('page.advisor.settings', {
                        abstract: true,
                        url: '/settings',
                        template: '<ui-view></ui-view>'
                    })

                    .state('page.advisor.settings.privacy', {
                        url: '/privacysettings',
                        templateUrl: 'app/advisor/settings/html/privacysettings.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Privacy Settings'
                        }
                    })

                    .state('page.advisor.settings.password', {
                        url: '/password',
                        templateUrl: 'app/advisor/settings/html/password_settings.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Password Settings'
                        }
                    })

                    .state('page.advisor.settings.info', {
                        url: '/profileinfo',
                        templateUrl: 'app/advisor/settings/html/profile_info.html',
                        ncyBreadcrumb: {
                            label: 'Settings / My Profile / Profile Info'
                        }
                    })

                    .state('page.advisor.settings.profile', {
                        url: '/profile',
                        templateUrl: 'app/advisor/advisor.info.html',
                        ncyBreadcrumb: {
                            label: 'Profile Info'
                        }
                    })

                    // advisor settings end

                    //advisor messaging start

                    .state('page.advisor.messages', {
                        abstract: true,
                        url: '/message',
                        template: '<ui-view></ui-view>',
                        controller: 'messageMainController'
                    })

                    .state('page.advisor.messages.list', {
                        url: '/list',
                        template: '<ui-view></ui-view>',
                        ncyBreadcrumb: {
                            label: 'Message'
                        }
                    })


                    //advisor Vault start

                    .state('page.advisor.vault-home', {
                        url: '/vault-home',
                        templateUrl: 'app/advisor/vault/html/vault.html',
                        controller: 'AdvisorVaultCtrl',
                        ncyBreadcrumb: {
                            label: 'Vault'
                        }
                    })

                    .state('page.advisor.vault-home.dashboard', {
                        url: '/root',
                        templateUrl: 'app/advisor/vault/html/vault-home.html',
                        controller: 'AdvisorVaultHomeCtrl',
                        ncyBreadcrumb: {
                            label: 'My Vault'
                        }
                    })

                    .state('page.advisor.vault-home.folder', {
                        url: '/root/:folderName',
                        templateUrl: 'app/advisor/vault/html/vault-folder.html',
                        controller: 'AdvisorVaultHomeCtrl',
                        params: {
                            obj: null
                        },
                        controllerAs: 'vaultCtrl',
                        ncyBreadcrumb: {
                            label: '{{vaultCtrl.FolderName}}'
                        }
                    })

                    .state('page.advisor.vault-home.search', {
                        url: '/search/:searchString',
                        templateUrl: 'app/advisor/vault/html/vault-home.html',
                        controller: 'AdvisorVaultHomeCtrl',
                        ncyBreadcrumb: {
                            label: 'Search Result'
                        }
                    })


                    /* advisor contacts starts*/

                    .state('page.advisor.contacts', {
                        abstract: true,
                        url: '/contacts',
                        template: '<ui-view></ui-view>',
                        controller: 'contactCtrl'
                    })

                    .state('page.advisor.contacts.clientDash', {
                        url: '/clientDash/:id/:name',
                        templateUrl: 'app/advisor/clientDashes/html/clientDashes.html',
                        controller: 'ClientDashesController',
                        controllerAs: 'clientDashesCtrl',
                        abstract: true

                    })
                    .state('page.advisor.contacts.clientDash.summary', {
                        url: '/summary',
                        templateUrl: 'app/client/home/html/clientDash.html',
                        ncyBreadcrumb: {
                            label: 'Clients / {{userName}} / Summary'
                        }
                    })
                    .state('page.advisor.contacts.clientDash.goals', {
                        url: '/goals',
                        templateUrl: 'app/client/goals/html/goal.html',
                        controller: 'goalsClientController',
                        controllerAs: 'ctrl',
                        ncyBreadcrumb: {
                            label: 'Clients / {{userName}} / Goals'
                        }
                    })
                    .state('page.advisor.contacts.clientDash.goals.dashboard', {
                        url: '/dashboard',
                        templateUrl: 'app/client/goals/html/goalDash.html',
                        controller: 'goalDashboardCtrl',
                        controllerAs: 'dashCtrl',
                        ncyBreadcrumb: {
                            label: 'Dashboard'
                        }
                    })
                    .state('page.advisor.contacts.clientDash.goals.id', {
                        url: '/:goalId/:goalName',
                        templateUrl: 'app/client/goals/html/individualGoalDash.html',
                        controller: 'individualGoalCtrl',
                        controllerAs: 'goalCtrl',
                        ncyBreadcrumb: {
                            label: '{{goalCtrl.goalName}}'
                        }
                    })
                    .state('page.advisor.clientprofile', {
                        url: '/clientprofile/:id/:name',
                        templateUrl: 'app/common/settings/html/profile_info.html',
                        ncyBreadcrumb: {
                            label: 'Home / {{clientName}} / Profile Info'
                        }
                    })
                    .state('page.advisor.contacts.create', {
                        url: '/createContact',
                        templateUrl: 'app/advisor/contacts/html/createContact.html',
                        ncyBreadcrumb: {
                            label: 'Clients / Create Contact'
                        }
                    })

                    .state('page.advisor.contacts.createsuccess', {
                        url: '/createSuccess',
                        templateUrl: 'app/advisor/contacts/html/createContactConfirm.html',
                        ncyBreadcrumb: {
                            label: 'Clients / Create Contact / Success'
                        }
                    })

                    .state('page.advisor.contacts.sendCredential', {
                        url: '/sendLoginCredential',
                        templateUrl: 'app/advisor/contacts/html/sendLoginCredential.html',
                        ncyBreadcrumb: {
                            label: 'Clients / send Login Credential'
                        }
                    })


                    .state('page.advisor.contacts.invite', {
                        url: '/inviteContact',
                        templateUrl: 'app/advisor/contacts/html/inviteContact.html',
                        ncyBreadcrumb: {
                            label: 'Clients / Invite Contact'
                        }
                    })
                    .state('page.advisor.feed', {
                        abstract: true,
                        url: '/feed',
                        template: '<ui-view></ui-view>'
                    })

                    .state('page.advisor.feed.activity', {
                        url: '/feed',
                        templateUrl: 'app/common/feed/html/feed.html',
                        controller: 'feedController',
                        ncyBreadcrumb: {
                            label: 'Feed / All'
                        }
                    })

                    .state('page.advisor.feed.id', {
                        url: '/feed/:userid/:name',
                        templateUrl: 'app/common/feed/html/feed.html',
                        controller: 'feedController',
                        ncyBreadcrumb: {
                            label: 'Feed / {{clientName}}'
                        }
                    })

                    .state('page.advisor.feed.systemNotification', {
                        url: '/systemNotifications',
                        templateUrl: 'app/common/feed/html/systemNotifications.html',
                        controller: 'notificationController',
                        ncyBreadcrumb: {
                            label: 'Feed / System Notification'
                        }
                    })


                $stateProvider.state('page.advisor.contacts.allcontactmain', {
                    url: '/allContacts',
                    templateUrl: 'app/advisor/contacts/html/allcontactsmain.html',
                    ncyBreadcrumb: {
                        label: 'Clients / All Contacts'
                    }


                })

                // 404 section

                    .state('page.404', {
                        abstract: true,
                        url: '/404',
                        template: '<ui-view></ui-view>'
                    })


                    .state('page.404.section', {
                        url: '/main',
                        templateUrl: 'app/common/page/html/404.html',

                    });


            }]
        );

})();
