(function() {
    'use strict';

    angular.module('app.core')
        .factory('appConfig', [appConfig])
        .config(['$mdThemingProvider', mdConfig])
         .config(['$mdDateLocaleProvider', mdDateConfig]);
        
        
        
       
            
     function mdDateConfig($mdDateLocaleProvider){
             $mdDateLocaleProvider.formatDate = function(date) {
                return moment(date).format('MM/D/YYYY');
                };
     }     
            
            
    function appConfig() {
        var pageTransitionOpts = [
            {
                name: 'Fade up',
                "class": 'animate-fade-up'
            }, {
                name: 'Scale up',
                "class": 'ainmate-scale-up'
            }, {
                name: 'Slide in from right',
                "class": 'ainmate-slide-in-right'
            }, {
                name: 'Flip Y',
                "class": 'animate-flip-y'
            }
        ];
        var date = new Date();
        var year = date.getFullYear();
        var main = {
            brand: 'Material',
            name: 'Lisa',
            year: year,
            layout: 'wide',                                 // String: 'boxed', 'wide'
            menu: 'vertical',                               // String: 'horizontal', 'vertical'
            isMenuCollapsed: false,                         // Boolean: true, false
            fixedHeader: true,                              // Boolean: true, false
            fixedSidebar: true,                             // Boolean: true, false
            pageTransition: pageTransitionOpts[0],          // Object: 0, 1, 2, 3 and build your own
            skin: '12'                                      // String: 11,12,13,14,15,16; 21,22,23,24,25,26; 31,32,33,34,35,36
        };
        var color = {
            primary:    '#009688',
            success:    '#8BC34A',
            info:       '#00BCD4',
            infoAlt:    '#7E57C2',
            warning:    '#FFCA28',
            danger:     '#F44336',
            text:       '#3D4051',
            gray:       '#EDF0F1'
        };
        
        var notification_icon = [ {
			name : 'account_error',
			val : 'fa fa-refresh awesome-icon'
		}, {
			name : 'large_purchase',
			val : 'fa fa-cart-plus awesome-icon'
		}, {
			name : 'balances',
			val : 'fa fa-money awesome-icon'
		}, {
			name : 'cash_flow',
			val : 'fa fa-area-chart awesome-icon'
		}, {
			name : 'monthly_performance',
			val : 'fa fa-level-up awesome-icon'
		}, {
			name : 'quarterly_performance',
			val : 'fa fa-level-up awesome-icon'
		}, {
			name : 'annual_performance',
			val : 'fa fa-level-up awesome-icon'
		}, {
			name : 'completed_trade',
			val : 'fa fa-line-chart awesome-icon'
		}, {
			name : 'add_an_account',
			val : 'fa fa-institution awesome-icon'
		}, {
			name : 'shared_vault_folder',
			val : 'fa fa-folder-open awesome-icon'
		}, {
			name : 'unshared_vault_folder',
			val : 'fa fa-folder-o awesome-icon'
		}, {
			name : 'shared_vault_file',
			val : 'fa fa-files-o awesome-icon'
		}, {
			name : 'unshared_vault_file',
			val : 'fa fa-file-o awesome-icon'
		}, {
			name : 'added_vault_file',
			val : 'fa fa-file awesome-icon'
		}, {
			name : 'added_vault_folder',
			val : 'fa fa-folder awesome-icon'
		}, {
			name : 'unread_message',
			val : 'fa fa-envelope awesome-icon'
		}, {
			name : 'added_user_account',
			val : 'fa fa-user-plus awesome-icon'
		}, {
			name : 'added_financial',
			val : 'fa fa-dollar awesome-icon'
		}, {
			name : 'deleted_vault_file',
			val : 'fa fa-remove awesome-icon'
		}, {
			name : 'deleted_vault_folder',
			val : 'fa fa-remove awesome-icon'
		}, {
			name : 'updated_goal',
			val : 'fa fa-bullseye awesome-icon'
		}, {
			name : 'updated_role_to_client',
			val : 'fa fa-user awesome-icon'
		}, {
			name : 'deleted_goal',
			val : 'fa fa-remove awesome-icon'
		}, {
			name : 'added_goal',
			val : 'fa fa-bullseye awesome-icon'
		}, {
			name : 'add_a_goal',
			val : 'fa fa-bullseye awesome-icon'
		}, {
			name : 'large_deposit',
			val : 'fa fa-money awesome-icon'
		} ];

        return {
            pageTransitionOpts: pageTransitionOpts,
            main: main,
            color: color,
            notification_icon: notification_icon
        }
    }

    function mdConfig($mdThemingProvider) {
        var cyanAlt = $mdThemingProvider.extendPalette('cyan', {
            'contrastLightColors': '500 600 700 800 900',
            'contrastStrongLightColors': '500 600 700 800 900'
        })
        var lightGreenAlt = $mdThemingProvider.extendPalette('light-green', {
            'contrastLightColors': '500 600 700 800 900',
            'contrastStrongLightColors': '500 600 700 800 900'
        })        

        $mdThemingProvider
            .definePalette('cyanAlt', cyanAlt)
            .definePalette('lightGreenAlt', lightGreenAlt);


        $mdThemingProvider.theme('default')
            .primaryPalette('teal', {
                'default': '500'
            })
            .accentPalette('cyanAlt', {
                'default': '500'
            })
            .warnPalette('red', {
                'default': '500'
            })
            .backgroundPalette('grey');
    }

})();