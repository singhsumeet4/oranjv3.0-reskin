angular.module('app.constant', [])
    .constant('AUTH_EVENTS', {
        invalidUser: 'user-not-valid',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized',
        noService: 'no-service'
    })
    .constant('USER_RIGHTS', {
        all: 'CRUD',
        create: 'CR',
        update: 'RU',
        read: 'R',
        delete: 'D'
    })
    .constant('USER_ROLES', {
        advisor: 'advisor',
        client: 'client'
    })
    .constant('employment', {
        options:[
                 {"text": "Employed", "value": "EMPLOYED"},
                 {"text": "Self employed", "value": "SELF_EMPLOYED"},
                 {"text": "Unemployed", "value": "UNEMPLOYED"},
                 {"text": "Retired", "value": "RETIRED"},
                 {"text": "Homemaker", "value": "HOMEMAKER"},
                 {"text": "Student", "value": "STUDENT"}
                 ]
    })
    
    .constant('residency', {
        status:[
                 {"text": "US Citizen", "value": "us-citizen"},
                 {"text": "Permanent Resident", "value": "permanent-resident"},
                 {"text": "Not a US Citizen", "value": "non-us-citizen"}
                 ]
    })
	
	 .constant('world',{
        countries:[ 
{"name": "United States", "code": "US"}, 
{"name": "Afghanistan", "code": "AF"}, 
{"name": "Åland Islands", "code": "AX"}, 
{"name": "Albania", "code": "AL"}, 
{"name": "Algeria", "code": "DZ"}, 
{"name": "American Samoa", "code": "AS"}, 
{"name": "AndorrA", "code": "AD"}, 
{"name": "Angola", "code": "AO"}, 
{"name": "Anguilla", "code": "AI"}, 
{"name": "Antarctica", "code": "AQ"}, 
{"name": "Antigua and Barbuda", "code": "AG"}, 
{"name": "Argentina", "code": "AR"}, 
{"name": "Armenia", "code": "AM"}, 
{"name": "Aruba", "code": "AW"}, 
{"name": "Australia", "code": "AU"}, 
{"name": "Austria", "code": "AT"}, 
{"name": "Azerbaijan", "code": "AZ"}, 
{"name": "Bahamas", "code": "BS"}, 
{"name": "Bahrain", "code": "BH"}, 
{"name": "Bangladesh", "code": "BD"}, 
{"name": "Barbados", "code": "BB"}, 
{"name": "Belarus", "code": "BY"}, 
{"name": "Belgium", "code": "BE"}, 
{"name": "Belize", "code": "BZ"}, 
{"name": "Benin", "code": "BJ"}, 
{"name": "Bermuda", "code": "BM"}, 
{"name": "Bhutan", "code": "BT"}, 
{"name": "Bolivia", "code": "BO"}, 
{"name": "Bosnia and Herzegovina", "code": "BA"}, 
{"name": "Botswana", "code": "BW"}, 
{"name": "Bouvet Island", "code": "BV"}, 
{"name": "Brazil", "code": "BR"}, 
{"name": "British Indian Ocean Territory", "code": "IO"}, 
{"name": "Brunei Darussalam", "code": "BN"}, 
{"name": "Bulgaria", "code": "BG"}, 
{"name": "Burkina Faso", "code": "BF"}, 
{"name": "Burundi", "code": "BI"}, 
{"name": "Cambodia", "code": "KH"}, 
{"name": "Cameroon", "code": "CM"}, 
{"name": "Canada", "code": "CA"}, 
{"name": "Cape Verde", "code": "CV"}, 
{"name": "Cayman Islands", "code": "KY"}, 
{"name": "Central African Republic", "code": "CF"}, 
{"name": "Chad", "code": "TD"}, 
{"name": "Chile", "code": "CL"}, 
{"name": "China", "code": "CN"}, 
{"name": "Christmas Island", "code": "CX"}, 
{"name": "Cocos (Keeling) Islands", "code": "CC"}, 
{"name": "Colombia", "code": "CO"}, 
{"name": "Comoros", "code": "KM"}, 
{"name": "Congo", "code": "CG"}, 
{"name": "Congo, The Democratic Republic of the", "code": "CD"}, 
{"name": "Cook Islands", "code": "CK"}, 
{"name": "Costa Rica", "code": "CR"}, 
{"name": "Cote D\"Ivoire", "code": "CI"}, 
{"name": "Croatia", "code": "HR"}, 
{"name": "Cuba", "code": "CU"}, 
{"name": "Cyprus", "code": "CY"}, 
{"name": "Czech Republic", "code": "CZ"}, 
{"name": "Denmark", "code": "DK"}, 
{"name": "Djibouti", "code": "DJ"}, 
{"name": "Dominica", "code": "DM"}, 
{"name": "Dominican Republic", "code": "DO"}, 
{"name": "Ecuador", "code": "EC"}, 
{"name": "Egypt", "code": "EG"}, 
{"name": "El Salvador", "code": "SV"}, 
{"name": "Equatorial Guinea", "code": "GQ"}, 
{"name": "Eritrea", "code": "ER"}, 
{"name": "Estonia", "code": "EE"}, 
{"name": "Ethiopia", "code": "ET"}, 
{"name": "Falkland Islands (Malvinas)", "code": "FK"}, 
{"name": "Faroe Islands", "code": "FO"}, 
{"name": "Fiji", "code": "FJ"}, 
{"name": "Finland", "code": "FI"}, 
{"name": "France", "code": "FR"}, 
{"name": "French Guiana", "code": "GF"}, 
{"name": "French Polynesia", "code": "PF"}, 
{"name": "French Southern Territories", "code": "TF"}, 
{"name": "Gabon", "code": "GA"}, 
{"name": "Gambia", "code": "GM"}, 
{"name": "Georgia", "code": "GE"}, 
{"name": "Germany", "code": "DE"}, 
{"name": "Ghana", "code": "GH"}, 
{"name": "Gibraltar", "code": "GI"}, 
{"name": "Greece", "code": "GR"}, 
{"name": "Greenland", "code": "GL"}, 
{"name": "Grenada", "code": "GD"}, 
{"name": "Guadeloupe", "code": "GP"}, 
{"name": "Guam", "code": "GU"}, 
{"name": "Guatemala", "code": "GT"}, 
{"name": "Guernsey", "code": "GG"}, 
{"name": "Guinea", "code": "GN"}, 
{"name": "Guinea-Bissau", "code": "GW"}, 
{"name": "Guyana", "code": "GY"}, 
{"name": "Haiti", "code": "HT"}, 
{"name": "Heard Island and Mcdonald Islands", "code": "HM"}, 
{"name": "Holy See (Vatican City State)", "code": "VA"}, 
{"name": "Honduras", "code": "HN"}, 
{"name": "Hong Kong", "code": "HK"}, 
{"name": "Hungary", "code": "HU"}, 
{"name": "Iceland", "code": "IS"}, 
{"name": "India", "code": "IN"}, 
{"name": "Indonesia", "code": "ID"}, 
{"name": "Iran, Islamic Republic Of", "code": "IR"}, 
{"name": "Iraq", "code": "IQ"}, 
{"name": "Ireland", "code": "IE"}, 
{"name": "Isle of Man", "code": "IM"}, 
{"name": "Israel", "code": "IL"}, 
{"name": "Italy", "code": "IT"}, 
{"name": "Jamaica", "code": "JM"}, 
{"name": "Japan", "code": "JP"}, 
{"name": "Jersey", "code": "JE"}, 
{"name": "Jordan", "code": "JO"}, 
{"name": "Kazakhstan", "code": "KZ"}, 
{"name": "Kenya", "code": "KE"}, 
{"name": "Kiribati", "code": "KI"}, 
{"name": "Korea, Democratic People\"S Republic of", "code": "KP"}, 
{"name": "Korea, Republic of", "code": "KR"}, 
{"name": "Kuwait", "code": "KW"}, 
{"name": "Kyrgyzstan", "code": "KG"}, 
{"name": "Lao People\"S Democratic Republic", "code": "LA"}, 
{"name": "Latvia", "code": "LV"}, 
{"name": "Lebanon", "code": "LB"}, 
{"name": "Lesotho", "code": "LS"}, 
{"name": "Liberia", "code": "LR"}, 
{"name": "Libyan Arab Jamahiriya", "code": "LY"}, 
{"name": "Liechtenstein", "code": "LI"}, 
{"name": "Lithuania", "code": "LT"}, 
{"name": "Luxembourg", "code": "LU"}, 
{"name": "Macao", "code": "MO"}, 
{"name": "Macedonia, The Former Yugoslav Republic of", "code": "MK"}, 
{"name": "Madagascar", "code": "MG"}, 
{"name": "Malawi", "code": "MW"}, 
{"name": "Malaysia", "code": "MY"}, 
{"name": "Maldives", "code": "MV"}, 
{"name": "Mali", "code": "ML"}, 
{"name": "Malta", "code": "MT"}, 
{"name": "Marshall Islands", "code": "MH"}, 
{"name": "Martinique", "code": "MQ"}, 
{"name": "Mauritania", "code": "MR"}, 
{"name": "Mauritius", "code": "MU"}, 
{"name": "Mayotte", "code": "YT"}, 
{"name": "Mexico", "code": "MX"}, 
{"name": "Micronesia, Federated States of", "code": "FM"}, 
{"name": "Moldova, Republic of", "code": "MD"}, 
{"name": "Monaco", "code": "MC"}, 
{"name": "Mongolia", "code": "MN"}, 
{"name": "Montserrat", "code": "MS"}, 
{"name": "Morocco", "code": "MA"}, 
{"name": "Mozambique", "code": "MZ"}, 
{"name": "Myanmar", "code": "MM"}, 
{"name": "Namibia", "code": "NA"}, 
{"name": "Nauru", "code": "NR"}, 
{"name": "Nepal", "code": "NP"}, 
{"name": "Netherlands", "code": "NL"}, 
{"name": "Netherlands Antilles", "code": "AN"}, 
{"name": "New Caledonia", "code": "NC"}, 
{"name": "New Zealand", "code": "NZ"}, 
{"name": "Nicaragua", "code": "NI"}, 
{"name": "Niger", "code": "NE"}, 
{"name": "Nigeria", "code": "NG"}, 
{"name": "Niue", "code": "NU"}, 
{"name": "Norfolk Island", "code": "NF"}, 
{"name": "Northern Mariana Islands", "code": "MP"}, 
{"name": "Norway", "code": "NO"}, 
{"name": "Oman", "code": "OM"}, 
{"name": "Pakistan", "code": "PK"}, 
{"name": "Palau", "code": "PW"}, 
{"name": "Palestinian Territory, Occupied", "code": "PS"}, 
{"name": "Panama", "code": "PA"}, 
{"name": "Papua New Guinea", "code": "PG"}, 
{"name": "Paraguay", "code": "PY"}, 
{"name": "Peru", "code": "PE"}, 
{"name": "Philippines", "code": "PH"}, 
{"name": "Pitcairn", "code": "PN"}, 
{"name": "Poland", "code": "PL"}, 
{"name": "Portugal", "code": "PT"}, 
{"name": "Puerto Rico", "code": "PR"}, 
{"name": "Qatar", "code": "QA"}, 
{"name": "Reunion", "code": "RE"}, 
{"name": "Romania", "code": "RO"}, 
{"name": "Russian Federation", "code": "RU"}, 
{"name": "RWANDA", "code": "RW"}, 
{"name": "Saint Helena", "code": "SH"}, 
{"name": "Saint Kitts and Nevis", "code": "KN"}, 
{"name": "Saint Lucia", "code": "LC"}, 
{"name": "Saint Pierre and Miquelon", "code": "PM"}, 
{"name": "Saint Vincent and the Grenadines", "code": "VC"}, 
{"name": "Samoa", "code": "WS"}, 
{"name": "San Marino", "code": "SM"}, 
{"name": "Sao Tome and Principe", "code": "ST"}, 
{"name": "Saudi Arabia", "code": "SA"}, 
{"name": "Senegal", "code": "SN"}, 
{"name": "Serbia and Montenegro", "code": "CS"}, 
{"name": "Seychelles", "code": "SC"}, 
{"name": "Sierra Leone", "code": "SL"}, 
{"name": "Singapore", "code": "SG"}, 
{"name": "Slovakia", "code": "SK"}, 
{"name": "Slovenia", "code": "SI"}, 
{"name": "Solomon Islands", "code": "SB"}, 
{"name": "Somalia", "code": "SO"}, 
{"name": "South Africa", "code": "ZA"}, 
{"name": "South Georgia and the South Sandwich Islands", "code": "GS"}, 
{"name": "Spain", "code": "ES"}, 
{"name": "Sri Lanka", "code": "LK"}, 
{"name": "Sudan", "code": "SD"}, 
{"name": "Suriname", "code": "SR"}, 
{"name": "Svalbard and Jan Mayen", "code": "SJ"}, 
{"name": "Swaziland", "code": "SZ"}, 
{"name": "Sweden", "code": "SE"}, 
{"name": "Switzerland", "code": "CH"}, 
{"name": "Syrian Arab Republic", "code": "SY"}, 
{"name": "Taiwan, Province of China", "code": "TW"}, 
{"name": "Tajikistan", "code": "TJ"}, 
{"name": "Tanzania, United Republic of", "code": "TZ"}, 
{"name": "Thailand", "code": "TH"}, 
{"name": "Timor-Leste", "code": "TL"}, 
{"name": "Togo", "code": "TG"}, 
{"name": "Tokelau", "code": "TK"}, 
{"name": "Tonga", "code": "TO"}, 
{"name": "Trinidad and Tobago", "code": "TT"}, 
{"name": "Tunisia", "code": "TN"}, 
{"name": "Turkey", "code": "TR"}, 
{"name": "Turkmenistan", "code": "TM"}, 
{"name": "Turks and Caicos Islands", "code": "TC"}, 
{"name": "Tuvalu", "code": "TV"}, 
{"name": "Uganda", "code": "UG"}, 
{"name": "Ukraine", "code": "UA"}, 
{"name": "United Arab Emirates", "code": "AE"}, 
{"name": "United Kingdom", "code": "GB"}, 
{"name": "United States Minor Outlying Islands", "code": "UM"}, 
{"name": "Uruguay", "code": "UY"}, 
{"name": "Uzbekistan", "code": "UZ"}, 
{"name": "Vanuatu", "code": "VU"}, 
{"name": "Venezuela", "code": "VE"}, 
{"name": "Viet Nam", "code": "VN"}, 
{"name": "Virgin Islands, British", "code": "VG"}, 
{"name": "Virgin Islands, U.S.", "code": "VI"}, 
{"name": "Wallis and Futuna", "code": "WF"}, 
{"name": "Western Sahara", "code": "EH"}, 
{"name": "Yemen", "code": "YE"}, 
{"name": "Zambia", "code": "ZM"}, 
{"name": "Zimbabwe", "code": "ZW"} 
]
    })
    
    .constant('Questionaries',{
        questions:[{
	question: "What is your current age?",
	options: [{
		lable: "18 - 25 years old",
		value: "18 - 25 years old"
	}, {
		lable: "41 - 55 years old",
		value: "41 - 55 years old"
	}, {
		lable: "over 71  years old",
		value: "over 71  years old"
	}, {
		lable: "26 - 40 years old",
		value: "26 - 40 years old"
	},{
		lable: "56 - 70 years old",
		value: "56 - 70 years old"
	}]
}, {
	question: "When i make a long-term investment, i will keep my money in that investment for:",
	options: [{
		lable: "1 - 2 years old",
		value: "1 - 2 years old"
	}, {
		lable: "3 - 4 years old",
		value: "3 - 4  years old"
	}, {
		lable: "5 - 6  years old",
		value: "5 - 6  years old"
	}, {
		lable: "7 - 8 years old",
		value: "7 - 8 years old"
	},{
		lable: "more then 8 years",
		value: "more then 8 years"
	}]
}, {
	question: "If my portfolio contained stock investments that lost about 30% in 3 months, i would:(Please select the answer that matches your strategy if you owned stock investments during this time period.)",
	options: [{
		lable: "sell all of my stock investments",
		value: "sell all of my stock investments"
	}, {
		lable: "sell a portion  of my remaining stock investments",
		value: "sell a portion  of my remaining stock investments"
	}, {
		lable: "Do not sell any stock investment and hold onto all of my stock",
		value: "Do not sell any stock investment and hold onto all of my stock"
	}, {
		lable: "Buy more stock investments",
		value: "Buy more stock investments"
	}]
}, {
	question: "If my portfolio contained bond investments that lost almost 4%, I would:(Please select the answer that matches your strategy if you owned stock investments during this time period.)",
	options: [{
		lable: "sell all of my bond investments",
		value: "sell all of my bond investments"
	}, {
		lable: "sell a portion  of my remaining bond investments",
		value: "sell a portion  of my remaining bond investments"
	}, {
		lable: "Do not sell any bond investment and hold onto all of my bond",
		value: "Do not sell any bond investment and hold onto all of my bond"
	}, {
		lable: "Buy more bond investments",
		value: "Buy more bond investments"
	}]
},{
	question: "You have just finished saving up enough money to go on your dream vacation in a month, but you just lost your job. What would you do?",
	options: [{
		lable: "Cancel the vacation",
		value: "Cancel the vacation"
	}, {
		lable: "Go on a much more modest vacation",
		value: "Go on a much more modest vacation"
	}, {
		lable: "Go on your dream vacation as planned. it may be just what you need and it could give you time to prepare for job hunting",
		value: "Go on your dream vacation as planned. it may be just what you need and it could give you time to prepare for job hunting"
	}, {
		lable: "Take a longer trip because you can get extra vacation perks and have a once-in-a-lifetime experience",
		value: "Take a longer trip because you can get extra vacation perks and have a once-in-a-lifetime experience"
	}]
},
 {
	question: "Which of the following statements is most true about your level of risk tolerance and your approach to reaching your goals through investing?",
	options: [{
		lable: "My investments should be completely safe; I do not ever want to risk losing any of my principal",
		value: "My investments should be completely safe; I do not ever want to risk losing any of my principal"
	}, {
		lable: "My investments should generate consistent income and provide extra spending money for me",
		value: "My investments should generate consistent income and provide extra spending money for me"
	}, {
		lable: "My investments should provide me some extra income for spending money and should also grow in value over time",
		value: "My investments should provide me some extra income for spending money and should also grow in value over time"
	}, {
		lable: "My investments should significantly grow in  value over time. I do not need my investments to generate income",
		value: "My investments should significantly grow in  value over time. I do not need my investments to generate income"
	}]
},{
	question: "If you are investing in a stock, which quality is the most important to you?",
	options: [{
		lable: "Blue Chip stocks that play dividends",
		value: "Blue Chip stocks that play dividends"
	}, {
		lable: "Well-Known and established companies that have potential for continued growth",
		value: "Well-Known and established companies that have potential for continued growth"
	}, {
		lable: "Technology companies that are making significant  advances and are selling at their low initial public offering price",
		value: "Technology companies that are making significant  advances and are selling at their low initial public offering price"
	}]
},{
	question: "When do you plan on withdrawing most of the money that you are currently investing?",
	options: [{
		lable: "At any time, so high liquidity levels is important",
		value: "At any time, so high liquidity levels is important"
	}, {
		lable: "2 - 5 years from now",
		value: "2 - 5 years from now"
	},{
		lable: "6 - 10 years from now",
		value: "6 - 10 years from now"
	}, {
		lable: "11 - 12 years from now",
		value: "11 - 12 years from now"
	}]
},
 {
	question: "What is your current level of annual income?",
	options: [{
		lable: "Less than $100K",
		value: "Less than $100K"
	}, {
		lable: "$100K - $250K",
		value: "$100K - $250K"
	}, {
		lable: "$250K - $500K",
		value: "$250K - $500K"
	}, {
		lable: "$500K - $1M",
		value: "$500K - $1M"
	},{
		lable: "Greater than $1M",
		value: "Greater than $1M"
	}]
},{
	question: "What amount of money do you have set aside for emergencies? This does not include borrowings or credit lines, but does include money in checking and saving accounts, CDs, money market funds, and assets in no-load open-end mutual funds that you can access",
	options: [{
		lable: "None",
		value: "None"
	}, {
		lable: "Enough to cover three months of expenses",
		value: "Enough to cover three months of expenses"
	}, {
		lable: "Enough to cover six months of expenses",
		value: "Enough to cover six months of expenses"
	}, {
		lable: "Enough to cover nine months of expenses",
		value: "Enough to cover nine months of expenses"
	},{
		lable: "Over twelve months of expenses",
		value: "Over twelve months of expenses"
	}]
},{
	question: "I do not expect any large expenses that would require me to withdraw funds from my investments.",
	options: [{
		lable: "Strongly Agree",
		value: "Strongly Agree"
	}, {
		lable: "Agree",
		value: "Agree"
	}, {
		lable: "Neutral",
		value: "Neutral"
	}, {
		lable: "Disagree",
		value: "Disagree"
	},{
		lable: "Strongly Disagree",
		value: "Strongly Disagree"
	}]
},{
	question: "Protecting my investments from losses is the most important part of my investing strategy",
	options: [{
		lable: "Strongly Agree",
		value: "Strongly Agree"
	}, {
		lable: "Agree",
		value: "Agree"
	}, {
		lable: "Neutral",
		value: "Neutral"
	}, {
		lable: "Disagree",
		value: "Disagree"
	},{
		lable: "Strongly Disagree",
		value: "Strongly Disagree"
	}]
},{
	question: "I always choose investments with the highest possible return",
	options: [{
		lable: "Strongly Agree",
		value: "Strongly Agree"
	}, {
		lable: "Agree",
		value: "Agree"
	}, {
		lable: "Neutral",
		value: "Neutral"
	}, {
		lable: "Disagree",
		value: "Disagree"
	},{
		lable: "Strongly Disagree",
		value: "Strongly Disagree"
	}]
},{
	question: "I seek investment strategies that grow steadily without sharp ups and downs",
	options: [{
		lable: "Strongly Agree",
		value: "Strongly Agree"
	}, {
		lable: "Agree",
		value: "Agree"
	}, {
		lable: "Neutral",
		value: "Neutral"
	}, {
		lable: "Disagree",
		value: "Disagree"
	},{
		lable: "Strongly Disagree",
		value: "Strongly Disagree"
	}]
},{
	question: "I seek investment strategies that grow at a high rate of return to reach my financial goals.",
	options: [{
		lable: "Strongly Agree",
		value: "Strongly Agree"
	}, {
		lable: "Agree",
		value: "Agree"
	}, {
		lable: "Neutral",
		value: "Neutral"
	}, {
		lable: "Disagree",
		value: "Disagree"
	},{
		lable: "Strongly Disagree",
		value: "Strongly Disagree"
	}]
},{
	question: "I do not want to experience a loss in an extended down market and wait several years to recover.",
	options: [{
		lable: "Strongly Agree",
		value: "Strongly Agree"
	}, {
		lable: "Agree",
		value: "Agree"
	}, {
		lable: "Neutral",
		value: "Neutral"
	}, {
		lable: "Disagree",
		value: "Disagree"
	},{
		lable: "Strongly Disagree",
		value: "Strongly Disagree"
	}]
},{
	question: "Low risk investments are my preference, even if inflation outpaces the returns.",
	options: [{
		lable: "Strongly Agree",
		value: "Strongly Agree"
	}, {
		lable: "Agree",
		value: "Agree"
	}, {
		lable: "Neutral",
		value: "Neutral"
	}, {
		lable: "Disagree",
		value: "Disagree"
	},{
		lable: "Strongly Disagree",
		value: "Strongly Disagree"
	}]
},{
	question: "Which of the following options best describes your attitudes for an investment's performance over the NEXT three years?",
	options: [{
		lable: "I don't mind if i lose money",
		value: "I don't mind if i lose money"
	}, {
		lable: "I can tolerate loss",
		value: "I can tolerate loss"
	}, {
		lable: "I can tolerate a small loss",
		value: "I can tolerate a small loss"
	}, {
		lable: "I'd have a hard time tolerating any losses",
		value: "I'd have a hard time tolerating any losses"
	},{
		lable: "I need to see at least a little return",
		value: "I need to see at least a little return"
	}]
},{
	question: "What is your goal for a long term investment?",
	options: [{
		lable: "To avoid losing money",
		value: "To avoid losing money"
	}, {
		lable: "To grow with caution",
		value: "To grow with caution"
	}, {
		lable: "To grow moderately",
		value: "To grow moderately"
	}, {
		lable: "To grow significantly",
		value: "To grow significantly"
	},{
		lable: "To grow aggressively",
		value: "To grow aggressively"
	}]
}]

  
        
    })
	
    .constant('AUTHORIZATION_KEY', {
    client_id: 'fiftyandfifty',
    client_secret: 'YNSL9l6WCXuDkl9DBdnRfZrzB2DJXctzrQ/tL89TvSE='
});