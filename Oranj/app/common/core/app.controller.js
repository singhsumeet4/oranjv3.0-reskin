(function () {
    'use strict';

    angular.module('app')
    .controller('AppCtrl', [ '$scope', '$rootScope', '$state', '$document', 'appConfig', 'AUTH_EVENTS','$mdSidenav', AppCtrl]) // overall control
    
    function AppCtrl($scope, $rootScope, $state, $document, appConfig, AUTH_EVENTS, $mdSidenav) {

        $scope.pageTransitionOpts = appConfig.pageTransitionOpts;
        $scope.main = appConfig.main;
        $scope.color = appConfig.color;
        
         $scope.toggleRight = buildToggler('right');
         function buildToggler(navID) {

             return function () {
                 $mdSidenav(navID)
                     .close()

             }
         }


         $scope.getCurrentRoute = function () {             
             $rootScope.prevRoute = $state.current.name;
         }


        $scope.$watch('main', function(newVal, oldVal) {
            // if (newVal.menu !== oldVal.menu || newVal.layout !== oldVal.layout) {
            //     $rootScope.$broadcast('layout:changed');
            // }
            //$rootScope.breadcrum = {};
            if (newVal.menu === 'horizontal' && oldVal.menu === 'vertical') {
                $rootScope.$broadcast('nav:reset');
            }
            if (newVal.fixedHeader === false && newVal.fixedSidebar === true) {
                if (oldVal.fixedHeader === false && oldVal.fixedSidebar === false) {
                    $scope.main.fixedHeader = true;
                    $scope.main.fixedSidebar = true;
                }
                if (oldVal.fixedHeader === true && oldVal.fixedSidebar === true) {
                    $scope.main.fixedHeader = false;
                    $scope.main.fixedSidebar = false;
                }
            }
            if (newVal.fixedSidebar === true) {
                $scope.main.fixedHeader = true;
            }
            if (newVal.fixedHeader === false) {
                $scope.main.fixedSidebar = false;
            }
        }, true);


        $rootScope.$on("$stateChangeSuccess", function (event, currentRoute, previousRoute) {
            $document.scrollTo(0, 0);
        });
        
        // $scope.$on(AUTH_EVENTS.notAuthorized, function (event) {
        //     alert("You are not allowed to access this resource");
        // });
        //
        // $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
        //     AuthService.logout();
        //     $state.go('page/signin');
        //      alert("Sorry, You have to login again.");
        //   
        // });
        
        /* Redirect from the Notification feed to the specified location*/
	    $scope.redirectVault = function(row) {
	    	//$state.go("page.client.vault-home.recent",{obj:{fileUrl:row.contextProperties.fileUrl,fileName:row.contextProperties.name,fileDate:row.contextProperties.lastModifiedDate,fileSize:row.contextProperties.fileSize,fileId:row.contextProperties.id}});
	    	$rootScope.sFlag = true;
	    	if(!$rootScope.isAdvisor){	    
		    	if(row.contextProperties.folder) {
		    		$state.go("page.client.vault-home.folder",{folderName:row.contextProperties.folder,obj:{fileUrl:row.contextProperties.fileUrl,fileName:row.contextProperties.name,fileDate:row.contextProperties.lastModifiedDate,fileSize:row.contextProperties.fileSize,fileId:row.contextProperties.id}});
		    	}
		    	else {
		    		$state.go("page.client.vault-home.dashboard",{folderName:row.contextProperties.folder,obj:{fileUrl:row.contextProperties.fileUrl,fileName:row.contextProperties.name,fileDate:row.contextProperties.lastModifiedDate,fileSize:row.contextProperties.fileSize,fileId:row.contextProperties.id}});
		    	}
	    	}
	    	
	    }
	    $scope.redirectMessage = function(row) {	    	
	    	if(!$rootScope.isAdvisor){	    	    	
	    		$state.go("page.messages");
	    	}
	    	
	    }
	    $scope.redirectProfile = function(row) {
	    	if(!$rootScope.isAdvisor)	    	
	    		$state.go("page.client.settings.info");
	    	
	    }
	    $scope.redirect_link = function(value) {
	    	if(!$rootScope.isAdvisor){
		    	if(value=="vault")
		    		$state.go("page.client.vault-home.dashboard");
		    	else if(value=="goal(s)")
		    		$state.go("page.client.home.goal.dashboard");
		    	else if(value=="financial")
		    		$state.go("page.client.home.accounts.dash");
		    	else if(value=="message")
		    		$state.go("page.messages");
		    	else if(value=="account")
		    		$state.go("page.client.home.investment.dash");
	    	}
	    }
	    /* End */
    }

})(); 
