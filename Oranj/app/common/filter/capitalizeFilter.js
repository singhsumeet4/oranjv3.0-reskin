﻿(function () {

    'use strict';
    angular.module('app').filter('capitalize', function () {
       
        return function (input) {
           // input = input.concat('_');
            //var str = "_";
            //input = input + str;;
            // // console.log('input', input);
            if (angular.isUndefinedOrNull(input))
                return '';

            input = input.replace('_', ' ');
           // input = input.replace(/[^a-zA-Z]/g, ' ');
        
            return input.toLowerCase().replace(/\b\w/g, function (m) {
                return m.toUpperCase();
            });            
        }

    });

})();