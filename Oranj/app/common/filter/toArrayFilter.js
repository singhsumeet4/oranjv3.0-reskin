(function (){

  'use strict';
  angular.module('app').filter('toArray', function () {     
   return function(obj) {
            if (!(obj instanceof Object)) return obj;
            
            var res = [];
            angular.forEach(obj, function(val, key){
                val.key = key;
                res.push(val);
            });
            return res;
        };
	});

})();