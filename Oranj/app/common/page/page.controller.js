(function () {
    'use strict';

    angular.module('app.page')
    .controller('authCtrl', ['$scope', '$state','configfactory','OranjApiService', 'AUTHORIZATION_KEY','UserProfileService','$cookies', '$timeout',authCtrl]);

    function authCtrl($scope, $state, configfactory, OranjApiService, AUTHORIZATION_KEY,UserProfileService,$cookies, $timeout) {

        $scope.loginErrorShow = false;
        $scope.form = {
            username: '',
            password: ''
        };




        function navigateUser() {

            UserProfileService.fetchUserRole().then(function (userRole) {
                if(userRole.isClient) {
                    $state.go('page.client.home.dash');
                    UserProfileService.fetchUserProfile();
                } else if(userRole.isAdvisor) {
                    $state.go('page.advisor.home');
                } else if(userRole.isAdmin) {
                	if(!$cookies.getObject('auth') && $scope.resData)
                    {
                        $cookies.putObject('auth', $scope.resData);
                    }
                    $timeout(function() {
                        location.href = '/admin_tool_dash.html';
                    }, 100)
                }
            }, function (response) {
                if (response.error === "invalid_grant") {
                    $scope.loginForm.username.invalid = true;
                    $scope.loginForm.password.invalid = true;
                    $scope.user_error = "Incorrect credentials";
                }
            });

        };

        navigateUser();

        $scope.submitLogin = function () {
            if (($scope.form.username == '' || typeof $scope.form.username == 'undefined') || ($scope.form.password == '' || typeof $scope.form.password == 'undefined')) {
                //angular.element('.alert-danger', angular.element('.login-form')).show();
                $scope.loginErrorShow = true;
                $scope.user_error = "Invalid credentials";
            }
            if ($scope.loginForm.$valid) {
                OranjApiService('login', {
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    headers: {
                        "Authorization": 'Basic ' + btoa(AUTHORIZATION_KEY.client_id + ':' + AUTHORIZATION_KEY.client_secret),
                        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                    },
                    data: {
                        'grant_type': "password",
                        'username': $scope.form.username + "/" + configfactory.firmName,
                        'password': $scope.form.password
                    }
                }).then(function (data) {
                    if (data.access_token) {
                    	$scope.resData = data;
                        navigateUser();
                    } else {
                        $scope.loginErrorShow = true;
                        $scope.user_error = "Invalid credentials";
                        //alert("invalid Credentials");
                    }
                }, function (response) {
                    if (response.data.error === "invalid_grant") {
                        $scope.loginErrorShow = true;
                        $scope.loginForm.username.invalid = true;
                        $scope.loginForm.password.invalid = true;
                        $scope.user_error = "Incorrect credentials";
                        //alert("Incorrect username.");
                    }
                    else {
                        $scope.loginErrorShow = true;
                        $scope.loginForm.username.invalid = true;
                        $scope.user_error = "The user is inactive.";
                        //alert("The user is inactive.");
                    }
                    // loader.style.display='none';
                });
            }
        };


    }

})();


