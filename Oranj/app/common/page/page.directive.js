(function () {
    'use strict';

    angular.module('app.page')
        .directive('customPage',['$state', customPage]);


    // add class for specific pages to achieve fullscreen, custom background etc.
    function customPage($state) {
        var directive = {
            restrict: 'A',
            link: customPageLinkFn
        };

        return directive;

        function customPageLinkFn($scope, $element) {
            var addBg;

            addBg = function(classes) {
                $element.removeClass('on-canvas body-wide body-err body-lock body-auth');
                if(classes) {
                    $element.addClass(classes);    
                }
            };

            addBg($state.current.bodyClass);

            $scope.$on('$stateChangeStart', function(evt, toState) {
                addBg(toState.bodyClass);
            })
        }
    }

})();


