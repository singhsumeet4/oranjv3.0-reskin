(function () {
    'use strict';

    angular.module('app.page')
     .controller('resetCtrl', ['$scope', '$state','configfactory','OranjApiService','$cookies', 'AUTHORIZATION_KEY', '$http', resetCtrl]);

    function resetCtrl($scope, $state,configfactory,OranjApiService,$cookies, AUTHORIZATION_KEY, $http) {
          // Hide & show password function
		$scope.hideShowNewPassword = function(){
		    if ($scope.newPass == 'password')
		      $scope.newPass = 'text';
		    else
		      $scope.newPass = 'password';
		};
		
		$scope.hideShowConfirmPassword = function(){
		    if ($scope.confirmNewPass == 'password')
		      $scope.confirmNewPass = 'text';
		    else
		      $scope.confirmNewPass = 'password';
		};
		
    	var $resetForm = angular.element('.form-reset');
    	$scope.resetPassword = "";
    	$scope.resetConfirmPassword = "";
    	$scope.errorMgs='';
    	
    	function getParameterByName(name) {
	        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	            results = regex.exec(window.location.href);
	        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	    }
    	
    	$scope.submitForgot = function () {
            $scope.errorMgs = '';
            if (($scope.email == '' || typeof $scope.email == 'undefined')) {
                $scope.errorMgs = 'Please enter an email id to reset the password.';
            }
            if ($scope.forgotForm.$valid) {
            	$http({
	   			    method: 'POST',
	   			    url: configfactory.host+'/password/recovery/email',
	   			    headers: {
	   			    	 "Authorization": 'Basic ' + btoa(configfactory.client_id + ':' + configfactory.client_secret)
	   			    },
	   			    data: {
	   	 				'email' : $scope.email
	   				}
	   			}).then(function (response) {
                    if (response.data.status === "success") {
                        $scope.errorMgs = 'Password reset instructions has been sent to your email';
                    } else {
                        $scope.errorMgs = response.data.message;
                    }
                 }, function () {
                    $scope.errorMgs = "Error occurred while resetting the password.";
                });
            }
        };
        
    	$scope.submitReset = function(){
			  $scope.errorMgs='';
	   		 //e.preventDefault();
	   		 if(($scope.resetPassword.trim() == '')){
	   			// alert('Please enter an email id to reset the password.');
				   $scope.errorMgs='Please enter password.';
	   		 }
	   		 else if($scope.resetPassword != $scope.resetConfirmPassword) {
	   			$scope.errorMgs='Password and Confirm Password do not match';
	   		 }
	   		 else if($scope.resetPassword.length < 5) {
	   			$scope.errorMgs='Password should have at least 5 characters';
	   		 }
	   		 else if($scope.resetForm.$valid) {
	   			 //angular.element(".forget-password").addClass("disabled");
	   			 //loader.style.display='block';
	   			 $http({
	   			    method: 'POST',
	   			    url: configfactory.host+"/password/recovery/reset?token=" + getParameterByName('token') + "&email=" + getParameterByName('email'),
		   			headers: {
	   			    	 "Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8'
	   			    },
	   			    data: $('#resetForm').serialize()
	   			}).
	   			then(function (response) {
	   				if (response.data.status === "success") {
	   					$scope.errorMgs='Password reseted with success!';
	   	            } else {
	   	            	$scope.errorMgs=response.data.message;
	   	            }
	   				//angular.element(".forget-password").removeClass("disabled");
	   				//loader.style.display='none';
	   			},function(response){
	   				//angular.element(".forget-password").removeClass("disabled");
	   				$scope.errorMgs="Error occured while resetting the password.";
	                   //loader.style.display='none';
	   	        });
	   		 }
    	};

    	$resetForm.submit(function (e) {
            e.preventDefault();
            $scope.submitReset();
        });
    }

})();


