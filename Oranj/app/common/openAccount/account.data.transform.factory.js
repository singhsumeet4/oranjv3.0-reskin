(function () {
	angular.module('app.matched')
	.factory('accountDataTransformFactory', [accountDataTransformFactory]);
	
	
	function accountDataTransformFactory(){
		
		var recommendedPortfolio = [];
		
		
		
		return {
			transformMatchedPortfolioData: function (portfolioData) {
                
                angular.forEach(portfolioData, function (item, index) {
                	var obj = {};
                	obj.portfolioName = item.name;
                	var options = [
                                   {
                                       label: 'US Stock',
                                       value: item.usStock,
                                       name: 'US Stock',
                                         color: '#fdce0b'

                                   },
                                   {
                                       label: 'Non US Stock',
                                       value: item.nonUsStock,
                                       name: 'Non US Stock',
                                      color: '#a3cd39'
                                   },
                                   {
                                       label: 'US Bond',
                                       value: item.usBond,
                                       name: 'US Bond',
                                       color: '#3eb649',
                                   },
                                   {
                                       label: 'Non US Bond',
                                       value: item.nonUsBond,
                                       name: 'Non US Bond',
                                      color: '#23c1e2',
                                   },
                                   {
                                       label: 'Preferred',
                                       value: item.preferred,
                                       name: 'Preferred',
                                      color: '#23c1e2',
                                   },
                                   {
                                       label: 'Convertible',
                                       value: item.convertible,
                                       name: 'Convertible',
                                       color: '#000285',
                                   },
                                   {
                                       label: 'Cash',
                                       value: item.cash,
                                       name: 'Cash',
                                       color: '#fdb45c',
                                   },
                                   {
                                       label: 'Other',
                                       value: item.other,
                                       name: 'Other',
                                       color: '#f7464a',
                                   }

                               ];
                	obj.profileData = options;
                	recommendedPortfolio.push(obj);
                });
               // console.log(recommendedPortfolio);
                return recommendedPortfolio;
            }
		};
	}
})();