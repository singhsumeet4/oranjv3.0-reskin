(function () {
    'use strict';

    angular.module('app.matched')
        .controller('matchedPortfolioCtrl', ['$scope', '$rootScope', '$log', 'OranjApiService' , '$state', 'GlobalFactory', 'accountOpeningService', 'accountFormDataService', '$location', '$mdDialog', 'UserProfileService', matchedPortfolioCtrl])

        //----------------SSN Directive Start ----------------------------

        .directive('validateSsn', function () {
            var SSN_REGEXP = /^(?!000)(?!666)(?!9)\d{3}[- ]?(?!00)\d{2}[- ]?(?!0000)\d{4}$/;
            var ssnPattern = {
                3: '-',
                5: '-'
            };
            return {
                require: 'ngModel',
                link: function (scope, elem, attr, ctrl) {
                    var formatSSN = function () {
                        var sTempString = ctrl.$viewValue;
                        sTempString = sTempString.replace(/\-/g, '');
                        var numbers = sTempString;
                        var temp = '';
                        for (var i = 0; i < numbers.length; i++) {
                            temp += (ssnPattern[i] || '') + numbers[i];
                        }
                        ctrl.$viewValue = temp;

                        scope.$apply(function () {
                            elem.val(ctrl.$viewValue);
                        });

                    };
                    ctrl.$parsers.unshift(function (viewValue) {
                        // test and set the validity after update.
                        var valid = SSN_REGEXP.test(viewValue);
                        ctrl.$setValidity('ssnValid', valid);
                        return viewValue;
                    });
                    // This runs when we update the text field
                    ctrl.$parsers.push(function (viewValue) {

                        var valid = SSN_REGEXP.test(viewValue);
                        ctrl.$setValidity('ssnValid', valid);
                        return viewValue;
                    });
                    elem.bind('blur', formatSSN);

                }
            };
        });

    //----------------SSN Directive End----------------------------


    function matchedPortfolioCtrl($scope, $rootScope, $log, OranjApiService, $state, GlobalFactory, accountOpeningService, accountFormDataService, $location, $mdDialog, UserProfileService) {

        $scope.addDymCLass = function (params) {
           return 'accqgroup' + params;
        };


        // matchedportfolio doughnut chart start

        Chart.defaults.global.responsive = true;
        $scope.questions= [];
        $scope.questionForm = [];

        $scope.questionList = function () {
        	$scope.riskToleranceData = [];
        	$scope.riskToleranceData = accountFormDataService.getRiskTolerance();
        	 OranjApiService('getQuestions').then(function (response) {
             	var counter=0;
             	if(response.status === 'success'){
             		for(var i=0;i<response.data.length;i++){
     	              	if(response.data[i].active){
     	              		$scope.questions[counter++]=response.data[i];
     	              	}
     	            }
             	}else{
             		$scope.questions = [];
             	}
             	
          	 }, function (response) {
              });
        };
       
        


        // for save data locally start
       // $scope.questions = Questionaries.questions;

        $scope.save = function (index) {
        	switch(index){
	        	case 1:
	        		$state.go('page.openaccountstep2');
	        		break;
	        	case 2:
	        		
	        		angular.forEach($scope.questionForm.$error.required, function(field) {
	        		    field.$setDirty();
	        		});
	        		if($scope.questionForm.$valid) {
	        			var answerData = [];
		        		var flag = true;
		        		for(var i=0; i<$scope.riskToleranceData.length; i++){
		        			if($scope.riskToleranceData[i]){
		        				answerData.push({'id' : $scope.riskToleranceData[i]});
	        				}else{
	        					flag = false;
	        				}
		        		}
		        		
		        		var postData = {};
		       			postData.answers = answerData;
		       			if(answerData.length > 0) {
		       				accountOpeningService.fetchMatchedPortfolio(postData).then(function(matchPortfolioObject) {
			       				if(matchPortfolioObject.status === 'success'){
			       					accountFormDataService.setselectedPortfolio(0);
			       					accountFormDataService.saveRiskTolerance($scope.riskToleranceData);
			       					$state.go('page.openaccountstep3');
			       				}else{
			       					GlobalFactory.showErrorAlert('No matched portfolio found.');
			       				}
			                }); 
		       			}
	        		}   
	        		break;
	        	case 3:
	        		$state.go('page.openaccountstep4');
	        		break;
        	}
        	
            //$log.debug($scope.questions);
            //console.log($scope.questions);
            // window.localStorage.set('selectedQues', [$scope.questions]);
            window.sessionStorage.setItem("savselectedQuesed", JSON.stringify($scope.questions));
        };

        $scope.selectedQuestion = JSON.parse(window.sessionStorage.getItem("savselectedQuesed"));


        // openAccountstep3 doughnut chart start


        var recommendedPortfolio = [];
        var doughnutOptionsStep3 = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,

        };


        
        //-------------------------------------------------------------------------------------

        $scope.selectedPortfolio = 0;
        /*$scope.goTo = function (left,index) {
            if ($scope.selectedPortfolio === 0) {
                recommendedPortfolioChart(1);
                $scope.selectedPortfolio = 1;
            }

            else {
            	$scope.selectedPortfolio = 0;
                recommendedPortfolioChart(0);
            }

        }*/
        
        function recommendedPortfolioChart(index) {
        	accountFormDataService.setselectedPortfolio(index);
        	$scope.matchPortfolio = recommendedPortfolio[index];
            try {
                document.getElementById("matchedportfolioDoughnutStep3").innerHTML = '';
                document.getElementById('my-legendStep3').innerHTML = '';
                var ctxStep4 = document.getElementById("matchedportfolioDoughnutStep3").getContext("2d");
                var myDoughnutStep3 = new Chart(ctxStep4).Doughnut(recommendedPortfolio[index].profileData, doughnutOptionsStep3);
                $scope.nameData = recommendedPortfolio[index].portfolioName;
                document.getElementById('my-legendStep3').innerHTML = myDoughnutStep3.generateLegend(); // for legend 
            }
            catch (e) {
            	console.log(e);
            }
        }
        
        $scope.goPrevious = function (){
        	if($scope.selectedPortfolio - 1 <=0){
        		$scope.selectedPortfolio = 0;
        	}else{
        		$scope.selectedPortfolio = $scope.selectedPortfolio -1;
        	}
        	recommendedPortfolioChart($scope.selectedPortfolio);
        };
        
        $scope.goNext = function (){
        	if(Number($scope.selectedPortfolio)+1 >= recommendedPortfolio.length){
        		$scope.selectedPortfolio = recommendedPortfolio.length - 1;
        	}else{
        		$scope.selectedPortfolio = Number($scope.selectedPortfolio)+1;
        	}
        	recommendedPortfolioChart($scope.selectedPortfolio);
        };

        
        $scope.resetAccountForm = function(){
        	var confirmation = $mdDialog.confirm({
                htmlContent: '<div>Are you sure you want to cancel this process? All data submitted will not be saved',
                ok: 'YES',
                cancel : 'NO'
            });
          
        	$mdDialog.show(confirmation).then(function(answer) {
           		accountFormDataService.resetAccountFormData();
           		if($rootScope.prevPath != null && $rootScope.prevPath != undefined) {
           			$location.path($rootScope.prevPath);
           		}else {
           			$state.go('page.client.home.dash');
           		}
           });
        	
        };
        
        $scope.getMatchedPortfolio = function (){
        	accountOpeningService.getMatchedPortfolio().then(function (matchPortfolioObject) {

                if(matchPortfolioObject.status === 'success'){
                	Chart.defaults.global.responsive = true;
                	recommendedPortfolio = matchPortfolioObject.portfolioChartData;
                	$scope.selectedPortfolio = accountFormDataService.getselectedPortfolio();
                	recommendedPortfolioChart($scope.selectedPortfolio);
                    $scope.recommendedPortfolioLength= recommendedPortfolio.length;
                }else{
   					GlobalFactory.showErrorAlert('No matched portfolio found.');
   					$state.go('page.openaccountstep2');
   				}
            });
        };

        // openAccountStep3 doughnut chart end



        // ===========================================================================


    }




})();