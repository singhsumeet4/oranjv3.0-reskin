(function () {
	angular.module('app.matched')
	.service('accountFormDataService',['$filter', function($filter) {
		
		var accountType = "";
		var flag = false;
		var formData = {
				  firstName: "",
				  middleName: "",
				  lastName: "",
				  ssnTin: "",
				  annualIncome: "",
				  dobStr: "",
				  maritalStatus: "",
				  email: "",
				  phone: "",
				  address1: "",
				  address2: "",
				  city: "",
				  state: "",
				  zip: "",
				  country: "",
				  statementFile: null,
				  statementFileName: null,
				  employmentStatus: "EMPLOYED",
				  employerName: "",
				  occupation: "",
				  typeOfBusiness: "",
				  employerStreetAddress: "",
				  employerCity: "",
				  employerState: "",
				  employerZip: "",
				  sourceOfIncome: "",
				  citzenship: "",
				  countrySecondaryCitzenship: "",
				  countryCitzenship: "",
				  countryBirth: "",
				  hasVisa: "false",
				  visaNumber: "",
				  visaExpirationStr: "",
				  trusteefirstName: "",
				  trusteemiddleName: "",
				  trusteelastName: "",
				  trusteePhone: "",
				  trusteeAddress1: "",
				  trusteeAddress2: "",
				  trusteeCity: "",
				  trusteeState: "",
				  trusteeZip: "",
				  trusteeCountry: "",
				  trusteeSsnTin: "",
				  trusteeEmail: "",
				  bankName: "",
				  bankAccountTitle: "",
				  bankAccountNumber: "",
				  bankRoutingNumber: "",
				  bankAccountType: "",
				  bankAmountOfTransaction: "",
				  recurringAmmount: "",
				  recurringTransferFrequency: "",
				  accountName: "",
				  initialContributionAmount: 0,
				  atNameOfDeliveringFirm: "",
				  atAccountNumber: "",
				  atAccountValue: "",
				  accountTitleRegistration: "",
				  addressDeliveringFirm: "",
				  phoneDeliveringFirm: "",
				  fundsDestiny: "",
				  titleOfTrust: "",
				  effectiveDateOfTrust: "",
				  trustTaxId: ""
				};
		var spouseData = {
				spouse_firstName: '',
				spouse_lastName: '',
				spouse_dobStr:'',
				spouse_annualIncome:''
		};
		var fileData = [];
		var selectedPortfolio = 0;
		var childrenStatus = 'no';
		var trusteeStatus = false;
		var benStatus  = false;
		var childrenDetails = [];
		var riskToleranceData = [];

		 var updateProperty = function(dataObj,check) {
			  flag = true;
			  if(angular.isObject(dataObj)){
				  angular.forEach(dataObj, function(item,index){
					  /*if (['dobStr','spouse_dobStr'].indexOf(index) != -1){
						  item = $filter('date')(item, "MM/dd/yyyy");
					  }*/
					  if(check === 1){
						  formData[index] = item;
					  }else{
						  spouseData[index] = item;
					  }
					  
				  });
			  }
			  
		  };
		  
		  var resetAccountFormData = function() {
			  	accountType = '';
				flag = false;
				formData = {
						  firstName: "",
						  middleName: "",
						  lastName: "",
						  ssnTin: "",
						  annualIncome: "",
						  dobStr: "",
						  maritalStatus: "",
						  email: "",
						  phone: "",
						  address1: "",
						  address2: "",
						  city: "",
						  state: "",
						  zip: "",
						  country: "",
						  statementFile: null,
						  statementFileName: null,
						  employmentStatus: "EMPLOYED",
						  employerName: "",
						  occupation: "",
						  typeOfBusiness: "",
						  employerStreetAddress: "",
						  employerCity: "",
						  employerState: "",
						  employerZip: "",
						  sourceOfIncome: "",
						  citzenship: "",
						  countrySecondaryCitzenship: "",
						  countryCitzenship: "",
						  countryBirth: "",
						  hasVisa: "false",
						  visaNumber: "",
						  visaExpirationStr: "",
						  trusteefirstName: "",
						  trusteemiddleName: "",
						  trusteelastName: "",
						  trusteePhone: "",
						  trusteeAddress1: "",
						  trusteeAddress2: "",
						  trusteeCity: "",
						  trusteeState: "",
						  trusteeZip: "",
						  trusteeCountry: "",
						  trusteeSsnTin: "",
						  trusteeEmail: "",
						  bankName: "",
						  bankAccountTitle: "",
						  bankRoutingNumber: "",
						  bankAccountNumber: "",
						  bankAccountType: "",
						  bankAccountValue: "",
						  bankAmountOfTransaction: "",
						  recurringAmount: "",
						  accountName: "",
						  initialContributionAmount: 0,
						  atNameOfDeliveringFirm: "",
						  atAccountNumber: "",
						  atAccountValue: "",
						  accountTitleRegistration: "",
						  addressDeliveringFirm: "",
						  phoneDeliveringFirm: "",
						  fundsDestiny: "",
						  titleOfTrust: "",
						  effectiveDateOfTrust: "",
						  trustTaxId: ""
						};
				spouseData = {
						spouse_firstName: '',
						spouse_lastName: '',
						spouse_dobStr:'',
						spouse_annualIncome:''
				};
				selectedPortfolio = 0;
				childrenStatus = 'no';
				childrenDetails = [];
				riskToleranceData = [];
			  
		  };
		  
		  var updateChild = function(dataObj){
			  var temp = angular.copy(dataObj);
			  childrenStatus = 'yes';
			  flag = true;
			  /*if(temp.dob){
				  temp.dob = $filter('date')(temp.dob, "MM/dd/yyyy");
			  }*/
			  childrenDetails.push(temp);
		  };
		  
		  var updateChildren = function(dataObj){
			  childrenDetails = dataObj;
		  };
		  
		  var resetFiles = function(dataObj){
			  fileData = dataObj;
		  };
		  
		  var addFile = function(dataObj){
			  fileData.push(dataObj);
		  };
		  
		  var getFiles = function(){
			  return fileData;
		  };
		  
		  var deleteChild = function(indx){
			  flag = true;
			  if (indx === 0 && childrenDetails.length === 1){
				  childrenStatus = 'no';
				}
				if(childrenDetails.length > 1) {
					childrenDetails.splice(indx,1);
				}
				return childrenDetails;
		  };
		  
		  var deleteAllChild = function (){
			  flag = true;
			  childrenStatus = 'no';
			  childrenDetails = [];
		  };
		  
		  var getChildernStatus = function (){
			  return childrenStatus;
		  };
		  
		  var getTrusteeStatus = function (){
			  return trusteeStatus;
		  };
		  
		  var setTrusteeStatus = function(value){
			  trusteeStatus = value;
		  };
		  
		  var getBenStatus = function (){
			  return benStatus;
		  };
		  
		  var setBenStatus = function(value){
			  benStatus = value;
		  };

		  var getFormData = function(){
		      return formData;
		  };

		  var setAccountType = function(value){
			  accountType = value;
		  };
		  
		  var getAccountType = function(){
			  return accountType;
		  };
		  
		  var setselectedPortfolio = function(value){
			  selectedPortfolio = value;
		  };
		  
		  var getselectedPortfolio = function(){
			  return selectedPortfolio;
		  };
		  		  
		  var getFlag= function(){
			  return flag;
		  };
		  
		  var getSpouseData = function(){
			  return spouseData;
		  };
		  
		  var getChildrenData = function(){
			  return childrenDetails;
		  };
		  
		  var saveRiskTolerance = function(data){
			  riskToleranceData = data;
		  };
		  
		  var getRiskTolerance = function(){
			  return riskToleranceData;
		  };
		  
		  return {
		    updateProperty: updateProperty,
		    resetAccountFormData:resetAccountFormData,
		    getFormData: getFormData,
		    setAccountType : setAccountType,
		    getAccountType : getAccountType,
		    updateChild    : updateChild,
		    deleteChild    : deleteChild,
		    deleteAllChild : deleteAllChild,
		    getFlag        : getFlag,
		    getChildernStatus : getChildernStatus,
		    getSpouseData  : getSpouseData,
		    getChildrenData: getChildrenData,
		    saveRiskTolerance : saveRiskTolerance,
		    getRiskTolerance : getRiskTolerance,
		    setselectedPortfolio: setselectedPortfolio,
		    getselectedPortfolio: getselectedPortfolio,
		    getTrusteeStatus: getTrusteeStatus,
		    setTrusteeStatus: setTrusteeStatus,
		    getBenStatus: getBenStatus,
		    setBenStatus: setBenStatus,
		    updateChildren: updateChildren,
		    addFile: addFile,
		    getFiles: getFiles,
		    resetFiles: resetFiles
		  };
	}]);

})();