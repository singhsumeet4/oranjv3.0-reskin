(function () {
    'use strict';

    angular.module('app.matched')
        .controller('OpenAccount', ['$scope','$rootScope','world', 'employment', 'residency', 'OranjApiService', 'accountFormDataService', '$state', 'GlobalFactory' , '$filter', '$location', '$mdDialog', 'UserProfileService', OpenAccount]);
    
    function OpenAccount ($scope,$rootScope,world, employment, residency, OranjApiService, accountFormDataService, $state, GlobalFactory, $filter, $location, $mdDialog, UserProfileService) {
        $rootScope.breadcrum="Open An Account";
	    $scope.step3 = {
	       fullname: 'John Christian Mckinley',
	       email: 'johnchristian@cogent.com',
	       phone: '650-555-1234',
	       ssn: '12345-67890',
	       annualincome: '$629,000',
	      city: 'Mountain View',
	      zipcode:'989907',
	      spousefullname:'Jennifer Taylor',
	      spouseannualincome:'$507,088',
	      childrenfullname:'Martin Magalona',
	      empworkstatus:'Contractual',
	      empoyername:'Brandon Voyd',
	      empoccupation:'Software Development',
	      emptypeofbuss:'Development Company',
	      emplegalstatus:'Temporary',
	      empcitizenship:'United States of America',
	      empcity:'Mountain View',
	      empzipcode:'989907',
	      bankname:'Wells Fargo',
	      factitle:'Nicholas Darwin',
	      accountno:'2290656935',
	      fundtransferaccountno:'2290978337',
	      routingno:'121000358' ,
	      transamount:'$209,000.00',
	       acname:'Marx Consuegra',
	       typeofaccount:'checking',
        };
     
       $scope.step3.trustee = ('Other Other1 '
       ).split(' ').map(function (trustee) {
           return { abbrev: trustee };
       });
       $scope.step3.trustSelected = false;
       $scope.step3.country = 'US',
       $scope.count = world.countries;
       $scope.options = employment.options;
       $scope.restatus = residency.status;
       $scope.benStatus = false;   
       $scope.trusteeType = "other";
       $scope.files = [];
          
       $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
       'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
       'WY').split(' ').map(function(state) {
           return {abbrev: state};
       });
       
       $scope.accounttype = [
                             {"text": "Primary", "value": "checking"},
                             {"text": "Savings", "value": "savings"}
                             ];
       $scope.recurringFrequency = [
                             {"text": "Weekly", "value": "WEEKLY"},
                             {"text": "Every other week", "value": "EVERY_OTHER_WEEK"},
                             {"text": "Monthly", "value": "MONTHLY"},
                             {"text": "Quarterly", "value": "QUARTERLY"},
                             {"text": "Annually", "value": "ANNUALY"},
                             {"text": "Every 6 months", "value": "EVERY_6_MONTH"},
                             {"text": "First friday of the month", "value": "FIRST_FRIDAY_OF_THE_MONTH"},
                             {"text": "Second friday of the month", "value": "SECOND_FRIDAY_OF_THE_MONTH"},
                             {"text": "Third friday of the month", "value": "THIRD_FRIDAY_OF_THE_MONTH"},
                             {"text": "Fourth friday of the month", "value": "FOURTH_FRIDAY_OF_THE_MONTH"},
                             {"text": "First business day of the month", "value": "FIRST_BUSINESS_DAY_OF_THE_MONTH"},
                             {"text": "Last business day of the month", "value": "LAST_BUSINESS_DAY_OF_THE_MONTH"}
                             ];
       
       $scope.BeneficiariesType =  [
						        {"text": "Primary", "value": "primary"},
						        {"text": "contingent", "value": "contingent"}
					        ];
       
       $scope.beneficiaryData = [
								  {'label_key':'parents','label_short':'Parents'},
								  {'label_key':'mother','label_short':'Mother'},
								  {'label_key':'sibling','label_short':'Sibling'},
		                          {'label_key':'grandparents','label_short':'Grandparents'},
		                          {'label_key':'grandchild','label_short':'Grandchild'},
		                          {'label_key':'parentsibling','label_short':'Parent\'s Sibling'},
		                          {'label_key':'siblingchild','label_short':'Sibling\'s Child'},
		                          {'label_key':'cousin','label_short':'Cousin'},
		                          {'label_key':'other','label_short':'Other'},
		                         ];
		             
    $scope.onChange = function (files) {
     if(files[0] === undefined){ return;}
     $scope.fileExt = files[0].name.split(".").pop();
   }
   
   $scope.isImage = function(ext) {
     if(ext) {
       return ext === "jpg" || ext === "jpeg"|| ext === "gif" || ext ==="png" || ext ==='JPG';
     }
   };
         
   $scope.firmName = OranjApiService.firmname;
      
   $scope.save = function (index) {
   	switch(index){
       	case 4:
       		if($scope.accType){
       			accountFormDataService.setAccountType($scope.accType);
       			$state.go('page.openaccountstep5');
       		}else{
       			GlobalFactory.showErrorAlert('Please select an account type.');
       		}
       		break;
       	case 5:
       		
       		angular.forEach($scope.account_form.$error.required, function(field) {
    		    field.$setTouched();
    		});
       		if($scope.account_form.$valid) { 
       			accountFormDataService.updateProperty($scope.clientDetails,1);
       			if($scope.clientDetails.maritalStatus === 'married'){
       				accountFormDataService.updateProperty($scope.spouse,2);
       			}
       			
       			if ($scope.children !== null && $scope.children.length > 0) {
		               	   
		            accountFormDataService.updateChildren($scope.children);
       			}
       			
       			$state.go('page.openaccountstep6');
       		}else{
       			return;
       		}
       		//$state.go('page.openaccountstep6');
       		break;
       	case 6:
       		angular.forEach($scope.bank_form.$error.required, function(field) {
    		    field.$setTouched();
    		});
       		if($scope.bank_form.$valid) { 
       			accountFormDataService.updateProperty($scope.clientDetails,1);
       			$state.go('page.openaccountstep7');
       		}else{
       			return;
       		}
       		/*if (typeof $rootScope.dropzone !="undefined")
    			$rootScope.dropzone.processQueue();*/
       		//$state.go('page.openaccountstep7');
       		break;
   	}
   };
   
   $scope.resetAccountForm = function(){
	   var confirmation = $mdDialog.confirm({
           htmlContent: '<div>Are you sure you want to cancel this process? All data submitted will not be saved',
           ok: 'YES',
           cancel : 'NO'
       });
     
       $mdDialog.show(confirmation).then(function(answer) {
       		accountFormDataService.resetAccountFormData();
       		if($rootScope.prevPath != null && $rootScope.prevPath != undefined) {
       			$location.path($rootScope.prevPath);
       		}else {
       			$state.go('page.client.home.dash');
       		}
       });
   };
   
   $scope.getAccountType = function (){
	   $scope.accType = accountFormDataService.getAccountType();
   };
   
   $scope.getAccountInfo = function () {
	   var flag = accountFormDataService.getFlag();
	   if(flag){
		   $scope.clientDetails = accountFormDataService.getFormData();
		   if($scope.clientDetails.recurringAmmount !== "") {
			   $scope.switchData.rt = true;
		   }
	   }else {
		   $scope.clientDetails = {};
	   }
   };
   
   $scope.getProfileInfo = function () {

	   var flag = accountFormDataService.getFlag();
	   $scope.accType = accountFormDataService.getAccountType();
	   $scope.childrenStatus = accountFormDataService.getChildernStatus();
	   $scope.switchData.trustee = accountFormDataService.getTrusteeStatus();
	   $scope.switchData.beneficiary = accountFormDataService.getBenStatus();
	   if($scope.accType == "personalTrust" || $scope.accType == "qualifiedRetirementPlan") {
		   $scope.trustSelected = true;
	   }
	   if(flag){
		   $scope.clientDetails = accountFormDataService.getFormData();
		   /*if($scope.clientDetails.dobStr){
			   $scope.clientDetails.dobStr = new Date($scope.clientDetails.dobStr);
			     $scope.clientDetails.dobStr = $filter('date')($scope.clientDetails.dobStr, "MM/dd/yyyy");
		   }*/
		   if($scope.clientDetails.maritalStatus === 'married'){
			   $scope.spouse ={};
			   $scope.spouse = accountFormDataService.getSpouseData();
			   /*var spouse = accountFormDataService.getSpouseData();
			   angular.forEach(spouse, function(val,key){
				   if(key === 'spouse_dobStr'){
					   $scope.spouse[key] = new Date(val);
				   }else{
					   $scope.spouse[key] = val;
				   }
					  
			   });*/
		   }
		   if($scope.childrenStatus === 'yes'){
			   $scope.children = [];
			   $scope.children = accountFormDataService.getChildrenData();
			   /*var children = accountFormDataService.getChildrenData();
			   angular.forEach(children, function(item,index){
				   $scope.children[index] = {};
				   angular.forEach(item, function(val,key){
					   if(key === 'dob'){
						   $scope.children[index][key] = new Date(val);
					   }else{
						   $scope.children[index][key] = val;
					   }
						  
				   });
			   });*/
		   }
	   }else{
		   OranjApiService('getUserProfileData',null,{'userid':''}).then(function (response) {
	    	   $scope.clientDetails = {};
	    	   if(response.status === 'success'){
	    		   var profile = response.data.profile;
	    		   $scope.clientDetails.firstName = profile.firstName;
	    		   $scope.clientDetails.middleName = profile.middleName;
	    		   $scope.clientDetails.lastName = profile.lastName;
	    		   $scope.clientDetails.annualIncome = profile.income;
	    		   $scope.clientDetails.phone = profile.phone;
	    		   $scope.clientDetails.dobStr = $filter('date')(profile.dob, "MM/dd/yyyy");
	    		   //$scope.clientDetails.dobStr = new Date($scope.clientDetails.dobStr);
	    		   $scope.clientDetails.email = profile.user.email;
	    		   $scope.clientDetails.maritalStatus = profile.maritalStatus;
	    		   $scope.clientDetails.employmentStatus = "EMPLOYED";
	    		   $scope.clientDetails.citzenship = "us-citizen";
	    		   $scope.clientDetails.country = "United States";
	    		   $scope.hasVisa = "no";
	    		   
	    		   accountFormDataService.updateProperty($scope.clientDetails,1);
	    		   
	    		   if (profile.spouse !== null) {
	    			   $scope.spouse = {};
	    			   $scope.spouse.spouse_firstName = profile.spouse.name;
	    			   $scope.spouse.spouse_middleName = profile.spouse.middle;
	    			   $scope.spouse.spouse_lastName = profile.spouse.last;
		               $scope.spouse.spouse_dobStr = $filter('date')(profile.spouse.dob, "MM/dd/yyyy");
		               //$scope.spouse.spouse_dobStr = new Date($scope.spouse.spouse_dobStr);
		               $scope.spouse.spouse_annualIncome = profile.spouse.income;
		               accountFormDataService.updateProperty($scope.spouse,2);
		           }
	    		   
	    		   if (profile.children !== null && profile.children.length > 0) {
		               $scope.childrenStatus = 'yes';
		               $scope.children = [];
		               for (var i = 0; i < profile.children.length; i++) {
		               	   $scope.children[i] = {};
		            	   $scope.children[i].firstName = profile.children[i].name;
		               	   $scope.children[i].middleName = profile.children[i].middle;
		               	   $scope.children[i].lastName = profile.children[i].last;
		               	   $scope.children[i].dob = $filter('date')(profile.children[i].dob, "MM/dd/yyyy");
		                   //$scope.children[i].dob = new Date(childDob);
		                   accountFormDataService.updateChild($scope.children[i]);
		               }
		           }
	    	   }
	       }, function (response) {
	    	   console.log(response);
	       });
	   }
      

   };
   
   $scope.spouseToggle = function (){
	   if($scope.clientDetails.maritalStatus === 'single'){
	       	if($scope.spouse){
	       		delete $scope.spouse;
	       	}
	       	
	  }else{
		  $scope.spouse = {
				  spouse_firstName: "",
				  spouse_middleName: "",
				  spouse_lastName: "",
				  spouse_dobStr: "",
				  spouse_annualIncome: ""
		       	};
       }
   };
   
   $scope.childToggle = function (){
	   
	   	if($scope.childrenStatus == 'yes'){
       		$scope.children = [{
       			firstName: "",
       			middleName: "",
       			lastName: "",
       			dob: ""
	        	}];
       	}else{
       		$scope.children = [];
       	}
   };
   
   $scope.bendToggle = function (){
	  	if($scope.switchData.beneficiary == true){
      		$scope.clientDetails.userBeneficiaryAccounts = [{
      			Beneficiary: "other",
      			type: "",
      			firstName: "",
      			middleName: "",
      			lastName: "",
      			ssnTin: "",
      			dob: "",
      			relationship: "",
      			phone: "",
      			phone: "",
      			City: "",
      			State: "",
      			zip: "",
      			country: "",
      			benAccPer:100
	        	}];
      		
      		if($scope.clientDetails.maritalStatus === 'married'){
    			$scope.beneficiaryData.push({'label_key':'spouse','label_short':'Spouse'});
    		}
    		
    		if($scope.childrenStatus === 'yes'){
    			$scope.beneficiaryData.push({'label_key':'child','label_short':'Child'});
    		}
    		
      	}else{
      		$scope.clientDetails.userBeneficiaryAccounts = [];
      	}
	  	accountFormDataService.setBenStatus($scope.switchData.beneficiary);
  };
  
   
   $scope.$watchCollection('[switchData.trustee, switchData.beneficiary]', function (n, o) {
	   
	   $scope.trustees = [];
	   if(n[0] === true || n[1] === true){
		   if($scope.spouse!=undefined) {
			   $scope.trustees.push({
					key:'spouse', value:$scope.spouse.spouse_firstName
				});
			   $scope.benStatus = true;
		   }
		   if($scope.children!=undefined && $scope.children.length > 0) {
			   angular.forEach($scope.children, function(item,index){
				   var childkey = 'child_'+index;
				   $scope.trustees.push({
					   key:childkey, value:item.firstName
					});
			   });
			   $scope.benStatus = true;
		   }
		   if($scope.trustees.length > 0) {
			   $scope.trustees.push({
					key:'other', value:'Other'
				});
		   }else {
			   $scope.benStatus = false;
		   }
		   
	   }
   });
   
   $scope.$watch(' switchData.rt', function (n, o) {
	   	if(n === true && n !== o){
	   		if($scope.clientDetails.recurringTransferFrequency === undefined || $scope.clientDetails.recurringTransferFrequency === "") {
	   			$scope.clientDetails.recurringTransferFrequency = "WEEKLY";
	   		}
	   	}else {
	   		$scope.clientDetails.recurringTransferFrequency = "";
	   		$scope.clientDetails.recurringAmmount = "";
	   	}
   });
      
   $scope.trusteeToggle = function (){
	   accountFormDataService.setTrusteeStatus($scope.switchData.trustee);
   };
   
   $scope.loadTrustee = function (){
	   if($scope.trusteeType == 'spouse') {
		   var tempTrustee = angular.copy($scope.spouse);
		   $scope.clientDetails.trusteefirstName = tempTrustee.spouse_firstName;
		   $scope.clientDetails.trusteemiddleName = tempTrustee.spouse_middleName;
		   $scope.clientDetails.trusteelastName = tempTrustee.spouse_lastName;
	   }else if($scope.trusteeType == 'other') {
		   $scope.clientDetails.trusteefirstName = "";
		   $scope.clientDetails.trusteemiddleName = "";
		   $scope.clientDetails.trusteelastName = "";
	   }else{
		   var keyArray = $scope.trusteeType.split('_');
		   var tempTrustee = angular.copy($scope.children[keyArray[1]]);
		   $scope.clientDetails.trusteefirstName = tempTrustee.firstName;
		   $scope.clientDetails.trusteemiddleName = tempTrustee.middleName;
		   $scope.clientDetails.trusteelastName = tempTrustee.lastName;
	   }
   };
   
   $scope.loadBen = function ($index){
	   if($scope.clientDetails.userBeneficiaryAccounts[$index].Beneficiary == 'spouse') {
		   var tempTrustee = angular.copy($scope.spouse);
		   $scope.clientDetails.userBeneficiaryAccounts[$index].firstName = tempTrustee.spouse_firstName;
		   $scope.clientDetails.userBeneficiaryAccounts[$index].middleName = tempTrustee.spouse_middleName;
		   $scope.clientDetails.userBeneficiaryAccounts[$index].lastName = tempTrustee.spouse_lastName;
		   $scope.clientDetails.userBeneficiaryAccounts[$index].dob = $filter('date')(tempTrustee.spouse_dobStr, "MM/dd/yyyy");
	   }else if($scope.clientDetails.userBeneficiaryAccounts[$index].Beneficiary == 'other') {
		   $scope.clientDetails.userBeneficiaryAccounts[$index].firstName = "";
		   $scope.clientDetails.userBeneficiaryAccounts[$index].middleName = "";
		   $scope.clientDetails.userBeneficiaryAccounts[$index].lastName = "";
		   $scope.clientDetails.userBeneficiaryAccounts[$index].dob = "";
	   }else{
		   var keyArray = $scope.clientDetails.userBeneficiaryAccounts[$index].Beneficiary.split('_');
		   var tempTrustee = angular.copy($scope.children[keyArray[1]]);
		   $scope.clientDetails.userBeneficiaryAccounts[$index].firstName = tempTrustee.firstName;
		   $scope.clientDetails.userBeneficiaryAccounts[$index].middleName = tempTrustee.middleName;
		   $scope.clientDetails.userBeneficiaryAccounts[$index].lastName = tempTrustee.lastName;
		   $scope.clientDetails.userBeneficiaryAccounts[$index].dob = $filter('date')(tempTrustee.dob, "MM/dd/yyyy");
	   }
   };
   
   $scope.removeChild = function ($index){
	   $scope.children.splice($index,1);
	   if($scope.children.length === 0 ){
	   		$scope.childrenStatus = 'no';
	   }
   };
   
   $scope.removeBen = function ($index){
	   $scope.clientDetails.userBeneficiaryAccounts.splice($index,1);
	   if($scope.clientDetails.userBeneficiaryAccounts.length === 0 ){
	   		$scope.switchData.beneficiary = false;
	   		accountFormDataService.setBenStatus(false);
	   }
   };
   
   $scope.addBen = function (){
	   
	    $scope.benFields = ["Beneficiary_", "BenFirstName_", "BenMiddleName_", "BenLastName_", "BenSsnTin_", "BenDob_", "BenPhone_", "BenCity_", "BenState_", "BenZip_", "BenCountry_", "benAccPer_"];
	    $scope.errFlag = false;
	    if($scope.clientDetails.userBeneficiaryAccounts.length > 0){
	   		var index = $scope.clientDetails.userBeneficiaryAccounts.length-1;
	   		angular.forEach( $scope.benFields, function(item,key){
				   if($scope.account_form[item+index].$invalid) {
					   $scope.errFlag = true;
				   }
				   $scope.account_form[item+index].$setTouched();
			 });
	   		if($scope.errFlag === true){
	       		return;
	   		}
  		}
		$scope.clientDetails.userBeneficiaryAccounts.push({
			Beneficiary: "other",
  			type: "",
  			firstName: "",
  			middleName: "",
  			lastName: "",
  			ssnTin: "",
  			dob: "",
  			relationship: "",
  			phone: "",
  			phone: "",
  			City: "",
  			State: "",
  			zip: "",
  			country: "",
  			benAccPer:0
		});
	  
  };

  $scope.checkBenAccPer = function ($index){
	  if($index != 0) {
		  var benAccTotal = 0;
		  for(var i=0; i<=$index; i++) {
			  
			  benAccTotal = parseInt(benAccTotal) + parseInt($scope.clientDetails.userBeneficiaryAccounts[i].benAccPer);
		  }
	  }
	  if(benAccTotal > 100) {
		  $scope.account_form['benAccPer_'+$index].$setValidity('benfec', false);
	  }
  }
     
   $scope.addChild = function (){
	   	if($scope.children.length >0){
	   		var index = $scope.children.length-1;
	   		if($scope.account_form['cFirstName_'+index].$invalid || $scope.account_form['cMiddleName_'+index].$invalid || $scope.account_form['cLastName_'+index].$invalid || $scope.account_form['cDob_'+index].$invalid){
	   			$scope.account_form['cFirstName_'+index].$setTouched();
	   			$scope.account_form['cMiddleName_'+index].$setTouched();
	   			$scope.account_form['cLastName_'+index].$setTouched();
	       		$scope.account_form['cDob_'+index].$setTouched();
	       		return;
	   		}
   		}
		$scope.children.push({
			firstName: "",
			middleName: "",
			lastName: "",
			dob: ""
		});
		
   };





      // file upload start
        $scope.deleteImage = function (index) {
            $scope.imgArray.splice(index, 1);
            $scope.files.splice(index, 1);
        }

        $scope.files = [];

        $scope.displayFile = function (files, file, newFiles, duplicateFiles, invalidFiles, event) {
		 if(newFiles)
            newFiles.forEach(function (element) {
                $scope.imgArray = [];
                var prop = {};
                prop.blobUrl = window.URL.createObjectURL(element);
                prop.name = element.name;
                var type = element.type.split('/')[0];
                prop.type = type;
                prop.size = element.size;

                $scope.imgArray.push(angular.copy(prop));
            }, this);
        }

        // file upload end
          
              
 }
    
})();