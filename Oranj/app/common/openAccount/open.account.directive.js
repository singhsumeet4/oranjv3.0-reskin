(function() {
   
    angular.module('app.matched')
    .directive('numericOnly', function(){
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {

                modelCtrl.$parsers.push(function (inputValue) {
                    var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

                    if (transformedInput!=inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    });
    
    angular.module('app.matched').directive('ngMin', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elem, attr, ctrl) {
                scope.$watch(attr.ngMin, function(){
                    if (ctrl.$isDirty) ctrl.$setViewValue(ctrl.$viewValue);
                });

                var isEmpty = function (value) {
                   return angular.isUndefined(value) || value === "" || value === null;
                }

                var minValidator = function(value) {
                  var min = scope.$eval(attr.ngMin) || 1;
                  if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('ngMin', false);
                    return undefined;
                  } else {
                    ctrl.$setValidity('ngMin', true);
                    return value;
                  }
                };

                ctrl.$parsers.push(minValidator);
                ctrl.$formatters.push(minValidator);
            }
        };
    });
    angular.module('app.matched').directive('accDropZone', function (GlobalFactory, OranjApiService, $rootScope, $cookies, accountFormDataService) {
        return function (scope, element, attrs) {

        	var apiUrl = OranjApiService.host + '/' + OranjApiService.firmname + '/vault/file-upload';
        
            var INVALID_VALID_FILE_EXTNS = ['bin', 'exe'];
            element.dropzone({ 
            	url: apiUrl,
            	maxFilesize: 20,
            	paramName: "fileMult",
            	autoProcessQueue: false,
            	acceptedFiles: ".gif, .jpeg,.bmp,.jpg,.gif, .png, .jpe,.psd,.pdf,.doc,.docx, .xls,.xlsx,.txt,.csv,.ppt,.pptx,.rtf,.mp3,.wav,.3gp,.flv,.mp4,.mpg,.avi,", 
    	        addRemoveLinks: true,
    	        parallelUploads: 20,
    	        headers: {
                    "Authorization": 'Bearer ' +  $cookies.getObject('auth').access_token
                },
            	init: function() {
            		// set reference to rootScope
            		var drop = this;       		
            		
              		drop.on('success', function(file, json) {
              			
              		});
              		
              		var existingFiles = accountFormDataService.getFiles();
              		
              		if(existingFiles.length > 0) {
              			scope.files = existingFiles;
              			/*for (i = 0; i < existingFiles.length; i++) {
              				drop.emit("addedfile", existingFiles[i]);
              				drop.emit("thumbnail", existingFiles[i]);
              				drop.emit("complete", existingFiles[i]);  
                        }*/
              			drop.handleFiles(existingFiles);
              		}
              		
              		$rootScope.dropzone = drop;
              		
              		drop.on('addedfile', function(file) {
                		scope.$apply(function(){
                			accountFormDataService.addFile(file);
                  			scope.files.push({file});
                		});
                		
                	
              		});
              		
              		drop.on('removedfile', function(file) {
              			var queFiles = drop.getQueuedFiles();
                		scope.$apply(function(){
                			accountFormDataService.resetFiles(queFiles);
                  			//scope.files.push({file});
                		});
                		
                		
              		});
              		
              
              		drop.on('drop', function(file) {
                		
              		});

              		drop.on('success', function(file, json) {
              			
              		});
              	
              		drop.on("queuecomplete",function(){
                 		//window.location.href ='../../client_dash_home.html';
          			});
              		
              		drop.on("processing", function(file) {
              			
              		});
              		
              		$("#file-upload-cancel").click(function(){
                		drop.removeAllFiles(true);
          			});

    		        $("#browse-file").click(function(){
    		          	$('.dropzone').click();
    		        });
            	}
            
          	});

        }
    });
})();