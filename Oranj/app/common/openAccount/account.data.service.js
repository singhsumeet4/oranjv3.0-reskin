(function () {
	angular.module('app.matched')
		.factory('accountOpeningService', ['OranjApiService', '$q', 'accountDataTransformFactory', accountOpeningService]);
		
	function accountOpeningService(OranjApiService, $q, accountDataTransformFactory) {
		var defer = $q.defer();
		return{
			fetchMatchedPortfolio: function (postData) {
                OranjApiService('getMatchedPortfolio', {
                    data: postData
                }).then(function (res) {
                        var data = res.data;
                        var portfolioChartData = accountDataTransformFactory.transformMatchedPortfolioData(data);

                        var matchPortfolioObj = {
                        	portfolioChartData: portfolioChartData,
                            status: res.status,
                        };
                        defer.resolve(matchPortfolioObj);
                    },
                    function () {
                    	var matchPortfolioObj = {
                                status: 'error'
                            };
                    	defer.resolve(matchPortfolioObj);
                    });

                return defer.promise;
            },
            getMatchedPortfolio: function () {
                return defer.promise;
            }
		};
	}
})();