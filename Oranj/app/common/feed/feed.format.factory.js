(function () {


    angular.module('app.feed')
        .factory('feedFormatFactory', [function () {
        	return {
                feedCombine: function (feeds, paginated) {
                	//angular.extend(feeds, paginated);
                	angular.forEach(paginated, function(value, key) {
                		angular.forEach(value, function(val, k) {
	                		if(!feeds[key]){
	                			feeds[key]=[];
	                		}
	                		feeds[key].push(val);
                		});
                	});
                    return feeds;
                }
            }
        }]);
})();
