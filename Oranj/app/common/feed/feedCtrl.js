(function () {



    angular.module("app.feed")


        .controller("feedController", ["$scope", 'configfactory', 'GlobalFactory', '$http', '$rootScope', '$filter', 'OranjApiService', '$timeout','$stateParams', 'feedFormatFactory', '$state','advisorDataService', feedController ])

// feed search and filter starts

    function feedController($scope, configfactory, GlobalFactory, $http, $rootScope, $filter, OranjApiService, $timeout,$stateParams, feedFormatFactory, $state,advisorDataService) {
    	$scope.filterFlag = 0;
    	$scope.TotalContact = 0;
		$scope.notFlag=0;
		$scope.busy = false;
		$rootScope.isAdvisor=false;
		$rootScope.clientName = ($stateParams.name != undefined) ? $stateParams.name : 'All';
		$rootScope.selectedclientid = $stateParams.userid;
    	$scope.checkBoxs =
			[{key:1, value:"All"},
			{key: 6, value:"Opened new account" },
			{key: 3, value:'Added/edited goal' },
			{ key:2, value:'Added/edited financial' },
			{ key:7, value:'Message' },
			{ key:5, value:'Updated profile' },
			{ key:4, value:'Uploaded document to the vault' }
            ]
            $scope.items = [];
		 	$scope.filterNumber = [1];
            $scope.flag = false;
            $scope.removeItem = function (item) {
                if (item.value === "All") {
                    $scope.flag = true;
                    $scope.items = [];
                    var checkBoxs = $scope.checkBoxs;
                    angular.forEach(checkBoxs, function (checkBox, index) {
                        checkBox.key = false;
                    });
                }
                else {
                    var index = $scope.items.indexOf(item);
                    $scope.items.splice(index, 1);
                    item.key = false;
                }

                if($scope.flag) {
                    //$scope.items[0].key = false;
                    $scope.items.splice(0,1);
                    $scope.flag= false;
                }
                $scope.filterSelection();
            }

          $scope.check = function (Item) {
          if (Item.value === "All") {
               $scope.flag= true;
              $scope.items = [];
              var checkBoxs = $scope.checkBoxs;
              if (Item.key === true) {
                  angular.forEach(checkBoxs, function (checkBox, index) {
                      $scope.items.push(checkBox);
                      checkBox.key = true;

                  });
              }
              else {
                  angular.forEach(checkBoxs, function (checkBox, index) {
                      $scope.items.push(checkBox);
                      $scope.items = [];
                      checkBox.key = false;
                  });
              }

          }
          else {
              if (Item.key) {
                  $scope.items.push(Item);
              }
              else {
                      if($scope.flag)
                      {
                      $scope.items[0].key = false;
                      $scope.items.splice(0,1);
                       $scope.flag= false;
                      }
                     var index = $scope.items.indexOf(Item);
                     $scope.items.splice(index, 1);
              }
          }
          $scope.filterSelection();
      }

        $scope.filterSelection = function () {
        	var checkBoxs = $scope.checkBoxs;
        	$scope.filterNumber = [];
            angular.forEach(checkBoxs, function (checkBox, index) {
                if(checkBox.key === true) {
                	if(checkBox.value == "Opened new account")
                		$scope.filterNumber.push(6);
                	else if(checkBox.value == "Added/edited goal")
                		$scope.filterNumber.push(3);
                	else if(checkBox.value == "Added/edited financial")
                		$scope.filterNumber.push(2);
                	else if(checkBox.value == "Uploaded document to the vault")
                		$scope.filterNumber.push(4);
                	else if(checkBox.value == "Updated profile")
                		$scope.filterNumber.push(5);
                	else if(checkBox.value == "Message")
                		$scope.filterNumber.push(7);
                }
            });
            if($scope.filterNumber.length == 0 || $scope.filterNumber.length == 5)
            	$scope.filterNumber = [1];
            $scope.filterFlag = 1;
            $scope.page = 0;
      	   	$scope.pageSize= 20;
      	   	$scope.busy = false;
      	   	$scope.hideloader=false;
            $scope.getfeedActivity();
        }
	  // feed search and filter ends
    	var role = GlobalFactory.getRole();

  	   	role.then(
  	            function(result){
  	            	if(result.data) {
  	     	 		  if (GlobalFactory.lookupRole( 'ROLE_USER', result.data )) {

  	     			  }else if (GlobalFactory.lookupRole( 'ROLE_PROSPECT', result.data ) || GlobalFactory.lookupRole( 'ROLE_CLIENT', result.data )) {

  	     			  }else if (GlobalFactory.lookupRole( 'ROLE_PROSPECT', result.data ) || GlobalFactory.lookupRole( 'ROLE_ADVISOR', result.data )) {
  	     				  $rootScope.isAdvisor=true;
		  	     	        $scope.isLoading = true;
		  	     	        $scope.pageSize= 20;
		  	     	        $scope.busy = true;
		  	     	        advisorDataService.fetchAllClientList().then(function (clientListObject) {
		  	     	            $scope.isLoading = false;
		  	     	            var data = clientListObject;
		  	     	            var contactCount = {'clientCount': 0, 'prospectsCount': 0};
		  	     	            if (angular.isArray(data)) {

		  	     	                data.sort(function (a, b) {
		  	     	                    if (a.lastName.toLowerCase() < b.lastName.toLowerCase()) {
		  	     	                        return -1;
		  	     	                    }
		  	     	                    if (a.lastName.toLowerCase() > b.lastName.toLowerCase()) {
		  	     	                        return 1;
		  	     	                    }
		  	     	                    return 0;
		  	     	                });

		  	     	                var tmp = {};
		  	     	                for (var i = 0; i < data.length; i++) {
		  	     	                    var letter = data[i].lastName.toUpperCase().charAt(0);
		  	     	                    if (tmp[letter] === undefined) {
		  	     	                        tmp[letter] = [];
		  	     	                    }
		  	     	                    tmp[letter].push(data[i]);
		  	     	                    if (angular.isArray(data[i].roles) && data[i].roles.length > 0) {
		  	     	                        if (data[i].roles[0].name === 'ROLE_CLIENT') {
		  	     	                            contactCount.clientCount = contactCount.clientCount + 1;
		  	     	                        }
		  	     	                        else if (data[i].roles[0].name === 'ROLE_PROSPECT') {
		  	     	                            contactCount.prospectsCount = contactCount.prospectsCount + 1;
		  	     	                        }
		  	     	                    }
		  	     	                }
		  	     	                clientListObject.sort(function (a, b) {
		  	     	                    if (a.firstName.toLowerCase() < b.firstName.toLowerCase()) {
		  	     	                        return -1;
		  	     	                    }
		  	     	                    if (a.firstName.toLowerCase() > b.firstName.toLowerCase()) {
		  	     	                        return 1;
		  	     	                    }
		  	     	                    return 0;
		  	     	                });
		  	     	                $scope.busy = false;

		  	     	                $scope.TotalContact = contactCount.clientCount + contactCount.prospectsCount;
		  	     	                $rootScope.$broadcast("clientContactsList", {
		  	     	                    contactsList: tmp,
		  	     	                    contactCount: contactCount,
		  	     	                    allContactsList: clientListObject
		  	     	                });
		  	     	            }
		  	     	        }, function (error) {
		  	     	            $scope.isLoading = false;
		  	     	            GlobalFactory.showErrorAlert(error.message || 'An error occurred');

		  	     	        });
  	     			  }else  {
  	     				  GlobalFactory.signout();
  	     			 }
  	     	 	   } else {
  	     	 		  GlobalFactory.signout();
  	     	 	   }
  	            }, function(){
  	            	GlobalFactory.signout();
  	            }
  	   	);

  	   	$scope.page = 0;
  	   	$scope.pageSize= 20;
  	   	$scope.feeds = {};
  	   	$scope.noFeed = true;
  	   	$scope.hideloader=false;
  	   	$scope.searchText = "";
  	   	$scope.getfeedActivity = function(){
  	   		if ($scope.busy === true) return;
  	   		$scope.busy = true;
  	   		var filter = $scope.filterNumber.join();
  	   		if($scope.searchText != "") {
  	   			var params = {
  	   				'userid'  : ($stateParams.userid != undefined) ? $stateParams.userid : '' ,
  	                'pageNum' : $scope.page,
  	                'pageSize': $scope.pageSize,
  	                'filter'  :filter,
  	                'search'  :$scope.searchText
  	            };
  	   		}
  	   		else {
  	   			var params = {
  	   				'userid'  : ($stateParams.userid != undefined) ? $stateParams.userid : '',
  	   				'pageNum' : $scope.page,
	                'pageSize': $scope.pageSize,
	                'filter'  :filter,
	                'search'  : ""
	            };
  	   		}
			OranjApiService('getfeedActivity', null, params
            ).then(function(response){
            	if(response.data && response.data.messages != null) {
            		var items = response.data.messages;
		          	if($scope.filterFlag == 1) {
		          		$scope.feeds = response.data.messages;
		          		$scope.filterFlag = 0;
		          		$scope.noFeed = Object.keys($scope.feeds).length > 0 ? true : false;
		          	}
		          	else {
		          		//angular.extend($scope.feeds, response.data.messages);
		          		$scope.feeds = feedFormatFactory.feedCombine($scope.feeds, response.data.messages);
		          		$scope.noFeed = Object.keys($scope.feeds).length > 0 ? true : false;

		          	}
		          	$scope.notFlag=1;
		          	$scope.page = $scope.page + 1;
            	}
            	else if($scope.page == 0) {
            		$scope.feeds = {};
            		$scope.page = 0;
              	   	$scope.pageSize= 20;
              	   	$scope.noFeed = false;
             		$scope.notFlag = 0;
            		$scope.hideloader=true;
            		$scope.busy = true;
            	}else {
            		$scope.notFlag = 0;
            		$scope.hideloader=true;
            		$scope.busy = true;
            	}
	          	if($scope.notFlag == 1) {
		          	$timeout(function () {
		          		$scope.busy = false;
		            }, 500);
				}
	           },function(response){

	         });
		};

		$scope.searchFeed = function() {
			$scope.busy = false;
			$scope.filterSelection();
		}

		$scope.formatDate = function(date){
	          var dateOut = new Date(date);
	          return dateOut;
	    };

	    $scope.getAvatar = function(avatarImg){
	    	var avatar = '../../../images/avatar.jpg';
            if (avatarImg != null && avatarImg != "") {
                var avatar = avatarImg;
            }
            return avatar;
	    };

		//$scope.getfeedActivity();
    }




})();
