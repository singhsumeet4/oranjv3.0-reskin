(function () {



    angular.module("app.feed")


        .controller("notificationController", ["$scope", 'configfactory', 'GlobalFactory', '$http', '$rootScope', '$filter', 'OranjApiService','appConfig', '$timeout', 'feedFormatFactory', notificationController])
	
	function notificationController($scope, configfactory, GlobalFactory, $http, $rootScope, $filter, OranjApiService,appConfig, $timeout, feedFormatFactory) {
    	/*Check if value present in the array and set the value for class_name*/ 
    	function IsInArray(array, name) {
    		$scope.class_name = "";	
    		for (var i = 0; i < array.length; i++) {
    			if (array[i].name == name) {
    				$scope.class_name = array[i].val;
    				return true;
    			}
    		}
    		return false;
    	}
    	/*End unction*/    	
    	var role = GlobalFactory.getRole();

  	   	role.then(
  	            function(result){
  	            	if(result.data) {
  	     	 		  if (GlobalFactory.lookupRole( 'ROLE_USER', result.data )) {
  	     	  		 	
  	     			  }else if (GlobalFactory.lookupRole( 'ROLE_PROSPECT', result.data ) || GlobalFactory.lookupRole( 'ROLE_CLIENT', result.data )) {  

  	     			  }else if (GlobalFactory.lookupRole( 'ROLE_PROSPECT', result.data ) || GlobalFactory.lookupRole( 'ROLE_ADVISOR', result.data )) {
  	     				  
  	     			  }else  {
  	     				  GlobalFactory.signout();
  	     			 }	
  	     	 	   } else {
  	     	 		  GlobalFactory.signout();
  	     	 	   }
  	            }, function(){
  	            	GlobalFactory.signout();
  	            }
  	   	); 	   	
  	   	$scope.busy = false;
	   	$scope.page = 0;
	   	$scope.pageSize= 20;
	   	$scope.notifications = {};
	   	$scope.noNotification = true;
	   	
  	   	$scope.getNotifications = function(){
  	   		if ($scope.busy === true) return;
  	   		$scope.busy = true;
  	   		$scope.hideloader=false;
  	   		var notFlag = 0;
  	   		var params = {
                'pageNum' : $scope.page,
                'pageSize': $scope.pageSize
            };
			OranjApiService('getNotifications', null, params).then(function(response){					
				if(response.data && (Object.keys(response.data).length > 0)) {
					angular.forEach(response.data, function(value, key){
						for(i=0;i<value.length;i++){
							if(IsInArray(appConfig.notification_icon, (value[i].type).toLowerCase())){						
								response.data[key][i].class_name=$scope.class_name;
							}
						}
					})					
				  	//angular.extend($scope.notifications, response.data);
		          	$scope.notifications = feedFormatFactory.feedCombine($scope.notifications, response.data);
		          	$scope.page = $scope.page + 1;
		          	$scope.noNotification = Object.keys($scope.notifications).length > 0 ? true : false;
		          	notFlag = 1;
            	}
            	else if($scope.page == 0) {
            		$scope.notifications = {};
            		$scope.page = 0;
              	   	$scope.pageSize= 20;
              	   	$scope.noNotification = false;
              	   	$scope.hideloader=true;	              	   	
              	   	notFlag = 0;              	  
            	}
            	else {
            		$scope.hideloader=true;	          		
            		notFlag = 0;
            	}
	          	if(notFlag == 1) {
		          	$timeout(function () {
		          		$scope.busy = false;
		            }, 500);
				}
	           },function(response){
	          	 
	         });
		};
		$scope.getNotifications();				
    }


    

})();