function ValueToCurrency(value){return  ' $' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}

function findSearch(arr, key, val) { // Find array element which has a key value of val 
    for (var ai, i = arr.length; i--;)
        if ((ai = arr[i]) && ai[key].toLowerCase() == val.toLowerCase())
            return ai;
    return null;
}

function getMax(arr, prop) {
    var max;
    for (var i = 0 ; i < arr.length ; i++) {
        if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
            max = arr[i];
    }
    return max;
}

// function getTotal(arr, prop) {
//     var total = 0;
//     for (var i = 0; i < arr.length; i++) {       
//         total += parseInt(arr[i][prop]);
//     }
//     return total;
// }

angular.isUndefinedOrNull = function(val) {
    return angular.isUndefined(val) || val === null || val === "";
};

Date.prototype.toDateOnlyFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate());
};

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
};

function getTotal(arr, prop) {
     var count=0;
    var total = arr.length;

    for (var i = 0; i < arr.length; i++) { 
        if(parseInt(arr[i][prop]) > 0)  
           count=count+1;
    }
    return count +" / "+ total;
};

