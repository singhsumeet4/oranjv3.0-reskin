(function () {
    angular.module("app.advisorSidebar",[])
        .controller('advisorSideBarController', ['$scope', '$state', 'GlobalFactory','$rootScope','UserProfileService','advisorUserVaultDataService', advisorSideBarController])

        
        function advisorSideBarController($scope, $state, GlobalFactory, $rootScope, UserProfileService,advisorUserVaultDataService) {
            $scope.global = GlobalFactory;
            $scope.chatContactList = [];
            $scope.conversationCount = 0;
            $scope.contactsList = [];
            $scope.allContacts = true;
      	
        $scope.navigate = function(listType,listValue)
        {
            console.log("listValue", listValue, listType);
            
            var temp = $scope[listType];
            $scope.clientList = false;
            $scope.prospect = false;            
            $scope[listType] = temp ? false : true;            
        }
        // for toogle clients

        $scope.toggle = function () {
            $scope.toggleStart = !$scope.toggleStart;
        }
        $scope.toggleClientFun = function () {
            $scope.toggleClient = !$scope.toggleClient;
        }
        $scope.toggleProstpectFun = function () {
            $scope.toggleProstpect = !$scope.toggleProstpect;
        }
        $scope.toggleRecentFun = function (recent) {
            $scope.toggleRecent = !$scope.toggleRecent;
            // return recent ?'fa fa-sort-desc':'fa fa-sort-asc';
        }

        $scope.toggleAllContactFun = function () {
            $scope.toggleAllContact = !$scope.toggleAllContact;
        }
        
        $scope.$on('clientListChat', function (event, chatContactList) {           
           $scope.chatContactList = chatContactList;
           $scope.conversationCount = chatContactList.recent.length + chatContactList.clients.length + chatContactList.prospects.length;

        })

        $scope.$on('clientContactsList', function (event, val) {    
            $scope.contactsList = val.contactsList;
            $scope.allContactsList = val.allContactsList;
            $scope.countClient = val.contactCount.clientCount;
            $scope.countProspects = val.contactCount.prospectsCount;
            $scope.countTotal = val.contactCount.clientCount + val.contactCount.prospectsCount;            
        });
        
        $scope.$on('vaultContactsList', function (event, val) {  
        	$scope.selectedContact = val.defaultClient;
        	$scope.clientList = true;
            $scope.contactsList = val.contactsList;
            $scope.clientLists = val.clientList;
            $scope.prospectList = val.prospectList;
            $scope.allContactsList = val.allContactsList;
            $scope.countClient = val.contactCount.clientCount;
            $scope.countProspects = val.contactCount.prospectsCount;
            $scope.countTotal = val.contactCount.clientCount + val.contactCount.prospectsCount;            
        });
        
        $scope.selectUserVault = function(item){
        	$scope.selectedContact = item.userId;
        	if($state.current.name !== 'page.advisor.vault-home.dashboard'){
        		advisorUserVaultDataService.setUserInfo(item);
        		$state.go('page.advisor.vault-home.dashboard');
        	}else{
        		$rootScope.$broadcast("clientVaultId", {
                    item: item,
                });
        	}
        	
        	
        }
        
        $scope.vaultAdvisorClick = function (vaultName, folderName) {
            $scope.searchString = "";
            if ($scope.currentState === 'page.client.vault-home.dashboard')                 
                $scope.sideVaultSearch = folderName;           
            else           
                $scope.sideVaultSearch = '';
            
            $rootScope.$broadcast("vaultAdvisorClick", { vaultName: vaultName, folderName: folderName });
        }
        
        /*$scope.searchVault = function (searchString) {
            if (!angular.isUndefinedOrNull(searchString))
                $state.go('page.client.vault-home.search', { searchString: searchString });
            else
                GlobalFactory.showErrorAlert('Input some value to search');
        }*/

    }
})();
