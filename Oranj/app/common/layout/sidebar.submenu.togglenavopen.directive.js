(function () {

    var STATES_SUBNAV_OPEN = [
        'page.client.home',
		'page.client.feed',
        'page.advisor.advisor',
        'page.advisor.contacts',
        'page.client.vault-home',
        'page.client.settings',
        'page.advisor.admin',
        'page.advisor.feed',
        'page.advisor.settings',
        'page.advisor.messages',
        'page.advisor.vault-home'
    ];

    angular.module('app.layout').directive('toggleNavCollapseMin', ['$rootScope','$state','$window', toggleNavCollapsedMin])
    // switch for mini style NAV, realted to 'collapseNav' directive
    function toggleNavCollapsedMin($rootScope, $state, $window) {
        var directive = {
            restrict: 'A',
            link: link
        };


        function link(scope, ele) {
            var subMenuToggleArrow, leftSideSubNav, leftSideNavContainer,
                $body = angular.element($window.document.body);

            scope.$on('$stateChangeSuccess', handleStateChange);


            scope.$on('nav:toggle', function() {
                subMenuToggleArrow =  ele.find('.toggle-nav-collapse-min i');

                subMenuToggleArrow.toggleClass("fa-angle-double-right fa-angle-double-left");
                $body.toggleClass("nav-collapsed-min");
            });


            function handleStateChange(event, toState) {
                var closeNav = true;
                //Refresh selector.
                subMenuToggleArrow =  ele.find('.toggle-nav-collapse-min i');
                leftSideSubNav = ele.find('ul.sub-nav');
                leftSideNavContainer =  $body.find('#nav-container');

                angular.forEach(STATES_SUBNAV_OPEN, function (state) {
                    if (toState.name.indexOf(state) > -1) {
                        closeNav = false;
                    }
                });

                console.log(' nav being hidden : '+closeNav);
                if (closeNav) {
                    $body.addClass('nav-collapsed-min');
                    $rootScope.$broadcast('nav:reset');
                    subMenuToggleArrow.addClass("fa-angle-double-right")
                        .removeClass("fa-angle-double-left");

                    //As discussed in R16-516, subnav will be removed and not shown at all.
                    if(leftSideSubNav.length) {
                        leftSideSubNav.css({'width': '0px'});
                    }

                    if(leftSideNavContainer.length) {
                        leftSideNavContainer.css({'width' : '0px'});
                    }
                }
                else {
                    // if(toState.name.indexOf(STATES_SUBNAV_OPEN[2]) > -1)
                    //      $body.addClass('nav-collapsed-min');
                    // else
                    //     $body.removeClass('nav-collapsed-min');
                    //
                    //      if(toState.name.indexOf(STATES_SUBNAV_OPEN[4]) > -3)
                    //      $body.addClass('nav-collapsed-min');
                    // else
                    //     $body.removeClass('nav-collapsed-min');
                    $body.removeClass('nav-collapsed-min');
                    subMenuToggleArrow.addClass("fa-angle-double-left")
                        .removeClass("fa-angle-double-right");
                    if(leftSideSubNav.length) {
                        leftSideSubNav.css({'width': ''});
                    }
                    if(leftSideNavContainer.length) {
                        leftSideNavContainer.css({'width' : ''});
                    }
                }
            }

            handleStateChange(null, $state.current);
        }

        return directive;
    }
})();
