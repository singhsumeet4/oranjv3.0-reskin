/**
 * Created by sumeetsingh on 6/29/16.
 */
(function() {
    
    var isSubMenuOpenRequired = false;

angular.module('app.layout').directive('subMenuOpenOn',['$state','$document', function($state, $document) {
    return {
        restrict: 'A',
        link: function(scope, ele, attr) {

            var $body = angular.element($document.body);

            scope.$on('$stateChangeSuccess', function() {
                setSubMenuOpen();
            });

            function setSubMenuOpen() {
                if($state.includes(attr.subMenuOpenOn)) {
                    ele.removeClass('ng-hide');
                    if(attr.subMenuOpenRequired) {
                        isSubMenuOpenRequired = true;
                    }

                } else {
                    ele.addClass('ng-hide');
                }
            }
            
            setSubMenuOpen();
        }
    }
}]);
})();
