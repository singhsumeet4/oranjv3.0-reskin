(function () {
    angular.module("app.layout")
        .controller('layoutController', ['$scope', '$state', 'GlobalFactory','$rootScope','UserProfileService', layoutController])
        .controller('sideBarController', ['$scope', '$state', 'GlobalFactory','$rootScope','UserProfileService','accountFormDataService', '$location', sideBarController])

        function layoutController($scope, $state, GlobalFactory,$rootScope, UserProfileService){


        }

        function sideBarController($scope, $state, GlobalFactory, $rootScope, UserProfileService, accountFormDataService, $location) {
        $scope.global = GlobalFactory;
        $scope.flag = false;
        $scope.sideAccountList = [];
        $scope.vaultActive = "init";
        $scope.currentState = $state.current.name;           

            var role = UserProfileService.fetchUserRole();
            role.then(
                function(userRole){
                    if(userRole.isClient) {
                        $rootScope.header = 'app/common/layout/html/header.client.html';
                        $rootScope.sideBar = 'app/common/layout/html/sidebar.client.html';
                    } else if(userRole.isAdvisor) {
                        $rootScope.header = 'app/common/layout/html/header.advisor.html';
                        $rootScope.sideBar = 'app/common/layout/html/sidebar.advisor.html';
                    } else if(userRole.isAdmin) {

                    }

                }, function(){
                    GlobalFactory.signout();
                }
            );

        $scope.signOut = function () {
            GlobalFactory.signout();
        };
        
        $scope.resetAccount = function () {
        	var path = $location.path();
        	$rootScope.prevPath = path;
        	accountFormDataService.resetAccountFormData();
        }

        $scope.searchVault = function (searchString) {
            if (!angular.isUndefinedOrNull(searchString))
                $state.go('page.client.vault-home.search', { searchString: searchString });
            else
                GlobalFactory.showErrorAlert('Input some value to search');
        }

        $scope.$on('vaultSearch', function (event, val) {            
            if (!angular.isUndefinedOrNull(val.searchString))
            {
                $scope.searchKeyword = val.searchString;
            }
            else {
                $scope.searchKeyword = "";               
            }
        })

        $scope.toggleSubNav = function() {
          //  $scope.$emit('nav:toggle');
             $rootScope.$broadcast('nav:toggle');
        };

        $scope.$on('accountDetails', function (event, val) {
            $scope.sideAccountList = val.sideAccountList;
        });      

        $scope.$on('currentStateStatus', function (event, val) {
            $scope.currentState = val;            
        });

        $scope.$on('updateGoalsList', function(event, goalsList) {
            $scope.goalsList = goalsList;
        })
        
        $scope.vaultClientClick = function (vaultName, folderName) {
            $scope.searchString = "";
            if ($scope.currentState === 'page.client.vault-home.dashboard')                 
                $scope.sideVaultSearch = folderName;           
            else           
                $scope.sideVaultSearch = '';
            
            $rootScope.$broadcast("vaultClientClick", { vaultName: vaultName, folderName: folderName });
        }
        
        $scope.setPrevPath = function (){
        	var path = $location.path();
        	$rootScope.prevPath = path;
        }
              

    }
})();
