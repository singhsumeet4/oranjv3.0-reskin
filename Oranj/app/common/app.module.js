(function () {
    'use strict';

    angular.module('app', [
        // Core modules
        //'LocalStorageModule',
        'app.templates',
        'app.constant',
        'app.Services',
        'app.core',
        'app.api',
        'app.client',
        'app.advisor',
        // Custom Feature modules
        'app.chart',
        'app.highcharts',
        'app.ui',
        'app.ui.form',
        'app.ui.form.validation',
        'app.page',
        'app.table',
        'app.clientDash',
        // 3rd party feature modules
        'mgo-angular-wizard',
        'ui.tree',
        'textAngular',
        'fcsa-number',
        // ngcookies
        'ngCookies',
        'chart.js',
        'rzModule',
        'app.linechart',
        'app.barcharts',
        'app.piechart',
        'app.advisorSidebar',
        //'app.clientDashboard',
        //'app.clientDashModule',
        'app.feed',
        'app.setting',
        'app.credential',
        'app.message',
        'app.addGoal',
        'app.editGoal',
        'app.matched',
        'app.layout',
        'app.transferAccount',
        'app.IndividualaggModule',
        'app.editFinencialModule',
        'app.addFinancialModule',
        'app.accountMain',
        'app.accountSpecific',
        'app.portfolio',
        'app.questionnaire_admin',
        '720kb.datepicker',
        'ng-currency',
        'app.disclaimer'
        //'app.createContact',



    ]);

})();

