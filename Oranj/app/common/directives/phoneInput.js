﻿(function () {
    'use strict';

    angular.module('app').directive('phoneInput', ['$filter', '$browser', function ($filter, $browser) {
        return {
            require: 'ngModel',
            link: function ($scope, $element, $attrs) {
                var listener = function () {
                    var value = $element.val().replace(/[^0-9]/g, '');
                    $element.val($filter('phoneFilter')(value, false));
                }

                $element.bind('change', listener);

                $element.bind('keydown', function (event) {
                    var key = event.keyCode;

                    if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
                        return;
                    }

                    $browser.defer(listener);

                });

                $element.bind('paste cut', function () {
                    $browser.defer(listener);
                });
            }
        }
    }]);
})();