﻿(function() {
    'use strict';
    angular.module('app').directive("scroll", function ($window, $rootScope) {
    return function (scope, element, attrs) {
        angular.element($window).bind("scroll", function () {
            if (this.pageYOffset <= 0) {              
                $rootScope.$broadcast("loadMessages");
            }
            scope.$apply();
        });
    };
    });
})();