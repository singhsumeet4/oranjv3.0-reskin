(function () {
    angular.module('app.piechart', [])
        .factory('piechartdata', [function piechartdayaFactory() {


            var pie = {};
            pie.radius = [35, 40];
            pie.labelBottom = {
                normal: {
                    color: '#585863',
                    label: {
                        show: true,
                        position: 'center'
                    },
                    labelLine: {
                        show: false,

                    }
                }
            };
            pie.labelTop = {
                normal: {
                    color: '#fff',
                    label: {
                        show: true,
                        position: 'center',
                        formatter: '{b}',
                        textStyle: {
                            color: '#999',
                            baseline: 'middle',
                            fontSize: 8,
                        }
                    },
                    labelLine: {
                        show: false
                    }
                }
            };
            pie.labelFromatter = {
                normal: {
                    label: {
                        formatter: function (params) {
                            return 100 - params.value + '%'
                        },
                        textStyle: {
                            color: '#fff',
                            baseline: 'middle',
                            fontSize: 15
                        }
                    }
                },
            };
            pie.options = {
                series: [
                    {
                        type: 'pie',
                        center: ['70%', '50%'],
                        radius: pie.radius,
                        itemStyle: pie.labelFromatter,
                        data: [
                            {name: '', value: 73, itemStyle: pie.labelTop},
                            {name: 'other', value: 27, itemStyle: pie.labelBottom}
                        ]
                    }
                ]
            };


            var pieChartCollection = {};
            pieChartCollection.data = [];
            pieChartCollection.reset = function () {
                pieChartCollection.data = [];
            }
            pieChartCollection.add = function (data) {
                pieChartCollection.data.push(data);
            }
            pieChartCollection.dataSchema = function () {
                return pie;
            }

            return pieChartCollection;

        }])
        .controller('pieChartController', ['$scope', 'piechartdata', function pieChartController($scope, piechartdata) {
            angular.forEach(piechartdata.data, function (piechartobj, i) {
                i = i + 1;
                $scope['poptions' + i] = piechartobj.options;
            });

        }])
})();
