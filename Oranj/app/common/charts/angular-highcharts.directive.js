(function() {
    angular.module('app.highcharts',[])
        .directive('oranjHighchart', ['$timeout', function($timeout) {
            return {
                scope: {
                    chartOptions: '=',
                    redraw: '&'
                },
                link: function(scope, ele, attr) {
                    scope.$watch('chartOptions', function(n,o) {
                        if(n && n !== o) {
                            $timeout(function() {
                                var chart = Highcharts.chart(ele[0],n);
                            }, 10);

                        }
                    })
                }
            }
        }])
})()
