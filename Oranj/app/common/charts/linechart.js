(function () {
  angular.module('app.linechart', [])
    .factory('linechartObjArray', [function linechartObjFactory() {
      var chartDataObj={}
     chartDataObj.chartDataArray=[];
     chartDataObj.reset=function(){
         chartDataObj.chartDataArray=[];
     }
      // var chartData = {};
      // chartData.labels = [];
      // chartData.series = [];
      // chartData.data;
      // chartData.options = {
      //   showScale: false,
      //   scaleShowGridLines: false,
      //   pointDot: false,
      //   bezierCurve: false,
      // }
      // chartData.colours = [{
      //   "fillColor": "#e0f0f1",
      //   "strokeColor": "#36b8b7",
      // }]

      return chartDataObj;

    }])
    .controller('linechartController', ['$scope', 'linechartObjArray', function linechartController($scope, linechartObjArray) {
      // performance chart main right starts
$scope.vlarus='hello';
angular.forEach(linechartObjArray.chartDataArray,function(linechartObj,i){
      i=i+1;
      $scope.type = "line";
      $scope['labels'+i] = linechartObj.labels;
      $scope['series'+i] = linechartObj.series;
      $scope['data'+i] = linechartObj.data;
   

      $scope['options'+i] = linechartObj.options;
      $scope['colours'+i] = linechartObj.colours;
})
    
    
    
      // performance chart main right ends
    }])
    .factory('jQlinechart', [function jQlinechartFactory() {
   
      //jquery chart starts


      Chart.types.Line.extend({
        name: "LineAlt",
        initialize: function (data) {
          var strokeColors = [];
          data.datasets.forEach(function (dataset, i) {
            if (dataset.dottedFromLabel) {
              strokeColors.push(dataset.strokeColor);
              dataset.strokeColor = "rgba(0,0,0,0)"
            }
          })

          Chart.types.Line.prototype.initialize.apply(this, arguments);

          var self = this;
          data.datasets.forEach(function (dataset, i) {
            if (dataset.dottedFromLabel) {
              self.datasets[i].dottedFromIndex = data.labels.indexOf(dataset.dottedFromLabel) + 1;
              self.datasets[i]._saved = {
                strokeColor: strokeColors.shift()
              }
            }
          })
        },
        draw: function () {
          Chart.types.Line.prototype.draw.apply(this, arguments);

          // from Chart.js library code
          var hasValue = function (item) {
            return item.value !== null;
          },
            nextPoint = function (point, collection, index) {
              return Chart.helpers.findNextWhere(collection, hasValue, index) || point;
            },
            previousPoint = function (point, collection, index) {
              return Chart.helpers.findPreviousWhere(collection, hasValue, index) || point;
            };

          var ctx = this.chart.ctx;
          var self = this;
          ctx.save();
          this.datasets.forEach(function (dataset) {
            ctx.beginPath();
            if (dataset.dottedFromIndex) {
              ctx.lineWidth = self.options.datasetStrokeWidth;
              ctx.strokeStyle = dataset._saved.strokeColor;

              // adapted from Chart.js library code
              var pointsWithValues = Chart.helpers.where(dataset.points, hasValue);
              Chart.helpers.each(pointsWithValues, function (point, index) {
                if (index >= dataset.dottedFromIndex)
                  if (dataset.fillColor === "transparent")
                    ctx.setLineDash([10, 10]);
                  else
                    ctx.setLineDash([3, 3]);
                else
                  ctx.setLineDash([]);

                if (index === 0) {
                  ctx.moveTo(point.x, point.y);
                }
                else {
                  if (self.options.bezierCurve) {
                    var previous = previousPoint(point, pointsWithValues, index);
                    var originalBezierCurveTo = ctx.bezierCurveTo;
                    ctx.bezierCurveTo(
                      previous.controlPoints.outer.x,
                      previous.controlPoints.outer.y,
                      point.controlPoints.inner.x,
                      point.controlPoints.inner.y,
                      point.x,
                      point.y
                    );

                  }
                  else {
                    ctx.lineTo(point.x, point.y);
                  }
                }

                ctx.stroke();
              }, this);
            }

          })
          ctx.restore();
        }
      });



      return {
        jQLineChart: function (canvasId, data, lineoptions) {
          try {

            var element = document.getElementById(canvasId); // notice the change
            var canvasDiv = document.getElementById("canvasDiv");
            var heightCanvas = 0;
            var widthCanvas = 0;
            if (element != undefined) {
              if (canvasDiv === undefined || canvasDiv === null) {
                canvasDiv = document.createElement("DIV");
                canvasDiv.id = "canvasDiv";

                element.parentNode.insertBefore(canvasDiv, element);
              }

              heightCanvas = element.clientHeight;
              widthCanvas = element.clientWidth;
              element.parentNode.removeChild(element);
            }


            var canvas = document.createElement("CANVAS");
            canvas.id = canvasId;
            canvas.clientHeight = heightCanvas;
            canvas.clientWidth = widthCanvas;
            var ctx = canvas.getContext("2d");

            canvasDiv.appendChild(canvas);


            //  var ctx = document.getElementById(canvasId).getContext("2d");
            var lineChart = new Chart(ctx).LineAlt(data, lineoptions);
          }
          catch (e){

          }
         
        }
      };



    }])

})();