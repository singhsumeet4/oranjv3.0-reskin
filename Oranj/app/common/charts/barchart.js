(function(){
     angular.module('app.barcharts', [])
     .factory('jQBarChart',[function jQBarChartFactory(){
          return {
        jQBarChartfun: function (canvasId, data, lineoptions,toolTip) {
          var ctx = document.getElementById(canvasId).getContext("2d");
          var barChart = new Chart(ctx).Bar(data, lineoptions,toolTip);
        }
      };

     }])
})();