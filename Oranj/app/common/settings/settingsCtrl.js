
(function () {



    angular.module("app.setting", ['chart.js', 'ui.bootstrap'])


        .controller("settingsCtrl", ["$scope", 'configfactory', 'GlobalFactory', "linechartObjArray", 'piechartdata', '$rootScope', '$filter', 'OranjApiService', settingsCtrl]);
        
    function settingsCtrl($scope, configfactory, GlobalFactory, linechartObjArray, piechartdata, $rootScope, $filter, OranjApiService) {
    	var role = GlobalFactory.getRole();

  	   	role.then(
  	            function(result){
  	            	if(result.data) {
  	     	 		  if (GlobalFactory.lookupRole( 'ROLE_USER', result.data )) {
  	     	  		 	
  	     			  }else if (GlobalFactory.lookupRole( 'ROLE_PROSPECT', result.data ) || GlobalFactory.lookupRole( 'ROLE_CLIENT', result.data )) {  
  	     				   	
  	     			  } else if (GlobalFactory.lookupRole('ROLE_ADVISOR', result.data)) {

	                  } else  {
  	     				  GlobalFactory.signout();
  	     			 }	
  	     	 	   } else {
  	     	 		  GlobalFactory.signout();
  	     	 	   }
  	            }, function(){
  	            	GlobalFactory.signout();
  	            }
  	   	);
  	   	
  	    $scope.getSettings = function(){
			OranjApiService('getSettings').then(function(response){
	          	$scope.emailSettings = response.data.userEmaiSettings;
	           },function(response){
	          	 console.log(response);
	         });
  	    };

  	    $scope.postSettings = function () {
  	        OranjApiService('updateSettings', {
  	            data: $scope.emailSettings
  	        }).then(function (response) {
  	            if (response.status === "success"){
  	            	GlobalFactory.showSuccessAlert("Setting Changed Successfully");
  	            }else if(response.status === "error"){
  	            	 GlobalFactory.showSuccessAlert(response.message);
  	            }
  	        }, function (response) {
  	            if (response.status === "error"){
  	            	GlobalFactory.showSuccessAlert(response.message);
  	            }
  	        });
  	    };

		$scope.getSettings();
    }
})();