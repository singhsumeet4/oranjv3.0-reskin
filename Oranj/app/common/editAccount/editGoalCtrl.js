(function () {
    'use strict';

    angular.module('app.editGoal', ['app.ui.form'])
        .controller('editGoalCtrl', ['$scope','$rootScope', 'piechartdata', 'jQlinechart','$mdSidenav', editGoalCtrl])
    //  .controller('ProfileInfoCtrl', ['$scope',ProfileInfoCtrl])

    function editGoalCtrl($scope,$rootScope, piechartdata, jQlinechartFactory,$mdSidenav) {
$rootScope.breadcrum="Edit a Goal";

//  $scope.toggleRight = buildToggler('right');
//  function buildToggler(navID) {

//       return function () {
//         $mdSidenav(navID)
//           .close()
          
//       }
//     }

// addGoalStep3

$scope.goals=[];

var account ={};
account.Name="Oscar Winnings";
account.Type="401K";
account.Institution="JP Morgan";
account.Balance="$1,192,014.73";
$scope.goals.push(account);

$scope.editAccount=function(){
   var account ={};
account.Name="Oscar Winnings";
account.Type="401K";
account.Institution="JP Morgan";
account.Balance="$1,192,014.73";
$scope.goals.push(account); 
}


        $scope.goalDetails = {};
        $scope.goalDetails.goalname = 'Marx Retirement',


            $scope.goalDetails.retirementAge = 65,
            $scope.ages = ('60 61 62 63 64 65 66 67 68 69 70 '
            ).split(' ').map(function (retirementAge) {
                return { abbrev: retirementAge };
            })


        // Engagment pie charts for "addGoal" and "addGoalStep3" start   
        piechartdata.reset();

        var pieSchema = piechartdata.dataSchema();
        pieSchema.options.series[0].data[0].value = 73;
        pieSchema.options.series[0].data[1].value = 100 - pieSchema.options.series[0].data[0].value;
        pieSchema.labelBottom.normal.color = '#585863';
        pieSchema.labelFromatter.normal.label.textStyle.color = '#fff';
        pieSchema.labelTop.normal.color = '#fff';
        pieSchema.labelFromatter.normal.label.formatter = function (params) {
            return 100 - params.value + '%'
        }

        piechartdata.add(pieSchema);

        // Engagment pie charts for "addGoal" and "addGoalStep3" end



        //jquery chart for "addGoal" and "addGoalStep3" start 


        var data = {
            labels: ["January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016", "October 2016", "November 2016", "December 2016", "January 2017"],
            datasets: [
                {
                    label: "Projected",
                    type: 'line',
                    fillColor: "transparent",
                    strokeColor: "rgba(58,62,55,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "#586fb2",
                    data: [0, 300000, 500000, 650000, 750000, 800000, 850000, 900000, 940000, 960000, 980000, 1000000, 1020000],
                    dottedFromLabel: "January 2016",

                },
            ]

        };

        var lineoptions = {
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,
            showScale: true,
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            scaleFontSize: 0
        }

        jQlinechartFactory.jQLineChart("canvas", data, lineoptions);//parameter sequence 1 canvas ID,2 Data to build, 3 options to configure

        //jquery chart for "addGoal" and "addGoalStep3" end

        
        // slider start

        $scope.percentages = {};
        $scope.percentages.low1 = 2;
        $scope.percentages.low2 = 6500;
        $scope.percentages.low3 = 4;
        $scope.percentages.low4 = 22.4;


        // slider 1 start
        var formatToPercentage1 = function (value) {
            return value + ' - fairly important';
        };

        $scope.percentages.options1 = {
            floor: 1,
            ceil: 4,
            translate: formatToPercentage1,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
        // slider 1 end



        // slider 2 start
        var formatToPercentage2 = function (value) {
            return ' $' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + ' / month';
        };

        $scope.percentages.options2 = {
            floor: 5500,
            ceil: 8500,
            translate: formatToPercentage2,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
        // slider 2 end


        // slider 3 start
        var formatToPercentage3 = function (value) {
            return value + ' - conservative moderate';
        };

        $scope.percentages.options3 = {
            floor: 0,
            ceil: 10,
            translate: formatToPercentage3,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
        // slider 3 end


        // slider 4 start
        var formatToPercentage4 = function (value) {
            return value + '%';
        };

        $scope.percentages.options4 = {
            floor: 0,
            ceil: 100,
            step: 0.1,
            precision: 1,
            translate: formatToPercentage4,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
        // slider 4 end



        // slider end

        // $scope.data = {
        //     group1: 'Look for a way to invest more',
        //     group2: '2'
        // };
        
        
        
        
        
        // search concept
        
        
        // =================
         var self = $scope;

    self.simulateQuery = false;
    self.isDisabled    = false;

    // list of `state` value/display objects
    self.states = loadAll();
    self.querySearch   = querySearch;
    // self.selectedItemChange = selectedItemChange;
  //  self.searchTextChange   = searchTextChange;

    self.newState = newState;

    function newState(state) {
      alert("Sorry! You'll need to create a Constituion for " + state + " first!");
    }

    function querySearch (query) {
      var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
          deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    // function searchTextChange(text) {
     
    // }

    // function selectedItemChange(item) {
   
    // }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
      var allStates = 'Accenture Business Process Outsourcing, Accendo Realtors, Accelerate Learning Center, Acculabs Group of Companies';

      return allStates.split(/, +/g).map( function (state) {
        return {
          value: state.toLowerCase(),
          display: state
        };
      });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }
     

    }



})();