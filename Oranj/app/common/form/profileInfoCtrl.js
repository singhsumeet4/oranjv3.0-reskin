(function () {
    'use strict';

    angular.module('app.ui.form')
        .controller('ProfileInfoCtrl', ['$scope','$rootScope','configfactory', 'GlobalFactory','$http', 'OranjApiService', '$filter','$stateParams', ProfileInfoCtrl]);
    
    function ProfileInfoCtrl($scope, $rootScope, configfactory, GlobalFactory, $http, OranjApiService, $filter,$stateParams) {
		$(function(){
			$("#avatar-upload").on('click', function(e){
			    e.preventDefault();
			    $("#upload-av:hidden").trigger('click');
			});
		});
        $scope.deleteStatus = false;
        $scope.todayDate = new Date();
        var minAge = 18;
        $scope.minAge = new Date($scope.todayDate.getFullYear() - minAge, $scope.todayDate.getMonth(), $scope.todayDate.getDate());
        $scope.newChild = [];     
        $rootScope.clientName=$stateParams.name;

  	   	if($stateParams.id==undefined)
  	   		$stateParams.id='';
        $scope.getUserProfile = function () {        	
        	OranjApiService('getUserProfileData',null,{'userid':$stateParams.id}).then(function (response) {
                $scope.profile = response.data.profile;
                if($scope.profile.dob !== null){
	                $scope.profile.dob = new Date($scope.profile.dob);
	            	$scope.profile.dob = $filter('date')($scope.profile.dob, "MM/dd/yyyy");
                }
	                if ($scope.profile.spouse !== null) {
	                    $scope.spouseStat = true;
	                    if($scope.profile.spouse.dob !==null){
		                    $scope.profile.spouse.dob = new Date($scope.profile.spouse.dob);
		                    $scope.profile.spouse.dob = $filter('date')($scope.profile.spouse.dob, "MM/dd/yyyy");
	                    }
	                }
                
                var avatar = '../../../images/avatar.jpg';
                $rootScope.avatarUrl = avatar;
                if ($scope.profile.avatarCompleteUrl !== null) {
                	avatar = $scope.profile.avatarCompleteUrl;
                }
                $scope.profile.avatarCompleteUrl = avatar;
                
                if($stateParams.id == '')
                	$rootScope.avatarUrl = avatar;   
                if ($scope.profile.children !== null && $scope.profile.children.length > 0) {
                    $scope.childStat = true;
                    for (var i = 0; i < $scope.profile.children.length; i++) {
                        if($scope.profile.children[i].dob !== null){
	                    	$scope.profile.children[i].dob = new Date($scope.profile.children[i].dob);
	                        $scope.profile.children[i].dob = $filter('date')($scope.profile.children[i].dob, "MM/dd/yyyy");                        
	                        delete $scope.profile.children[i].dobStr;
	                     }
                    }
                }
            }, function (response) {
            	console.log(response);
            });

        };
        /*To check the file size*/
        function validateFileSize(file) {
            var sizeInMb = (file.size / 1000);
            if (sizeInMb > 250)
                return false;
            else
                return true;
        };
        /*end*/
        /*To check the file extension*/
        function validateFileExtension(filename) {
            var extn = filename.split(".").pop().toLowerCase();
            var VALID_FILE_EXTNS = ['png', 'jpg', 'jpeg', 'bmp', 'gif'];

            if (VALID_FILE_EXTNS.indexOf(extn) > -1)
                return true;
            else
                return false;
        };
        /*end*/  
        function readURL(input) {
            if (input.files && input.files[0]) {
         	   var file = input.files[0];
         	   if (!validateFileExtension(file.name)) {
                    GlobalFactory.showErrorAlert("You can't upload files of this type.");
                    $scope.fileErrorflag = true;
                    if($scope.profile.avatarCompleteUrl != null) {
                 	   $('#adv-avatar').attr('src', $scope.profile.avatarCompleteUrl);
                    }
                    return false;
                }

                if (!validateFileSize(file)) {
                    GlobalFactory.showErrorAlert('Maximum Upload size is 250 KB.');
                    $scope.fileErrorflag = true;
                    if($scope.profile.avatarCompleteUrl != null) {
                 	   $('#adv-avatar').attr('src', $scope.profile.avatarCompleteUrl);
                    }
                    return false;
                }
                
                $scope.fileErrorflag = false;
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#adv-avatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        /*To load the profile logo*/
        $scope.loadProfileLogo = function(input){
            readURL(input);
            $scope.deleteStatus = false;
        };
        /*end*/
        /*To remove the profile logo*/
        $scope.removeAvatar = function(){
     	   
     	   $('#adv-avatar').attr('src', 'images/avatar.jpg');
     	   $scope.deleteStatus = true;
        };
        /*end*/
        $scope.addChild = function () {
            $scope.childrenObj = {};
            $scope.childrenObj.name = '';       // firstname
            $scope.childrenObj.middle = '';     // middlename
            $scope.childrenObj.last = '';       // lastname
            $scope.childrenObj.dob = '';        // date of birth
            $scope.profile.children.push($scope.childrenObj);
        };
        
        $scope.removeChild = function (arr) {
            var index = $scope.profile.children.indexOf(arr);
            $scope.profile.children.splice(index, 1);

            if($scope.profile.children.length==0)
                $scope.childStat=false;
        };

        $scope.changeChildStatus = function () {
            if (!$scope.childStat)
            {
                $scope.profile.children = [];
            }
            else {
                $scope.childStat = true;
                $scope.addChild();
            }
        };

        $scope.changeSpouseStatus = function () {
            if (!$scope.spouseStat) {
                $scope.profile.spouse = {};
            }
            else {
                $scope.spouseStat = true;               
            }
        };

        $scope.updateProfileInfo = function () {
  		   $scope.isDisabled = true;
		   $scope.updateAvatar(); // to update the avatar
            if (!$scope.childStat){
                $scope.newChild = [];
            }else {                
                $scope.newChild = [];
                for (var i = 0; i < $scope.profile.children.length; i++) {
                    $scope.newChild.push($scope.profile.children[i]);
                    $scope.newChild[i].id = $scope.profile.children[i].id;
                }
            }
            
            if (!angular.isUndefinedOrNull($scope.profile.spouse)) {
                var spouse = {
                    "id": $scope.profile.spouse.id,
                    "name": $scope.profile.spouse.name,
                    "middle": $scope.profile.spouse.middle,
                    "last": $scope.profile.spouse.last,
                    "dob": $scope.profile.spouse.dob,
                    "income": $scope.profile.spouse.income
                };
            }
            $scope.spouse = $scope.spouseStat === true ? spouse : null;
            $scope.profile.maritalStatus = $scope.spouseStat === true ? 'married' : 'single';
            
            $scope.newProfile = {
                "id": $scope.profile.id,
                "firstName": $scope.profile.firstName,
                "middleName": $scope.profile.middleName,
                "lastName": $scope.profile.lastName,
                "dob": $scope.profile.dob,
                "nickName": $scope.profile.nickName,
                "user": {
                    "email": $scope.profile.user.email
                },
                "phone": $scope.profile.phone,
                "income": $scope.profile.income,
                "spouse": $scope.spouse,
                "children": $scope.newChild,
                "maritalStatus": $scope.profile.maritalStatus
            };
            OranjApiService('updateProfileInfo', {
                data: $scope.newProfile
            },{'userid':$stateParams.id}).then(function (response) {
                if (response.status === 'success'){
                    GlobalFactory.showSuccessAlert('Profile Updated Successfully');
                }
                else {
                    GlobalFactory.showErrorAlert(response.message);
                }
            }, function (response) {
                if (response.status === 'error'){
                    GlobalFactory.showErrorAlert(response.message);
                }
            });
           
        };

        $scope.limitKeypress = function (event, value, maxLength) {
            if (value != undefined && value.toString().length >= maxLength) {
                event.preventDefault();
            }
        }

        $scope.updateAvatar = function () {
        	
     	   if($scope.deleteStatus == false) {
    		   var headers = {
                       'Content-Type': undefined
                   };
        	   var input = angular.element('#upload-av');
        	   $scope.newUserFile = input[0].files[0];
        	  
	           	if(typeof  $scope.newUserFile != 'undefined' &&  $scope.newUserFile.size > 0 && $scope.fileErrorflag == false)
	           	{
	       			
	           		$scope.formdata = new FormData();
	           		$scope.formdata.append('avatarContent', $scope.newUserFile);
		               
		           	OranjApiService('updateAvatar', {
		                    data: $scope.formdata,
		                    transformRequest: angular.identity,
		                    headers: headers
		                },{'userid':$stateParams.id}).then(function (response) {
		                    
		                }, function (response) {
		                	GlobalFactory.showErrorAlert('Server error in updation.');
		                });
	           	}
    	   }else {
    		   OranjApiService('deleteAvatar',null,{'userid':$stateParams.id}).then(function (response) {
                   if (response.status === 'success') {
                       var avatar = '../../../images/avatar.jpg';     
                       $('#adv-avatar').attr('src', avatar);              
                   }
               }, function (response) {
                   if (response.status === 'error') {
                       GlobalFactory.showSuccessAlert(response.message);
                   }
               });
    	   } 
        }

        $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
       'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
       'WY').split(' ').map(function (state) {
           return { abbrev: state };
       });
       
       $scope.getAdvFile = function(){
        	$scope.adv_show = false;
        	 OranjApiService('getAdv').then(function (response) {
        		 if(response.status === 'success'){
        			 if(typeof response.data !== 'undefined' && response.data['is-available-adv-file']){
        				 $scope.adv_show = true;
            			 $scope.adv_file = response.data['adv-file'];
            			 $scope.advFileName = response.data['advFileName'];
        			 }
        		 }
        	 }, function (response) {console.log(response);
             });
        };
        
        $scope.showAdv = function(adv_file){
        	window.open(adv_file, '_blank');
        };
        /* function to get the advisor profile*/
        $scope.getAdvisorProfile = function () {        	
        	OranjApiService('getuserAdvisorInfo').then(function (response) {
        		if(response.status=="success"){
	                $scope.advisorinfo = response.data;
	                $scope.advisorinfo.bio =  $scope.advisorinfo.bio ? String($scope.advisorinfo.bio).replace(/<[^>]+>/gm, '') : '';
	                $scope.advisorinfo.advisorname = $scope.advisorinfo.firstName + " " + $scope.advisorinfo.lastName;
	                var avatar = '../../../images/avatar.jpg';
	                if ($scope.advisorinfo.advisorLogo !== null) {
	                	avatar = $scope.advisorinfo.advisorLogo;
	                }
	                $scope.advisorinfo.advisorLogo = avatar;
        		}
            }, function (response) {
            	console.log(response);
            });

        };        
        /*end*/
        $scope.getAdvisorProfile();
        $scope.getUserProfile();
        $scope.getAdvFile();

    }
})();