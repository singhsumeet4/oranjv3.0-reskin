(function () {
    'use strict';

    angular.module('app.ui.form.validation')
        .controller('FormConstraintsCtrl', ['$scope', '$filter','$rootScope', FormConstraintsCtrl]);
        
    function FormConstraintsCtrl($scope, $filter,$rootScope) {
                
        var original;
        $rootScope.breadcrum="Settings / My Profile / Privacy Settings";
        $scope.form = {
            required: '',
            minlength: '',
            maxlength: '',
            length_rage: '',
            type_something: '',
            confirm_type: '',
            foo: '',
            email: '',
            url: '',
            num: '',
            minVal: '',
            maxVal: '',
            valRange: '',
            pattern: ''
        };

        original = angular.copy($scope.form);
        $scope.revert = function () {
            $scope.form = angular.copy(original);
            return $scope.form_constraints.$setPristine();
        };
        $scope.canRevert = function () {
            return !angular.equals($scope.form, original) || !$scope.form_constraints.$pristine;
        };
        $scope.canSubmit = function () {
            return $scope.form_constraints.$valid && !angular.equals($scope.form, original);
        };
        $scope.submitForm = function () {
            $scope.showInfoOnSubmit = true;
            return $scope.revert();
        };
        $scope.dateValidate = function (myDate) {
            var mgs="Should be minimum 18 years old";
              $scope.dateMessage = "";
            if (myDate === undefined)
                return true;

            var selectedMonth = $filter('date')(myDate, 'MM');//December-November like
            var selectedDay = $filter('date')(myDate, 'dd'); //01-31 like
            var selectedYear = $filter('date')(myDate, 'yyyy');//2014 like
            var today = new Date();
            if (Math.abs(selectedYear - today.getFullYear()) > 18)
                return true;
            if (Math.abs(selectedYear - today.getFullYear()) === 18) {
                if (selectedMonth > today.getMonth()+1)
                    return true;
                if (selectedMonth < today.getMonth()+1) {
                    $scope.dateMessage = mgs;
                    return false;
                }
                if (selectedMonth === today.getMonth()+1) {
                    if (selectedDay > today.getDay())
                        return true;
                    if (selectedDay <= today.getDay()) {
                        $scope.dateMessage = mgs;
                        return false;
                    }
                }

            }
            else {
                $scope.dateMessage = mgs;
                return false;
            }


        }
    }

})(); 