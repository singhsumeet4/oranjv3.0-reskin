(function () {
    'use strict';

    angular.module('app.ui.form.validation')
    .controller('MaterialLoginCtrl', ['$scope', MaterialLoginCtrl]);
    
    function MaterialLoginCtrl($scope) {
        var original;

        $scope.user = {
            email: '',
            passowrd: ''
        }

        original = angular.copy($scope.user);
        // https://github.com/angular/material/issues/1903
        $scope.revert = function () {
            $scope.user = angular.copy(original);
            $scope.material_login_form.$setPristine();
            $scope.material_login_form.$setUntouched();
            return;
        };
        $scope.canRevert = function () {
            return !angular.equals($scope.user, original) || !$scope.material_login_form.$pristine;
        };
        $scope.canSubmit = function () {
            return $scope.material_login_form.$valid && !angular.equals($scope.user, original);
        };
        $scope.submitForm = function () {
            $scope.showInfoOnSubmit = true;
            return $scope.revert();
        };
    }
})();