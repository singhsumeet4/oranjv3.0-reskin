(function () {
    'use strict';

    angular.module('app.ui.form')
        .controller('passSettings', ['$scope', '$rootScope', 'OranjApiService', 'GlobalFactory', passSettings]);
    
    function passSettings($scope, $rootScope, OranjApiService, GlobalFactory) {
           // Hide & show password function
	$scope.inputType = 'password';
    $scope.inputTypenew = 'password';
    $scope.inputTypeconfirm = 'password';
  
  // Hide & show password function
  $scope.hideShowPassword = function(){
    if ($scope.inputType == 'password')
      $scope.inputType = 'text';
    else
      $scope.inputType = 'password';
  };
		
		$scope.hideNewPassword = function(){
		    if ($scope.inputTypenew == 'password')
		      $scope.inputTypenew = 'text';
		    else
		      $scope.inputTypenew = 'password';
		};

        $scope.hideNewConfirmPassword = function(){
		    if ($scope.inputTypeconfirm == 'password')
		      $scope.inputTypeconfirm = 'text';
		    else
		      $scope.inputTypeconfirm = 'password';
		};
       
        $scope.settings = {};                
        $scope.changePass = function () {
            OranjApiService('changePass', {
                data: $scope.settings
            }).then(function (response) {
                if (response.status == "success")
                    GlobalFactory.showSuccessAlert("Password Changed Successfully");
                if (response.status == "error")
                    GlobalFactory.showSuccessAlert(response.message);
            }, function (response) {
                if (response.status == "error")
                    GlobalFactory.showSuccessAlert(response.message);
            });
        }
    }	
    
})();