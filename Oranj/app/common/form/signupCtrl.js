(function () {
    'use strict';

    angular.module('app.ui.form.validation')
    .controller('SignupCtrl', ['$scope', SignupCtrl]);
    
    function SignupCtrl($scope) {
        var original;

        $scope.user = {
            name: '',
            email: '',
            password: '',
            age: ''
        };

        $scope.showInfoOnSubmit = false;

        original = angular.copy($scope.user);

        $scope.revert = function () {
            $scope.user = angular.copy(original);
            $scope.form_signup.$setPristine();
        };

        $scope.canRevert = function () {
            return !angular.equals($scope.user, original) || !$scope.form_signup.$pristine;
        };

        $scope.canSubmit = function () {
            return $scope.form_signup.$valid && !angular.equals($scope.user, original);
        };

        $scope.submitForm = function () {
            $scope.showInfoOnSubmit = true;
            return $scope.revert();
        };

    }
    
})();