(function () {
    'use strict';

    angular.module('app.ui.form')
        .directive('uiRangeSlider', uiRangeSlider)
        .directive('uiSpinner', uiSpinner)
        .directive('uiWizardForm', uiWizardForm)
        .directive("compareTo", compareTo)
      .directive('allowPattern', allowPatternDirective);



        function allowPatternDirective() {
            return {
                restrict: "A",
                compile: function(tElement, tAttrs) {
                    return function(scope, element, attrs) {
                // I handle key events
                        element.bind("keypress", function(event) {
                            var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                            var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.
                
                // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                            if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                    event.preventDefault();
                                return false;
                            }
                
                        });
                    };
                }
            };
        }


    // Dependency: http://www.eyecon.ro/bootstrap-slider/ OR https://github.com/seiyria/bootstrap-slider
    function uiRangeSlider() {
        return {
            restrict: 'A',
            link: function(scope, ele) {
            ele.slider();
            }            
        }
    }

    // Dependency: https://github.com/xixilive/jquery-spinner
    function uiSpinner() {
        return {
            restrict: 'A',
            compile: function(ele, attrs) { // link and compile do not work together
            ele.addClass('ui-spinner');
            return {
                post: function() {
                ele.spinner();
                }
            };
            }
            // link: // link and compile do not work together
        }
    }


    // Dependency: https://github.com/rstaib/jquery-steps
    function uiWizardForm() {
        return {
            restrict: 'A',
            link: function(scope, ele) {
            ele.steps()
            }            
        }
    }
function compareTo() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
};
})(); 


