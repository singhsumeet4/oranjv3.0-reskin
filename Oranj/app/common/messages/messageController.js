var messageApp = angular.module('app.message', ['btford.socket-io']);

messageApp.controller('messageController', ['$scope', '$rootScope', '$stateParams', 'socket', '$sanitize', '$timeout', 'GlobalFactory', 'UserProfileService', messageController]);
function messageController($scope, $rootScope, $stateParams, socket, $sanitize, $timeout, GlobalFactory, UserProfileService) {

    var role = GlobalFactory.getRole();

    role.then(
            function (result) {
                if (result.data) {
                    if (GlobalFactory.lookupRole('ROLE_USER', result.data)) {

                    } else if (GlobalFactory.lookupRole('ROLE_PROSPECT', result.data) || GlobalFactory.lookupRole('ROLE_CLIENT', result.data)) {

                    } else if (GlobalFactory.lookupRole('ROLE_ADVISOR', result.data)) {

	                } else {
                        GlobalFactory.signout();
                    }
                } else {
                    GlobalFactory.signout();
                }
            }, function () {
                GlobalFactory.signout();
            }
    );

    var typing = false;
    var lastTypingTime;
    var TYPING_TIMER_LENGTH = 400;
    var USER_TYPES = ['ROLE_ADVISOR', 'ROLE_CLIENT'];
    var MESSAGES_LENGTH = 10;
    var CLIENT_CHAT_INDEX = 1;
    var userInfo;
    var chatCollection = {};
    var connected = false;
    $scope.global = GlobalFactory;

    $scope.isAdvisor = false;
    $scope.typing = false;
    $scope.sender = true;
    $scope.today = new Date().toDateOnlyFormat();
    $scope.isLoading = false;
    var clientData; //= localStorageService.get('clientInfo');
    UserProfileService.fetchUserProfile().then(function (data) {             
        clientData = data;
        console.log('$scope.clientInfo', data);
        socketprogram(); 

        $timeout(function() {
            socketprogram(); 
        }, 1000);        
        console.log('after program');
    }, function (error) {
        console.log('error', error);

    });
    //socketprogram();


    function socketprogram() {
        console.log("yeah i am called");
        $scope.$on('loadMessages', function (event, val) {
            //console.log('load scroll', $scope.isLoading);          
            $scope.loadOldMessages();           
        });

        $scope.disableChatInput = true;

        //Add colors
        var COLORS = [
            '#dee0e2', '#8374b5'
        ];

        //initializing messages 
        $scope.global.messages = {}

        if (!angular.isUndefinedOrNull(clientData)) {
            // $scope.advisorAvatar = localStorageService.get('clientInfo').advisor.avatar;
            $scope.advisorAvatar = clientData.advisor.advisorAvatarCompleteUrl;
        }

        socket.on('connect', function () {
            //console.log('socket inside');
            connected = true;
            // On login display welcome message
            socket.on('login', function (data) {
                CLIENT_CHAT_INDEX = 1;
                //Set the value of connected flag
                $scope.connected = true
                $scope.number_message = message_string(data.numUsers.totAdvisors + data.numUsers.totClients)
                if (!$scope.isAdvisor) {
                    // load initial chat list for client
                    getClientChatHistory(userInfo.senderId, userInfo.email, CLIENT_CHAT_INDEX, MESSAGES_LENGTH);
                }
            });

            //Whenever the server emits 'new message', update the chat body
            socket.on('new_message', function (data) {
                if (data.message && data.username) {
                    addMessageToList(data.username, true, data.message, data.date, false)
                }
                if (!angular.isUndefinedOrNull(data.messageId)) {
                    updateMesssageStatus([data.messageId], data.userId, userInfo.myAdvisorEmail);
                }
            });

            // Whenever the server emits 'user joined', log it in the chat body
            socket.on('user joined', function (data) {
                addMessageToList("", false, data.username + " joined")
                addMessageToList("", false, message_string(data.numUsers))
            });

            // Whenever the server emits 'user left', log it in the chat body
            socket.on('user left', function (data) {
                addMessageToList("", false, data.username + " left")
                addMessageToList("", false, message_string(data.numUsers))
            });

            //Whenever the server emits 'typing', show the typing message
            socket.on('typing', function (data) {
                addChatTyping(data);
            });

            // Whenever the server emits 'stop typing', kill the typing message
            socket.on('stop typing', function (data) {
                removeChatTyping(data.username);
            });

            // Whenever the server emits 'userIsTyping',notify to advisor that client is typing
            socket.on('userIsTyping', function (msg, userId) {
                if (!$scope.isAdvisor)
                    $scope.typing = true;

                $scope.typingText = msg;
                $timeout(stopTyping, 1000);
            });

            // push client chat history from server
            socket.on('pushClientChatHistory', function (chatList) {
                populateChatListForClient(chatList);
            });

            // push ClientList
            socket.on('pushClientList', function (clientList) {
                globalService.clientList = clientList;
                $rootScope.$broadcast("clientListUpdated");
            });

            // on client is online
            socket.on('clientOnline', function (clientId) {
                $rootScope.$broadcast("clientOnline", clientId);
            });

            // on client is offline
            socket.on('clientOffline', function (clientId) {
                $rootScope.$broadcast("clientOffline", clientId);
            });

            // on updateUnreadMsgCount
            socket.on('updateUnreadMsgCount', function (numOfMsgUpdated, clientId) {
                $rootScope.$broadcast("updateUnreadMsgCount", numOfMsgUpdated, clientId);
            });

            // increase UnreadMsgCount on new message push from server
            socket.on('increaseUnreadMsgCount', function (clientId) {
                $rootScope.$broadcast("increaseUnreadMsgCount", clientId);
            });
        });

        //  	// function called when user hits the send button
        $scope.sendMessage = function () {        
            window.scrollTo(0,document.body.scrollHeight);    
            if (angular.isUndefinedOrNull($scope.message))
                return;

            var currentTime = new Date();
            // setClientInfo();
            addMessageToList(userInfo.email, true, $scope.message, currentTime, true);
            sendChatMessageToServer($scope.message, currentTime);
            socket.emit('stop typing');
            $scope.message = "";
        };

        // function called on Input Change
        $scope.updateTyping = function () {
            sendUpdateTyping();
        };

        // load more messages while scroll down
        $scope.loadOldMessages = function () {
            console.log("loadoldmessage called");
            getClientChatHistory(userInfo.senderId, userInfo.email, CLIENT_CHAT_INDEX, MESSAGES_LENGTH);
        };

        // Display message by adding it to the message list
        function addMessageToList(username, style_type, message, currentTime, isSender) {
            username = $sanitize(username);
            var currentDate = new Date().toDateOnlyFormat();
            //removeChatTyping(username);
            var color = style_type ? getUsernameColor(username) : null;
            var chatObj = { content: $sanitize(message), style: style_type, username: username, color: color, date: currentTime, isSender: isSender }
            // if current user is advisor then seperate the messages based on selected client
            if ($scope.isAdvisor) {
                //chatCollection[userInfo.selectedUserId].messages.push({content:$sanitize(message),style:style_type,username:username,color:color, date : currentTime, isSender: isSender});
                if (chatCollection[userInfo.selectedUserId].messages.hasOwnProperty(currentDate)) {
                    chatCollection[userInfo.selectedUserId].messages[currentDate].messages.push(chatObj);
                }
                else {
                    chatCollection[userInfo.selectedUserId] = {
                        date: currentDate,
                        messages: [chatObj]
                    }
                    $scope.messages = chatCollection[userInfo.selectedUserId].messages;
                }
            }
            else {
                //$scope.global.messages.push({content:$sanitize(message),style:style_type,username:username,color:color, date : currentTime, isSender: isSender});
                if ($scope.global.messages.hasOwnProperty(currentDate)) {
                    $scope.global.messages[currentDate].messages.push(chatObj);
                }
                else {
                    $scope.global.messages[currentDate] = {
                        date: currentDate,
                        messages: [chatObj]
                    };
                }
            }
        };

        // push message for advisor coming from server 
        function pushMessageFromServer(userId, username, message, chatTime, messageId) {
            if (!angular.isUndefinedOrNull(chatCollection[userId])) {
                var key = new Date(chatTime).toDateOnlyFormat();
                if (angular.isUndefinedOrNull(chatCollection[userId].messages[key])) {
                    chatCollection[userId].messages[key] = { messages: [] };
                }
                chatCollection[userId].messages[key].messages.push({ content: $sanitize(message), username: username, date: chatTime, isSender: false });
            }
        };

        //Generate color for the same user.
        function getUsernameColor(username) {
            // Compute hash code
            var hash = 7;
            for (var i = 0; i < username.length; i++) {
                hash = username.charCodeAt(i) + (hash << 5) - hash;
            }
            // Calculate color
            var index = Math.abs(hash % COLORS.length);
            return COLORS[index];
        };

        // Updates the typing event
        function sendUpdateTyping() {
            if (connected) {
                if (!typing) {
                    typing = true;
                    socket.emit('typing', userInfo);
                }
            }
            lastTypingTime = (new Date()).getTime();
            $timeout(function () {
                var typingTimer = (new Date()).getTime();
                var timeDiff = typingTimer - lastTypingTime;
                if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                    socket.emit('stop typing');
                    typing = false;
                }
            }, TYPING_TIMER_LENGTH)
        };

        // Adds the visual chat typing message
        function addChatTyping(data) {
            addMessageToList(data.username, true, " is typing");
        };

        // Removes the visual chat typing message
        function removeChatTyping(username) {
            $scope.global.messages = $scope.global.messages.filter(function (element) { return element.username != username || element.content != " is typing" })
        };

        // Return message string depending on the number of users
        function message_string(number_of_users) {
            return number_of_users === 1 ? "there's 1 participant" : "there are " + number_of_users + " participants"
        };

        // send new message to server
        function sendChatMessageToServer(message, msgTime) {
            socket.emit('ChatMessage', userInfo, message, msgTime);
        };

        function stopTyping() {
            $scope.typing = false;
        };

        function getUsername() {
            var clientInfo = clientData;// localStorageService.get('clientInfo');
            if (!angular.isUndefinedOrNull(clientInfo))
                return clientInfo.fullName;
        };

        // when user select the client add to chatCollection and with no messages
        function addClientToChatCollection(clientId) {
            if (angular.isUndefinedOrNull(chatCollection[clientId])) {
                chatCollection[clientId] = {
                    messages: {},
                    pageIndex: 1
                }
            }
        };

        // populate the message of selected client
        function populateChatListByClient(clientId) {
            $scope.global.messages = [];
            if (!angular.isUndefinedOrNull(chatCollection[clientId])) {
                if (chatCollection[clientId].messages) {//.length == 0){
                    //getAdvisorChatHistory(clientId, userInfo.email, chatCollection[clientId].pageIndex, MESSAGES_LENGTH);
                    getAdvisorChatHistory();
                }
                else {
                    $scope.global.messages = chatCollection[clientId].messages;
                }
            }
        };

        // update message status read to true when received messasge
        function updateMesssageStatus(messageIdArray, clientId, advisorEmail) {
            socket.emit('updateMessageStatus', messageIdArray, clientId, advisorEmail);
        };

        // populate chat list when client logged-in
        function getClientChatHistory(clientId, clientEmail, pageNumber, nPerPage) {
            //console.log("getclientchathfh", clientId, clientEmail, pageNumber, nPerPage);
            socket.emit('clientChatHistory', clientId, clientEmail, pageNumber, nPerPage);
        };

        // populate chat list when advisor logged-in & selected a client
        function getAdvisorChatHistory() {
            socket.emit('advisorChatHistory', userInfo.selectedUserId, userInfo.email, chatCollection[userInfo.selectedUserId].pageIndex, MESSAGES_LENGTH);
        };

        // populate chat list for client
        function populateChatListForClient(chatList) {
            $scope.isLoading = false;
            //console.log('Object.keys(chatList).length', Object.keys(chatList).length);
            //console.log('chatList', chatList);
            if (!angular.isUndefinedOrNull(chatList)) {//} && angular.isArray(chatList)){    
                if (Object.keys(chatList).length > 0) {
                    //console.log("gg", CLIENT_CHAT_INDEX, chatList);
                    CLIENT_CHAT_INDEX += 1;
                    $scope.global.messages = formatClientChatList(chatList);
                }
            }
        };

        function formatClientChatList(chatList) {
            var previousChat = $scope.global.messages;
            for (var key in chatList) {
                if (previousChat.hasOwnProperty(key)) {
                    var temp = [];

                    for (var j = 0; j < chatList[key].messages.length; j++) {
                        if (angular.isUndefinedOrNull(previousChat[key].messages[j]))
                            temp.push({ _id: chatList[key].messages[j]._id, content: $sanitize(chatList[key].messages[j].message), username: userInfo.username, date: chatList[key].messages[j].createdAt, isSender: userInfo.senderId == chatList[key].messages[j].receiverId ? false : true });

                        else if (previousChat[key].messages[j]._id !== chatList[key].messages[j]._id)
                            temp.push({ _id: chatList[key].messages[j]._id, content: $sanitize(chatList[key].messages[j].message), username: userInfo.username, date: chatList[key].messages[j].createdAt, isSender: userInfo.senderId == chatList[key].messages[j].receiverId ? false : true });
                    }
                    temp = temp.concat(previousChat[key].messages);
                    previousChat[key].messages = temp;
                }
                else {
                    var obj = { date: key, messages: [] };
                    for (var j = 0; j < chatList[key].messages.length; j++) {
                        obj.messages.push({ _id: chatList[key].messages[j]._id, content: $sanitize(chatList[key].messages[j].message), username: userInfo.username, date: chatList[key].messages[j].createdAt, isSender: userInfo.senderId == chatList[key].messages[j].receiverId ? false : true });
                    }
                    previousChat[obj.date] = obj;
                }
            }
            return previousChat;
        };

        // update status of unread messages to read
        function updateUnreadMsgStatus(chatList) {
            if (!angular.isUndefinedOrNull(chatList)) {
                var onlyUnreadMessageIds = [];
                for (var key in chatList) {
                    chatList[key].messages.map(function (obj) {
                        if (!obj.readStatus) onlyUnreadMessageIds.push(obj._id);
                    });
                }
                if (onlyUnreadMessageIds.length > 0)
                    updateMesssageStatus(onlyUnreadMessageIds, globalService.selectedClientToChat.id, userInfo.email);
            }
        };

        function initUserInfo() {
            //Add user
            userInfo = {
                userType: 'ROLE_CLIENT',//localStorageService.get('userType'),
                email: clientData.email,//localStorageService.get('email'),
                username: getUsername()
            }

            if (userInfo.userType === USER_TYPES[1]) {                   // client
                var clientInfo = clientData;// localStorageService.get('clientInfo');
                if (!angular.isUndefinedOrNull(clientInfo)) {
                    userInfo.myAdvisorEmail = clientInfo.advisor.email;
                    userInfo.senderId = clientInfo.id;
                    userInfo.myAdvisorId = clientInfo.advisor.id;
                }
            }
            socket.emit('joinChat', userInfo);
        };


        // initial
        function init() {
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 'slow');
            if ($scope.isAdvisor) {
                $scope.isClientSelected = false;
            }
            else {
                $scope.disableChatInput = false;
            }
            initUserInfo();
        };

        init();
    }
}
