(function () {
    angular.module("app.disclaimer")
        .controller('disclaimerController', ['$scope', '$state', 'GlobalFactory','$rootScope','OranjApiService', 'UserProfileService', '$location', disclaimerController])


        function disclaimerController($scope, $state, GlobalFactory, $rootScope, OranjApiService, UserProfileService, $location) {
                  

            var role = UserProfileService.fetchUserRole();
            role.then(
                function(userRole){
                    if(userRole.isClient) {
                       
                    } else if(userRole.isAdvisor) {
                        
                    } else if(userRole.isAdmin) {

                    }

                }, function(){
                    GlobalFactory.signout();
                }
            );

        $scope.signOut = function () {
            GlobalFactory.signout();
        };
        
       $scope.getDisclaimer = function(){
    	   OranjApiService('getFirmDisclaimer').then(function (response) {
       		if(response.status === 'success'){
       			$scope.disclaimer = response.data.disclaimer;
       		}else{
       			$scope.disclaimer = '';
       		}
       		
       	}, function (response) {

	        });
       }
        
        $scope.disclaimerCount = function (){
        	OranjApiService('disclaimerCount').then(function (response) {
        		if(response.status === 'success'){
        			if($rootScope.prevPath != null && $rootScope.prevPath != undefined && $rootScope.prevPath != '/page/disclaimer') {
               			$location.path($rootScope.prevPath);
               		}else {
               			$state.go('page.client.home.dash');
               		}
        		}else{
        			GlobalFactory.showErrorAlert('Unable to perform action');
        		}
        	}, function (response) {
        			GlobalFactory.showErrorAlert('Unable to perform action');
	        });
        }

    }
})();
