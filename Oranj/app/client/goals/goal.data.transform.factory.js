(function () {


    angular.module('app.client.goal')
        .factory('goalsDataTransformFactory', ['goalHeaderPieChartFactory', function (piechartdataFactory) {

            var GoalDashHeader = {
                totalNumberOfGoals: 0,
                totalNeeded: 0,
                totalHave: 0
            };

            var PRIORITIES = {
                VERY_IMPORTANT: {
                    rank: 1,
                    className: 'goal-very-important',
                    colorCode: '#12243F'
                },
                IMPORTANT: {
                    rank: 2,
                    className: 'goal-important',
                    colorCode: '#2F559C'
                },
                AVERAGE: {
                    rank: 3,
                    className: 'goal-average',
                    colorCode: '#5084C0'
                },
                BELOW_AVERAGE: {
                    rank: 4,
                    className: 'goal-below-average',
                    colorCode: '#45ABF0'
                },
                LEAST_IMPORTANT: {
                    rank: 5,
                    className: 'goal-least-important',
                    colorCode: '#79C9FF'
                }
            };

            function GoalGridRow(rawGridObject) {


                var priority = PRIORITIES[rawGridObject.priority];
                if (!rawGridObject.splashDate) {
                    console.log('no splash date');
                }
                return {
                    id: rawGridObject.id,
                    name: rawGridObject.name,
                    type: rawGridObject.type,
                    percentCompleted: rawGridObject.percentageCompleted,
                    currentNeeded: rawGridObject.currentNeeded,
                    currentSavings: rawGridObject.currentSavings,
                    neededAtTheEnd: rawGridObject.neededAtTheEnd,
                    priorityCode: rawGridObject.priority,
                    priority: priority.rank,
                    className: priority.className,
                    creationDate: rawGridObject.creationDate,
                    endDate: rawGridObject.splashDate,
                    colorCode: priority.colorCode,
                    accountsList : rawGridObject.accountDetail
                }
            }

            return {
                transformGoalsDashHeaderData: function (goalsList) {
                    var totalHave = 0,
                        totalNeeded = 0;

                    GoalDashHeader.totalNumberOfGoals = goalsList.length;
                    angular.forEach(goalsList, function (stat, index) {

                        totalNeeded = totalNeeded + parseFloat(stat.neededAtTheEnd);
                        totalHave = totalHave + parseFloat(stat.currentSavings);
                        //totalHave = stat.
                    });

                    GoalDashHeader.totalHave = totalHave;
                    GoalDashHeader.totalNeeded = totalNeeded;

                    return GoalDashHeader;
                },
                transformGoalHeaderPieChartOptions: function (totalHave, totalNeeded) {
                    return piechartdataFactory(totalHave, totalNeeded);

                },
                transformGoalGridData: function (rawGridRows) {
                    var goalsGridList = [];
                    angular.forEach(rawGridRows, function (rawRow) {
                        var goalGridRow = new GoalGridRow(rawRow);
                        goalsGridList.push(goalGridRow);
                    });

                    goalsGridList.sort(function (a, b) {
                        if (a.priority === b.priority) {
                            if (a.name.toLowerCase() < b.name.toLowerCase()) {
                                return -1;
                            } else if (a.name.toLowerCase() > b.name.toLowerCase()) {
                                return 1;
                            }

                            return 0;
                        } else if (a.priority < b.priority) {
                            return -1;
                        } else {
                            return 1;
                        }

                    });

                    return goalsGridList;
                },
                transformGoalChartData: function (rawGridRows, dateRange) {

                    var chartObject = {
                        labels: [],
                        goalsDashChartSeries: [],
                        tickPositions: [],
                        zones: []
                    };

                    angular.forEach(rawGridRows, function (rawRow, index) {
                        var creationDate = moment(rawRow.creationDate).startOf('day');

                        var endDate = moment(rawRow.endDate).endOf('day');
                        var now = moment().startOf('day');


                        var minDate = moment(dateRange.minDate).startOf('day');
                        var maxDate = moment(dateRange.maxDate).endOf('day');

                        if (endDate.isAfter(minDate) && creationDate.isBefore(maxDate)) {
                            var high, low;

                            high = endDate.isSameOrAfter(maxDate) ? maxDate.diff(now) : endDate.diff(now);
                            low = creationDate.isSameOrBefore(minDate) ? minDate.diff(now) : creationDate.diff(now);


                            var tooltipData = {
                                creationDate: creationDate,
                                endDate: endDate,
                                name: rawRow.name,
                                have: rawRow.currentSavings,
                                needed: rawRow.neededAtTheEnd,
                                completed: rawRow.percentCompleted

                            };

                            var goalColumn = {
                                low: low,
                                high: high,
                                name: rawRow.name,
                                percentCompleted: rawRow.percentCompleted,
                                id: index,
                                tooltip: tooltipData
                            };


                            // These will be on the vertical axis (xAxis) as graph is inverted
                            chartObject.labels.push(rawRow.name);

                            // Set percentage completed colored zone

                            var columnRange = high - low;
                            var zoneRange = (columnRange * (rawRow.percentCompleted / 100)) + low;

                            var zone = {
                                high: zoneRange,
                                low: low,
                                color: rawRow.colorCode,
                                tooltip: tooltipData
                            };

                            chartObject.goalsDashChartSeries.push(goalColumn);

                            chartObject.zones.push(zone);

                        } else {
                             console.log('end date was before min date endDate , minDate, start date: '+endDate.format('MMM d, YY')+' : '+minDate.format('MMM d, YY')+' : '+creationDate.format('MMM d, YY'));
                        }
                    });

                    var tickPositions = [];
                    var current = moment().startOf('day');
                    var minDate = moment(dateRange.minDate).startOf('day');
                    var maxDate = moment(dateRange.maxDate).endOf('day');

                    var duration = moment.duration(maxDate.diff(minDate)),
                        additionFactor = 'months';
                    var interval = Math.floor(duration / 12),
                        numberOfTicks = 12;

                    //if year is 1 or more
                    if (Math.floor(duration.asYears()) >= 2) {
                        interval = Math.floor(duration.asMonths() / numberOfTicks);
                        additionFactor = 'months';
                    } else if (Math.floor(duration.asYears()) >= 1) {
                        interval = 1;
                        numberOfTicks = Math.floor(duration.asMonths());
                        additionFactor = 'months';
                    } else if (Math.floor(duration.asMonths()) >= 1) {
                        //if less than 12 months
                        numberOfTicks = Math.floor(duration.asWeeks());
                        interval = 1;
                        additionFactor = 'weeks';
                    } else if (Math.floor(duration.asDays()) >= 1) {
                        //if less than 30 days
                        interval = 1;
                        numberOfTicks = duration.asDays();
                        additionFactor = 'days';
                    }


                    //console.log(interval + ' in unit of : ' + additionFactor + ' : number of ticks : ' + numberOfTicks);
                    //tickPositions.push(minDate.diff(current));

                    for (var i = 0; i < (numberOfTicks - 1); i++) {
                        minDate = moment(dateRange.minDate).startOf('day');
                        var actualTick = minDate.add(interval * i, additionFactor);
                        if (actualTick.isAfter(current) && moment(tickPositions[i - 1] + current.valueOf()).isBefore(current)) {
                            tickPositions.splice(i - 1, 1, 0);
                        }
                        tickPositions.push(actualTick.diff(current));
                    }


                    tickPositions.push(maxDate.diff(current));
                    //  }

                    console.log(tickPositions);

                    chartObject.tickPositions = tickPositions;
                    return chartObject;
                }
            }
        }]);
})();
