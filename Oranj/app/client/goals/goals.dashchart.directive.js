(function () {

    angular.module('app.client.goal')
        .directive('goalsDashChart', function () {
            return {
                scope: {
                    chartOptions: '='
                },
                link: function (scope, ele) {
                    var chart = null;
                    scope.$watch('chartOptions', function (n, o) {
                        if (n) {
                            /*if (chart) {
                                //
                                chart.yAxis[0].update(n.yAxis, false);
                                chart.series[0].setData(n.series[0].data, false);
                                chart.series[1].setData(n.series[1].data, false);
                                //chart.redraw();
                                chart.setSize(chart.containerWidth, n.chart.height);
                            } else {*/
                                chart = Highcharts.chart(ele[0], scope.chartOptions);
                            //}
                        }
                    });
                }
            };
        });
})();
