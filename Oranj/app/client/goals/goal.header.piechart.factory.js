(function () {

    var chartOptions = {
        chart: {
            type: 'pie',
            height: 100,
            backgroundColor: '#29303d',
            spacing: [0, 0, 0, 0],
            shadow: false
        },
        plotOptions: {
            pie: {
                innerSize: 70,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        title: {
            text: '',
            verticalAlign: 'middle',
            style: {
                color: '#fff',
                fontSize: 15
            }
        }

    };

    angular.module('app.client.goal')
        .factory('goalHeaderPieChartFactory', function () {


            return function (totalHave, totalNeeded) {

                //To fix generating NaN on dividing with 0.
                if(totalNeeded ==  0) {
                    totalNeeded = 1;
                }

                chartOptions.title.text = parseInt(((totalHave/totalNeeded)*100))+ '%';
                chartOptions.series = [{
                    data: [{
                        color: '#969696',
                        y: (totalNeeded-totalHave),
                    }, {
                        color: '#36B8B7',
                        y: totalHave
                    }],
                    dataLabels: {
                        enabled: false
                    }
                }];


                return chartOptions;
            }
        })
})
();
