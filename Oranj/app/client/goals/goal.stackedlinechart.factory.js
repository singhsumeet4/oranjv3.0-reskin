(function () {

    angular.module('app.client.goal').factory('individualGoalDashChartFactory', ['dateFilter','currencyFilter', function (dateFilter, currencyFilter) {

        var contributionTotalPerDay = {};

        function findAccount(accountList, accountId) {
            for(var i=0;i<accountList.length; i++) {
                if(accountList[i].accountId == accountId) {
                  return accountList[i];
                }
            }
        }



        return {
            getIndividualGoalChart: function (historicalData, accountList, goalDetails) {

                var series = [];
                angular.forEach(historicalData, function (dataArr, accountId) {
                    var data = [];
                    var account = findAccount(accountList, accountId);

                    angular.forEach(dataArr, function (point, key) {

                        if(accountId != 0) {
                            var pointDate = moment(point.updateDate).startOf('day').format('MM/DD/YYYY');
                            if (contributionTotalPerDay[pointDate]) {
                                contributionTotalPerDay[pointDate] += point.currentSavings;
                            } else {
                                contributionTotalPerDay[pointDate] = point.currentSavings;
                            }
                        }

                        data.push({
                            y: point.currentSavings,
                            x: point.updateDate,
                            marker: false
                        })
                    });

                    var seriesObj = {
                        name: accountId,
                        data: data,
                        trackByArea: true,
                        zoneAxis: 'x',
                        lineWidth: 2,
                        tooltip: {
                            headerFormat: '<table>',
                            footerFormat: '</table>'
                        }
                    };

                    if (accountId == 0) {
                        seriesObj.type = 'line';
                        seriesObj.color = 'black';
                        seriesObj.dashStyle = 'dash';

                        seriesObj.tooltip.pointFormatter = function() {
                            //console.log(this);
                            return '<tr><td colspan="2">'+dateFilter(this.x, "MMM dd, yyyy")+'</td></tr>' +
                                '<tr><td>Need: </td><td>'+currencyFilter(this.y)+'</td></tr>' +
                                '<tr><td>Have: </td><td>'+currencyFilter(contributionTotalPerDay[moment(this.x).startOf('day').format('MM/DD/YYYY')])+'</td></tr>';
                        };

                    } else {
                        seriesObj.type = 'area';
                        seriesObj.zones = [{
                            value: moment(new Date()).startOf('day')
                        }, {
                            dashStyle: 'dash'
                        }];

                        seriesObj.tooltip.pointFormatter = function() {
                            return '<tr><td>'+account.accountName+': </td><td>'+currencyFilter(this.y)+'</td></tr>';
                        }
                    }


                    series.push(seriesObj);

                });

                return {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: "datetime",
                        title: {
                            text: ''
                        },
                        labels: {
                            enabled: false
                        },
                        tickInterval: 3600
                    },
                    yAxis: {
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: ''
                        },
                        gridLineWidth: 0,
                        tickWidth: 1,
                        tickInterval: 10
                    },
                    plotOptions: {
                        area: {
                            stacking: 'normal',
                            lineColor: '#666666',
                            lineWidth: 1,
                            marker: {
                                lineWidth: 1,
                                lineColor: '#666666'
                            }
                        },
                        line: {
                            stacking: null
                        }
                    },
                    series: series,
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        useHTML: true,
                        borderWidth: 0,
                        style: {color: '#fff'},
                        shared: true,
                        split: true,
                        distance: 10,
                        shadow: false,
                        backgroundColor: 'rgba(0,0,0,0.9)',
                        borderColor: 'rgba(0,0,0,0.9)',
                    }
                };
            }
        }
    }]);
})();
