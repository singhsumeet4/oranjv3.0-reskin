(function () {
    'use strict';


    angular.module('app.client.goal')
        .controller('goalDashboardCtrl', ['goalDataService', 'goalsStackedChart', '$scope','$mdDialog', '$rootScope','GlobalFactory', '$state', '$stateParams',
            goalDashboardCtrl]);

    function goalDashboardCtrl(goalDataService, goalsStackedChart, $scope, $mdDialog, $rootScope, GlobalFactory, $state, $stateParams) {
        var ctrl = this;

        ctrl.chartType = 'horizontalBar';
        ctrl.range = {
            minDate: moment().subtract(6, 'months').startOf('month'),
            maxDate: moment().endOf('year')
        };

        ctrl.rangeLimit = {
            limitMaxDate: moment().subtract(6, 'months').startOf('month'),
            limitMinDate: moment()
        };

        ctrl.rangeInit = {
            minDate: moment().subtract(6, 'months').startOf('month'),
            maxDate: moment().endOf('year')
        };

        // Code to handle advisor's view of client's goals dahsboard
        ctrl.isAdvisorView = false;
        if($state.includes('page.advisor')) {
            ctrl.isAdvisorView = true;
            ctrl.userName = $stateParams.name;
            ctrl.id = $stateParams.id;
        }

        goalDataService.getGoalsStats().then(function (goalDashObject) {
            ctrl.goalDashHeader = goalDashObject.goalDashHeader;
            ctrl.goalDashHeaderPieChart = goalDashObject.goalDashHeaderPieChart;
            ctrl.goalsDashGrid = goalDashObject.goalsDashGrid;
            ctrl.goalAchievementDate = moment(goalDashObject.goalAchievementDate).format('MMMM YYYY');


            ctrl.chartOptions = goalsStackedChart.getGoalsChart(ctrl.goalsDashGrid, ctrl.range, true);


            $scope.$watch(function () {
                return ctrl.range;
            }, function (n, o) {
                if (n !== o ) {
                    ctrl.rangeLimit.limitMaxDate = moment(ctrl.range.minDate, 'MMMM DD, YYYY');
                    ctrl.rangeLimit.limitMinDate = moment(ctrl.range.maxDate, 'MMMM DD, YYYY');
                    ctrl.chartOptions = goalsStackedChart.getGoalsChart(ctrl.goalsDashGrid, ctrl.range, false);
                }
            }, true);
        });


        ctrl.deleteGoal = function(goal) {
             var confirmation = $mdDialog.confirm({
                 title: 'Are you sure?',
                 htmlContent: '<div> You would like to delete your '+goal.type+' goal called "' +goal.name+'" ?',
                 ok: 'Delete',
                 cancel : 'Cancel'
             });

             $mdDialog.show(confirmation).then(function(answer) {
            	goalDataService.deleteGoal(goal.id).then(function(goalDashObject) {
            		if(goalDashObject.status === 'success'){
                    	GlobalFactory.showSuccessAlert('Your '+goal.type+' goal called "'+goal.name+'" has been successfully deleted.');
                    }else if(goalDashObject.status === 'error' && goalDashObject.message === 'Could not delete goal '){
                    	GlobalFactory.showErrorAlert('This goal no longer exists.');
                    }else{
                    	GlobalFactory.showErrorAlert('There was an issue deleting your '+goal.type+' goal called "'+goal.name+'", please try again later.');
                    }
            		refreshGoal();
            	});

             });

        };

        var refreshGoal = function (){
        	goalDataService.fetchGoalsStats().then(function(goalDashObject) {
                $rootScope.$broadcast('updateGoalsList',  goalDashObject.goalsDashGrid);
                ctrl.goalDashHeader = goalDashObject.goalDashHeader;
                ctrl.goalDashHeaderPieChart = goalDashObject.goalDashHeaderPieChart;
                ctrl.goalsDashGrid = goalDashObject.goalsDashGrid;
                ctrl.goalAchievementDate = moment(goalDashObject.goalAchievementDate).format('MMMM YYYY');
                ctrl.chartOptions = goalsStackedChart.getGoalsChart(ctrl.goalsDashGrid, ctrl.range, true);
            });
        };


    }
})();
