(function () {
    'use strict';

    angular.module('app.client.goal')
        .controller('goalsClientController', ['goalDataService','$rootScope','$state','$stateParams',
            function (goalDataService, $rootScope, $state, $stateParams) {


                if($state.includes('page.advisor')) {
                    goalDataService.fetchGoalsStats($stateParams.id);
                } else  {
                    goalDataService.fetchGoalsStats().then(function(goalDashObject) {
                        $rootScope.$broadcast('updateGoalsList',  goalDashObject.goalsDashGrid);
                    });
                }
        }]);
})();
