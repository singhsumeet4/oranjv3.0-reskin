(function () {
    angular.module('app.client.goal')
        .factory('goalsStackedChart', ['goalsDataTransformFactory','$filter', function (goalsDataTransformFactory, $filter) {
            return {
                getGoalsChart: function (chartGridRows, dateRange, init) {

                    var chartObject = goalsDataTransformFactory.transformGoalChartData(chartGridRows, dateRange, init);

                    var MIN_CHART_HEIGHT = 300;

                    var calChartHeight = (chartObject.goalsDashChartSeries.length * 35 + 100);
                    var chartOptions = {
                        yAxis: {
                            labels: {
                                formatter: function () {
                                    if (this.value === 0) {
                                        return '<strong style="font-size: 15px;font-weight: bold;">Today</strong>';
                                    }
                                    var time = this.value + moment().startOf('day').valueOf();
                                   return '<span style="font-size: 12px;">' + moment(time).format("MMM D, YY") + '</span>';
                                    //return time;
                                }
                            },
                            tickPositions: chartObject.tickPositions,
                            title: false,
                            gridLineWidth: 0,
                            lineWidth: 1,
                            tickWidth: 1,
                            tickLength: 5,
                            gridLineColor: '#F5F6F7',
                            plotLines: [{
                                color: 'black',
                                value: 0,
                                width: 3,
                                zIndex: 10
                            }],
                            offset: 0
                        },
                        xAxis: {
                            visible: false,
                            categories: chartObject.labels
                        },
                        chart: {
                            type: 'bar',
                            height: calChartHeight > MIN_CHART_HEIGHT ? calChartHeight : MIN_CHART_HEIGHT
                        },
                        series: [{
                            type: 'columnrange',
                            data: chartObject.goalsDashChartSeries
                        }, {
                            type: 'columnrange',
                            data: chartObject.zones,
                            dataLabels: {
                                enabled: false
                            },
                            minPointLength: 0
                        }],
                        credits: {
                            enabled: false
                        },
                        plotOptions: {
                            columnrange: {
                                borderColor: '#969696',
                                color: '#F5F6F7',
                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        if (this.y === this.point.low) {
                                            //Return percentage completed
                                            return this.point.percentCompleted + '%';
                                        } else {
                                            return this.point.name;
                                        }
                                    },
                                    crop: false,
                                    reserveSpace: true,
                                    inside: false
                                },
                                pointWidth: 35,
                                pointPadding: 0,
                                groupPadding: 0,
                                minPointLength: 10,
                                pointPlacement: 0,
                                stacking: 'normal',
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        title: {
                            text: 'GOALS BREAKDOWN',
                            align: 'left'
                        },
                        tooltip: {
                            backgroundColor: 'rgba(0,0,0,0.9)',
                            borderColor: 'rgba(0,0,0,0.9)',
                            headerFormat: '<table>',
                            footerFormat: '</table>',
                            pointFormatter: function() {
                              return '<tr><td><strong>Goal:</strong> </td><td>'+this.tooltip.name+'</td></tr>' +
                                '<tr><td><strong>Start Date: </strong></td><td>'+this.tooltip.creationDate.format('MM/D/YY')+'</td></tr>' +
                                  '<tr><td><strong>End Date: </strong></td><td>'+this.tooltip.endDate.format('MM/D/YY')+'</td></tr>' +
                                  '<tr><td><strong>Have:</strong> </td><td>'+$filter('currency')(this.tooltip.have)+'</td></tr>' +
                                  '<tr><td><strong>Need: </strong></td><td>'+$filter('currency')(this.tooltip.needed)+'</td></tr>'+
                                  '<tr><td><strong>Completed: </strong></td><td>'+this.tooltip.completed+' %</td></tr>'
                            },
                            useHTML: true,
                            style: {
                                color: '#fff'
                            }
                        }
                    };
                    return chartOptions;

                }
            }
        }])
})();
