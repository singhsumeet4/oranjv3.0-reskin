(function () {

    angular.module('app.client.goal').factory('goalDataService', ['OranjApiService', '$q', 'goalsDataTransformFactory', goalDataService]);

    function goalDataService(OranjApiService, $q, goalsDataTransformFactory) {
        var defer = $q.defer();
        return {
            fetchGoalsStats: function (id) {
                defer = $q.defer();
                var apiCall;

                if (id) {
                    apiCall = OranjApiService('getGoalStatsForUser', {}, {userId: id});
                } else {
                    apiCall = OranjApiService('getGoalStats');
                }

                apiCall.then(function (res) {
                        if (res.status === 'success') {
                            var data = res.data.goals;
                            //var goalAchievementDate = res.data['project-goal-achievement-date'];
                            var goalHeader = goalsDataTransformFactory.transformGoalsDashHeaderData(data);
                            var goalDashHeaderPieChart = goalsDataTransformFactory.transformGoalHeaderPieChartOptions(goalHeader.totalHave, goalHeader.totalNeeded);
                            var goalDashGridRows = goalsDataTransformFactory.transformGoalGridData(data);

                            var goalDashObj = {
                                goalDashHeader: goalHeader,
                                goalDashHeaderPieChart: goalDashHeaderPieChart,
                                goalsDashGrid: goalDashGridRows
                            };
                            defer.resolve(goalDashObj);
                        } else {
                            defer.reject();
                        }
                    },
                    function () {
                        defer.reject()
                    });

                return defer.promise;
            },
            getGoalsStats: function () {
                return defer.promise;
            },
            getGoalData: function (id) {
                var goalInfoDefer = $q.defer();

                this.getGoalsStats().then(function (goalDashObj) {
                    angular.forEach(goalDashObj.goalsDashGrid, function (goalRow) {
                        if (goalRow.id == id) {
                            var goalDashHeaderPieChart = goalsDataTransformFactory.transformGoalHeaderPieChartOptions(goalRow.currentSavings, goalRow.neededAtTheEnd);
                            goalInfoDefer.resolve({goalDetail: goalRow, pieChartConfig: goalDashHeaderPieChart});
                        }
                    });
                });

                return goalInfoDefer.promise;

            },
            getGoalLinkedAccounts: function (id, userId) {
                var goalLinkedAccntsQ = $q.defer(), apiCall;

                //UserProfileService.fetchUserProfile().then(function (userProfile) {
                if (userId) {
                    apiCall = OranjApiService('getGoalAccountsClientAdvisor', {}, {id: id, userId: userId});
                } else {
                    apiCall = OranjApiService('getGoalAccountsClient', {}, {id: id});
                }

                apiCall.then(function (res) {
                    if (res.status === 'success') {
                        goalLinkedAccntsQ.resolve(res.data['goal-accounts']);
                    } else {
                        goalLinkedAccntsQ.reject();
                    }
                });
                //});
                return goalLinkedAccntsQ.promise;
            },
            getGoalHistoricalData: function (id, dateRange, userId) {
                var startDate = moment(dateRange.minDate).format('YYYY-MM-DD');
                var endDate = moment(dateRange.maxDate).format('YYYY-MM-DD');


                var goalHistoricalContribQ = $q.defer(), apiCall;

                if (userId) {
                    apiCall = OranjApiService('getGoalContributionHistoryAdvisor', {}, {
                        startDateParam: startDate,
                        endDateParam: endDate,
                        goalId: id,
                        userId: userId
                    });
                } else {
                    apiCall = OranjApiService('getGoalContributionHistory', {}, {
                        startDateParam: startDate,
                        endDateParam: endDate,
                        goalId: id
                    });
                }

                apiCall.then(function (res) {
                    goalHistoricalContribQ.resolve(res.data);
                });


                return goalHistoricalContribQ.promise;

            },
            deleteGoal: function (id) {
                return OranjApiService('goalDelete', {}, {goalId: id});
            }
        };
    }
})();
