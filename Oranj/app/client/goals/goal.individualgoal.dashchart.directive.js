(function () {

    angular.module('app.client.goal')
        .directive('goalAccountHistoryChart', function () {
            return {
                scope: {
                    chartOptions: '=',
                    setAccountColors: '&'
                },
                link: function (scope, ele) {
                    scope.$watch('chartOptions', function (n, o) {
                        if (n) {
                            // if (chart) {
                            //      chart.series = n.series;
                            //     // chart.series[1].setData(n.series[1].data, false);
                            //     chart.redraw();
                            //     //chart.setSize(chart.containerWidth, chart.height);
                            // } else {
                            var chart = Highcharts.chart(ele[0], scope.chartOptions);
                            scope.setAccountColors({chartSeries:chart.series});
                            //}
                        }
                    });
                }
            };
        });
})();
