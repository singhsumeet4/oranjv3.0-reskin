(function() {
   
    angular.module('app.client.goal')
        .directive('goalPieChart', function() {
     
        return {
            scope: {
                chartOptions: '=goalPieChart'
            },
            link: function(scope,ele) {
                var chart = null;
                
                scope.$watch('chartOptions', function (n, o) {
                    if (n) {
                        if (chart) {
                        } else {
                            chart = Highcharts.chart(ele[0], scope.chartOptions);
                        }
                    }
                });
            }
        }
    })
})();
