(function () {

    angular.module('app.client.goal')
        .controller('individualGoalCtrl', ['goalDataService', '$stateParams', 'individualGoalDashChartFactory', '$scope', '$state',
            function (goalDataService, $stateParams, individualGoalDashChartFactory, $scope, $state) {
                var ctrl = this;

                /* Goal name for breadcrumb */
                ctrl.goalName = $stateParams.goalName;

                ctrl.chartType = 'horizontalBar';
                ctrl.range = {
                    minDate: moment().subtract(6, 'months').startOf('month'),
                    maxDate: moment().endOf('year')
                };

                ctrl.rangeLimit = {
                    limitMaxDate: moment().subtract(6, 'months').startOf('month'),
                    limitMinDate: moment()
                };

                ctrl.rangeInit = {
                    minDate: moment().subtract(6, 'months').startOf('month'),
                    maxDate: moment().endOf('year')
                };


                ctrl.sliderOptions = {
                    floor: 0,
                    ceil: 100,
                    readOnly: true,
                    hideLimitLabels: true,
                    translate: function (value, sliderId, label) {
                        return value + ' %';
                    }
                };

                ctrl.setAccountColors = function (chartSeries) {
                    angular.forEach(chartSeries, function (seriesObj, key) {
                        angular.forEach(ctrl.accountsList, function (account, index) {
                            if (account.accountId == seriesObj.name) {
                                account.color = seriesObj.color;
                            }
                        })
                    })
                };



                goalDataService.getGoalData($stateParams.goalId).then(function (goalDetail) {

                    //console.log(goalDetail);
                    ctrl.goalDetail = goalDetail.goalDetail;
                    ctrl.goalHeaderPieChart = goalDetail.pieChartConfig;

                    //stateParams.id is present when the individual goal view is opened from the Advisor's End.
                    goalDataService.getGoalLinkedAccounts($stateParams.goalId, $stateParams.id).then(function (accountsList) {
                        ctrl.accountsList = accountsList;

                        goalDataService.getGoalHistoricalData($stateParams.goalId, ctrl.range, $stateParams.id).then(function (goalAccountsHistoricalData) {
                            ctrl.accountContributionChartOptions = individualGoalDashChartFactory
                                .getIndividualGoalChart(goalAccountsHistoricalData, ctrl.accountsList, ctrl.goalDetail);

                            $scope.$watch(function () {
                                return ctrl.range;
                            }, function (n, o) {
                                if (n !== o) {
                                    ctrl.rangeLimit.limitMaxDate = moment(ctrl.range.minDate, 'MMMM dd, yyyy');
                                    ctrl.rangeLimit.limitMinDate = moment(ctrl.range.maxDate, 'MMMM dd, yyyy');

                                    goalDataService.getGoalHistoricalData($stateParams.goalId, ctrl.range, $stateParams.id).then(function (goalAccountsHistoricalData) {
                                        ctrl.accountContributionChartOptions = individualGoalDashChartFactory.getIndividualGoalChart(goalAccountsHistoricalData);
                                    });
                                }
                            }, true);

                        });


                    });


                });


            }]);

})();
