angular.module("app.clientDash", [])

.controller("networthCtrl", function($scope, linechartObjArray, jQBarChart, piechartdata, OranjApiService, $filter, $q, $timeout) {

    //netwoth 1 chart start

    //Bar Cahrt

    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100)
    };

    $scope.date = {
        date1: 'March 21,2016',
        date2: 'April 21,2016'
    }

    function initChartData() {

        promiseA = OranjApiService('getAssets'); //Assets
        promiseB = OranjApiService('getLiabilities'); //Liabilities
        promiseC = OranjApiService('getNetworthTotal'); //Networth

        $q.all([promiseA, promiseB, promiseC])
            .then(function(results) {
                $scope.totalAssets = results[0].data.assets;
                $scope.totalLiabilities = results[1].data.liabilities;
                $scope.totalNetworth = results[2].data.totalNetworth;
                // createNetworthChartData();

                $scope.totalAssetsFormat = $filter('currency')($scope.totalAssets, '$', 2);
                $scope.totalLiabilitiesFormat = $filter('currency')($scope.totalLiabilities, '$', 2);
                $scope.totalNetworthFormat = $filter('currency')($scope.totalNetworth, '$', 2);
            });
    }


    // TODAY'S NET WORTH BREAKDOWN Start

     var yAxisCount = 2;
   var chartFun= (function () {
      $('#netbreakdown').highcharts({
        chart: {
          type: 'bar',
          // spacingRight: 60,
        },
        title: {
          text: null
        },

        xAxis:
        {
          lineWidth: 0,
          minorGridLineWidth: 0,
          minorTickLength: 0,
          tickLength: 0,
          categories: ['Assets', 'Liabilities', 'Net Worth'],
          align: 'left',

          labels: {
            rotation: 0,
            style: {
              fontSize: '16px',
              fontFamily: 'Open Sans',
              color: '#000000',
            },

            useHTML: true,
            formatter: function () {
              return '<div class="label">' + this.value + '</div>';
            }
          },
          position: {
            align: 'left'
          },

          title: {
            text: null
          },
        },

        yAxis: {
          value: '',
          min: 0,
          max: 1000000,
          tickInterval: 100000,
          gridLineWidth: 0,
          lineWidth: 0,
          tickLength: 10,
          tickWidth: 1,
          title: {
            text: '',
            align: 'high'
          },
          labels: {
            rotation: 0,
            step: 2,
            style: {
              fontSize: '10px',
              fontFamily: 'Open Sans',
              fontStyle: 'italic',

            },

            formatter: function () {
              var data = $filter('currency')(this.value, this.value === 0?'':'$', 2);
              return data;
            },

          }
        },
        colors: [
          '#2f4e9f',
          '#11bfe0',
          '#40b449'
        ],

        tooltip: {
           backgroundColor: '#191919',
          // forecolor: '#ffffff',
           borderColor: 'black',
           borderRadius: 10,
          //  shared: true,
          // borderWidth: 3,
          formatter: function () {
            var data = this.point.liabilities ?  ("(" + $filter('currency')(this.y, '$', 2) + ")") : $filter('currency')(this.y, '$', 2);
            return '<div><div style="color:'+ (this.color) +'">\u2b1b</div><span style="color: #ffffff; font-Size: 10px;">' + data + '</span></div>';
              // return '<div><span style="color:{point.color}"></span><span style="color:#ffffff">' + data + '</span></div>';U+2B1B
          },
           positioner: function(labelWidth, labelHeight, point) {         
                         var tooltipX, tooltipY;
                            if (point.plotX + labelWidth > this.chart.plotWidth) {
                                tooltipX = point.plotX + this.chart.plotLeft - labelWidth - 40;
                            } else {
                                tooltipX = point.plotX + this.chart.plotLeft - 40;
                            }
                            tooltipY = point.plotY + this.chart.plotTop - 60;
                            return {
                                x: tooltipX,
                                y: tooltipY
                            };       
                    }
        },

        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true,
              inside: true,
              align: 'left',
              x: 240,
              style: {
                fontSize: '15px',
                fontFamily: 'Open Sans',
                color: '#808080',
              },
              formatter: function () {
                var data = this.point.liabilities ? "(" + $filter('currency')(this.y, '$', 2) + ")" : $filter('currency')(this.y, '$', 2);
                return '<span style="position:absolute;right:0px;display:block !important;">' + data + '</span>';
              },

            }
          }
        },

        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          enabled: false,
          backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
          shadow: true
        },
        credits: {
          enabled: false
        },
        series: [{
          maxPointWidth: '30',
          //  pointInterval: '100000' ,
          // pointPadding: 0,
          name: '',
          data: [
            { y: 600000, color: '#2f4e9f', liabilities: false },
            { y: 500000, color: '#11bfe0', liabilities: true },
            { y: 100000, color: '#40b449', liabilities: false },
          ],
          useHTML: true,
          formatter: function () {
            return '<div class="label2">' + this.value + '</div>';
          }
        }]
      });
    });
 //chartFun();
    $timeout(function () {
     chartFun();
    }, 500);


    function dateRange(range1, range2) {
        var fstDate = new Date(range1);
        var sndDate = new Date(range2);
        var dateArray = new Array();
        var counterDate = fstDate.getDate();
        var evenFlag = false;
        if ((counterDate % 2) === 0) {
            evenFlag = true;
        }
        var count = 0;
        while (fstDate <= sndDate) {

            dateArray.push(angular.copy(fstDate));
            counterDate = counterDate + 2;
            if (counterDate > 30) {
                count++;
                fstDate = new Date(fstDate.setDate(1))
                fstDate = new Date(fstDate.setMonth(fstDate.getMonth() + count));
                counterDate = evenFlag ? 1 : 1;
            }
            fstDate = new Date(fstDate.setDate(counterDate))
                // console.log(counterDate);
        }
        console.log(dateArray);
        return dateArray;


    }

    // TODAY'S NET WORTH BREAKDOWN End


    // CHANGES IN NET WORTH CHART STARTS

    $(function() {
        $('#changesnetworth').highcharts({
            chart: {
                type: 'areaspline'
            },
            title: {
                text: null
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                tickInterval: 1 * 24 * 3600 * 1000,
                labels: {
                    step: 2,
                    formatter: function() {
                        var data = $filter('date')(this.value, 'MM/dd');
                        return data;
                    },

                }
            },
            yAxis: {
                value: '',
                min: 0,
                max: 1000000,
                tickInterval: 100000,
                gridLineWidth: 0,
                lineWidth: 1,
                tickLength: 10,
                tickWidth: 1,
                title: {
                    text: '',
                    align: 'high'
                },


                labels: {
                    step: 2,

                    formatter: function() {
                        var data = $filter('currency')(this.value, '$', 2);
                        return data;
                    },

                }
            },
            tooltip: {
                backgroundColor: '#191919',
                forecolor: '#ffffff',
                borderColor: 'black',
                formatter: function() {
                    var data = '<span style="color:#ffffff">' + $filter('date')(this.x, 'MMM d, y') + ' : ' + $filter('currency')(this.y, '$ ', 2) + '</span>';
                    return data;
                }

            },
            plotOptions: {
                areaspline: {
                    marker: {
                        enabled: false,
                        // radius: 2,
                    }
                },
                series: {
                    pointStart: Date.UTC(2016, 2, 21),
                    pointInterval: 24 * 3600 * 1000 // one day
                }
            },

            series: [{
                showInLegend: false,
                color: '#e0f3f3',
                data: [578900, 650000, 680000, 600000, 530000, 670000, 700000, 600000, 580000, 670000, 845000, 810000, 530000, 480000, ]
                    // data: [578900, 650000, 680000, 600000, 530000, 670000, 700000, 600000, 580000, 670000, 845000, 810000, 530000, 480000,578900, 650000, 680000, 600000, 530000, 670000, 700000, 600000, 580000, 670000, 845000, 810000, 530000, 480000,845000, 810000, 530000, 480000,845000, 810000]
            }]
        });
    });



    // CHANGES IN NET WORTH CHART END









    // Engagment pie charts

    piechartdata.reset();
    var pieSchema = piechartdata.dataSchema();
    pieSchema.options.series[0].data[0].value = 4;
    pieSchema.options.series[0].data[1].value = 10 - pieSchema.options.series[0].data[0].value;
    pieSchema.labelBottom.normal.color = '#ecf7f6';
    pieSchema.labelFromatter.normal.label.textStyle.color = '#262626';
    pieSchema.labelTop.normal.color = '#36b8b7';
    pieSchema.labelFromatter.normal.label.formatter = function(params) {
        return 10 - params.value
    }

    piechartdata.add(pieSchema);

    // performance chart main right starts
    var numberOfCharts = 1;
    for (i = 0; i < numberOfCharts; i++) {
        var linechartObj = {};
        linechartObj.labels = ["December 2015", "January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016", "October 2016", "November 2016", "December 2016", "January 2017"];
        linechartObj.series = [''];
        linechartObj.data = [
            [278900, 294570, 323535, 423561, 423561, 353561, 451244, 413454, 424545, 493951, 564483, 570389, 598080, 629012]
        ];


        linechartObj.options = {
            showScale: false,
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,
            tooltipTemplate: function(value) {
                return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },

        };

        linechartObj.colours = [{
            "fillColor": "#e0f0f1",
            "strokeColor": "#36b8b7",

        }];
        linechartObjArray.reset();
        linechartObjArray.chartDataArray.push(linechartObj);
    }

    // performance chart main right ends


    // Assets json data start
    $scope.networthsDetailsinv = [{
        "ClientID": 1,
        "Name": "Investments",
        "value": "70000.00",

        "investments": [{
                "Type": "IRA",
                "Name": "Leo's Fund",
                "Balance": "300000.00",
                "Change": "3300.71",

            },

        ]
    }, ];

    $scope.networthsDetailsprop = [{
            "ClientID": 1,
            "Name": "Property",
            "value": "500000.00",

            "property": [{
                "Item": "Home",
                "Details": "1801 N Sedgick St., 60614",
                "Balance": "300000.00",
                // "Change": "15,000 (1%)",

            }, {
                "Item": "2nd Home",
                "Details": "1346 Main St., 60606",
                "Balance": "200000.00",
                // "Change": "5,000 (0.5%)",

            }, ],

        },

    ];


    $scope.networthsDetailscash = [{
        "ClientID": 1,
        "Name": "Cash",
        "value": "30000.00",

        "property": [{
            "Item": "A.B Savings",
            "Details": "Savings",
            "Balance": "30000",
            "Change": " 0.00",

        }, ]
    }, ];

    $scope.networthsDetailscustom = [{
            "ClientID": 1,
            "Name": "Custom",
            "value": "20000.00",

            "property": [{
                    "Item": "Art Collection",
                    "Details": "Art",
                    "Balance": "5000.00",
                    // "Change": "15,000 (1%)",

                },

            ],

        },

    ];
    // Assets json data end




    // Liabilities json data start

    $scope.creditCardLiabilities = [{
        "ClientID": 1,
        "Name": "Credit Card",
        "value": "5000.00",

        "creditCard": [{
                "Name": "Chase Credit Card",
                "Type": "Credit Card",
                "Balance": " 5000.00",
                "DayChange": " 100.00",

            }

        ]
    }, ];

    $scope.loanLiabilities = [{
        "ClientID": 1,
        "Name": "Loan",
        "value": "495000",

        "loan": [{
                "Name": "Citi Heloc",
                "Type": "Loan",
                "Balance": "95000.00",
                "DayChange": "0.00",

            }, {
                "Name": "Bank of America",
                "Type": "Mortgage",
                "Balance": "400000.00",
                "DayChange": "600.00",
            },

        ]
    }, ];

    // Liabilities json data end

    $scope.calcPercentage = function(value) {
        return (value / 100);
    }

    function init() {
        initChartData();
    };
    init();


})

.controller('AppCtrls', function($scope, $timeout, $mdSidenav, $log) {
        $scope.appRightMenu = true;
        $scope.toggleLeft = buildDelayedToggler('left');
        $scope.toggleRight = buildToggler('right');
        $scope.isOpenRight = function() {
            return $mdSidenav('right').isOpen();
        };
        /**
         * Supplies a function that will continue to operate until the
         * time is up.
         */
        function debounce(func, wait, context) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildDelayedToggler(navID) {

            return debounce(function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function() {
                        $log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }

        function buildToggler(navID) {

            return function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function() {
                        $log.debug("toggle " + navID + " is done");
                        $scope.appRightMenu = !$scope.appRightMenu;
                    });
            }
        }
    })
    .controller('LeftCtrl', function($scope, $timeout, $mdSidenav, $log) {
        $scope.close = function() {
            $mdSidenav('left').close()
                .then(function() {
                    $log.debug("close LEFT is done");
                });
        };
    })
    .controller('RightCtrl', function($scope, $timeout, $mdSidenav, $log) {
        $scope.close = function() {
            $mdSidenav('right').close()
                .then(function() {
                    $log.debug("close RIGHT is done");
                });
        };
    });
