(function (){

 	'use strict';

 	angular.module('app.client.insurance').controller('InsuranceLTCCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceLTCCtrl]);

 	function InsuranceLTCCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter,$window){

        $scope.getRiskListForInsurance('LTC');

        var w = angular.element($window);
        w.bind('resize', function () {
			$scope.getRiskListForInsurance('LTC');
		});

     }

})();
