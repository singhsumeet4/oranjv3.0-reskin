(function (){

 	'use strict';

 	angular.module('app.client.insurance').controller('InsuranceLTDCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceLTDCtrl]);

 	function InsuranceLTDCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter,$window){

        $scope.getRiskListForInsurance('LTD');
       
        var w = angular.element($window);
        w.bind('resize', function () {
			$scope.getRiskListForInsurance('LTD');
		});

     }

})();
