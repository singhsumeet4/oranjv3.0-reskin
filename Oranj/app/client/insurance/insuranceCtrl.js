(function (){

 	'use strict';

 	angular.module('app.client.insurance').controller('InsuranceCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceCtrl]);

 	function InsuranceCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter, $window){

        $scope.getRiskListForInsurance('All');

        var w = angular.element($window);
        w.bind('resize', function () {			
			$scope.getRiskListForInsurance('All');
		});
        
     }

})();
