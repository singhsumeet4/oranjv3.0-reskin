(function () {

	angular.module('app.client.insurance').factory('insuranceDataService', ['OranjApiService', '$q', insuranceDataService]);

	function insuranceDataService(OranjApiService, $q) {

		  var _type='', _insuranceData = $q.defer();

		return {				
			 fetchInsuranceData: function (type) {
                if(type!=_type) {
                	_type=type;
                    OranjApiService('getRiskListForInsurance').then(function (res) {
                        if (res.status == 'success') {                           
                            _insuranceData.resolve(res);                            
                        }
                    }, function () {
                       
                    });
                }

                return _insuranceData.promise;
            }
		}
	}
})();
