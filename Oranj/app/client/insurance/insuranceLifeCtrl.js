(function (){

 	'use strict';

 	angular.module('app.client.insurance').controller('InsuranceLifeCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceLifeCtrl]);

 	function InsuranceLifeCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter,$window){

        $scope.getRiskListForInsurance('Life');

        var w = angular.element($window);
          w.bind('resize', function () {         
          $scope.getRiskListForInsurance('Life');
        });
    }

})();
