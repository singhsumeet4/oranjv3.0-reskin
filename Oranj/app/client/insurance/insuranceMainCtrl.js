(function (){

 	'use strict';

 	angular.module('app.client.insurance').controller('InsuranceMainCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter','UserProfileService' ,'insuranceDataService', InsuranceMainCtrl]);

 	function InsuranceMainCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService, $filter, UserProfileService,insuranceDataService) {
     
 	    var role = GlobalFactory.getRole();

 	    role.then(
  	            function (result) {
  	                if (result.data) {
  	                    if (GlobalFactory.lookupRole('ROLE_USER', result.data)) {

  	                    } else if (GlobalFactory.lookupRole('ROLE_PROSPECT', result.data) || GlobalFactory.lookupRole('ROLE_CLIENT', result.data)) {

  	                    } else {
  	                        GlobalFactory.signout();
  	                    }
  	                } else {
  	                    GlobalFactory.signout();
  	                }
  	            }, function () {
  	                GlobalFactory.signout();
  	            }
  	   	);
 
 	   $scope.lifeArray = {};
 	   $scope.longCareArray = {};
 	   $scope.longDisabilityArray = {};
 	   $scope.policies = 0;
 	   $scope.lifeHave = 0;
 	   $scope.longCareHave = 0;
       $scope.longDisabilityHave = 0;
 	   
        UserProfileService.fetchUserProfile().then(function (data) {
            $scope.maritalStatus = data.profile.maritalStatus;
        }, function (error) {
            console.log('error', error);

        });
        

 	    $scope.getRiskListForInsurance = function(type) {
 		    
 		    insuranceDataService.fetchInsuranceData(type).then(function(response){                
                $scope.lifeHave = response.data.insurance.LIFE.header.have;
 			    $scope.longCareHave = response.data.insurance.LONG_TERM_CARE.header.have;
                $scope.longDisabilityHave = response.data.insurance.LONG_TERM_DISABILITY.header.salaryCovered;                    
                $scope.lifeNeed = response.data.insurance.LIFE.header.needed;
                $scope.longCareNeed = response.data.insurance.LONG_TERM_CARE.header.needed;
                $scope.longDisabilityNeed = response.data.insurance.LONG_TERM_DISABILITY.header.neededSalary;
                $scope.lifeSpouseHave = response.data.insurance.LIFE.header.spouseHeader.have;
                $scope.lifeYoursHave = response.data.insurance.LIFE.header.yoursHeader.have;
                $scope.longCareSpouseHave = response.data.insurance.LONG_TERM_CARE.header.spouseLTCHeader.have;
                $scope.longCareYoursHave = response.data.insurance.LONG_TERM_CARE.header.yoursLTCHeader.have;
                $scope.longDisabilitySpouseHave = response.data.insurance.LONG_TERM_DISABILITY.header.spouseLTDHeader.have;  
                $scope.longDisabilityYoursHave = response.data.insurance.LONG_TERM_DISABILITY.header.yoursLTDHeader.have;                    
                $scope.lifeSpouseNeed = response.data.insurance.LIFE.header.spouseHeader.needed;
                $scope.lifeYoursNeed = response.data.insurance.LIFE.header.yoursHeader.needed;
                $scope.longCareSpouseNeed = response.data.insurance.LONG_TERM_CARE.header.spouseLTCHeader.needed;
                $scope.longCareYoursNeed = response.data.insurance.LONG_TERM_CARE.header.yoursLTCHeader.needed;
                $scope.longDisabilityYoursNeed = response.data.insurance.LONG_TERM_DISABILITY.header.yoursLTDHeader.needed;
                $scope.longDisabilitySpouseNeed = response.data.insurance.LONG_TERM_DISABILITY.header.spouseLTDHeader.needed;
                $scope.longDisabilityHavePercen = response.data.insurance.LONG_TERM_DISABILITY.header.salaryPercent;
                $scope.longDisabilityNeedPercen = response.data.insurance.LONG_TERM_DISABILITY.header.neededPercent;
                $scope.lifeArray = response.data.insurance.LIFE.insurance;
                $scope.longCareArray = response.data.insurance.LONG_TERM_CARE.insurance;
                $scope.longDisabilityArray = response.data.insurance.LONG_TERM_DISABILITY.insurance;
                $scope.lifePrec = Math.round((($scope.lifeHave/$scope.lifeNeed)* 100));
                $scope.lifeYoursPrec = Math.round((($scope.lifeYoursHave/$scope.lifeYoursNeed)* 100));
                $scope.lifeSpousePrec = Math.round((($scope.lifeSpouseHave/$scope.lifeSpouseNeed)* 100));
                $scope.longCarePrec = Math.round((($scope.longCareHave/$scope.longCareNeed)* 100));
                $scope.longCareYoursPrec = Math.round((($scope.longCareYoursHave/$scope.longCareYoursNeed)* 100));
                $scope.longCareSpousePrec = Math.round((($scope.longCareSpouseHave/$scope.longCareSpouseNeed)* 100));
                $scope.longDisabilityPrec = Math.round((($scope.longDisabilityHave/$scope.longDisabilityNeed)* 100));
                $scope.longDisabilityYoursPrec = Math.round((($scope.longDisabilityYoursHave/$scope.longDisabilityYoursNeed)* 100));
                $scope.longDisabilitySpousePrec = Math.round((($scope.longDisabilitySpouseHave/$scope.longDisabilitySpouseNeed)* 100));
                
                $scope.lifeBenefitChartYoursAge = response.data.insurance.LIFE.header.yoursBenefitGraph.age;
                $scope.lifeBenefitChartSpouseAge = response.data.insurance.LIFE.header.spouseBenefitGraph.age;
                $scope.lifeBenefitChartYoursdeathBenefits = response.data.insurance.LIFE.header.yoursBenefitGraph.deathBenefits;
                $scope.lifeBenefitChartSpousedeathBenefits = response.data.insurance.LIFE.header.spouseBenefitGraph.deathBenefits;
                $scope.lifeBenefitChartYourshaves = response.data.insurance.LIFE.header.yoursBenefitGraph.haves;
                $scope.lifeBenefitChartSpousehaves = response.data.insurance.LIFE.header.spouseBenefitGraph.haves;
                $scope.lifeBenefitChartYoursneeds = response.data.insurance.LIFE.header.yoursBenefitGraph.needs;
                $scope.lifeBenefitChartSpouseneeds = response.data.insurance.LIFE.header.spouseBenefitGraph.needs;
                
                var colorArray = [];
                if(type == "All")
                {
                    $scope.policies = response.data.header.count;
                    var doughnutData = [{name:'Life','y':500},
                        {name:'Long Term Care','y':500},
                        {name:'Long Term Disability','y':500}
                    ];
                    var overviewData = [{name:'Life','y':$scope.lifeHave},
                        {name:'Long Term Care','y':$scope.longCareHave},
                        {name:'Long Term Disability','y':$scope.longDisabilityHave}
                    ];
                    if ($scope.lifePrec == 'Infinity')
                        $scope.lifePrec = 0;
                    if ($scope.longCarePrec == 'Infinity')
                        $scope.longCarePrec = 0;
                    if ($scope.longDisabilityPrec == 'Infinity')
                        $scope.longDisabilityPrec = 0;
                    var coverageData = [{name:'Life','data':[$scope.lifePrec]},
                        {name:'Long Term Care','data':[$scope.longCarePrec]},
                        {name:'Long Term Disability','data':[$scope.longDisabilityPrec]}
                    ];
                    if(overviewData[0].y > 0)
                        colorArray[0] = '#188079';
                    else
                        colorArray[0] = '#ffffff';
                    if(overviewData[1].y > 0)
                        colorArray[1] = '#36B8B7';                    
                    else
                        colorArray[1] = '#ffffff';
                    if(overviewData[2].y > 0)
                        colorArray[2] = '#CBE8E8';
                    else
                        colorArray[2] = '#ffffff';

                    $scope.overviewChartCreation(doughnutData,overviewData,getTotal(overviewData, 'y'),colorArray);
                    $scope.barChart(coverageData);
                    $scope.life= [];
                    for(var key in $scope.lifeArray)
                    {
                        for(var i=0;i<$scope.lifeArray[key].length;i++)
                            {
                                $scope.life.push($scope.lifeArray[key][i]);
                            }
                    }
                    $scope.longCare = [];
                    for(var key in $scope.longCareArray)
                    {
                        for(var i=0;i<$scope.longCareArray[key].length;i++)
                            {
                                $scope.longCare.push($scope.longCareArray[key][i]);
                            }
                    }
                    $scope.longDisability = [];
                    for(var key in $scope.longDisabilityArray)
                    {
                        for(var i=0;i<$scope.longDisabilityArray[key].length;i++)
                            {
                                $scope.longDisability.push($scope.longDisabilityArray[key][i]);
                            }
                    }

                    $scope.lifeSum = 0;
                    for (var i = 0; i < $scope.life.length; i++) {
                        $scope.lifeSum = $scope.lifeSum + $scope.life[i].deathBenefit;
                    }
                    
                    $scope.longCareSum = 0;
                    for (var i = 0; i < $scope.longCare.length; i++) {
                        $scope.longCareSum = $scope.longCareSum + $scope.longCare[i].dailyBenefit;
                    }
                  
                    $scope.longDisabilitySum = 0;
                    for (var i = 0; i < $scope.longDisability.length; i++) {
                        $scope.longDisabilitySum = $scope.longDisabilitySum + $scope.longDisability[i].coverageAmount;
                    }
                   
                    $scope.insurenceLife = [
                        {
                            "ClientID": 1,
                            "Name": "Life",
                            "value": $scope.lifeSum,
                            "Life": $scope.life
                        },
                    ];

                    $scope.insurenceLongTermcare = [
                        {
                            "ClientID": 1,
                            "Name": "Long Term care",
                            "value": $scope.longCareSum,
                            "LongTermCare": $scope.longCare,
                        },
                    ];

                    $scope.insurenceLongTermDisability = [
                        {
                            "ClientID": 1,
                            "Name": "Long Term Disability",
                            "value": $scope.longDisabilitySum,
                            "LongTermDisability": $scope.longDisability
                        },
                    ];       	  
                }
                if(type == "Life")
                {
                    $scope.lifePolicies = response.data.insurance.LIFE.header.count;
                    $scope.yourLifePolicies= [];
                    $scope.spouseLifePolicies= [];
                    for(var i=0;i<$scope.lifeArray['YourLifeInsurances'].length;i++)
                    {
                        $scope.yourLifePolicies.push($scope.lifeArray['YourLifeInsurances'][i]);
                    }
                    for(var i=0;i<$scope.lifeArray['SpouseLifeInsurances'].length;i++)
                    {
                        $scope.spouseLifePolicies.push($scope.lifeArray['YourLifeInsurances'][i]);
                    }
                    var haveNeedLifeChart = [{name:'Have','y':$scope.lifeYoursHave},
                        {name:'Need','y':$scope.lifeYoursNeed}
                    ];
                    $scope.colorDecide(haveNeedLifeChart,$scope.lifeYoursPrec);
                    $scope.benefitChart($scope.lifeBenefitChartYoursAge,$scope.lifeBenefitChartYourshaves,$scope.lifeBenefitChartYoursneeds);
                }
               if(type == "LTC")
                {    
                    $scope.longCarePolicies = response.data.insurance.LONG_TERM_CARE.header.count;
                    var haveNeedLTCChart = [{name:'Have','y':$scope.longCareYoursHave},
                        {name:'Need','y':$scope.longCareYoursNeed}
                    ];
                    $scope.colorDecide(haveNeedLTCChart,$scope.longCareYoursPrec);
                    $scope.yourLTCPolicies= [];
                    $scope.spouseLTCPolicies= [];
                    $scope.LTCYourData = [];
                    $scope.LTCYourLabels = [];
                    $scope.LTCSpouseData = [];
                    $scope.LTCSpouseLabels= [];
                    for(var i=0;i<$scope.longCareArray['YourLongTermCare'].length;i++)
                    {
                        $scope.yourLTCPolicies.push($scope.longCareArray['YourLongTermCare'][i]);
                    }
                    for(var i=0;i<$scope.longCareArray['SpouseLongTermCare'].length;i++)
                    {
                        $scope.spouseLTCPolicies.push($scope.longCareArray['SpouseLongTermCare'][i]);
                    }
                    for(var i=0;i<$scope.yourLTCPolicies.length;i++)
                    {
                        for(key in $scope.yourLTCPolicies[i])
                        {                            
                            if(key == "contractDuration")
                            {
                                if($scope.yourLTCPolicies[i][key] == 'LIFE' || $scope.yourLTCPolicies[i][key] == 'OTHER')
                                    $scope.LTCYourData.push('$85');
                                else
                                    $scope.LTCYourData.push('$' + $scope.yourLTCPolicies[i][key]);
                            }
                            if(key == "dailyBenefit")
                            {
                                if([$scope.yourLTCPolicies[i][key]] == 'LIFE' || [$scope.yourLTCPolicies[i][key]] == 'OTHER')
                                    var dataLabel = [85];
                                else
                                    var dataLabel = [$scope.yourLTCPolicies[i][key]];
                            }
                            if(key == "name")
                                var nameLabel = $scope.yourLTCPolicies[i][key];
                        }
                        $scope.LTCYourLabels.push({name:nameLabel,data:dataLabel, pointPadding: 0,
                                    groupPadding: 0,
                                    borderWidth: 0});
                    }
                    $scope.coverageChart($scope.LTCYourData,$scope.LTCYourLabels);
                    for(var i=0;i<$scope.spouseLTCPolicies.length;i++)
                    {
                        for(key in $scope.spouseLTCPolicies[i])
                        {
                            if(key == "contractDuration"){

                                if($scope.LTCSpouseLabels[i][key] == 'LIFE' || $scope.LTCSpouseLabels[i][key] == 'OTHER')
                                    $scope.LTCSpouseData.push('$85');
                                else
                                    $scope.LTCSpouseData.push('$' + $scope.LTCSpouseLabels[i][key]);
                            }
                            if(key == "dailyBenefit")
                            {
                                if([$scope.yourLTCPolicies[i][key]] == 'LIFE' || [$scope.yourLTCPolicies[i][key]] == 'OTHER')
                                    var dataLabel = [85];
                                else
                                    var dataLabel = [$scope.yourLTCPolicies[i][key]];
                            }
                            if(key == "name")
                                var nameLabel = $scope.LTCSpouseLabels[i][key];
                        }
                        $scope.LTCSpouseLabels.push({name:nameLabel,data:dataLabel, pointPadding: 0,
                                    groupPadding: 0,
                                    borderWidth: 0});
                    }
                }
                if(type == "LTD")
                {
                    $scope.longDisabilityPolicies = response.data.insurance.LONG_TERM_DISABILITY.header.count; 
                    var haveNeedLTDChart = [{name:'Have','y':$scope.longDisabilityYoursHave},
                        {name:'Need','y':$scope.longDisabilityYoursNeed}
                    ];
                    $scope.colorDecide(haveNeedLTDChart,$scope.longDisabilityYoursPrec);
                    $scope.yourLTDPolicies= [];
                    $scope.spouseLTDPolicies= [];
                    $scope.LTDYourData = [];
                    $scope.LTDYourLabels= [];
                    $scope.LTDSpouseData = [];
                    $scope.LTDSpouseLabels= [];
                    for(var i=0;i<$scope.longDisabilityArray['YourLifeTermDisablity'].length;i++)
                    {
                        $scope.yourLTDPolicies.push($scope.longDisabilityArray['YourLifeTermDisablity'][i]);
                    }
                    for(var i=0;i<$scope.longDisabilityArray['SpouseLifeTermDisablity'].length;i++)
                    {
                        $scope.spouseLTDPolicies.push($scope.longDisabilityArray['SpouseLifeTermDisablity'][i]);
                    }
                    for(var i=0;i<$scope.yourLTDPolicies.length;i++)
                    {
                        for(key in $scope.yourLTDPolicies[i])
                        {
                            if(key == "coveragePercentage")
                                $scope.LTDYourData.push($scope.yourLTDPolicies[i][key] + '%');
                            if(key == "contractDuration")
                            {
                                if([$scope.yourLTDPolicies[i][key]] == 'OTHER' || [$scope.yourLTDPolicies[i][key]] == 'LIFE')
                                    var dataLabel = [85];
                                else
                                    var dataLabel = [$scope.yourLTDPolicies[i][key]];
                            }
                            if(key == "name")
                                var nameLabel = $scope.yourLTDPolicies[i][key];
                        }
                        $scope.LTDYourLabels.push({name:nameLabel,data:dataLabel, pointPadding: 0,
                                        groupPadding: 0,
                                        borderWidth: 0});
                    }
                    $scope.coverageChart($scope.LTDYourData,$scope.LTDYourLabels);
                    for(var i=0;i<$scope.spouseLTDPolicies.length;i++)
                    {
                        for(key in $scope.spouseLTDPolicies[i])
                        {
                            if(key == "coveragePercentage")
                                $scope.LTDSpouseData.push($scope.spouseLTDPolicies[i][key] + '%');
                           if(key == "contractDuration")
                            {
                                if([$scope.yourLTDPolicies[i][key]] == 'OTHER'|| [$scope.yourLTDPolicies[i][key]] == 'LIFE')
                                    var dataLabel = [85];
                                else
                                    var dataLabel = [$scope.yourLTDPolicies[i][key]];
                            }
                            if(key == "name")
                                var nameLabel = $scope.yourLTDPolicies[i][key];
                        }
                        $scope.LTDSpouseLabels.push({name:nameLabel,data:dataLabel, pointPadding: 0,
                                        groupPadding: 0,
                                        borderWidth: 0});
                    }
                }

 	        });               
        };

        $scope.chartChanged = function(status,type)
        {
            var colorArray = [];
            var haveNeedLifeChart = [{name:'Have','y':$scope.lifeYoursHave},
                        {name:'Need','y':$scope.lifeYoursNeed}
                    ];
            var haveNeedLTCChart = [{name:'Have','y':$scope.longCareYoursHave},
                {name:'Need','y':$scope.longCareYoursNeed}
            ];
            var haveNeedLTDChart = [{name:'Have','y':$scope.longDisabilityYoursHave},
                        {name:'Need','y':$scope.longDisabilityYoursNeed}
                    ];
            if(status == 'your')
            {
                if(type == 'LIFE')
                {
                    $scope.colorDecide(haveNeedLifeChart,$scope.lifeYoursPrec);
                    $scope.benefitChart($scope.lifeBenefitChartYoursAge,$scope.lifeBenefitChartYourshaves,$scope.lifeBenefitChartYoursneeds);               
                }
                if(type == 'LTC')
                {
                    $scope.colorDecide(haveNeedLTCChart,$scope.longCareYoursPrec);
                    $scope.coverageChart($scope.LTCYourData,$scope.LTCYourLabels);
                }
                if(type == 'LTD')
                {
                    $scope.colorDecide(haveNeedLTDChart,$scope.longDisabilityYoursPrec);
                    $scope.coverageChart($scope.LTDYourData,$scope.LTDYourLabels);
                }
            }

            if(status == 'spouse'){
                if(type == 'LIFE')
                {
                    $scope.colorDecide(haveNeedLifeChart,$scope.lifeSpousePrec);
                    $scope.benefitChart( $scope.lifeBenefitChartSpouseAge,$scope.lifeBenefitChartSpousehaves,$scope.lifeBenefitChartSpouseneeds);
                }
                if(type == 'LTC')
                {
                    $scope.colorDecide(haveNeedLTCChart,$scope.longCareSpousePrec);
                    $scope.coverageChart($scope.LTCSpouseData,$scope.LTCSpouseLabels);
                }
                if(type == 'LTD')
                {
                    $scope.colorDecide(haveNeedLTDChart,$scope.longDisabilitySpousePrec);
                    $scope.coverageChart($scope.LTDSpouseData,$scope.LTDSpouseLabels);
                }
            }

        }

        $scope.colorDecide = function(data,prec)
        {
            var colorArray = [];
            var doughnutData = data;
            if(data[0].y > 0)
                colorArray[0] = '#188079';
            else
                colorArray[0] = '#ffffff';
            if(data[1].y > 0)
                colorArray[1] = '#36B8B7';                    
            else
                colorArray[1] = '#ffffff';
            $scope.overviewChartCreation(doughnutData,data,prec+ '%',colorArray);
        }
    
       // doughnut Chart start
        
        $scope.overviewChartCreation =function(doughnutData,overviewData,tt,colorArray) {
            $('#containerDoughnut').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                    events: {
                        load: function () {
                            var chart = this,
                                rend = chart.renderer,
                                pie = chart.series[0],
                                left = chart.plotLeft + pie.center[0],
                                top = chart.plotTop + pie.center[1],
                                text = rend.text(tt, left, top).attr({ 'text-anchor': 'middle', 'font-size': '25px' }).add() ;
                        }
                    }
                },

                colors: colorArray,
                credits: {
                    enabled: false
                },
                tooltip: {
                    backgroundColor: '#191919',
                    forecolor: '#ffffff',
                    borderColor: 'black',
                    borderRadius: 10,
                    borderWidth: 3,
                    valueSuffix: '$',
                    formatter: function () {
                        var a = '<span style="color:#ffffff">' + overviewData[0].name + ': ' + ((overviewData[0].y > 0) ? ($filter('currency')(overviewData[0].y, '$', 2)) : 'n/a') + '</span>' + ' <br/>'
                            + '<span style="color:#ffffff">' + overviewData[1].name + ":" + ((overviewData[1].y > 0) ? ($filter('currency')(overviewData[1].y, '$', 2)) : 'n/a') + '</span>' + ' <br/>'
                            if(overviewData[2])
                                return a + '<span style="color:#ffffff">' + overviewData[2].name + ': ' + ((overviewData[2].y > 0) ? ($filter('currency')(overviewData[2].y, '$', 2)) : 'n/a') + '</span>';
                            else
                                return a;
                    }

                },
                plotOptions: {
                    pie: {
                        borderColor: '#eeeeee',
                        size: '100%',
                        borderWidth: '2',
                        dataLabels: {
                            enabled: false,
                            distance: -10,

                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        showInLegend: true,
                        startAngle: 0,
                        endAngle: 360,
                        center: ['45%', '40%']
                    }
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    x: 10,
                    itemMarginBottom: 14,
                    shadow: false,
                    verticalAlign: 'top',
                    y: 40,
                    floating: false,
                    margin: 10,
                    backgroundColor: '#FFFFFF',
                    itemStyle: {
                        color: '#404040',
                        fontWeight: 'normal',
                        margin: '80px',
                        fontSize: '14px',
                        fontFamily: 'Open Sans "sans-serif"',
                    }
                },

                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    innerSize: '65%',
                    data: doughnutData,
                    size: '100%'


                }],
                title: {
                    text: '',
                    style: {
                        color: '#404040',
                        fontWeight: 'normal',
                        margin: '80px',
                        fontSize: '20px',
                        fontFamily: 'Open Sans "sans-serif"',
                    }

                },
            });
        };

        // doughnut chart end

        //  Bar Chart start

        $scope.barChart = function (coverageData) {

            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: null,
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    labels: {
                        enabled: false,//default is true
                    },
                    categories: ['', '', ''],
                },
                yAxis: {
                    min: 0,
                    max: 100,
                    tickInterval: 10,
                    gridLineWidth: 0,
                    lineWidth: 1,
                    title: {
                        text: '% of coverage'
                    },
                    labels: {
                        formatter: function () {
                            return this.value + ' %';
                        },
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Open Sans',
                            fontStyle: 'italic',

                        },
                    },
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    x: 10,
                    itemMarginBottom: 14,
                    verticalAlign: 'top',
                    y: 40,
                    floating: false,
                    margin: 10,
                    backgroundColor: '#FFFFFF',
                    itemStyle: {
                        color: '#404040',
                        fontWeight: 'normal',
                        margin: '80px',
                        fontSize: '14px',
                        fontFamily: 'Open Sans "sans-serif"',
                    }
                },
                colors: [
                    '#188079',
                    '#36B8B7',
                    '#CBE8E8'
                ],
                tooltip: {
                    enabled: false,
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {


                    column: {
                        // stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            inside: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || '#676767',
                            style: {
                                textShadow: 'false',
                                color: 'black',
                            },
                            formatter: function () {
                                if (Math.round(this.y) > 0)
                                    return this.y + '%';
                               else
                                    return '0%';
                            }
                        }
                    }
                },
                series: coverageData
            });
        };

        //Bar Chart Ends

        // Coverage chart for LTC and LTD
         $scope.coverageChart = function(data,labels){
            $('#horizontalBar').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: data,
                    title: {
                        text: null
                    },
                     labels: {
                        overflow: 'justify',
                    }
                },
                yAxis: {
                        gridLineWidth: 0,
                    min: 0,
                    title: {
                        text: '#Of years of coverage',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify',
                    },
                    tickInterval : 1
                },
                tooltip: {
                    valueSuffix: ''
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
                credits: {
                    enabled: true
                },
                series: labels,
                exporting: {
                 enabled: false
                        }
            });

        }
        // end of Coverage chart

         //Benefit Chart for Life

        $scope.benefitChart = function(age,have,need){

            Chart.defaults.global.responsive = true;   //charts created to be responsive
            
            var data = {
                labels: age,

                datasets: [
                    {
                        // 2012
                        label: 'Have',
                        fillColor: '#36B8B7',
                        strokeColor: '#36B8B7',
                        data: have
                    },
                    {

                        // 2013
                        label: 'Need',
                        fillColor: '#CBE8E8',
                        strokeColor: '#CBE8E8',
                        data: need
                    }
                ],

            };

            var lineoptions = {
                // scaleLabel: "<%= '$'+value%>",
                scaleLabel: function (value) { return ' $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
                scaleFontStyle: "italic",
                scaleFontColor: "#3D4051",
                scaleFontSize: 10,
                responsive: true,
                barValueSpacing: 4,
                scaleShowGridLines: false,
                multiTooltipTemplate: function (value) { return ' $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
                legendTemplate: '<table>'
                + '<% for (var i=0; i<datasets.length; i++) { %>'
                + '<tr><td><div class=\"boxx\" style=\"background-color:<%=datasets[i].fillColor %>\"></div></td>'
                + '<% if (datasets[i].label) { %><td><%= datasets[i].label %></td><% } %></tr><tr height="5"></tr>'
                + '<% } %>'
                + '</table>',


            }
            if(document.getElementById('benefitChart')!= undefined)
            {
              var ctx = document.getElementById('benefitChart').getContext('2d');
              Chart.types.StackedBar.extend({
                  name: "LineAlt2",
                  initialize: function (data) {
                      Chart.types.StackedBar.prototype.initialize.apply(this, arguments);
                      this.scale.xLabelRotation = 0;
                      var yLabels = this.scale.yLabels
                      yLabels.forEach(function (label, i) {
                          if ((i % 2) !== 0)
                              yLabels[i] = '';
                      });
                      var xLabels = this.scale.xLabels
                      xLabels.forEach(function (label, i) {
                          if (i % 10 !== 0)
                              xLabels[i] = '';
                      })
                  }
              });
              var chart = new Chart(ctx).LineAlt2(data, lineoptions, { stacked: true });
            }

            if(document.getElementById('benefitLegend') != undefined)
            {
              document.getElementById('benefitLegend').innerHTML = chart.generateLegend(); // for legend
            }
        }


 	};

})();
