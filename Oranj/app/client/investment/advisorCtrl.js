(function () {
    'use strict';

    angular.module('app.chart')
        .controller('advisorCtrl', ['$scope', advisorCtrl]);

    // Advisor doughnut chart start

    function advisorCtrl($scope) {

        Chart.defaults.global.responsive = true;



        var advisorDoughnutData = [

           {
                label: 'Non US Bond',
                value: 80,
                name: 'Non US Bond',
                color: '#23c1e2',

            },
            {
                label: 'Cash',
                value: 210,
                name: 'Cash',
               color: '#fdb45c',

            },

            {
                label: 'US Bond',
                value: 430,
                name: 'US Bond',
                color: '#3eb649',

            },

            {
                label: 'Non US Stock',
                value: 220,
                name: 'Non US Stock',
                color: '#a3cd39'

            },

            {
                label: 'US Stock',
                value: 1548,
                name: 'US Stock',
                color: '#fdce0b'

            },
        ];

        var advisorDoughnutOptions = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,
            scaleShowLabels: true,
            scaleOverlay: true,
            // scaleFontColor : "#666",

        }


        var ctx2 = document.getElementById("advisorDoughnut").getContext("2d");

        var myDoughnut2 = new Chart(ctx2).Doughnut(advisorDoughnutData, advisorDoughnutOptions);

        document.getElementById('advisor-legend').innerHTML = myDoughnut2.generateLegend(); // for legend 

        $scope.advDougCtr = getMax(advisorDoughnutData, 'value').value;



    }


    // Advisor doughnut chart end

})(); 