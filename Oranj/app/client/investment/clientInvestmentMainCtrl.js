(function () {
    'use strict';

    angular.module('app.client.investment')
        .controller('clientInvestmentCtrl', ['$scope', '$timeout', 'linechartObjArray', 'piechartdata', '$rootScope', 'OranjApiService', '$filter', clientInvestmentCtrl]);
        

    function clientInvestmentCtrl($scope, $timeout, linechartObjArray, piechartdata, $rootScope, OranjApiService, $filter) {

        var endDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        var startDate = moment(new Date()).subtract(30, 'days').format("MM-DD-YYYY");
        $scope.date = {'startDate':startDate,'endDate':endDate};
        //$rootScope.breadcrum = 'Home / Investment';
        // Engagment pie charts  start
        piechartdata.reset();
        var pieSchema = piechartdata.dataSchema();

        pieSchema.options.series[0].radius = [45, 41];
        pieSchema.options.series[0].data[0].value = 68;
        pieSchema.options.series[0].data[1].value = 100 - pieSchema.options.series[0].data[0].value;
        pieSchema.labelBottom.normal.color = '#ECF7F6';
        pieSchema.labelFromatter.normal.label.textStyle.color = '#36B8B7';
        pieSchema.labelFromatter.normal.label.textStyle.fontSize = 23;
        // pieSchema.labelBottom.normal.label.position= 'center';

        pieSchema.labelTop.normal.label.position = 'center';

        pieSchema.labelTop.normal.color = '#36B8B7';

        // pieSchema.options.series. center= ['50%', '20%'],
        pieSchema.labelFromatter.normal.label.formatter = function (params) {
            return 100 - params.value + '%'
        }

        piechartdata.add(pieSchema);

        // Engagment pie charts end


        var linechartObj = {}
        linechartObj.labels = ["January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016", "October 2016"];
        linechartObj.series = [''];
        linechartObj.data = [
            [450000, 430000, 470000, 519000, 530500, 520800, 540300, 599000, 610000, 700000],
            //   [50000, 80000, 60000, 120000, 140000, 110000, 180000, 200000, 170000, 210000],

        ];
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
        $scope.date = { 'date1': 'July 01, 2016', 'date2': 'July 07, 2016' }

        linechartObj.options = {
            showScale: false,
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            pointHitDetectionRadius: 2,
            // zindex:1,

        };

        // linechartObj.style=[{
        //     zIndex=10
        // }];

        linechartObj.colours = [{
            "fillColor": "#e0f0f1",
            "strokeColor": "#36b8b7",

        }];

        linechartObjArray.reset();
        linechartObjArray.chartDataArray.push(linechartObj);
        $scope.chartLabels = [];
        $scope.chartData = [];
        // performance chart main right ends

        function initInvestmentPerformaceChart() {
            $scope.chartLabels = [];
            $scope.chartData = [];
        
            var startDate = $filter('date')(new Date($scope.date.startDate), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.endDate), 'MM-dd-yyyy');
            OranjApiService('getInvestmentPerformaceChart', null, {
                'startDate': startDate,
                'endDate': endDate
            }
            ).then(function (response) {
                for (var i = 0; i < response.data.values.length; i++) {
                    for (var key in response.data.values[i]) {
                        if (key == 'date')
                            $scope.chartLabels.push($filter('date')(new Date(response.data.values[i][key]), 'MMMM yyyy'));
                        if (key == 'value')
                            $scope.chartData.push(response.data.values[i][key]);
                    }
                }
                createPerformanceChartData();

            }, function (response) {

            });
        };
        $scope.changeDates = function () {
            initInvestmentPerformaceChart();
        }
        // clientInvestmentMain doughnut chart start
        

        Chart.defaults.global.responsive = true;

        var doughnutData = [

            {
                label: 'Non US Bond',
                value: 80,
                name: 'Non US Bond',
                color: '#23c1e2',

            },
            {
                label: 'Cash',
                value: 210,
                name: 'Cash',
               color: '#fdb45c',

            },

            {
                label: 'US Bond',
                value: 430,
                name: 'US Bond',
                color: '#3eb649',

            },

            {
                label: 'Non US Stock',
                value: 220,
                name: 'Non US Stock',
                color: '#a3cd39'

            },

            {
                label: 'US Stock',
                value: 1548,
                name: 'US Stock',
                color: '#fdce0b'

            },

        ];

        var doughnutOptions = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,

        };
        // if (document.getElementById("investmentMainDoughnut") !== null) {
        //     var ctx1 = document.getElementById("investmentMainDoughnut").getContext("2d");
        //     var myDoughnut = new Chart(ctx1).Doughnut(doughnutData, doughnutOptions);

        //     document.getElementById('my-legend').innerHTML = myDoughnut.generateLegend(); // for legend 
        // }
        // clientInvestmentMain doughnut chart end

        function createPerformanceChartData() {
            //jquery chart starts

            Chart.types.Line.extend({
                name: "LineAlt",
                initialize: function (data) {
                    var strokeColors = [];
                    data.datasets.forEach(function (dataset, i) {
                        if (dataset.dottedFromLabel) {
                            strokeColors.push(dataset.strokeColor);
                            dataset.strokeColor = "rgba(0,0,0,0)"
                        }
                    })

                    Chart.types.Line.prototype.initialize.apply(this, arguments);

                    var self = this;
                    data.datasets.forEach(function (dataset, i) {
                        if (dataset.dottedFromLabel) {
                            self.datasets[i].dottedFromIndex = data.labels.indexOf(dataset.dottedFromLabel) + 1;
                            self.datasets[i]._saved = {
                                strokeColor: strokeColors.shift()
                            }
                        }
                    })
                },
                draw: function () {
                    Chart.types.Line.prototype.draw.apply(this, arguments);

                    // from Chart.js library code
                    var hasValue = function (item) {
                        return item.value !== null;
                    },
                        nextPoint = function (point, collection, index) {
                            return Chart.helpers.findNextWhere(collection, hasValue, index) || point;
                        },
                        previousPoint = function (point, collection, index) {
                            return Chart.helpers.findPreviousWhere(collection, hasValue, index) || point;
                        };

                    var ctx = this.chart.ctx;
                    var self = this;
                    ctx.save();
                    this.datasets.forEach(function (dataset) {
                        ctx.beginPath();
                        if (dataset.dottedFromIndex) {
                            ctx.lineWidth = self.options.datasetStrokeWidth;
                            ctx.strokeStyle = dataset._saved.strokeColor;

                            // adapted from Chart.js library code
                            var pointsWithValues = Chart.helpers.where(dataset.points, hasValue);
                            Chart.helpers.each(pointsWithValues, function (point, index) {
                                if (index >= dataset.dottedFromIndex)
                                    if (dataset.fillColor === "transparent")
                                        ctx.setLineDash([10, 10]);
                                    else
                                        ctx.setLineDash([3, 3]);
                                else
                                    ctx.setLineDash([]);

                                if (index === 0) {
                                    ctx.moveTo(point.x, point.y);
                                }
                                else {
                                    if (self.options.bezierCurve) {
                                        var previous = previousPoint(point, pointsWithValues, index);
                                        var originalBezierCurveTo = ctx.bezierCurveTo;
                                        ctx.bezierCurveTo(
                                            previous.controlPoints.outer.x,
                                            previous.controlPoints.outer.y,
                                            point.controlPoints.inner.x,
                                            point.controlPoints.inner.y,
                                            point.x,
                                            point.y
                                        );

                                    }
                                    else {
                                        ctx.lineTo(point.x, point.y);
                                    }
                                }

                                ctx.stroke();
                            }, this);
                        }

                    })
                    ctx.restore();
                }
            });

            var data = {
                labels: $scope.chartLabels,
                datasets: [

                    {
                        // label: "Retirement",
                        type: 'line',
                        fillColor: "#ffffff",
                        strokeColor: "#716ea8",
                        pointColor: "#fff4d3",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#586fb2",



                        data: $scope.chartData,
                        // dottedFromLabel: "August 2016"

                    },
                ],
            };

            var lineoptionsTwo = {
                scaleShowGridLines: false,
                pointDot: false,
                bezierCurve: false,
                showScale: true,
                tooltipTemplate: function (value) {
                    return value.label.toString() + ' , $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                scaleFontSize: 0
            }

            var ctx = document.getElementById("canvas").getContext("2d");
            console.log("data", data);
            new Chart(ctx).LineAlt(data, lineoptionsTwo);

            //jquery chart ends 
        }

        function init() {
            initInvestmentPerformaceChart();
        };
        init();

    }

})();