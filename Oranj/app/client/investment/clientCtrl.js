(function () {
    'use strict';

    angular.module('app.chart')
        .controller('clientCtrl', ['$scope', 'OranjApiService', 'GlobalFactory', '$state', '$stateParams', '$filter', clientCtrl]);


    function clientCtrl($scope, OranjApiService, GlobalFactory, $state, $stateParams, $filter) {

        $scope.global = GlobalFactory;
        var vm = this;
        vm.accountName = $stateParams.name;
        var endDate = $filter('date')(new Date(), 'MMMM dd yyyy');
        var startDate = moment(new Date()).subtract(30, 'days').format('MMMM DD YYYY');
        $scope.date = {'startDate':startDate,'endDate':endDate};
        $scope.maxDate = $filter('date')(new Date(), 'MMMM dd, yyyy');

        $scope.showLabelsOnChart = {
            startDate: '',
            endDate: '',
            startBalance: '',
            endBalance: '',
            change: '',
            changePercentage: ''
        };
        
        $scope.getInvestementAccountHoldings = function(type,yodleeItemAccountId)
        {
        	$scope.global.investmentSpecificAccountHolding = [];
        	$scope.noHoldingText = "";
                OranjApiService('getInvestementAccountHoldingsYodlee',null,{
                        yodleeItemAccountId: yodleeItemAccountId
                    }).then(function(response){
                        $scope.global.investmentSpecificAccountHolding = response.data.holdings;
                        $scope.specificAccountHolding = response.data.holdings;
                        if(response.data.holdings == null) {
                        	$scope.noHoldingText = "No holdings info available";
                        }
                        if($scope.global.investmentAccountSideList != undefined){
                            for( var i =0 ;i<$scope.global.investmentAccountSideList.length;i++)
                            {
                                if($scope.global.investmentAccountSideList[i] != undefined)
                                    if($scope.global.investmentAccountSideList[i].accountId == yodleeItemAccountId){
                                        $scope.global.investmentAccountSideList[i].holdings = response.data.holdings;
                                        $scope.investmentAccountDetails[i].holdings = response.data.holdings;
                                    }                        
                            }
                        }
                    },function(response){
                     
                });
        }

        $scope.getInvestmentHeader = function () {
            OranjApiService('getInvestmentHeader').then(function (response) {
                
                if (response.status == "success") {
                    $scope.numberOfAccount = response.data.numberOfAccount;
                    $scope.totalBalance = response.data.totalBalance;
                    $scope.goalCount = response.data.goalCount;
                    $scope.changedValue = response.data.totalChangedValue;
                    //$scope.changedValue = 11450.13;
                    $scope.percentageChange = response.data.totalChangePercentage;
                    //$scope.percentageChange = 1.45;
                    $scope.changeStatus = response.data.changePositive;
                    
                }
            }, function (response) {

            });
        };

        $scope.getInvestmentMainAssetChart = function () {
            OranjApiService('getInvestmentMainAssetChart').then(function (response) {
                
                if (response.status == "success") {
                    var doughnutData = [];
                    for (var i = 0; i < response.data.investmentPieChart['Asset Class'].length; i++) {
                        var doughnutDataobj = {};
                        doughnutDataobj.label = response.data.investmentPieChart['Asset Class'][i].name;
                        doughnutDataobj.value = response.data.investmentPieChart['Asset Class'][i].value;
                        switch(doughnutDataobj.label){
                        	case 'US Stock':
                        		doughnutDataobj.color = "#fdce0b"
                        		break;                    		
                        	case 'Non US Stock':
                        		doughnutDataobj.color = "#a3cd39"
            				    break;                    		
                        	case 'US Bond':
                				doughnutDataobj.color = "#3eb649"
                				break;                    		
                        	case 'Non US Bond':
                    			doughnutDataobj.color = "#23c1e2"
                    			break;                   		 
                    		case 'Preferred':
                        		doughnutDataobj.color = "#2e4ea1"
                        		break;                   		                        	
                        	case 'Convertible':
                        		doughnutDataobj.color = "#000258"
                        		break;                   			
                        	case 'Cash':
                   				doughnutDataobj.color = "#fdb45c"
                   				break;
                        	case 'Other':
                       			doughnutDataobj.color = "#f7464a"
                       			break;
                       		case 'Unknown':
                        		doughnutDataobj.color = "#d1cdcd"
                        		break;
                       		default:
                        		doughnutDataobj.color = "#d1cdcd"
                        		                        		
                        		
                        }
                        
                        doughnutData.push(doughnutDataobj);
                    }

                    assetChartCreation(doughnutData);

                    // console.log('doughnutData', doughnutData);
                }
            }, function (response) {

            });
        };

        $scope.getInvestmentMainPercent = function () {
            var startDate = $filter('date')(new Date($scope.date.startDate), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.endDate), 'MM-dd-yyyy');
            
            if(new Date($scope.date.startDate) <= new Date($scope.date.endDate)) {
            	OranjApiService('getInvestmentPerformaceChart', null, {
                    'startDate': startDate,
                    'endDate': endDate
                }).then(function (response) {

                    $scope.chartLabelsTemp = [];
                    $scope.chartLabels = [];
                    $scope.chartDataTemp = {};
                    $scope.chartData = [];
                    $scope.chartDateData = {};
                    if(response.data.values.length > 0) {
                    	 for (var i = 0; i < response.data.values.length; i++) {
                    		 response.data.values[i].performanceChart.forEach(function (element) {
                    			 var found = $filter('filter')($scope.chartLabelsTemp, {date: element.date}, true);                            	 
                                 if (found.length) {
                                     var selected = found[0].date.toString();
                                     $scope.chartDataTemp[selected].y = Number($scope.chartDataTemp[selected].y) + Number(element.value);
                                     $scope.chartDateData[selected].push({name: element.accountName, value:element.value});
                                 }else{
                                	$scope.chartLabelsTemp.push({date:element.date});	
                                 	var dateStr = element.date.toString();
                                 	$scope.chartDataTemp[dateStr] = {y: element.value, date: element.date};
                                 	$scope.chartDateData[dateStr] =  [{name: element.accountName, value:element.value}];
                                 }                             	
                             	
                             });
                    	 }
                        
                        angular.forEach($scope.chartDataTemp, function(value, key) {
                        	  $scope.chartData.push({y: value.y, date:value.date});
                        });
                        
                        $scope.chartLabelsTemp.forEach(function (element) {
                        	$scope.chartLabels.push(element.date);
                        });
                        $scope.length = $scope.chartLabels.length;
                    }
                    if ($scope.chartLabels.length > 0 && $scope.chartData.length > 0) {                    	                        
                    	$scope.showLabelsOnChart.startDate = $scope.chartData[0].date;
                        $scope.showLabelsOnChart.startBalance = $scope.chartData[0].y;                        
                        $scope.showLabelsOnChart.endDate = $scope.chartLabels[$scope.chartLabels.length - 1];
                        $scope.showLabelsOnChart.endBalance = $scope.chartData[$scope.chartLabels.length - 1].y;

                        $scope.showLabelsOnChart.change = $scope.showLabelsOnChart.endBalance - $scope.showLabelsOnChart.startBalance;
                        
                        if ($scope.showLabelsOnChart.change > 0) {
                            $scope.showLabelsOnChart.changePercentage = ($scope.showLabelsOnChart.startBalance / $scope.showLabelsOnChart.change) * 100;
                        }
                        else {
                            $scope.showLabelsOnChart.changePercentage = 0;
                        }

                    }
                    createPerformanceChartData();
                }, function (response) {

                });
            }else {
            	GlobalFactory.showErrorAlert(' Start date should not be older than the End date');
            }
            
        };

        $scope.getInvestmentAccounts = function () {
            OranjApiService('getInvestmentAccounts').then(function (response) {                
                $scope.investmentAccountDetails = response.data.accounts;                
                if( $scope.investmentAccountDetails.length > 0){
                	 $scope.investmentAccountDetails.sort(function(a, b){
                		 	return a.name > b.name;
            	  	   });
            	}
                $scope.global.investmentAccountSideList = $scope.investmentAccountDetails;
                if ($stateParams.hasOwnProperty('id')) {
                    $scope.getInvestmentHoldingsAssetChart($stateParams.id);
                    $scope.getInvestmentHoldingsPercent($stateParams.id);
                }

            }, function (response) {

            });
        };

        $scope.getInvestmentHoldingsAssetChart = function (id) {
        	var startDate = $filter('date')(new Date($scope.date.startDate), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.endDate), 'MM-dd-yyyy');        	
            OranjApiService('getInvestmentHoldingsAssetChart', null, {
                'yodleeItemAccountId': id                
            }).then(function (response) {
                var doughnutData = [];
                for (var i = 0; i < response.data.investmentPieChart['Asset Class'].length; i++) {
                    var doughnutDataobj = {};
                    doughnutDataobj.label = response.data.investmentPieChart['Asset Class'][i].name;
                    doughnutDataobj.value = response.data.investmentPieChart['Asset Class'][i].value;
                    doughnutData.push(doughnutDataobj);
                }

                assetChartCreation(doughnutData);

            }, function (response) {

            });
        };

        $scope.getInvestmentHoldingsPercent = function (id) {
            $scope.chartLabels = [];
            $scope.chartData = [];
            var startDate = $filter('date')(new Date($scope.date.startDate), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.endDate), 'MM-dd-yyyy');
            OranjApiService('getInvestmentHoldingsPercent', null, {
                'yodleeItemAccountId': id,
                'startDate': startDate,
                'endDate': endDate
            }).then(function (response) {
                // console.log("response percentageChange", response);
                if (response.status == 'success') {
                    for (var i = 0; i < response.data.values.length; i++) {
                        for (var key in response.data.values[i]) {
                            if (key == 'date')
                                $scope.chartLabels.push(response.data.values[i][key]);
                            if (key == 'value')
                                $scope.chartData.push(response.data.values[i][key]);
                            $scope.length = $scope.chartLabels.length;
                        }
                    }

                    if ($scope.chartLabels.length > 0 && $scope.chartData.length > 0) {
                        $scope.showLabelsOnChart.startDate = $scope.chartLabels[0];
                        $scope.showLabelsOnChart.startBalance = $scope.chartData[0];

                        $scope.showLabelsOnChart.endDate = $scope.chartLabels[$scope.chartLabels.length - 1];
                        $scope.showLabelsOnChart.endBalance = $scope.chartData[$scope.chartLabels.length - 1];

                        $scope.showLabelsOnChart.change = $scope.showLabelsOnChart.endBalance - $scope.showLabelsOnChart.startBalance;
                        if ($scope.showLabelsOnChart.change > 0) {
                            $scope.showLabelsOnChart.changePercentage = ($scope.showLabelsOnChart.startBalance / $scope.showLabelsOnChart.change) * 100;
                        }
                        else {
                            $scope.showLabelsOnChart.changePercentage = 0;
                        }

                    }
                    createPerformanceChartData();
                }

            }, function (response) {

            });
        };

        $scope.changeDatesMain = function () {
            $scope.getInvestmentMainPercent();
        };

        $scope.changeDatesSpecific = function () {
            $scope.getInvestmentHoldingsPercent($stateParams.id);
        };

        //Asset
        function assetChartCreation(doughnutDataArr) {
            Chart.defaults.global.responsive = true;

            var doughnutData = doughnutDataArr;
            var Total = 0.0;
            doughnutData.forEach(function (element) {
                Total = Total + element.value;
            });

            var doughnutOptions = {
                tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' (' + $filter('currency')((value.value / Total) * 100, '', 2) + '%)' },
                percentageInnerCutout: 75,
                segmentShowStroke: false,
                responsive: true

            };
            if (document.getElementById("investmentDoughnut") !== null) {
                var ctx1 = document.getElementById("investmentDoughnut").getContext("2d");
                var myDoughnut = new Chart(ctx1).Doughnut(doughnutData, doughnutOptions);

                document.getElementById('investment-legend').innerHTML = myDoughnut.generateLegend(); // for legend 
            }


        };
        //Asset Chart end

        //Performance Chart Starts
        function createPerformanceChartData() {
        	
            function dateRange(range1, range2) {
                var fstDate = new Date(range1);
                var sndDate = new Date(range2);
                var dateArray = new Array();
                var counterDate = fstDate.getDate();
                var evenFlag = false;
                if ((counterDate % 2) === 0) {
                    evenFlag = true;
                }
                var count = 0;
                while (fstDate >= sndDate) {

                    dateArray.push(angular.copy(fstDate));
                    counterDate = counterDate + 2;
                    if (counterDate > 31) {
                        count++;
                        fstDate = new Date(fstDate.setDate(1))
                        fstDate = new Date(fstDate.setMonth(fstDate.getMonth() + count));
                        counterDate = evenFlag ? 2 : 1;
                    }
                    fstDate = new Date(fstDate.setDate(counterDate))
                    // console.log(counterDate);
                }
                // console.log(dateArray);
                return dateArray;

            };
            
                        
            $(function () {
                $('#containerInvestment').highcharts({
                    chart: {
                        type: 'line',
                        backgroundColor: 'transparent'
                    },
                    title: {
                        text: null
                    },
                    xAxis: {     
                    	categories: $scope.chartLabels,
                        
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            rotation: 0,
                            style: {
                                fontSize: '8px',
                                fontFamily: 'Open Sans',
                                fontStyle: 'italic',

                            },
                            formatter: function () {
                                var data = $filter('date')(this.value, 'M/d');
                                return data;
                            },

                        }


                    },
                    yAxis: {                    	
                        min: 0,
                        max: 1000000,
                        tickInterval:100000,
                        gridLineWidth: 0,
                        lineWidth: 1,
                        title: {
                            text: '',
                            align: 'high'
                        },

                        labels: {
                            rotation: 0,
                            style: {
                                fontSize: '8px',
                                fontFamily: 'Open Sans',
                                fontStyle: 'italic',

                            },
                            formatter: function () {                            	
                                var data = $filter('currency')(this.value, '$', 2);                                
                                return data;
                            },

                        }

                    },
                    tooltip: {
                        backgroundColor: '#191919',
                        forecolor: '#ffffff',
                        borderColor: 'black',
                        borderRadius: 10,
                        borderWidth: 3,
                        valueSuffix: '$',
                        formatter: function () {
                        	var nameTxt = "";
	                        angular.forEach($scope.chartDateData[this.point.date], function(value, key) {
	                        	nameTxt += '<span style="color:#ffffff">' + value.name +' : '+ $filter('currency')(value.value) + '<span>' + ' <br/>'
	                        });
                            return '<span style="color:#ffffff">' + $filter('date')(this.x, 'MM/dd/yyyy') + ': ' + $filter('currency')(this.y, '$', 2) +'<br/>' + nameTxt;
                        }                        
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: false
                            },
                            enableMouseTracking: true,
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        showInLegend: false,
                        data: $scope.chartData,
                    }]
                });
            });
        }


        if (!$stateParams.hasOwnProperty('id')) {
            $scope.getInvestmentHeader();
            $scope.getInvestmentMainAssetChart();
            $scope.getInvestmentMainPercent();
            $scope.getInvestmentAccounts();
        }
        else {
            $scope.getInvestmentAccounts();//specific
        }

    }



})(); 