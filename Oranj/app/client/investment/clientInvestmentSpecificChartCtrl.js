// var app = angular.module('app.fund', []);

(function () {
    'use strict';

    angular.module('app.client.investment')
        .controller('clientInvestmentSpecificChartCtrl', ['$scope', '$timeout', 'linechartObjArray', 'piechartdata','$rootScope', clientInvestmentSpecificChartCtrl]);

    function clientInvestmentSpecificChartCtrl($scope, $timeout, linechartObjArray, piechartdata,$rootScope) {
        $rootScope.breadcrum = "Home / Investment / Leo's Fund"; 

        var linechartObj = {}
        linechartObj.labels = ["January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016", "October 2016"];
        linechartObj.series = [''];
        linechartObj.data = [
            [50000, 80000, 60000, 120000, 140000, 110000, 180000, 200000, 170000, 210000, 230000, 250000, 280000],

        ];
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        linechartObj.options = {
            showScale: false,
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,


        };

        linechartObj.colours = [{
            "fillColor": "#e0f0f1",
            "strokeColor": "#36b8b7",

        }];

        linechartObjArray.reset();
        linechartObjArray.chartDataArray.push(linechartObj);
        // performance chart main right ends




        // leo doughnut chart start

        Chart.defaults.global.responsive = true;

        var doughnutData = [

            {
                label: 'US Bond',
                value: 320,
                name: 'US Bond',
                color: '#3eb649',

            },
            {
                label: 'US Stock',
                value: 1800,
                name: 'US Stock',
                color: '#fdce0b'

            },

        ];

        var doughnutOptions = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 75,
            segmentShowStroke: false,
            responsive: true,

        };

        // var ctx1 = document.getElementById("leosFundDoughnut").getContext("2d");
        // var myDoughnut = new Chart(ctx1).Doughnut(doughnutData, doughnutOptions);

        // document.getElementById('leosFund-legend').innerHTML = myDoughnut.generateLegend(); // for legend 


        // leo doughnut chart end

    }

})(); 