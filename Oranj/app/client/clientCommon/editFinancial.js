(function () {
    'use strict';

    angular.module('app.editFinencialModule', [])
        .controller('editFinencialCtrl', ['$scope', 'world', editFinencialCtrl])

    function editFinencialCtrl($scope, world) {
        $scope.coverage = {};

        $scope.editData = {

            policyName: "Bob's Life",
            deathBenefit: '$860,000.00',
            carrier: 'State Farm',
            permiumPayment: '$2,000.00',
            beneficiaries: 'Full Name',
            contingentPer: '75%',
            policypdate: '06/22/2016',
            policyexpdate: '08/22/2016',
        };

        $scope.editfinProperty = {
            propertyType: "Home",
            address: '1801 N Sedgwick St.,60614',
            city: 'Mountain View',
            zip: '9899081',
            myvalue: '$ 9,890,763.00',
            propertyvalue: '$ 0.00',
            state: "CA",
        };

        $scope.addfinancialcus = {
            accountname: "",
            initialvalue: "",
            currentvalue: "",



        }

        $scope.fintype = ('Private Investment, Public Investment').split(',').map(function (addfinancialcus) {
            return { abbrev: addfinancialcus };
        })
        $scope.addfinancialcus.data = 'Private Investment',


            $scope.user = {
                state: "CA",
            }

        $scope.coverageTypeData = ('Life, Long Term Care, Long Term Disability').split(',').map(function (coverageData) {
            return { abbrev: coverageData };
        })
        $scope.coverage.coverageData = 'Life',

            $scope.policyType = ('Term Life, Permanent Life').split(', ').map(function (policyData) {
                return { abbrev: policyData };
            })
        $scope.coverage.policyData = "Term Life",

            $scope.policyTerm = ('10 11 12').split(' ').map(function (policyTerms) {
                return { abbrev: policyTerms };
            })
        $scope.coverage.policyTerms = '12',

            $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
                'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
                'WY').split(' ').map(function (state) {
                    return { abbrev: state };
                })
        $scope.coverage.state = 'CA',

            $scope.editfinProperty.country = 'US',
            $scope.count = world.countries;


        $scope.purchasedBy = ('Private, Company Provided').split(', ').map(function (purchased) {
            return { abbrev: purchased };
        })
        $scope.coverage.purchased = 'Private',

            $scope.years = ('year month').split(' ').map(function (year) {
                return { abbrev: year };
            })
        $scope.coverage.year = 'year',

            $scope.contingent = ('Contingent Primary').split(' ').map(function (conti) {
                return { abbrev: conti };
            })
        $scope.coverage.conti = 'Contingent'


    }

})();