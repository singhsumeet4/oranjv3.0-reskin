(function () {
    'use strict';

    angular.module('app.editFinencialLTDModule', [])
        .controller('editFinencialLTDCtrl', ['$scope', 'world', editFinencialLTDCtrl])

    function editFinencialLTDCtrl($scope, world) {
        $scope.coverage = {};

        $scope.editData = {

            policyName: "Bob's Life",
            carrier: 'State Farm',
            percentageSalaryCovered: '50',
            salaryCovered: '$ 50,000',
            purchasedate: '06/22/2016'
        };

        $scope.coverageTypeData = ('Life, Long Term Care, Long Term Disability').split(', ').map(function (coverageData) {
            return { abbrev: coverageData };
        })
        $scope.coverage.coverageData = "Long Term Disability",


        $scope.contractLength = ('10 11 12 13 14 15').split(' ').map(function (contractLength) {
                return { abbrev: contractLength };
            })
        $scope.coverage.contractLength = '15',

        $scope.purchasedBy = ('Private, Company Provided').split(', ').map(function (purchased) {
            return { abbrev: purchased };
        })
        $scope.coverage.purchased = 'Private';


    }

})();