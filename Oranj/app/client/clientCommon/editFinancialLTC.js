(function () {
    'use strict';

    angular.module('app.editFinencialLTCModule', [])
        .controller('editFinencialLTCCtrl', ['$scope', 'world', editFinencialLTCCtrl])

    function editFinencialLTCCtrl($scope) {
        $scope.coverage = {};

        $scope.editData = {

            policyName: "Marx Insurance",
            carrier: 'State Farm',
            perDayBenefit: '$ 150',
            homeHealthCare: '$ 100',
            AdultDayCare: '$ 100',   
            purchasedate: '06/22/2016',
            assistedLivingFacility: '$ 100'
        };


        $scope.coverageTypeData = ('Life, Long Term Care, Long Term Disability').split(', ').map(function (coverageData) {
            return { abbrev: coverageData };
        })
        $scope.coverage.coverageData = "Long Term Care",

        $scope.contractLength = ('10 11 12').split(' ').map(function (contractLength) {
                return { abbrev: contractLength };
            })
        $scope.coverage.contractLength = '10',

        $scope.eliminationPeriod = ('28 29 30').split(' ').map(function (eliminationPeriods) {
                return { abbrev: eliminationPeriods };
            })
        $scope.coverage.eliminationPeriods = '28';

    }

})();