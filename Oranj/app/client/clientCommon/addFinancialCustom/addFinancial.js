(function() {

    'use strict';

    angular.module('app.addFinancialModule', ['fcsa-number'])
    .controller('addFinancialController', ['$scope', addFinancialController])

    function addFinancialController($scope) {
        
        $scope.addFinancialData={
            accountName:'Matt Account',
            initialValue:'0.00',
            currentValue:'0.00',
        }

        $scope.finencialType =("Private Investment,Public Investment").split(',').map(function (addfinancial) {
            return { abbrev: addfinancial };
        })

        $scope.finencialType.defaultaddfinancial = "Private Investment"

        //   $scope.AccountType = ("Private Investment,Public Investment").split(',').map(function (Account) {
        //         return { abbrev: Account };
        //     })
        // $scope.addFinancialData.Account = "Private Investment"
    }
    
})();