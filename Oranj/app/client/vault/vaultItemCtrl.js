;(function (){

 	'use strict';

 	angular.module('app.ctrl').controller('VaultItemCtrl', VaultItemCtrl);

 	VaultItemCtrl.$inject = ['$scope', '$mdDialog', '$mdMedia'];

 	function VaultItemCtrl($scope, $mdDialog, $mdMedia){
 		$scope.isChatWindowVisible = true;
 		$scope.isPdfHeaderVisible = false;

 		$scope.toggleChatWindow = function(){
 			if($scope.isChatWindowVisible)
 				$scope.isChatWindowVisible = false;
 			else
 				$scope.isChatWindowVisible = true;
 		};


 		$scope.showAdvanced = function(ev) {
		    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
		    $scope.isPdfHeaderVisible = true;

		    $mdDialog.show({				
				templateUrl: 'dialog1.tmpl.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				//clickOutsideToClose:true,
				fullscreen: useFullScreen
		    })
		    .then(function(answer) {
		      	$scope.status = 'You said the information was "' + answer + '".';
		    }, function() {
		      	$scope.status = 'You cancelled the dialog.';
		    });

	  //     	$scope.cancel = function() {
	  //     		$scope.isPdfHeaderVisible = false;
			//     $mdDialog.cancel();
			// };

			$scope.answer = function(answer) {
			    $mdDialog.hide(answer);
			};

		    $scope.$watch(function() {
		      	return $mdMedia('xs') || $mdMedia('sm');
		    }, function(wantsFullScreen) {
		      	$scope.customFullscreen = (wantsFullScreen === true);
		    });
	  	};

  		$scope.cancel = function() {
  			$scope.isPdfHeaderVisible = false;
		    $mdDialog.cancel();
		};
 	};

})();