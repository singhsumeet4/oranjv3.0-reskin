(function () {
    'use strict';

    angular.module('app.client.vault')
   .controller('VaultHomeCtrl',  VaultHomeCtrl);

    VaultHomeCtrl.$inject = ['$scope', '$state', '$rootScope', '$cookies', 'GlobalFactory', '$http', '$mdDialog', '$mdMedia', 'OranjApiService', '$window', '$q', '$stateParams', '$timeout', 'vaultDataService'];

    function VaultHomeCtrl($scope, $state, $rootScope, $cookies, GlobalFactory, $http, $mdDialog, $mdMedia, OranjApiService, $window, $q, $stateParams, $timeout, vaultDataService) {

        $scope.showCreateFolderDiv = false;
        $scope.isChatWindowVisible = true;
        $scope.isPdfHeaderVisible = false;
        $scope.global = GlobalFactory;
        $scope.global.vaultsFolderName = "vault";
        $scope.global.sharedData.isRootFolder = true;
        $scope.searchKeyword = '';
        var vm = this;
        var role = GlobalFactory.getRole();
        
        role.then(
  	            function (result) {
  	                if (result.data) {
  	                    if (GlobalFactory.lookupRole('ROLE_USER', result.data)) {

  	                    } else if (GlobalFactory.lookupRole('ROLE_PROSPECT', result.data) || GlobalFactory.lookupRole('ROLE_CLIENT', result.data)) {

  	                    } else if (GlobalFactory.lookupRole('ROLE_ADVISOR', result.data)) {

  	                    } else {
  	                        GlobalFactory.signout();
  	                    }
  	                } else {
  	                    GlobalFactory.signout();
  	                }
  	            }, function () {
  	                GlobalFactory.signout();
  	            }
  	   	);

        $scope.initVault = function () {

            $scope.vaultHeader = "My Vault";
            $scope.openFolderStat = "vault";
            $scope.global.vaultsFolderName = "vault";
            $scope.folderCount = 0;
            $scope.global.fileCount = 0;
            $scope.isRecentActive = false;
            $scope.isVaultActive = true;
            $scope.isStarredActive = false;
            $scope.global.sharedData.isRootFolder = true;

            vaultDataService.fetchVaultRootList()   
           .then(function (results) {

               var data = vaultDataService.prepareVaultRootData(results)
               $scope.vaultFolders = data.vaultFolders;
               $scope.global.sharedData.sideVaultList = data.vaultFolders;
               $scope.vaultFiles = data.vaultFiles;
               $scope.global.fileCount = $scope.vaultFiles.length + data.totalFileCount + data.folderCount;
               $scope.totalCount = data.folderCount + $scope.vaultFiles.length;

               $scope.global.sharedData.sideVaultList.sort(function (a, b) {
                   if (a.folder.toLowerCase() < b.folder.toLowerCase()) return -1;
                   if (a.folder.toLowerCase() > b.folder.toLowerCase()) return 1;
                   return 0;
               });

           });

        };

        $scope.recentVault = function () {

            $scope.vaultHeader = "Recent";
            $scope.openFolderStat = "recent"
            $scope.global.vaultsFolderName = "recent";

            $scope.isRecentActive = true;
            $scope.isVaultActive = false;
            $scope.isStarredActive = false;

            vaultDataService.fetchVaultRecentList().then(function (results) {
                var data = vaultDataService.prepareVaultRecentData(results)
                $scope.vaultFolders = [];
                $scope.vaultFiles = [];

                $scope.vaultFolders = data.vaultFolders;
                $scope.vaultFiles = data.vaultFiles;

                $scope.global.fileCount = $scope.vaultFiles.length + $scope.vaultFolders.length;
                $scope.totalCount = $scope.global.fileCount;
            });
        }
            
        $scope.starredVault = function () {

            $scope.vaultHeader = "Starred";
            $scope.openFolderStat = "starred"
            $scope.global.vaultsFolderName = "starred";         

            $scope.isRecentActive = false;
            $scope.isVaultActive = false;
            $scope.isStarredActive = true;

            OranjApiService('getVaultStarFolder').then(function (response) {
                $scope.vaultFolders = [];
                if (response.status == "success") {
                    $scope.vaultFolders = response.data.folders;
                    $scope.folderCount = $scope.vaultFolders.length;
                }
            }, function (response) {

            });

            OranjApiService('getVaultStarFile').then(function (response) {
                $scope.vaultFiles = [];
                if (response.status == "success") {
                    $scope.vaultFiles = response.data.userFiles;
                    $scope.global.fileCount = $scope.vaultFiles.length;
                }
                $scope.totalCount = $scope.folderCount + $scope.fileCount;
            }, function (response) {

            });

        };
               
        $scope.files = [];
      
        $scope.isChatWindowVisible = false;
        $scope.showChatWindow = function () {
            $scope.isChatWindowVisible = true;
        };

        $scope.hideChatWindow = function () {
            $scope.isChatWindowVisible = false;
        };

        $scope.getFolderName = function (name) {
            GlobalFactory.vaultsFolderName = name;
        }

        $scope.getFolderData = function (folder) {          
            $scope.openFolderStat = folder;
            $scope.global.vaultsFolderName = folder;
            $scope.global.sharedData.isRootFolder = false;
                   
            $scope.vaultFolders = [];            
            OranjApiService('getVaultFolderData', null, {
                'folder': folder
            }
            ).then(function (response) {
                if (response.status == 'success') {
                    $scope.vaultFiles = response.data.userFiles;
                    $scope.totalCount = $scope.vaultFiles.length;
                   // $scope.totalCount = $scope.global.fileCount;
                }
                else {
                    $scope.vaultFiles = [];
                   // $scope.global.fileCount = 0;
                    $scope.totalCount = 0;
                }
            }, function (response) {

            });
            
            $scope.vaultHeader = folder;
        };

        $scope.deleteFolder = function (folder) {
        	
        	var confirmation = $mdDialog.confirm({
                title: 'Are you sure?',
                htmlContent: '<div> This folder <b>'+folder+'</b> will be deleted immediately.',
                ok: 'Delete',
                cancel : 'Cancel'
            });
          
            $mdDialog.show(confirmation).then(function(answer) {
            	OranjApiService('vaultDeleteFolder', {
                    data: {
                        "folder": folder,
                    }
                }).then(function (response) {
     			    if ($scope.openFolderStat == "vault") {
     			        $scope.initVault();
     			    } else if ($scope.openFolderStat == "starred") {
     			        $scope.starredVault();
     			    } else if ($scope.openFolderStat == "recent") {
     			        $scope.recentVault();
     			    } else {
     			        $scope.getFolderData($scope.openFolderStat);
     			    }
     			}, function (response) {

     			});
            });

            

        };

        $scope.deleteFile = function (id,name) {
        	var confirmation = $mdDialog.confirm({
                title: 'Are you sure?',
                htmlContent: '<div> This file <b>'+name+'</b> will be deleted immediately.',
                ok: 'Delete',
                cancel : 'Cancel'
            });

        	$mdDialog.show(confirmation).then(function(answer) {
	            OranjApiService('vaultDeleteFile', null, {
	                'id': id
	            }
	 			).then(function (response) {
	 			    if ($scope.openFolderStat == "vault") {
	 			        $scope.initVault();
	 			    } else if ($scope.openFolderStat == "starred") {
	 			        $scope.starredVault();
	 			    } else if ($scope.openFolderStat == "recent") {
	 			        $scope.recentVault();
	 			    } else {
	 			        $scope.getFolderData($scope.openFolderStat);
	 			    }
	 			}, function (response) {
	
	 			});
        	});

        };

        $scope.updateVault = function (id, status) {

            if (status == true) {
                status = false;
            } else {
                status = true;
            }
            OranjApiService('updateVault', {
                data: {
                    'id': id,
                    'starred': status
                }
            }).then(function (response) {
                if ($scope.openFolderStat == "vault") {
                    $scope.initVault();
                } else if ($scope.openFolderStat == "starred") {
                    $scope.starredVault();
                } else if ($scope.openFolderStat == "recent") {
                    $scope.recentVault();
                } else {
                    $scope.getFolderData($scope.openFolderStat);
                }
            }, function (response) {

            });

        };

        $scope.shareVault = function (id, status) {

            if (status == true) {
                status = false;
            } else {
                status = true;
            }
            OranjApiService('updateVault', {
                data: {
                    'id': id,
                    'shared': status
                }
            }).then(function (response) {
                if ($scope.openFolderStat == "vault") {
                    $scope.initVault();
                } else if ($scope.openFolderStat == "starred") {
                    $scope.starredVault();
                } else if ($scope.openFolderStat == "recent") {
                    $scope.recentVault();
                } else {
                    $scope.getFolderData($scope.openFolderStat);
                }
            }, function (response) {

            });
        };

        $scope.toggleChatWindow = function () {
            if ($scope.isChatWindowVisible)
                $scope.isChatWindowVisible = false;
            else
                $scope.isChatWindowVisible = true;
        };

        $scope.showAdvanced = function (ev, fileUrl, fileName, fileDate, fileSize, fileId) {
        	
        	$scope.global.vaultsfilePath = '';
        	$scope.global.vaultsName = '';
        	OranjApiService('getVaultFilePath', null, {
                'id': fileId
            }
            ).then(function (response) {
                if (response.status == 'success') {
                	 $scope.filePath = response.data.vaultFile;
                	 $scope.global.vaultsfilePath= $scope.filePath;
                	 $scope.global.vaultsName = fileName;
                     var fileArray = fileUrl.split('.');
                     
                     
                     $scope.global.vaultsfileType=  "text/" + fileArray[fileArray.length - 1];            
                     $scope.fileType = "text/" + fileArray[fileArray.length - 1];
                     $scope.filePath1 = $scope.filePath;
                     $scope.fileName = fileName;
                     $scope.fileDate = fileDate;
                     $scope.fileSize = fileSize;
                     $scope.fileId   = fileId;
                     $scope.global.vaultsfileId=fileId;
                     $scope.isChatWindowVisible = true;

                     var filetypelist = ["jpg", "jpeg", "png", "gif", "pdf", "bmp"];
                     var typeindex = filetypelist.indexOf(fileArray[fileArray.length - 1]);
                     if (typeindex >= 0) {

                         var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
                         $scope.isPdfHeaderVisible = true;

                         $mdDialog.show({
                             templateUrl: 'dialog1.tmpl.html',
                             parent: angular.element(document.body),
                             targetEvent: ev,
                             controller: 'VaultHomeCtrl',                   
                             //clickOutsideToClose:true,
                             fullscreen: useFullScreen,
                             preserveScope: true,
                             onComplete: function () {

                             }
                         })
         			    .then(function (answer) {
         			        console.log('test');
         			        $scope.status = 'You said the information was "' + answer + '".';
         			    }, function () {
         			        $scope.status = 'You cancelled the dialog.';
         			    });
                     }
                     else {
                         var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
                         $scope.isPdfHeaderVisible = true;

                         $mdDialog.show({
                             templateUrl: 'dialog2.tmpl.html',
                             parent: angular.element(document.body),
                             targetEvent: ev,
                             controller: 'VaultHomeCtrl',                    
                             //clickOutsideToClose:true,
                             fullscreen: useFullScreen,
                             preserveScope: true,
                             onComplete: function () {

                             }
                         })
                     }
                     $scope.answer = function (answer) {
                         $mdDialog.hide(answer);
                     };

                     $scope.$watch(function () {
                         return $mdMedia('xs') || $mdMedia('sm');
                     }, function (wantsFullScreen) {
                         $scope.customFullscreen = (wantsFullScreen === true);
                     });                	 
                }
                else {
                   
                }
            }, function (response) {

            });
        	

        };

        $scope.cancel = function () {
            $scope.isPdfHeaderVisible = false;
            $mdDialog.cancel();
        };

        $scope.downloadFromList = function (e, item) {
            //console.log(item);
        	OranjApiService('getVaultFilePath', null, {
                'id': item.id
            }
            ).then(function (response) {
                if (response.status == 'success') {
                	 $scope.filePath = response.data.vaultFile;
                	 $scope.global.vaultsfilePath= $scope.filePath;
                	 $scope.global.vaultsName = item.name;
                	 $scope.fileName = item.name;
                     $scope.downloadfile(e,item.id,$scope.fileName,$scope.global.vaultsfilePath);
                }
                else {
                   
                }
            }, function (response) {

            });
            
           
        }

        $scope.downloadfile = function (e, id, fileName, filePath) {
            e = e || window.event;
            e.preventDefault();

            var fileURL = filePath;
            var fileName = fileName;
            if (!window.ActiveXObject) {
                //console.log('non ie');
                var save = document.createElement('a');
                save.href = fileURL;
                save.target = '_blank';
                save.download = fileName || 'unknown';

                try {
                    var evt = new MouseEvent('click', {
                        'view': window,
                        'bubbles': true,
                        'cancelable': false
                    });
                    save.dispatchEvent(evt);
                    (window.URL || window.webkitURL).revokeObjectURL(save.href);
                } catch (e) {
                    //console.log('catch');
                    window.open(fileURL, fileName);
                }
            }            // for IE < 11
            else if (!!window.ActiveXObject && document.execCommand) {
                //console.log('ie 11');
                var _window = window.open(fileURL, '_blank');
                _window.document.close();
                _window.document.execCommand('SaveAs', true, fileName || fileURL)
                _window.close();
            }
            $scope.increaseCount(id);
        };
        
        $scope.increaseCount = function (id) {
        	OranjApiService('downloadFrequency', null, {
                'id': id
            }).then(function (response) {
                
            }, function (response) {

            });
        };

        $scope.showHideDiv = function () {

            if ($scope.showCreateFolderDiv == true) {
                $scope.showCreateFolderDiv = false;
            } else {
                $scope.showCreateFolderDiv = true;
            }

            if ($scope.showCreateFolderDiv == undefined) {
                $scope.showCreateFolderDiv = true;
            }
            if ($scope.showCreateFolderDiv == true) {
                $scope.add = !$scope.add;

                setTimeout(function () {
                    var element = $window.document.getElementById('newName');
                    if (element)
                        element.focus();
                }, 0);
            }
        };

        $scope.createFolder = function () {

            if ($scope.newName != null) {
                OranjApiService('vaultCreateFolder', {
                    data: {
                        "folder": $scope.newName,
                        "shared": false,
                        "type": "folder"
                    }
                }).then(function (response) {
                    $scope.newName = null;
                    if (response.status == "success") {
                        $scope.showCreateFolderDiv = false;
                        $scope.initVault();
                    } else {
                        alert(response.message);
                        $scope.showCreateFolderDiv = false;
                        $scope.initVault();
                    }

                }, function (response) {

                });
            } else {
                alert("Folder Name is required. Please try again.");
                $scope.showCreateFolderDiv = false;
            }
        };

        // code for Show rename field

        $scope.showRenameFolderField = function (id, name) {
            $scope.folderId = id;
            $scope.folderName = name;
        }

        $scope.renameFolder = function (folderNewName) {
            if (folderNewName != undefined) {
                OranjApiService('vaultRenameFolder', {
                    data: {
                        "id": $scope.folderId,
                        "folder": folderNewName,
                    }
                }).then(function (response) {
                    if (response.status == "success") {
                        $scope.initVault();
                    } else {
                        alert(response.message);
                        $scope.initVault();
                    }
                }, function (response) {

                });
            } else {
                $scope.folderName = null;
            }
        };
        //For sorting
        $scope.propertyFolder = 'folder';
        $scope.propertyName = 'name';
        $scope.reverse = false;

        $scope.sortBy = function (propertyFolder, propertyName) {
            $scope.reverse = ($scope.propertyFolder === propertyFolder) ? !$scope.reverse : false;
            $scope.propertyFolder = propertyFolder;
            $scope.propertyName = propertyName;
        };

        $scope.sortByDynamic = function (propertyFolder, propertyName) {
            var flag = 0;

            if ($scope.vaultFolders != undefined && $scope.vaultFolders.length > 0) {
                flag = flag + 1;
            }
            if ($scope.vaultFiles != undefined && $scope.vaultFiles.length > 0) {
                flag = flag + 1;
            }
            if (flag == 1) {
                $scope.reverse = ($scope.propertyFolder === propertyFolder) ? !$scope.reverse : false;
                $scope.propertyFolder = propertyFolder;
                $scope.propertyName = propertyName;
            }
        };

        $scope.vaultUpload = function (elem, event) {

            var folderName = "";
            var vaultUploadUrl = 'vaultUploadRoot';                 

            if (($scope.global.vaultsFolderName != "vault") && ($scope.global.vaultsFolderName != "recent") && ($scope.global.vaultsFolderName != "starred")) {
                folderName = $scope.global.vaultsFolderName;
                vaultUploadUrl = 'vaultUploadFolder';
            } else {
                folderName = "";
                vaultUploadUrl = 'vaultUploadRoot';
            }

            var files = elem.files;
            fileUploadHelper(files, vaultUploadUrl, folderName, 0, function (totalUploadedFiles) {
                if (!angular.isUndefinedOrNull(totalUploadedFiles)) {
                    if (files.length == totalUploadedFiles) {
                        elem.value = null;
                        $scope.global.showSuccessUploadFile();
                        if ($scope.openFolderStat == "vault") {
                            $scope.initVault();
                        }
                        else {
                            console.log('$scope.global.fileCount before', $scope.global.fileCount);
                            $scope.getFolderData($scope.openFolderStat);
                            onSuccess = function (res) {
                                var data = vaultDataService.prepareVaultData(res);
                                $scope.global.fileCount = data.totalFileCount;
                            }
                            vaultDataService.fetchVaultRootList().then(onSuccess, function (err) { console.log(error) });                           
                                                      
                        }
                    }
                }
            });
        }                 

        function validateFileSize(file) {
            var sizeInMb = (file.size / (1024 * 1024));

            if (sizeInMb > 10)
                return false;
            else
                return true;
        };

        function validateFileExtension(filename) {
            var extn = filename.split(".").pop().toLowerCase();
            var INVALID_VALID_FILE_EXTNS = ['bin', 'exe'];

            if (INVALID_VALID_FILE_EXTNS.indexOf(extn) > -1)
                return false;
            else
                return true;
        };
                
        function fileUploadHelper(files, vaultUploadUrl, folderName, index, cb) {
            var file = files[index];
            if (angular.isUndefinedOrNull(file)) return;

            var formData = new FormData();
            formData.append('fileMult', file);

            if (!validateFileExtension(file.name)) {
                GlobalFactory.showErrorAlert("You can't upload files of this type.");
                return cb();
            }

            if (!validateFileSize(file)) {
                GlobalFactory.showErrorAlert('Maximum Upload size is 10 MB.');
                return cb();
            }              

            function success(res) {
                if (res.status == 'success') {
                    index++;
                    if (files.length == index) {
                        return cb(index)
                    }
                    else {
                        fileUploadHelper(files, vaultUploadUrl, folderName, index, cb)
                    }
                }
                else {
                    GlobalFactory.showErrorAlert(res.message || 'An error occurred with the file upload');
                    return cb();
                }
            }

            function failed(error) {
                GlobalFactory.showErrorAlert(error.message || 'An error occurred with the file upload');
                return cb();
            }         

            var headers = {'Content-Type': undefined }
            OranjApiService(vaultUploadUrl, {
                data: formData,
                transformRequest: angular.identity,
                headers: headers
            }, { 'folder': folderName }).then(function (response) {
                success(response);
            }, function (response) {
                failed(response);
            });
        };

        $scope.$on('vaultClientClick', function (event, val) {   
            if (val.vaultName == 'newFolder') {
                $scope.showHideDiv();
            }
            else if (val.vaultName == 'vaultupload') {                
                setTimeout(function () {
                    angular.element('#upload').trigger('click');
                }, 0);
            }
            else if (val.vaultName == 'vaultSearch') {
                $scope.searchKeyword = val.folderName;
            }
        });

        $scope.searchVault = function (searchString) {

            $scope.vaultHeader = "Search";
            $scope.openFolderStat = "search"
            $scope.global.vaultsFolderName = "search";

            $scope.isRecentActive = true;
            $scope.isVaultActive = false;
            $scope.isStarredActive = false;

            vaultDataService.fetchVaultSearchList(searchString).then(function (results) {
               $scope.vaultFolders = [];
               $scope.vaultFiles = [];
               if (results.status === "success") {
                   $scope.vaultFolders = results.data.folders;
                   $scope.vaultFiles = results.data.files;
                  
                   $scope.totalCount = $scope.vaultFiles.length + $scope.vaultFolders.length;
                  // $scope.global.fileCount = $scope.vaultFiles.length + $scope.vaultFolders.length;
               }
               else {                  
                   $scope.totalCount = 0;
                  // $scope.global.fileCount = 0;
               }
            }, function (error) {
                $scope.vaultFolders = [];
                $scope.vaultFiles = [];
                $scope.totalCount = 0;
            });
        };

        $scope.searchVaultMain = function (searchString) {
            if (!angular.isUndefinedOrNull(searchString))
                $state.go('page.client.vault-home.search', { searchString: searchString });
            else
                $scope.global.showErrorAlert('Input some value to search');
        }
      
        if ($state.current.name === 'page.client.vault-home.recent') {
            $scope.global.sharedData.isRootFolder = false;
            $scope.recentVault();
        }
        else if ($state.current.name === 'page.client.vault-home.dashboard') {
            $scope.initVault();
        }
        else if ($state.current.name === 'page.client.vault-home.search') {
            var searchString = $stateParams.searchString
            $scope.searchVault(searchString);
        }
        else if ($state.current.name === 'page.client.vault-home.folder') {

            if (!$stateParams.hasOwnProperty('folderName')) {
                $scope.initVault();
            }
            else {
                $scope.vaultFiles = [];              
                $scope.totalCount = 0;               
                vm.FolderName = $stateParams.folderName;
                $scope.getFolderData($stateParams.folderName);
            }
        }

        $rootScope.$broadcast("vaultSearch", { searchString: $stateParams.searchString });
        var itemObj = $stateParams.obj;
    	$stateParams.obj = null;
        if(itemObj) {
        	
//        	$scope.Timer = $interval(function () {
//        		$interval.cancel($scope.Timer);//alert("sdf");
//        		$scope.showAdvanced(null, itemObj.fileUrl, itemObj.fileName, itemObj.fileDate, itemObj.fileSize, itemObj.fileId);
//            }, 5000);
        	var timer1 = $timeout(function () {
        		//$interval.cancel($scope.Timer);alert("sdf");
        		if(angular.element('#file'+itemObj.fileId)) {//alert("123");
            		//angular.element('#file'+itemObj.fileId).trigger('click');
        			//$event.stopPropagation();
            		//angular.element('#file'+itemObj.fileId).triggerHandler('click');
        			var timer2 = $timeout(function() {
        				if($rootScope.sFlag) {
        					$timeout.cancel( timer2 );
	            			angular.element('#file'+itemObj.fileId).triggerHandler('click');
	            			itemObj = null;
	            			$rootScope.sFlag = false;
	            		}
        				
            		 }, 100);
        			
            	}
            	else {
            		
            	}
        		$timeout.cancel( timer1);
            }, 3000);
        	
        }
        
    };

})();


