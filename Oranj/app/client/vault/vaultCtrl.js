(function () {
    'use strict';

    angular.module('app.client.vault')
        .controller('VaultCtrl', ['OranjApiService', '$scope', '$state', '$q', 'vaultDataService', function (OranjApiService, $scope, $state, $q, vaultDataService) {

            vaultDataService.fetchVaultRootList().then(function (res) {
                function onSuccess(res) {
                    var data = vaultDataService.prepareVaultData(res);
                    $scope.global.sharedData.sideVaultList = data.sideVaultList;

                    $scope.currentState = $state.current.name;
                    if ( $scope.currentState === 'page.client.vault-home.recent') {

                    }
                    else {
                        $scope.global.fileCount = data.totalFileCount;
                    }

                    $scope.global.sharedData.sideVaultList.sort(function (a, b) {
                        if (a.folder.toLowerCase() < b.folder.toLowerCase()) return -1;
                        if (a.folder.toLowerCase() > b.folder.toLowerCase()) return 1;
                        return 0;
                    });
                }
                vaultDataService.fetchVaultRootList().then(onSuccess, function (err) { console.log(error) });

            }, function (error) {

            });


        }]);
})();
