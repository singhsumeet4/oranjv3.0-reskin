;(function (){

 	'use strict';

 	angular.module('app.ctrl', ['ngMaterial']).controller('VaultShareCtrl', VaultShareCtrl);

 	VaultShareCtrl.$inject = ['$scope', '$mdDialog', '$mdMedia'];

 	function VaultShareCtrl($scope, $mdDialog, $mdMedia){
 		$scope.showAdvanced = function(ev) {
		    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

		    $mdDialog.show({				
				templateUrl: 'dialog1.tmpl.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				fullscreen: useFullScreen
		    })
		    .then(function(answer) {
		      	$scope.status = 'You said the information was "' + answer + '".';
		    }, function() {
		      	$scope.status = 'You cancelled the dialog.';
		    });

	      	$scope.cancel = function() {
			    $mdDialog.cancel();
			};

			$scope.answer = function(answer) {
			    $mdDialog.hide(answer);
			};

		    $scope.$watch(function() {
		      	return $mdMedia('xs') || $mdMedia('sm');
		    }, function(wantsFullScreen) {
		      	$scope.customFullscreen = (wantsFullScreen === true);
		    });
	  	};
 	};

})();