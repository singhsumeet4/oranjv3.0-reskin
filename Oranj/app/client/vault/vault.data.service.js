﻿(function () {

	angular.module('app.client.vault').factory('vaultDataService', ['OranjApiService', '$q', vaultDataService]);

	function vaultDataService(OranjApiService, $q) {

		return {

			fetchVaultRootList: function () {
				var defer = $q.defer();
				var promiseA = OranjApiService('getVaultFolderListAll');  //Folder List
				var promiseB = OranjApiService('getVaultFileListAll'); //file List

				function onSuccess(res) {
					defer.resolve(res);
				}

				function onError(error) {
					defer.reject(error);
				}
                
				$q.all([promiseA, promiseB]).then(onSuccess, onError);			   
				return defer.promise;
			},

			fetchVaultSearchList: function (searchString) {
			    var defer = $q.defer();
			    function onSuccess(res) {
			        defer.resolve(res);
			    }

			    function onError(error) {
			        defer.reject(error);
			    }

			    OranjApiService('getVaultSearchList',{
			    	data: {
                        "searchString": searchString
                    }
			    }).then(onSuccess, onError);
			   
			    return defer.promise;
			},

			fetchVaultRecentList: function () {
			    var defer = $q.defer();
			    function onSuccess(res) {
			        defer.resolve(res);
			    }

			    function onError(error) {
			        defer.reject(error);
			    }

			    OranjApiService('getVaultRecentList').then(onSuccess, onError);

			    return defer.promise;
			},

			prepareVaultData: function (res) {
				var dataObj = {
					folderCount: 0,
					totalFileCount: 0,
					sideVaultList: ''
				};
				var folders = [];
				var files = [];
                
				if (res[0].status === 'success' && res[1].status === 'success') {
					folders = res[0].data.folders;
					files = res[1].data.userFiles;
				}
				else if (res[0].status === 'success') {
					folders = res[0].data.folders;					
				}
				if (res[1].status === 'success') {					
					files = res[1].data.userFiles;
				}

				dataObj.sideVaultList = folders;
				var folderCount = folders.length;
				var totalFileCount = 0;

				folders.map(function (item) {
					totalFileCount += item.totalFilesCount;
				});

				var vaultFiles = files;
				dataObj.totalFileCount = vaultFiles.length + totalFileCount + folderCount;
				dataObj.folderCount = folderCount;			

				return dataObj;
			},

			prepareVaultRootData: function (results) {
			    var dataObj = {
			        vaultFolders: '',
			        vaultFiles:'',
			        folderCount: 0,
			        sideVaultList: '',
			        totalFileCount:0
			    };
			
			    if (results[0].status == "success" && results[1].status == "success") {
			        dataObj.vaultFolders = results[0].data.folders;
			        dataObj.folderCount = dataObj.vaultFolders.length;
			        dataObj.sideVaultList = results[0].data.folders;

			        dataObj.totalFileCount = 0;

			        dataObj.vaultFolders.map(function (item) {
			            dataObj.totalFileCount += item.totalFilesCount;
			        });

			        dataObj.vaultFiles = results[1].data.userFiles;			     
			    }
			    else if (results[0].status == "success") {
			        dataObj.vaultFolders = results[0].data.folders;
			        dataObj.folderCount = $scope.vaultFolders.length;
			        dataObj.sideVaultList = results[0].data.folders;

			        dataObj.totalFileCount = 0;

			        dataObj.vaultFolders.map(function (item) {
			            dataObj.totalFileCount += item.totalFilesCount;
			        });

			        dataObj.vaultFiles = [];			       
			    }
			    else if (results[1].status == "success") {

			        dataObj.totalFileCount = 0;
			        dataObj.vaultFiles = results[1].data.userFiles;			        
			    }                
			    return dataObj;
			},

			prepareVaultRecentData: function (response) {
			    var dataObj = {
			        vaultFolders: [],
			        vaultFiles: []
			    };

			    if (response.status === "success") {
			        for (var i = 0; i < response.data.userFiles.length; i++) {
			            if (response.data.userFiles[i].type === 'folder') {
			                dataObj.vaultFolders.push(response.data.userFiles[i]);
			            }
			            else if (response.data.userFiles[i].type === 'file') {
			                dataObj.vaultFiles.push(response.data.userFiles[i]);
			            }
			        }
			    }
			    return dataObj;
			}


		}
	}
})();
