(function () {
    'use restrict';

    angular.module('app.accountMain', [])
        .controller('clientAccountMainCtrl', ['$scope', '$rootScope', 'jQlinechart','OranjApiService','$filter', '$state', 'GlobalFactory', clientAccountMainCtrl])


    function clientAccountMainCtrl($scope, $rootScope, jQlinechartFactory, OranjApiService, $filter, $state, globalFactory) {

        function createHistoryChartData() {
            window.$filter = $filter;
            var data = {
                labels: $scope.chartDate,
                datasets: [
                    {
                        label: "Debit",
                        type: 'line',
                        fillColor: "transparent",
                        strokeColor: "red",
                        pointColor: "red",
                        pointStrokeColor: "red",
                        pointHighlightFill: "red",
                        pointHighlightStroke: "red",
                        data: $scope.dataDebit,

                    },
                    {
                        label: "Credit",
                        type: 'line',
                        fillColor: "transparent",
                        strokeColor: "#3DB548",
                        pointColor: "#3DB548",
                        pointStrokeColor: "#3DB548",
                        pointHighlightFill: "#3DB548",
                        pointHighlightStroke: "#3DB548",
                        data: $scope.dataCredit,
                    },
                ]
            };

            var lineoptions = {
                scaleShowGridLines: false,
                pointDot: false,
                bezierCurve: false,
                showScale: true,
                multiTooltipTemplate: function (value) { return ' $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
                tooltipTemplate: '<%if (label){%><%=moment(new Date(label)).format("MMMM DD,YYYY")%> : <%}%><%=$filter("currency")(value, "$" , 0)%>',
                scaleFontSize: 10,
                scaleLabel: "<%= ' $' + Number(value)%>",
                legendTemplate: '<table class=\"box_table\"><tr>'
                + '<% for (var i=0; i<datasets.length; i++) { %>'
                + '<td><div class=\"boxx\" style=\"background-color:<%=datasets[i].strokeColor %>\"></div></td>'
                + '<% if (datasets[i].labelShow) { %><td class=\"box_bar\"><%= datasets[i].labelShow %></td><% } %>'
                + '<% } %>'
                + '</tr></table>',
            }

            jQlinechartFactory.jQLineChart("canvas", data, lineoptions);//parameter sequence 1 canvas ID,2 Data to build, 3 options to configure          
        }
        
        $scope.networthsDetailsinv = [];
        var endDate = $filter('date')(new Date(), 'MMMM dd yyyy');
        var startDate = moment(new Date()).subtract(30, 'days').format('MMMM DD YYYY');        
        $scope.date = { 'start': startDate, 'end': endDate };
     
        $scope.yodleeItemAccountId = '';
        $scope.hasTranscations = false;
        $scope.networthsDetailsinv = [];     
        globalFactory.sharedData.yodleeItemAccountId = 0;
        var cacheObjTrans = [];
     
        // Client Account Main Header Api Calling
       
        $scope.getAccountMainHeader = function(){
            OranjApiService('getAccountMainHeader').then(function(response){
                if(response.status == "success")
                    $scope.headerData = response.data;
            },function(response){
                 
            });
        };        

        $scope.$on('accountDetails', function (event, val) {
            $scope.networthsDetailsinv = val.sideAccountList;
        });        

        $scope.getTranscations = function (id) {
            var start = $filter('date')(new Date($scope.date.start), 'yyyy-MM-dd');
            var end = $filter('date')(new Date($scope.date.end), 'yyyy-MM-dd');
            var TransData = { 'startDate': start, 'endDate': end, "yodleeItemAccountId": id };
            if (cacheObjTrans.hasOwnProperty(id)) {                
                var transPresent = true;
            }
            if (!transPresent) {
                OranjApiService('getTranscations', {
                    data: JSON.stringify(TransData)
                }).then(function (response) {
                    if (response.status == "success") {
                        for (var i = 0 ; i < $scope.networthsDetailsinv.length; i++) {
                            if ($scope.networthsDetailsinv[i] != undefined)
                                if ($scope.networthsDetailsinv[i].accountId == id) {
                                    response.data.transactions.transactions.sort(function (a, b) {
                                        var dateA = new Date(a.transactionDate), dateB = new Date(b.transactionDate);
                                        return dateB - dateA;
                                    });
                                    $scope.networthsDetailsinv[i].transcations = response.data.transactions.transactions;
                                }
                        }
                    }
                    cacheObjTrans[id] = response.data.transactions.transactions;
                }, function (response) {

                });
            }
        };
        $scope.getHistoryGraph = function () {
            var start = $filter('date')(new Date($scope.date.start), 'MM-dd-yyyy');
            var end = $filter('date')(new Date($scope.date.end), 'MM-dd-yyyy');
            OranjApiService('getAccountMainHistoryChart', null, {
                'startDateString': start,
                'endDateString': end
            }).then(function (response) {
                console.log("response",response);
                if (response.status == 'success') {
                   // $scope.date.end = $filter('date')(response.data.transactionsMap.endDate, 'MMMM dd yyyy');
                   // $scope.date.start = $filter('date')(response.data.transactionsMap.startDate, 'MMMM dd yyyy');
                    var credit = response.data.creditLineChart;
                    var debit = response.data.debitLineChart;
                    var ctxCanvas=document.getElementById("canvas");

                    if(Object.keys(credit).length == false && Object.keys(debit).length == false)
                    {
                        $scope.historyChart = true;
                        ctxCanvas.height=0;
                    }
                    else
                    {
                        ctxCanvas.height=200;
                        $scope.historyChart = false;
                        var chartData = new Array();
                        var dateKey = [];
                        for (var key in credit) {
                            dateKey.push(key.split('T')[0]);// storing the credit date into array for reference
                            chartData.push({ date: new Date(key.split('T')[0]), credit: Number(credit[key]), debit: 0 });//insert date with credit data and debit's default value as 0
                        }

                        for (var key in debit) {
                            var index = $.inArray(key.split('T')[0], dateKey);//checking for the date with credit data
                            if (index > -1) {
                                chartData[index].debit = -Math.abs(Number(debit[key]));//update the debit data
                            } else {
                                chartData.push({ date: new Date(key.split('T')[0]), credit: 0, debit: -Math.abs(Number(debit[key])) });//insert date with debit data and credit value as 0
                            }
                        }
                        
                        if (chartData.length > 0) {
                            chartData.sort(function (a, b) {
                                var dateA = a.date, dateB = b.date;
                                return dateA - dateB; //sort by date ascending
                            });
                        }

                        $scope.dataCredit = [];
                        $scope.dataDebit = [];
                        $scope.chartDate = [];
                        for (key in chartData) {
                            $scope.dataCredit.push(chartData[key].credit);
                            $scope.dataDebit.push(chartData[key].debit);
                            $scope.chartDate.push($filter('date')(new Date(chartData[key].date), 'MM/dd'));
                        }                    
                        createHistoryChartData();
                    }
                }
            }, function (error) {
                
            });
        };

        $scope.changeDate = function () {
            if (new Date($scope.date.start) <= new Date($scope.date.end)) {
                $scope.getHistoryGraph();
            }
            else {
                globalFactory.showErrorAlert(' Start date should not be older than the End date');
            }
        }        
       
        $scope.getAccountMainHeader();
        $scope.getListOfAccount();
        $scope.getHistoryGraph();      

    }


})();