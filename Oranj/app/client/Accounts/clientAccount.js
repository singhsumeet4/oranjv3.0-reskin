﻿(function () {
    'use restrict';

    angular.module('app.client.account', [])
        .controller('clientAccountCtrl', ['$scope', '$rootScope', 'jQlinechart', 'OranjApiService', '$filter', '$state', 'GlobalFactory', clientAccountCtrl])


    function clientAccountCtrl($scope, $rootScope, jQlinechartFactory, OranjApiService, $filter, $state, GlobalFactory) {

        var role = GlobalFactory.getRole();

        role.then(
  	            function (result) {
  	                if (result.data) {
  	                    if (GlobalFactory.lookupRole('ROLE_USER', result.data)) {

  	                    } else if (GlobalFactory.lookupRole('ROLE_PROSPECT', result.data) || GlobalFactory.lookupRole('ROLE_CLIENT', result.data)) {

  	                    } else {
  	                        GlobalFactory.signout();
  	                    }
  	                } else {
  	                    GlobalFactory.signout();
  	                }
  	            }, function () {
  	                GlobalFactory.signout();
  	            }
  	   	);
                      
        $scope.todayDate = new Date();

        $scope.getListOfAccount = function () {
            OranjApiService('getListOfAccount').then(function (response) {
                if(response.status == "success"){
                    var networthsDetailsinv = response.data.accounts;
                    $rootScope.$broadcast("accountDetails", { sideAccountList: networthsDetailsinv });
                }
            }, function (response) {

            });
        };
        $scope.getListOfAccount();
    }


})();