(function () {
    'use restrict';

    var app= angular.module('app.accountSpecific', [])

    app.controller('clientAccountSpecificCtrl', ['$scope', '$rootScope', 'jQlinechart', 'OranjApiService', '$filter', '$state', 'GlobalFactory','$stateParams', clientAccountSpecificCtrl])
    function clientAccountSpecificCtrl($scope, $rootScope, jQlinechartFactory, OranjApiService, $filter, $state, globalFactory, $stateParams) {
                
        var endDate = $filter('date')(new Date(), 'MMMM dd yyyy');
        var startDate = moment(new Date()).subtract(30, 'days').format('MMMM DD YYYY');
        $scope.date = { 'start': startDate, 'end': endDate };
       
        $scope.accountData = [];
       
        $scope.headers = {
            'accountName': "",
            'balance': 0,
            'goal': 0
        }
        var vm = this;
        vm.accountName = $stateParams.accountName;
       
        $scope.getTranscations = function (id) {
            var startDate = $filter('date')(new Date($scope.date.start), 'yyyy-MM-dd');
            var endDate = $filter('date')(new Date($scope.date.end), 'yyyy-MM-dd');
            var TransData = { 'startDate': startDate, 'endDate': endDate, "yodleeItemAccountId": id }
            OranjApiService('getTranscations', {
                data: JSON.stringify(TransData)
            }).then(function (response) {   
                if (response.status == "success") {
                    $scope.transcations = response.data.transactions.transactions;
                }
            }, function (error) {
               
            });
        };

        $scope.historyChart = function(yodleeItemAccountId)
        {
            $scope.chartDate = [];
            $scope.chartData = [];
            var startDate = $filter('date')(new Date($scope.date.start), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.end), 'MM-dd-yyyy');
            OranjApiService('getTransactionHistoryChart', null, {
                'yodleeItemAccountId':yodleeItemAccountId,
                'startDateString': startDate,
                'endDateString': endDate
            }).then(function (response) {   
                if (response.status == "success") {
                    for(key in response.data)
                    {                       
                        $scope.chartDate.push($filter('date')(new Date(key), 'MM/dd'));
                        $scope.chartData.push(response.data[key]);
                    }
                    createHistoryChartData();
                }
            }, function (error) {
               
            });
        }

        $scope.getGoalDataHeader = function (yodleeItemAccountId) {         
            OranjApiService('getAccountSpecificGoal', null, {
                'yodleeItemAccountId': yodleeItemAccountId               
            }).then(function (response) {
                if (response.status == 'success') {                    
                    $scope.headers = response.data;               
                }                
            }, function (error) {

            });
        };   

        $scope.changeDate = function () {
            var yodleeItemAccountId = $stateParams.id;     
            if (new Date($scope.date.start) <= new Date($scope.date.end)) {
                $scope.getTranscations(yodleeItemAccountId);
                $scope.historyChart(yodleeItemAccountId);
            }
            else {
                globalFactory.showErrorAlert(' Start date should not be older than the End date');
            }
        }  

        function createHistoryChartData() {
            window.$filter = $filter;
            var data = {
                labels: $scope.chartDate,             
                datasets: [
                    {
                        label: "Debit",
                        type: 'line',
                        fillColor: "transparent",
                        strokeColor: "#3E51AF",
                        pointColor: "#3E51AF",
                        pointStrokeColor: "#3E51AF",
                        pointHighlightFill: "#3E51AF",
                        pointHighlightStroke: "#3E51AF",
                        data: $scope.chartData,
                    }
                ]
            };

            var lineoptions = {
                scaleShowGridLines: false,
                pointDot: false,
                bezierCurve: false,
                showScale: true,
                multiTooltipTemplate: function (value) { return ' $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
                tooltipTemplate: '<%if (label){%><%=moment(new Date(label)).format("MM/DD")%> : <%}%><%=$filter("currency")(value, "$" , 0)%>',
                scaleFontSize: 10,
                scaleLabel: "<%= ' $' + Number(value)%>",
                legendTemplate: '<table class=\"box_table\"><tr>'
                + '<% for (var i=0; i<datasets.length; i++) { %>'
                + '<td><div class=\"boxx\" style=\"background-color:<%=datasets[i].strokeColor %>\"></div></td>'
                + '<% if (datasets[i].labelShow) { %><td class=\"box_bar\"><%= datasets[i].labelShow %></td><% } %>'
                + '<% } %>'
                + '</tr></table>',
            }
            jQlinechartFactory.jQLineChart("canvas", data, lineoptions);//parameter sequence 1 canvas ID,2 Data to build, 3 options to configure
        }

        $scope.$on('accountDetails', function (event, val) {
            $scope.accountData = val.sideAccountList;           
            for (var i = 0; i < $scope.accountData.length; i++) {
                if ($scope.accountData[i].accountId == $stateParams.id) {
                    $scope.headerchangeValue = $scope.accountData[i].changeValue;
                    $scope.headerchangePositive = $scope.accountData[i].changePositive;
                    $scope.headerchangePercent = $scope.accountData[i].changePercent;
                }
            }
        });
       
        function init() {
          
            var yodleeItemAccountId = $stateParams.id;
            $scope.headers.accountName = $stateParams.accountName;
                     
            for (var i = 0; i < $scope.sideAccountList.length; i++) {
                if ($scope.sideAccountList[i].accountId == $stateParams.id) {
                    $scope.headerchangeValue = $scope.sideAccountList[i].changeValue;
                    $scope.headerchangePositive = $scope.sideAccountList[i].changePositive;
                    $scope.headerchangePercent = $scope.sideAccountList[i].changePercent;
                    $scope.balance = $scope.sideAccountList[i].value;
                    $scope.accountName = $scope.sideAccountList[i].name;
                }
            }            
            $scope.getTranscations(yodleeItemAccountId);
            $scope.getGoalDataHeader(yodleeItemAccountId);
            $scope.historyChart(yodleeItemAccountId);           
        };
        init();


    }


})();