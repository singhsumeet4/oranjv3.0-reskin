 
(function () {
	angular.module("app.client.home")


        .controller('activityFeed', ['$scope', activityFeed]);
        
        function activityFeed($scope) {
        var imagePath = 'images/g1.jpg';

        $scope.messages = [
            {
                face: imagePath,
                notes: "Leonardo DiCaprio added two new documents to his vault tagged #ChaseStatement."
            },
            {
                face: imagePath,
                notes: " Albus Dumbledore won the lottery yesterday in Illinois."
            },
            {
                face: imagePath,
                notes: "Albus Dumbledore sent you a message:'Hey David, I'd like to start planning for my new mansion. Let's talk soon.'"
            },
            {
                face: imagePath,
                notes: "Harry created a new goal to retire when he is 65 years old and another goal of buying a home next May."
            },

            // {
            //     face : imagePath,
            //     notes: " At vero eos et accusamus et iusto odio dignissimos ducimus qui"
            // },


        ];
    }

})();
