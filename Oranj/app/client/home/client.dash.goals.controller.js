(function () {

    angular.module('app.client.home')
        .controller('clientDashGoalsController', ['goalDataService', '$state', '$stateParams', function (goalDataService, $state, $stateParams) {
            var ctrl = this;

            ctrl.showGoalChartDiv = true;

            if ($state.includes('page.advisor')) {
                goalDataService.fetchGoalsStats($stateParams.id).then(goalsSuccess);
            } else {
                goalDataService.fetchGoalsStats().then(goalsSuccess);
            }
            

            function goalsSuccess(goalDashObj) {
                ctrl.goalsList = goalDashObj.goalsDashGrid;
                ctrl.goalsTable = [];

                for (var i = 0; i < ctrl.goalsList.length; i += 2) {
                    ctrl.goalsTable.push([ctrl.goalsList[i], ctrl.goalsList[i + 1]]);
                }

            }
            
        }])
})();
