(function () {

    angular.module('app.client.home')
        .controller('clientDashLiquidityController', ['homeDashLiquidityFactory', 'clientDashDataService', '$state', '$stateParams',
            function (homeDashLiquidityFactory, clientDashDataService, $state, $stateParams) {
                var ctrl = this;

                ctrl.showLiquidityChartDiv = true;

                var dateRange = {
                    startDate: moment(new Date()).subtract(30, 'days').format('YYYY-MM-DD'),
                    endDate: moment(new Date()).format('YYYY-MM-DD')
                };

                function liquidityDashChartSuccess(chartData) {
                    ctrl.liquidityTotal = chartData.liquidityTotal;

                    ctrl.liquidityChange = chartData.liquidityChange;

                    ctrl.liquidityChange.liquidityPercent = ctrl.liquidityChange.liquidityPercent.toFixed(2);

                    ctrl.liquidityChart = homeDashLiquidityFactory.getLiquidityChart(chartData.liquidityChartData);

                }

                function liquidityAccountsSuccess(accntsList) {
                    angular.forEach(accntsList, function(accnt) {
                        accnt.changePercent = accnt.changePercent.toFixed(2);
                    })
                    ctrl.liquidityAccnts = accntsList;
                }
                
                if($state.includes('page.advisor')) {
                    clientDashDataService.getLiquidityDashChartData(dateRange, $stateParams.id).then(liquidityDashChartSuccess);

                    clientDashDataService.getLiquidityAccounts($stateParams.id).then(liquidityAccountsSuccess);
                } else {
                    clientDashDataService.getLiquidityDashChartData(dateRange).then(liquidityDashChartSuccess);

                    clientDashDataService.getLiquidityAccounts().then(liquidityAccountsSuccess);
                }

            }]);
})();
