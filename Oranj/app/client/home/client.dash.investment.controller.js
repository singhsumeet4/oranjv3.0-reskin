(function () {
    angular.module('app.client.home')
        .controller('clientDashInvestmentController', ['clientDashDataService', 'homeDashInvestmentFactory', '$state', '$stateParams',
            function (clientDashDataService, homeDashInvestmentFactory, $state, $stateParams) {
                var ctrl = this;
                ctrl.showInvestmentChartDiv = true;

                var dateRange = {
                    startDate: moment(new Date()).subtract(30, 'days').format('MM-DD-YYYY'),
                    endDate: moment(new Date()).format('MM-DD-YYYY')
                };

                function investmentDashChartSuccess(investmentData) {
                    ctrl.investmentTotal = investmentData.investmentTotal;
                    ctrl.investmentChange = investmentData.investmentChange;
                    ctrl.investmentChange.investmentPercent = ctrl.investmentChange.investmentPercent.toFixed(2);

                    ctrl.investmentChart = homeDashInvestmentFactory.getInvestmentDashChart(investmentData.investmentChartData);
                }

                function investmentAccountsSuccess(accnts) {
                    ctrl.investmentAccnts = homeDashInvestmentFactory.getInvestementAccntsList(accnts);
                    console.log(accnts);
                }

                if ($state.includes('page.advisor')) {
                    clientDashDataService.getInvestementDashChartData(dateRange, $stateParams.id).then(investmentDashChartSuccess);

                    clientDashDataService.getInvestmentAccounts($stateParams.id).then(investmentAccountsSuccess);
                } else {
                    clientDashDataService.getInvestementDashChartData(dateRange).then(investmentDashChartSuccess);

                    clientDashDataService.getInvestmentAccounts().then(investmentAccountsSuccess);
                }




            }]);
})();
