(function() {

    angular.module('app.client.home')
        .controller('clientDashInsuranceCtrl', ['clientDashDataService','homeDashInsuranceFactory','$state', '$stateParams',
            function(clientDashDataService, homeDashInsuranceFactory,  $state, $stateParams) {
            var ctrl = this;

            ctrl.showInsuranceChartDiv = true;

                if ($state.includes('page.advisor')) {
                    clientDashDataService.getInsuranceDashData($stateParams.id).then(insuranceSuccess);
                } else {
                    clientDashDataService.getInsuranceDashData().then(insuranceSuccess);
                }


                function insuranceSuccess(insuranceData) {
                    console.log(insuranceData);

                    var chartData = {
                        lifeHave :insuranceData.insurance.LIFE.header.have,
                        longCareHave : insuranceData.insurance.LONG_TERM_CARE.header.have,
                        longDisabilityHave : insuranceData.insurance.LONG_TERM_DISABILITY.header.salaryCovered
                    };

                    ctrl.insuranceDashChart = homeDashInsuranceFactory.getInsuranceDashChart(chartData);

                    ctrl.categorizedInsurancenAccnts = homeDashInsuranceFactory.getInsuranceAccnts(insuranceData.insurance);
                    //console.log(ctrl.categorizedInsurancenAccnts);
                    //console.log(ctrl.categorisedAccnts);
                }

        }]);
})();
