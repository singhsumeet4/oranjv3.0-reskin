(function () {

    angular.module('app.client.home')
        .service('clientDashDataService', ['OranjApiService', '$q', function (OranjApiService, $q) {


            this.getNetworthDashData = function (id) {
                var defer = $q.defer(), liablitity, financials;

                if (id) {
                    liablitity = OranjApiService('getClientNetworthLiablityAdvisor', {}, {userId: id});
                    financials = OranjApiService('getClientNetworthFinancialAdvisor', {}, {userId: id});
                } else {
                    liablitity = OranjApiService('getClientNetworthLiablity');
                    financials = OranjApiService('getClientNetworthFinancial');
                }

                $q.all([liablitity, financials]).then(function (res) {
                    var liabilityRes = res[0],
                        financialsRes = res[1];
                    if (liabilityRes.status === 'success' && financialsRes.status === 'success') {
                        defer.resolve({chart: liabilityRes.data, table: financialsRes.data});
                    } else {
                        defer.reject();
                    }
                });

                return defer.promise;
            };

            this.getInvestementDashChartData = function (dateRange, id) {
                var defer = $q.defer(),
                    investementTotalsProm, percentChangeProm, chartDetailsProm;

                if (id) {
                    investementTotalsProm = OranjApiService('getInvestmentTotalForUser', {}, {userId: id});
                    percentChangeProm = OranjApiService('getInvestmentPercentWithoutDateForUser', {}, {userId: id});

                    dateRange.userId = id;
                    chartDetailsProm = OranjApiService('getInvestmentPerformaceChartAdvsior', {}, dateRange);
                } else {
                    investementTotalsProm = OranjApiService('getInvestmentTotal');
                    percentChangeProm = OranjApiService('getInvestmentPercentWithoutDate');


                    chartDetailsProm = OranjApiService('getInvestmentPerformaceChart', {}, dateRange);
                }

                $q.all([investementTotalsProm, percentChangeProm, chartDetailsProm]).then(function (res) {
                    var resObj = {};

                    resObj.investmentTotal = res[0].status === 'success' ? res[0].data.totalInvestment : 0,
                        resObj.investmentChange = res[1].status === 'success' ? res[1].data : {},
                        resObj.investmentChartData = res[2].status === 'success' ? res[2].data.values : [];


                    defer.resolve(resObj);
                });


                return defer.promise;
            };

            this.getInvestmentAccounts = function (id) {
                var defer = $q.defer(), yodleeAccntHoldingsProm, quovoAccntHoldingsProm, accntsProm;

                if (id) {
                    yodleeAccntHoldingsProm = OranjApiService('getAllInvestmentAccountHoldingsYodleeAdvisor', {}, {userId: id}),
                        quovoAccntHoldingsProm = OranjApiService('getAllInvestmentAccountHoldingsYodleeAdvisor', {}, {userId: id}),
                        accntsProm = OranjApiService('getInvestmentAccountsAdvisor', {}, {userId: id});

                } else {
                    yodleeAccntHoldingsProm = OranjApiService('getAllInvestmentAccountHoldingsYodlee'),
                        quovoAccntHoldingsProm = OranjApiService('getAllInvestmentAccountHoldingsQuovo'),
                        accntsProm = OranjApiService('getInvestmentAccounts');

                }


                $q.all([accntsProm, yodleeAccntHoldingsProm, quovoAccntHoldingsProm]).then(function (res) {
                    var allAccnts = res[0],
                        yodleeAccntHoldings = res[1],
                        quovoAccntHoldings = res[2];

                    if (allAccnts.status === 'success' && yodleeAccntHoldings.status === 'success' && quovoAccntHoldings.status === 'success') {
                        defer.resolve({
                            accnts: allAccnts.data.accounts,
                            quovoHoldings: quovoAccntHoldings.data.holdings,
                            yodleeHoldings: yodleeAccntHoldings.data.holdings
                        });
                    } else {
                        defer.reject();
                    }

                });

                return defer.promise;
            };


            this.getLiquidityDashChartData = function (dateRange, id) {
                var defer = $q.defer(), liquidityTotalProm, liquidityChangeProm, liquidityChartProm;

                if (id) {
                    dateRange.userId = id;

                    liquidityTotalProm = OranjApiService('getLiquidityTotalAdvisor', {}, {userId: id});
                    liquidityChangeProm = OranjApiService('getLiquidityPercentAdvisor', {}, dateRange);
                    liquidityChartProm = OranjApiService('getNetworthHistoryAdvisor', {}, dateRange);

                } else {
                    liquidityTotalProm = OranjApiService('getLiquidityTotal');
                    liquidityChangeProm = OranjApiService('getLiquidityPercent', {}, dateRange);
                    liquidityChartProm = OranjApiService('getNetworthHistory', {}, dateRange);

                }

                $q.all([liquidityTotalProm, liquidityChangeProm, liquidityChartProm])
                    .then(function (res) {
                        var liquidityTotal = res[0].data.totalLiquidity,
                            liquidityChange = res[1].data,
                            liquidityChartData = res[2].data.allNetworthHistoryListInDateRange;

                        defer.resolve({
                            liquidityTotal: liquidityTotal,
                            liquidityChange: liquidityChange,
                            liquidityChartData: liquidityChartData
                        });
                    });

                return defer.promise;

            };

            this.getLiquidityAccounts = function (id) {
                var defer = $q.defer(), apiCall;

                if (id) {
                    apiCall = OranjApiService('getLiquidityAccountsAdvisor', {}, {userId: id});
                } else {
                    apiCall = OranjApiService('getLiquidityAccounts');
                }

                apiCall.then(function (res) {
                    if (res.status === 'success') {
                        //console.log('liquidity accnts');
                        //console.log(res.data);
                        defer.resolve(res.data);
                    } else {
                        defer.reject();
                    }
                });

                return defer.promise;
            };

            this.getInsuranceDashData = function (id) {
                var defer = $q.defer(), apiCall;

                if (id) {
                    apiCall = OranjApiService('getRiskListForInsuranceForUser', {}, {userId: id});
                } else {
                    apiCall = OranjApiService('getRiskListForInsurance');
                }

                apiCall.then(function (res) {
                    if (res.status === 'success') {
                        defer.resolve(res.data);
                    }
                });

                return defer.promise;
            };
        }]);
})();
