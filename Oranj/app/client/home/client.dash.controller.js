(function() {

    angular.module('app.client.home')
        .controller('clientHomeDashController', ['UserProfileService','$state', function(UserProfileService, $state) {

            var ctrl = this;
            ctrl.hideHeader = false;

            if(moment().format('A') === 'AM') {
                ctrl.headerGreeting = 'Morning';
            } else {
                ctrl.headerGreeting = 'Afternoon';
            }
            
            //Hide the Client Dash header when in advisor mode.
            if($state.includes('page.advisor')) {
                ctrl.hideHeader = true;
            } else {
                ctrl.hideHeader = false;

                UserProfileService.fetchUserProfile().then(function(userProfile) {
                    ctrl.name = userProfile.profile.firstName;
                });
            }


        }]);
})();
