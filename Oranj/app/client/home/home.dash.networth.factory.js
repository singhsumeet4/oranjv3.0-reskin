(function () {


    angular.module('app.client.home')
        .factory('homeDashNetworthFactory', ['$filter', function ($filter) {


            return {
                getNetworthChartOptions: function (networthData) {

                    var networthChartOptions = {
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: null
                        },
                        xAxis:
                        {
                            lineWidth: 0,
                            minorGridLineWidth: 0,
                            minorTickLength: 0,
                            tickLength: 0,
                            categories: ['Assets', 'Liabilities', 'Net Worth'],
                            align: 'left',

                            labels: {
                                rotation: 0,
                                style: {
                                    fontSize: '16px',
                                    fontFamily: 'Open Sans',
                                    color: '#000000',
                                },

                                useHTML: true,
                                formatter: function () {
                                    return '<div class="label">' + this.value + '</div>';
                                }
                            },
                            position: {
                                align: 'left'
                            },

                            title: {
                                text: null
                            },
                        },
                        yAxis: {
                            value: '',
                            min: 0,
                            max: 1000000,
                            tickInterval: 100000,
                            gridLineWidth: 0,
                            lineWidth: 0,
                            tickLength: 10,
                            tickWidth: 1,
                            title: {
                                text: '',
                                align: 'high'
                            },
                            labels: {
                                rotation: 0,
                                step: 2,
                                style: {
                                    fontSize: '10px',
                                    fontFamily: 'Open Sans',
                                    fontStyle: 'italic',

                                },

                                formatter: function () {
                                    var data = $filter('currency')(this.value, this.value === 0 ? '' : '$', 2);
                                    return data;
                                },

                            }
                        },
                        colors: [
                            '#2f4e9f',
                            '#11bfe0',
                            '#40b449'
                        ],

                        tooltip: {
                            backgroundColor: '#191919',
                            // forecolor: '#ffffff',
                            borderColor: 'black',
                            borderRadius: 10,
                            //  shared: true,
                            // borderWidth: 3,
                            useHTML: true,
                            formatter: function () {
                                var data = this.point.liabilities ? ("(" + $filter('currency')(this.y, '$', 2) + ")") : $filter('currency')(this.y, '$', 2);
                                return '<div><div style="background-color:' + this.color + ';width:10px;height:10px;display:inline-block;"></div>&nbsp;<span style="color: #ffffff; font-Size: 10px;">' + data + '</span></div>';
                                // return '<div><span style="color:{point.color}"></span><span style="color:#ffffff">' + data + '</span></div>';U+2B1B
                            },
                            positioner: function (labelWidth, labelHeight, point) {
                                var tooltipX, tooltipY;
                                if (point.plotX + labelWidth > this.chart.plotWidth) {
                                    tooltipX = point.plotX + this.chart.plotLeft - labelWidth - 40;
                                } else {
                                    tooltipX = point.plotX + this.chart.plotLeft - 40;
                                }
                                tooltipY = point.plotY + this.chart.plotTop - 60;
                                return {
                                    x: tooltipX,
                                    y: tooltipY
                                };
                            }
                        },

                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true,
                                    inside: true,
                                    align: 'left',
                                    x: 250,
                                    style: {
                                        fontSize: '15px',
                                        fontFamily: 'Open Sans',
                                        color: '#808080',
                                    },
                                    formatter: function () {
                                        var data = this.point.liabilities ? "(" + $filter('currency')(this.y, '$', 2) + ")" : $filter('currency')(this.y, '$', 2);
                                        return '<span style="position:absolute;right:0px;display:block !important;">' + data + '</span>';
                                    },

                                }
                            }
                        },

                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'top',
                            x: -40,
                            y: 80,
                            floating: true,
                            borderWidth: 1,
                            enabled: false,
                            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                            shadow: true
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            maxPointWidth: '30',
                            //  pointInterval: '100000' ,
                            // pointPadding: 0,
                            name: '',
                            data: [
                                { y: 600000, color: '#2f4e9f', liabilities: false },
                                { y: 500000, color: '#11bfe0', liabilities: true },
                                { y: 100000, color: '#40b449', liabilities: false },
                            ],
                            useHTML: true,
                            formatter: function () {
                                return '<div class="label2">' + this.value + '</div>';
                            }
                        }]
                    };

                    return networthChartOptions;
                },


                getNetworthFinancials: function (rawData) {
                    console.log(rawData);
                    var financialsList = {};
                    //loop across all available accounts (assets/liability)
                    angular.forEach(rawData, function (accountCats, index) {
                        angular.forEach(accountCats, function (accountList, cat) {
                            var financialObj = {
                                category: cat,
                                isOpen: false,
                                accntList: accountList,
                                totalVal: 0
                            };

                            angular.forEach(accountList, function (accnt) {
                                financialObj.totalVal += accnt.value;
                            });

                            financialsList[cat] = financialObj;
                        });
                    });
                    console.log(financialsList);
                    return financialsList;
                }
            }
        }]);
})();
