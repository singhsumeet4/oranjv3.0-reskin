(function () {

    angular.module('app.client.home')
        .factory('homeDashInsuranceFactory', ['$filter', function ($filter) {

            return {
                getInsuranceAccnts : function(rawData) {

                    var categorizedInsuranceAccnts = {
                        'LIFE' : {
                            title : 'Life',
                            have : 0,
                            accnts : [],
                            isOpen: false
                        },
                        'LONG_TERM_CARE' : {
                            title : 'Long Term Care',
                            have : 0,
                            accnts : [],
                            isOpen: false
                        },
                        'LONG_TERM_DISABILITY' : {
                            title : 'Long Term Disability',
                            have : 0,
                            accnts : [],
                            isOpen: false
                        }
                    };


                    angular.forEach(rawData, function(lifeInsurance, type) {
                        if(type === 'LONG_TERM_DISABILITY') {
                            categorizedInsuranceAccnts[type].have = lifeInsurance.header.salaryCovered;
                        } else {
                            categorizedInsuranceAccnts[type].have = lifeInsurance.header.have;
                        }

                        angular.forEach(lifeInsurance.insurance, function(subCatAccnts, subCat) {
                            angular.forEach(subCatAccnts, function(accnt) {
                                categorizedInsuranceAccnts[type].accnts.push(accnt);
                            });
                        });
                    });

                    return categorizedInsuranceAccnts;

                },
                getInsuranceDashChart: function (chartData) {

                    var doughnutData = [{name: 'Life', 'y': 500},
                        {name: 'Long Term Care', 'y': 500},
                        {name: 'Long Term Disability', 'y': 500}
                    ];

                    var overviewData = [{name: 'Life', 'y': chartData.lifeHave},
                        {name: 'Long Term Care', 'y': chartData.longCareHave},
                        {name: 'Long Term Disability', 'y': chartData.longDisabilityHave}
                    ];

                    var colorArray = [];

                    if (overviewData[0].y > 0)
                        colorArray[0] = '#188079';
                    else
                        colorArray[0] = '#ffffff';
                    if (overviewData[1].y > 0)
                        colorArray[1] = '#36B8B7';
                    else
                        colorArray[1] = '#ffffff';
                    if (overviewData[2].y > 0)
                        colorArray[2] = '#CBE8E8';
                    else
                        colorArray[2] = '#ffffff';

                    var tt = getTotal(overviewData, 'y');

                    var insuranceDoughnutChart = {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false,
                            events: {
                                load: function () {
                                    var chart = this,
                                        rend = chart.renderer,
                                        pie = chart.series[0],
                                        left = chart.plotLeft + pie.center[0],
                                        top = chart.plotTop + pie.center[1],
                                        text = rend.text(tt, left, top).attr({'text-anchor': 'middle'}).add();
                                }
                            }
                        },

                        colors: colorArray,
                        credits: {
                            enabled: false
                        },
                        tooltip: {
                            backgroundColor: '#191919',
                            forecolor: '#ffffff',
                            borderColor: 'black',
                            borderRadius: 10,
                            borderWidth: 3,
                            valueSuffix: '$',
                            formatter: function () {
                                var a = '<span style="color:#ffffff">' + overviewData[0].name + ': ' + ((overviewData[0].y > 0) ? ($filter('currency')(overviewData[0].y, '$', 2)) : 'n/a') + '</span>' + ' <br/>'
                                    + '<span style="color:#ffffff">' + overviewData[1].name + ":" + ((overviewData[1].y > 0) ? ($filter('currency')(overviewData[1].y, '$', 2)) : 'n/a') + '</span>' + ' <br/>'
                                if (overviewData[2])
                                    return a + '<span style="color:#ffffff">' + overviewData[2].name + ': ' + ((overviewData[2].y > 0) ? ($filter('currency')(overviewData[2].y, '$', 2)) : 'n/a') + '</span>';
                                else
                                    return a;
                            }

                        },
                        plotOptions: {
                            pie: {
                                borderColor: '#eeeeee',
                                size: '100%',
                                borderWidth: '2',
                                dataLabels: {
                                    enabled: false,
                                    distance: -10,

                                    style: {
                                        fontWeight: 'bold',
                                        color: 'white',
                                        textShadow: '0px 1px 2px black'
                                    }
                                },
                                showInLegend: true,
                                startAngle: 0,
                                endAngle: 360,
                                center: ['45%', '40%']
                            }
                        },

                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            x: 10,
                            itemMarginBottom: 14,
                            shadow: false,
                            verticalAlign: 'top',
                            y: 40,
                            floating: false,
                            margin: 10,
                            backgroundColor: '#FFFFFF',
                            itemStyle: {
                                color: '#404040',
                                fontWeight: 'normal',
                                margin: '80px',
                                fontSize: '14px',
                                fontFamily: 'Open Sans "sans-serif"',
                            }
                        },

                        series: [{
                            type: 'pie',
                            name: 'Browser share',
                            innerSize: '60%',
                            data: doughnutData,
                            size: '60%'


                        }],
                        title: {
                            text: '',
                            style: {
                                color: '#404040',
                                fontWeight: 'normal',
                                margin: '80px',
                                fontSize: '20px',
                                fontFamily: 'Open Sans "sans-serif"',
                            }

                        }
                    };

                    return insuranceDoughnutChart;
                }
            }
        }]);
})();
