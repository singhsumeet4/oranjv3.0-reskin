(function () {
    'use strict';

    angular.module('app.chart')
        .controller('clientInvestmentCtrl', ['$scope', '$timeout', 'linechartObjArray', 'piechartdata','$rootScope', clientInvestmentCtrl]);
 
    function clientInvestmentCtrl($scope, $timeout, linechartObjArray, piechartdata, $rootScope) {

       

        $rootScope.breadcrum='Home / Investment';
        // Engagment pie charts  start
        piechartdata.reset();
        var pieSchema = piechartdata.dataSchema();

        pieSchema.options.series[0].radius = [45, 41];
        pieSchema.options.series[0].data[0].value = 68;
        pieSchema.options.series[0].data[1].value = 100 - pieSchema.options.series[0].data[0].value;
        pieSchema.labelBottom.normal.color = '#ECF7F6';
        pieSchema.labelFromatter.normal.label.textStyle.color = '#36B8B7';
        pieSchema.labelFromatter.normal.label.textStyle.fontSize = 23;
        // pieSchema.labelBottom.normal.label.position= 'center';

        pieSchema.labelTop.normal.label.position = 'center';

        pieSchema.labelTop.normal.color = '#36B8B7';

        // pieSchema.options.series. center= ['50%', '20%'],
        pieSchema.labelFromatter.normal.label.formatter = function (params) {
            return 100 - params.value + '%'
        }

        piechartdata.add(pieSchema);

        // Engagment pie charts end


        var linechartObj = {}
        linechartObj.labels = ["January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016", "October 2016"];
        linechartObj.series = [''];
        linechartObj.data = [
            [450000, 430000, 470000, 519000, 530500, 520800, 540300, 599000, 610000, 700000],
            //   [50000, 80000, 60000, 120000, 140000, 110000, 180000, 200000, 170000, 210000],

        ];
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
          $scope.date = {
            date1: 'March 21,2016',
            date2: 'April 21,2016'} 

        linechartObj.options = {
            showScale: false,
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            pointHitDetectionRadius: 2,
            // zindex:1,

        };
        
        // linechartObj.style=[{
        //     zIndex=10
        // }];

        linechartObj.colours = [{
            "fillColor": "#e0f0f1",
            "strokeColor": "#36b8b7",

        }];

        linechartObjArray.reset();
        linechartObjArray.chartDataArray.push(linechartObj);
        // performance chart main right ends




        // clientInvestmentMain doughnut chart start

        Chart.defaults.global.responsive = true;

        var doughnutData = [

            {
                label: 'Non US Bond',
                value: 80,
                name: 'Non US Bond',
                color: '#23c1e2',

            },
            {
                label: 'Cash',
                value: 210,
                name: 'Cash',
                 color: '#fdb45c',
            },

            {
                label: 'US Bond',
                value: 430,
                name: 'US Bond',
                color: '#3eb649',

            },

            {
                label: 'Non US Stock',
                value: 220,
                name: 'Non US Stock',
                 color: '#a3cd39'

            },

            {
                label: 'US Stock',
                value: 1548,
                name: 'US Stock',
                color: '#fdce0b'

            },

        ];

        var doughnutOptions = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,

        };
        if (document.getElementById("investmentMainDoughnut") !== null) {
            var ctx1 = document.getElementById("investmentMainDoughnut").getContext("2d");
            var myDoughnut = new Chart(ctx1).Doughnut(doughnutData, doughnutOptions);

            document.getElementById('my-legend').innerHTML = myDoughnut.generateLegend(); // for legend 
        }
        // clientInvestmentMain doughnut chart end

    }

})(); 