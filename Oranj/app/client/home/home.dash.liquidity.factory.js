(function() {

    angular.module('app.client.home')
        .factory('homeDashLiquidityFactory',['$filter', function($filter) {


            return {
                getLiquidityChart: function(chartData) {
                   var chartObj = {
                        chart: {
                            type: 'line',
                                backgroundColor: 'transparent'
                        },
                        title: {
                            text: null
                        },

                        xAxis: {
                            type: 'datetime',
                                title: {
                                text: '',
                                    align: 'high'
                            },
                            labels: {
                                rotation: 0,
                                    style: {
                                    fontSize: '8px',
                                        fontFamily: 'Open Sans',
                                        fontStyle: 'italic',

                                },
                                formatter: function () {
                                    var data = $filter('date')(this.value, 'M/d');
                                    return data;
                                },
                            }
                        },
                        yAxis: {
                            min: 0,
                                max: 1000000,
                                tickInterval: 200000,
                                gridLineWidth: 0,
                                lineWidth: 1,
                                title: {
                                text: '',
                                    align: 'high'
                            },

                            labels: {
                                rotation: 0,
                                    style: {
                                    fontSize: '8px',
                                        fontFamily: 'Open Sans',
                                        fontStyle: 'italic',

                                },
                                formatter: function () {
                                    var data = $filter('currency')(this.value, '$', 2);
                                    return data;
                                }

                            }

                        },
                        tooltip: {
                            backgroundColor: '#191919',
                                forecolor: '#ffffff',
                                borderColor: 'black',
                                borderRadius: 10,
                                borderWidth: 3,
                                valueSuffix: '$',
                                formatter: function () {
                                return '<span style="color:#ffffff">' + $filter('date')(this.x, 'MM/dd/yyyy') + ': ' +
                                    $filter('currency')(this.y, '$', 2) + '</span>' + ' <br/>'
                                }
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: false
                                },
                                enableMouseTracking: true,
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            showInLegend: false,
                            //data: [{ y: 110000, OscarWinnings: 99000 }, { y: 305000, OscarWinnings: 120000 }, { y: 400000, OscarWinnings: 130000 }, { y: 410000, OscarWinnings: 140000 }, { y: 420000, OscarWinnings: 150000 }, { y: 460000, OscarWinnings: 160000 }, { y: 500000, OscarWinnings: 170000 }, { y: 480000, OscarWinnings: 180000 }, { y: 510000, OscarWinnings: 210000 }, { y: 620000, OscarWinnings: 230000 }, { y: 680110, OscarWinnings: 25011000 }, { y: 720000, OscarWinnings: 270000 }, { y: 750000, OscarWinnings: 310000 }, { y: 810000, OscarWinnings: 410000 }, { y: 790000, OscarWinnings: 550000 }],
                            data: []
                        }]
                    };

                    angular.forEach(chartData, function(liquidityInst) {
                        chartObj.series[0].data.push({
                            y: liquidityInst.liquidyValue,
                            x: moment(liquidityInst.date,'MM/DD/YYYY').valueOf()
                        });
                    });

                    return chartObj;
                }
            };
        }]);
})();
