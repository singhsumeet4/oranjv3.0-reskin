(function () {

    angular.module('app.client.home')
        .controller('clientDashNetworthController', ['clientDashDataService', 'homeDashNetworthFactory', '$state', '$stateParams',
            function (clientDashDataService, homeDashNetworthFactory,  $state, $stateParams) {

                var ctrl = this;

                ctrl.showNetworthChartDiv = true;

                function networthDashSucess(data) {

                    ctrl.noDataSnapshot = false;
                    ctrl.networthChart = homeDashNetworthFactory.getNetworthChartOptions(data.chart);


                    ctrl.networthFinancials = homeDashNetworthFactory.getNetworthFinancials(data.table);
                }
                
                function networthDashError() {
                    ctrl.noDataSnapshot = true;
                }
                
                
                if($state.includes('page.advisor')) {
                    clientDashDataService.getNetworthDashData($stateParams.id).then(networthDashSucess, networthDashError);    
                } else {
                    clientDashDataService.getNetworthDashData().then(networthDashSucess, networthDashError);
                }
                
            }]);
})();
