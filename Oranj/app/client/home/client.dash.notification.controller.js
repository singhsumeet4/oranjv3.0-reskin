(function() {

    angular.module('app.client.home')
        .controller('clientDashNotificationController', ['OranjApiService', '$state', '$stateParams',
            function(OranjApiService, $state, $stateParams) {

            var ctrl = this;

            OranjApiService('getfeedActivity', {}, {
            	userid:$stateParams.id || '',
                pageSize: 5,
                pageNum: 0,
                filter: 1,
                search:null
            }).then(function(res) {

                ctrl.items = res.data.messages;
            });
        }]);
})();
