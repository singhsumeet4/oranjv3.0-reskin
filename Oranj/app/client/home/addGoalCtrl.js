(function () {
    'use strict';

    angular.module('app.addGoal', ['app.ui.form'])
        .controller('addGoalCtrl', ['$scope', '$rootScope', 'piechartdata', 'jQlinechart', '$mdSidenav', 'OranjApiService', '$filter', 'GlobalFactory','UserProfileService', addGoalCtrl])
    
    function addGoalCtrl($scope, $rootScope, piechartdata, jQlinechartFactory, $mdSidenav, OranjApiService, $filter, GlobalFactory, UserProfileService) {
                
        var clientData;
        UserProfileService.fetchUserProfile().then(function (data) {                
            clientData = data;            
        }, function (error) {           

        });

        $scope.goals = [];
        $scope.account = {};

        $scope.addAccount = function () {
            OranjApiService('getListOfAccount').then(function (response) {
                if(response.status == "success"){
                    $scope.account = {};
                    $scope.account = response.data.accounts[0];
                    console.log("account",$scope.account);
        
                }
            }, function (response) {

            });
        };
        $scope.addAccount();

        $scope.searchAccountShow = true;
        $scope.search = function (searchAccount) {
            $scope.matchAccount = $scope.account.filter(function (account) {
                if (angular.isUndefinedOrNull(account.name) || angular.isUndefinedOrNull(searchAccount))
                    return;

                if (account.name.toLowerCase().indexOf(searchAccount.toLowerCase()) !== -1) return true;
            });
        };

        $scope.setAccount = function (searchAccount) {
            $scope.searchAccountShow = false;

            $scope.matchAccount = $scope.account.filter(function (account) {
                if (angular.isUndefinedOrNull(account.name))
                    return;

                if (account.name.toLowerCase().indexOf(searchAccount.toLowerCase()) !== -1) return true;
            });
            
        };

        $scope.closeSearchlist = function () {

            $scope.searchAccount = "";
            $scope.matchAccount = [];
        }


        $scope.step1 = true;
        $scope.step2 = false;
        $scope.step3 = false;
        $scope.lowerReturn = '1';
        $scope.sellPortions = '1';

        $scope.goalDetailsRetirement = {
            'name': '',
            'priority': 'VERY_IMPORTANT',
            'retirement': {
                'age': '', 'monthlyWithdrawal': '', 'longTermInvestment': '6', 'sellPortions': '1', 'lowerReturn': '1'
            },
            'type': 'RETIREMENT'
        };

        $scope.goalDetailsCollege = {
            'name': '',
            'priority': 'VERY_IMPORTANT',
            'college': {
                'collegeId': '', 'collegeName': '', 'longTermInvestment': '', 'collegeState': '', 'cost': '', 'dependentId': '', 'dependentType': ''
                , 'lowerReturn': '1', 'sellPortions': '1'
            },
            'type': 'COLLEGE'
        };

        $scope.goalDetailsInsurance = {
            'name': '',
            'priority': 'VERY_IMPORTANT',
            'risk': {
                'coverFullCollegeExpenses': '', 'coverFullMortgage': '', 'coverHalfCollegeExpenses': '6', 'coverHalfMortgage': '', 'dependentType': '1', 'insuredId': '', 'replaceFullMonthlyExpenses': '', 'replaceHalfMonthlyExpenses': '', 'replaceIncome': ''
            },
            'type': 'Insurance'
        };

        $scope.goalDetailsHome = {
            'name': '',
            'priority': 'VERY_IMPORTANT',
            'home': {
                'date': '', 'interestRate': '', 'longTermInvestment': '6', 'purchasePrice': '', 'term': '1', 'type': '',
                'lowerReturn': '1', 'sellPortions': '1'
            },
            'type': 'HOME'
        };


        $scope.goalDetailsSpecialEvent = {
            'name': '',
            'priority': 'VERY_IMPORTANT',
            'specialEvent': {
                'cost': '', 'date': '', 'dependentId': '', 'dependentType': '', 'longTermInvestment': '1', 'occasion': '',
                'lowerReturn': '1', 'sellPortions': '1'
            },
            'type': 'SPECIAL_EVENT'
        };

        $scope.goalDetailsCustomGoal = {
            'name': '',
            'priority': 'VERY_IMPORTANT',
            'custom': {
                'date': '', 'estatePlanningTargetAmount': '0', 'longTermInvestment': '', 'lowerReturn': '1', 'sellPortions': '1', 'withdrawingMoney': '0', 'type': ''
            },
            'type': 'CUSTOM'
        };

        $scope.collegeStates = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY').split(' ').map(function (state) {
            return { abbrev: state };
        });
        // API to call to get the list of college by state code
        $scope.getListOfState = function (colState) {

            $scope.goalDetailsCollege.college.collegeState = colState;//$scope.form.college.collegeState;

            OranjApiService('getcollegeListByState', null, {
                'state': colState
            }
           ).then(function (response) {
               if (response.status == 'success') {
                   $scope.collegeList = response.data;
                   $scope.collegeList.sort(function (a, b) {
                       if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                       if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                       return 0;
                   });
               }
           }, function (response) {

           });
        };
        $scope.getListOfCollege = function (colId) {
            $scope.goalDetailsCollege.college.collegeId = colId;
            for (var i = 0; i < $scope.collegeList.length; i++) {
                if ($scope.collegeList[i].id == colId)
                    $scope.goalDetailsCollege.college.collegeName = $scope.collegeList[i].name;
            }
        }

        var agevar = 0;
        for (var i = 1; i <= 90; i++) {
            agevar += ' ' + i;
        }
      
        $scope.ages = (agevar).split(' ').map(function (retirementAge) {
            return { abbrev: retirementAge };
        });

        $scope.insuranceType = ('HOME SECOND_HOME INVESTMENT_PROPERTY LAND OTHER'
        ).split(' ').map(function (type) {
            return { abbrev: type };
        });

        //$scope.funding = ('FIXED_RATE_30_YEAR FIXED_RATE_15_YEAR CASH'
        //).split(' ').map(function (fundingType) {
        //    return { abbrev: fundingType };
        //});

        $scope.funding = [{ 'abbrev': '30 year Fixed Rate', 'val': 'FIXED_RATE_30_YEAR' },
            { 'abbrev': '15 year Fixed Rate', 'val': 'FIXED_RATE_15_YEAR' },
            { 'abbrev': 'Cash', 'val': 'CASH' },
        ];

        //$scope.occasion = ('WEDDING ANNIVERSARY BAR_MITZVAH VACATION OTHER'
        //).split(' ').map(function (occasionType) {
        //    return { abbrev: occasionType };
        //});

        $scope.occasion = [{ 'abbrev': 'WEDDING ', 'val': 'WEDDING ANNIVERSARY' },
            { 'abbrev': 'ANNIVERSARY', 'val': 'ANNIVERSARY' },
            { 'abbrev': 'BAR/BAT MITZVAH', 'val': 'BAR_MITZVAH' },
            { 'abbrev': 'VACATION', 'val': 'VACATION' },
            { 'abbrev': 'OTHER', 'val': 'OTHER' }
        ];

        $scope.customGoalType = ('INVESTMENT_RETURN TAX_SAVINGS ESTATE_PLANNING SPECIAL_NEEDS OTHER'
        ).split(' ').map(function (type) {
            return { abbrev: type };
        });

        $scope.taxSavingPurpose = ('EFFICIENCY MINIMIZE_RISK MAXIMIZE_RETURNS STRATEGIC'
        ).split(' ').map(function (type) {
            return { abbrev: type };
        });

        $scope.estatePlanningPurpose = ('TAX_EFFICIENCY WEALTH_TRANSFER'
        ).split(' ').map(function (type) {
            return { abbrev: type };
        });

        $scope.specialNeedPurpose = ('MEDICAL_CARE DEVELOPMENTAL_DISABILITY CHRONIC_ILLNESS MENTAL_ILLNESS'
        ).split(' ').map(function (type) {
            return { abbrev: type };
        });


        $scope.optionSelectedsellPortions = function (sellPortions) {
            $scope.goalDetailsRetirement.retirement.sellPortions = sellPortions;
            $scope.goalDetailsCollege.college.sellPortions = sellPortions;
            $scope.goalDetailsHome.home.sellPortions = sellPortions;
            $scope.goalDetailsSpecialEvent.specialEvent.sellPortions = sellPortions;
            $scope.goalDetailsCustomGoal.custom.sellPortions = sellPortions;
        };

        $scope.optionSelectedInvestment = function (lowerReturn) {
            $scope.goalDetailsRetirement.retirement.lowerReturn = lowerReturn;
            $scope.goalDetailsCollege.college.lowerReturn = lowerReturn;
            $scope.goalDetailsHome.home.lowerReturn = lowerReturn;
            $scope.goalDetailsSpecialEvent.specialEvent.lowerReturn = lowerReturn;
            $scope.goalDetailsCustomGoal.custom.lowerReturn = lowerReturn;
        };

        $scope.goalType = function (goalType) {
            $scope.goalDetailsRetirement.type = goalType;
            $scope.type = goalType;
        };

        $scope.dateFormat = function (date) {
            var dateNow = $filter('date')(new Date(date), 'MM/dd/yyyy');            
            $scope.goalDetailsHome.home.date = dateNow;
            $scope.goalDetailsSpecialEvent.specialEvent.date = dateNow;
            $scope.goalDetailsCustomGoal.custom.date = dateNow;
        }

        $scope.goalsAdd = function () {            
            if ($scope.type == 'RETIREMENT')
                $scope.data = $scope.goalDetailsRetirement;
            else if ($scope.type == 'COLLEGE')
                $scope.data = $scope.goalDetailsCollege;
            else if ($scope.type == 'Insurance') {
                if ($scope.goalDetailsInsurance.risk.coverFullCollegeExpenses == "100" || ($scope.goalDetailsInsurance.risk.coverFullCollegeExpenses && $scope.goalDetailsInsurance.risk.coverFullCollegeExpenses != "50" && $scope.goalDetailsInsurance.risk.coverFullCollegeExpenses != "false")) {
                    $scope.goalDetailsInsurance.risk.coverHalfCollegeExpenses = false;
                    $scope.goalDetailsInsurance.risk.coverFullCollegeExpenses = true;
                }
                else if ($scope.goalDetailsInsurance.risk.coverFullCollegeExpenses == "50" || ($scope.goalDetailsInsurance.risk.coverHalfCollegeExpenses && $scope.goalDetailsInsurance.risk.coverFullCollegeExpenses != "false")) {
                    $scope.goalDetailsInsurance.risk.coverHalfCollegeExpenses = true;
                    $scope.goalDetailsInsurance.risk.coverFullCollegeExpenses = false;
                }
                else {
                    $scope.goalDetailsInsurance.risk.coverHalfCollegeExpenses = false;
                    $scope.goalDetailsInsurance.risk.coverFullCollegeExpenses = false;
                }
                if ($scope.goalDetailsInsurance.risk.coverFullMortgage == "100" || ($scope.goalDetailsInsurance.risk.coverFullMortgage && $scope.goalDetailsInsurance.risk.coverFullMortgage != "50" && $scope.goalDetailsInsurance.risk.coverFullMortgage != "false")) {
                    $scope.goalDetailsInsurance.risk.coverHalfMortgage = false;
                    $scope.goalDetailsInsurance.risk.coverFullMortgage = true;
                }
                else if ($scope.goalDetailsInsurance.risk.coverFullMortgage == "50" || ($scope.goalDetailsInsurance.risk.coverHalfMortgage && $scope.goalDetailsInsurance.risk.coverFullMortgage != "false")) {
                    $scope.goalDetailsInsurance.risk.coverHalfMortgage = true;
                    $scope.goalDetailsInsurance.risk.coverFullMortgage = false;
                }
                else {
                    $scope.goalDetailsInsurance.risk.coverHalfMortgage = false;
                    $scope.goalDetailsInsurance.risk.coverFullMortgage = false;
                }

                if ($scope.goalDetailsInsurance.risk.replaceIncome == "replaceIncome") {
                    $scope.goalDetailsInsurance.risk.replaceIncome = true;
                    $scope.goalDetailsInsurance.risk.replaceFullMonthlyExpenses = false;
                    $scope.goalDetailsInsurance.risk.replaceHalfMonthlyExpenses = false;
                }
                else if ($scope.goalDetailsInsurance.risk.replaceIncome == "replaceFullMonthlyExpenses") {
                    $scope.goalDetailsInsurance.risk.replaceIncome = false;
                    $scope.goalDetailsInsurance.risk.replaceFullMonthlyExpenses = true;
                    $scope.goalDetailsInsurance.risk.replaceHalfMonthlyExpenses = false;
                } else {
                    $scope.goalDetailsInsurance.risk.replaceIncome = false;
                    $scope.goalDetailsInsurance.risk.replaceFullMonthlyExpenses = false;
                    $scope.goalDetailsInsurance.risk.replaceHalfMonthlyExpenses = true;
                }
                $scope.data = $scope.goalDetailsInsurance;
            }
            else if ($scope.type == 'HOME')
                $scope.data = $scope.goalDetailsHome;
            else if ($scope.type == 'SPECIAL_EVENT')
                $scope.data = $scope.goalDetailsSpecialEvent;
            else if ($scope.type == 'CUSTOM_GOAL')
                $scope.data = $scope.goalDetailsCustomGoal;

            OranjApiService('goalsAdd', {
                data: $scope.data
            }
            ).then(function (response) {
                if(response.data.status == 'success')
                {
                    $scope.goalDetails = response.data.goal;
                    console.log("goalDetails",$scope.goalDetails);
                    $scope.goalDetailsType = response.data.goal.type.toLowerCase();
                }
            }, function (response) {
                if (response.data.status == 'error')
                    GlobalFactory.showErrorAlert(response.data.message);
            });
        };
        // API to call to get the Dependents
        $scope.getDependents = function () {
            OranjApiService('getDependents', null, {

            }
           ).then(function (response) {
               if (response.status == 'success') {
                   $scope.goalCollegeDependents = [];
                   $scope.goalCollegeDependents.push({
                       "dependentType": "ME",
                       "name": "Self"
                   });
                   if (response.data.dependents.children.length != 0) {
                       var childrensArray = response.data.dependents.children;
                       for (var i = 0; i < childrensArray.length; i++) {
                           $scope.goalCollegeDependents.push({
                               "dependentType": "CHILD",
                               "id": childrensArray[i].id,
                               "name": childrensArray[i].name
                           });
                       }
                   }
                   if (response.data.dependents.hasOwnProperty('spouse')) {
                       $scope.goalCollegeDependents.push({
                           "dependentType": "SPOUSE",
                           "id": response.data.dependents.spouse.id,
                           "name": response.data.dependents.spouse.name
                       });
                   }
                   $scope.prepareInsuredList(response.data.dependents.spouse);
                   $scope.prepareFundingItems(response.data.dependents);
                   $scope.prepareIndividualItems(response.data.dependents);
               } else {
                   //self
                   // $scope.global.specialEventFundingItems = [];
                   $scope.goalCollegeDependents = [];
                   $scope.goalCollegeDependents.push({
                       "dependentType": "ME",
                       "name": "Self"
                   });
                   // $scope.global.insuredList = [];
                   // $scope.global.specialEventFundingItems.push({
                   //     "dependentType": "ME",                          
                   //     "name": "Self"
                   // });
               }
           }, function (response) {

           });
        };
        $scope.getDependents();

        $scope.getDependent = function (dependentName) {
            var clientId = '';          
            if (!angular.isUndefinedOrNull(clientData)) {
                clientId = clientData.id;
            }

            for (var i = 0; i < $scope.goalCollegeDependents.length; i++) {
                if ($scope.goalCollegeDependents[i].name == dependentName) {
                    if ($scope.goalCollegeDependents[i].name == 'Self')
                        $scope.goalDetailsCollege.college.dependentId = clientId;
                    else
                        $scope.goalDetailsCollege.college.dependentId = $scope.goalCollegeDependents[i].id;
                    $scope.goalDetailsCollege.college.dependentType = $scope.goalCollegeDependents[i].dependentType;
                }
            }
        }

        // Prepeare InsuredList for add insurance goal
        $scope.prepareInsuredList = function (spouseDetail) {
            $scope.insuredList = [];
            //  Self
            $scope.insuredList.push({
                "dependentType": "ME",
                "name": "Self"
            });
            // Spouse
            $scope.insuredList.push({
                "dependentType": "SPOUSE",
                "insuredId": spouseDetail.id,
                "name": spouseDetail.name
            });
        };

        $scope.getInsured = function (insuredName) {
            var clientId = '';        
            if (!angular.isUndefinedOrNull(clientData)) {
                clientId = clientData.id;
            }

            for (var i = 0; i < $scope.insuredList.length; i++) {
                if ($scope.insuredList[i].name == insuredName) {
                    if ($scope.insuredList[i].name == 'Self')
                        $scope.goalDetailsInsurance.risk.insuredId = clientId;
                    else
                        $scope.goalDetailsInsurance.risk.insuredId = $scope.insuredList[i].insuredId;
                    $scope.goalDetailsInsurance.risk.dependentType = $scope.insuredList[i].dependentType;
                }
            }
        };
       
        // Prepare funding items for add goal special events
        $scope.prepareFundingItems = function (dependentsObj) {
            $scope.specialEventFundingItems = [];
            //self
            $scope.specialEventFundingItems.push({
                "dependentType": "ME",
                "name": "Self"
            });
            // Spouse
            $scope.specialEventFundingItems.push({
                "dependentType": "SPOUSE",
                "insuredId": dependentsObj.spouse.id,
                "name": dependentsObj.spouse.name
            });
            // child
            for (var i = 0; i < dependentsObj.children.length; i++) {
                $scope.specialEventFundingItems.push({
                    "dependentType": "CHILD",
                    "insuredId": dependentsObj.children[i].id,
                    "name": dependentsObj.children[i].name
                });
            }
        };

        $scope.getFunding = function (fundingName) {
            var clientId = '';
            if (!angular.isUndefinedOrNull(clientData)) {
                clientId = clientData.id;
            }
            for (var i = 0; i < $scope.specialEventFundingItems.length; i++) {
                if ($scope.specialEventFundingItems[i].name == fundingName) {
                    if ($scope.specialEventFundingItems[i].name == 'Self')
                        $scope.goalDetailsSpecialEvent.specialEvent.dependentId = clientId;
                    else
                        $scope.goalDetailsSpecialEvent.specialEvent.dependentId = $scope.specialEventFundingItems[i].insuredId;
                    $scope.goalDetailsSpecialEvent.specialEvent.dependentType = $scope.specialEventFundingItems[i].dependentType;
                }
            }
        };

        $scope.prepareIndividualItems = function (dependentsObj) {
            $scope.specialNeedIndividualItems = [];
            //self
            $scope.specialNeedIndividualItems.push({
                "dependentType": "ME",
                "name": "Self"
            });
            // Spouse
            $scope.specialNeedIndividualItems.push({
                "dependentType": "SPOUSE",
                "insuredId": dependentsObj.spouse.id,
                "name": dependentsObj.spouse.name
            });
            // child
            for (var i = 0; i < dependentsObj.children.length; i++) {
                $scope.specialNeedIndividualItems.push({
                    "dependentType": "CHILD",
                    "insuredId": dependentsObj.children[i].id,
                    "name": dependentsObj.children[i].name
                });
            }
        };

        $scope.getIndividual = function (fundingName) {
            var clientId = '';    
            if (!angular.isUndefinedOrNull(clientData)) {               
                clientId = clientData.id;               
            }

            for (var i = 0; i < $scope.specialNeedIndividualItems.length; i++) {
                if ($scope.specialNeedIndividualItems[i].name == fundingName) {
                    if ($scope.specialNeedIndividualItems[i].name == 'Self')
                        $scope.goalDetailsCustomGoal.custom.specialNeedsIndividualId = clientId;
                    else
                        $scope.goalDetailsSpecialEvent.specialEvent.specialNeedsIndividualId = $scope.specialNeedIndividualItems[i].insuredId;
                    $scope.goalDetailsCustomGoal.custom.specialNeedsIndividualType = $scope.specialNeedIndividualItems[i].dependentType;
                }
            }
        };

        $scope.routeChangedContinue = function (stepNumber) {
            if (stepNumber == 'step1') {
                $scope.step1 = false;
                $scope.step2 = true;
                $scope.step3 = false;
            }
            if (stepNumber == 'step2') {
                $scope.step1 = false;
                $scope.step2 = false;
                $scope.step3 = true;
            }
        };

        $scope.routeChangedCancel = function (stepNumber) {
            if (stepNumber == 'step2') {
                $scope.step1 = true;
                $scope.step2 = false;
                $scope.step3 = false;
            }
            if (stepNumber == 'step3') {
                $scope.step1 = false;
                $scope.step2 = true;
                $scope.step3 = false;
            }
        };


        // Engagment pie charts for "addGoal" and "addGoalStep3" start   
        piechartdata.reset();

        var pieSchema = piechartdata.dataSchema();
        pieSchema.options.series[0].data[0].value = 73;
        pieSchema.options.series[0].data[1].value = 100 - pieSchema.options.series[0].data[0].value;
        pieSchema.labelBottom.normal.color = '#585863';
        pieSchema.labelFromatter.normal.label.textStyle.color = '#fff';
        pieSchema.labelTop.normal.color = '#fff';
        pieSchema.labelFromatter.normal.label.formatter = function (params) {
            return 100 - params.value + '%'
        }

        piechartdata.add(pieSchema);

        // Engagment pie charts for "addGoal" and "addGoalStep3" end



        //jquery chart for "addGoal" and "addGoalStep3" start 


        var data = {
            labels: ["January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016", "October 2016", "November 2016", "December 2016", "January 2017"],
            datasets: [
                {
                    label: "Projected",
                    type: 'line',
                    fillColor: "transparent",
                    strokeColor: "rgba(58,62,55,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "#586fb2",
                    data: [0, 300000, 500000, 650000, 750000, 800000, 850000, 900000, 940000, 960000, 980000, 1000000, 1020000],
                    dottedFromLabel: "January 2016",

                },
            ]

        };

        var lineoptions = {
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,
            showScale: true,
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            scaleFontSize: 0
        }

        jQlinechartFactory.jQLineChart("canvas", data, lineoptions);//parameter sequence 1 canvas ID,2 Data to build, 3 options to configure

        //jquery chart for "addGoal" and "addGoalStep3" end


        // slider start

        $scope.percentages = {};
        $scope.percentages.low1 = 2;
        $scope.percentages.low2 = 6500;
        $scope.percentages.low3 = 4;
        $scope.percentages.low4 = 22.4;


        // slider 1 start
        var formatToPercentage1 = function (value) {

            if (value == 1) {
                $scope.goalDetailsRetirement.priority = 'VERY_IMPORTANT';
                $scope.goalDetailsCollege.priority = 'VERY_IMPORTANT';
                $scope.goalDetailsInsurance.priority = 'VERY_IMPORTANT';
                $scope.goalDetailsHome.priority = 'VERY_IMPORTANT';
                $scope.goalDetailsSpecialEvent.priority = 'VERY_IMPORTANT';
                $scope.goalDetailsCustomGoal.priority = 'VERY_IMPORTANT';
            }
            if (value == 2) {
                $scope.goalDetailsRetirement.priority = 'IMPORTANT';
                $scope.goalDetailsCollege.priority = 'IMPORTANT';
                $scope.goalDetailsInsurance.priority = 'IMPORTANT';
                $scope.goalDetailsHome.priority = 'IMPORTANT';
                $scope.goalDetailsSpecialEvent.priority = 'IMPORTANT';
                $scope.goalDetailsCustomGoal.priority = 'IMPORTANT';
            }
            if (value == 3) {
                $scope.goalDetailsRetirement.priority = 'AVERAGE';
                $scope.goalDetailsCollege.priority = 'AVERAGE';
                $scope.goalDetailsInsurance.priority = 'AVERAGE';
                $scope.goalDetailsHome.priority = 'AVERAGE';
                $scope.goalDetailsSpecialEvent.priority = 'AVERAGE';
                $scope.goalDetailsCustomGoal.priority = 'AVERAGE';

            }
            if (value == 4) {
                $scope.goalDetailsRetirement.priority = 'BELOW_AVERAGE';
                $scope.goalDetailsCollege.priority = 'BELOW_AVERAGE';
                $scope.goalDetailsInsurance.priority = 'BELOW_AVERAGE';
                $scope.goalDetailsHome.priority = 'BELOW_AVERAGE';
                $scope.goalDetailsSpecialEvent.priority = 'BELOW_AVERAGE';
                $scope.goalDetailsCustomGoal.priority = 'BELOW_AVERAGE';
            }
            if (value == 5) {
                $scope.goalDetailsRetirement.priority = 'LEAST_IMPORTANT';
                $scope.goalDetailsCollege.priority = 'LEAST_IMPORTANT';
                $scope.goalDetailsInsurance.priority = 'LEAST_IMPORTANT';
                $scope.goalDetailsHome.priority = 'LEAST_IMPORTANT';
                $scope.goalDetailsSpecialEvent.priority = 'LEAST_IMPORTANT';
                $scope.goalDetailsCustomGoal.priority = 'LEAST_IMPORTANT';
            }

            return value + ' - ' + $filter('capitalize')($scope.goalDetailsRetirement.priority);
        };

        String.prototype.capitalize = function () {
            return this.toLowerCase().replace(/\b\w/g, function (m) {
                return m.toUpperCase();
            });
        }

        $scope.percentages.options1 = {
            floor: 1,
            ceil: 4,
            translate: formatToPercentage1,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
        // slider 1 end



        // slider 2 start
        var formatToPercentage2 = function (value) {
            $scope.goalDetailsRetirement.retirement.monthlyWithdrawal = value;
            return ' $' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.00' + ' / month';
        };

        $scope.percentages.options2 = {
            floor: 0,
            ceil: 50000,
            translate: formatToPercentage2,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
        // slider 2 end


        // slider 3 start
        var formatToPercentage3 = function (value) {
            $scope.goalDetailsRetirement.retirement.longTermInvestment = value;
            $scope.goalDetailsCollege.college.longTermInvestment = value;
            $scope.goalDetailsHome.home.longTermInvestment = value;
            $scope.goalDetailsSpecialEvent.specialEvent.longTermInvestment = value;
            $scope.goalDetailsCustomGoal.custom.longTermInvestment = value;
            return value + ' - conservative moderate';
        };

        $scope.percentages.options3 = {
            floor: 0,
            ceil: 10,
            translate: formatToPercentage3,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
        // slider 3 end


        // slider 4 start
        var formatToPercentage4 = function (value) {
            return value + '%';
        };

        $scope.percentages.options4 = {
            floor: 0,
            ceil: 100,
            step: 0.1,
            precision: 1,
            translate: formatToPercentage4,
            showSelectionBar: true,
            hideLimitLabels: true,
            getSelectionBarColor: function (value) { return '#999'; },
            getPointerColor: function (value) { return '#999'; },

        };
      

        // search concept


        // =================
        var self = $scope;

        self.simulateQuery = false;
        self.isDisabled = false;

        // list of `state` value/display objects
        self.states = loadAll();
        self.querySearch = querySearch;
        // self.selectedItemChange = selectedItemChange;
        //  self.searchTextChange   = searchTextChange;

        self.newState = newState;

        function newState(state) {
            alert("Sorry! You'll need to create a Constituion for " + state + " first!");
        }

        function querySearch(query) {
            var results = query ? self.states.filter(createFilterFor(query)) : self.states,
                deferred;
            if (self.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
                return deferred.promise;
            } else {
                return results;
            }
        }

        // function searchTextChange(text) {

        // }

        // function selectedItemChange(item) {

        // }

        /**
         * Build `states` list of key/value pairs
         */
       function loadAll() {
            var allStates = 'Accenture Business Process Outsourcing, Accendo Realtors, Accelerate Learning Center, Acculabs Group of Companies';

            return allStates.split(/, +/g).map(function (state) {
                return {
                    value: state.toLowerCase(),
                    display: state
                };
            });
        }

        /**
         * Create filter function for a query string
         */
       function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(state) {
                return (state.value.indexOf(lowercaseQuery) === 0);
            };
        }


    }



})();