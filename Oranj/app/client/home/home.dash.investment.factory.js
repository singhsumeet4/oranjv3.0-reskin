(function () {
    angular.module('app.client.home')
        .factory('homeDashInvestmentFactory', ['currencyFilter', function (currencyFilter) {


            return {
                getInvestmentDashChart: function (data) {
                    var chartObj = {
                        chart: {
                            type: 'spline',
                            spacing: [0, 0, 0, 0]
                        },
                        series: [{
                            color: '',
                            linewidth: 1,
                            data: []
                        }],
                        credits: {
                            enabled: false
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        legend: {
                            enabled: false
                        },
                        tooltip: {
                            backgroundColor: 'rgba(0,0,0,0.9)',
                            borderColor: 'rgba(0,0,0,0.9)',
                            headerFormat: '<table>',
                            footerFormat: '</table>',
                            pointFormatter: function () {
                                var tooltip =  '<tr><td>' + this.tooltipData.date + ': </td><td>' + this.tooltipData.balance + '</td></tr>';

                                angular.forEach(this.tooltipData.accnts, function(accnt) {
                                    tooltip += '<tr><td>'+accnt.accountName+': </td><td>' + currencyFilter(accnt.value) + '</td></tr>';
                                });

                                return tooltip;

                            },
                            useHTML: true,
                            style: {
                                color: '#fff'
                            }
                        },
                        title: {
                            text: '',
                            style: {display: "none"}
                        },
                        yAxis: {
                            min: 0,
                            max: 1000000,
                            tickInterval: 200000,
                            gridLineWidth: 0,
                            lineWidth: 1,
                            title: {
                                text: '',
                                align: 'high'
                            },

                            labels: {
                                rotation: 0,
                                style: {
                                    fontSize: '8px',
                                    fontFamily: 'Open Sans',
                                    fontStyle: 'italic'

                                },
                                formatter: function () {
                                    var data = currencyFilter(this.value, '$', 2);
                                    return data;
                                }

                            }

                        }
                    };

                    angular.forEach(data, function (investment) {
                        console.log('investment value is : ' + investment.totalBalance);
                        chartObj.series[0].data.push({
                            x: investment.date, y: investment.totalBalance,
                            tooltipData: {
                                date: moment(investment.date).format('MM/DD/YYYY'),
                                balance : currencyFilter(investment.totalBalance),
                                accnts: investment.performanceChart
                            }
                        });
                    });

                    return chartObj;
                },
                getInvestementAccntsList: function (accntsObj) {
                    var investementAccounts = {}, accnts = accntsObj.accnts;

                    if(angular.isArray(accnts)) {
                        angular.forEach(accnts, function (accnt) {
                            accnt.holdings = [];
                            accnt.isOpen = false;
                            accnt.changeValue = accnt.changeValue.toFixed(2);
                            investementAccounts[accnt.accountId] = accnt;
                        });
                    } else {
                        accnts.holdings = [];
                        accnts.isOpen = false;
                        accnts.changeValue = accnts.changeValue.toFixed(2);
                        investementAccounts[accnts.accountId] = accnts;
                    }


                    angular.forEach(accntsObj.yodleeHoldings, function (holding) {
                        if (investementAccounts[holding.accountId]) {
                            investementAccounts[holding.accountId].holdings.push({
                                name: holding.description,
                                stockSymbol: holding.symbol,
                                price: holding.price,
                                changePer: holding.percentageChange || 0,
                                changePositive: holding.percentageChange > 0,
                                changeValue: holding.dailyChange,
                                value: holding.value
                            });
                        }
                    });


                    angular.forEach(accntsObj.quovoHoldings, function (holding) {
                        if (investementAccounts[holding.accountId]) {
                            investementAccounts[holding.accountId].holdings.push({
                                name: holding.portfolioName,
                                price: holding.price,
                                stockSymbol: holding.ticker,
                                value: holding.value
                            });
                        }
                    });


                   /* if(angular.isArray(accnts.quovoAccnts)) {
                        angular.forEach(accnts.quovoAccnts, function (accnt) {
                            accnt.holdings = [];
                            accnt.isOpen = false;
                            accnt.changeValue = accnt.changeValue.toFixed(2);
                            investementAccounts[accnt.accountId] = accnt;
                        });
                    } else {
                        accnts.quovoAccnts.holdings = [];
                        accnts.quovoAccnts.isOpen = false;
                        accnts.quovoAccnts.changeValue = accnts.quovoAccnts.changeValue.toFixed(2);
                        investementAccounts[accnts.quovoAccnts.accountId] = accnts.quovoAccnts;
                    }
                    */





                    return investementAccounts;
                }
            }
        }])
})();
