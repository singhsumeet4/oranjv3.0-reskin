
(function () {
    'use strict';

    angular.module('app.ctrl',['ngMaterial'])
        .controller('RightCtrl',['$scope', '$timeout', '$mdSidenav', '$log',RightCtrl]);
        
    function RightCtrl($scope, $timeout,$mdSidenav,$log){
      $scope.close = function() {
      $mdSidenav('right').close().then(function () {
          $log.debug("close RIGHT is done");
        });
      };
    };



})();