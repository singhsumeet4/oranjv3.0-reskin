
(function () {
    'use strict';

    angular.module('app.ctrl',['ngMaterial'])
        .controller('LeftCtrl',['$scope', '$timeout', '$mdSidenav', '$log',LeftCtrl]);
        
    function LeftCtrl($scope, $timeout,$mdSidenav,$log){
        
      $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });
      };
    };
    
   
})();