(function (){

 	'use strict';

 	angular.module('app.advisor.insurance').controller('InsuranceLifeAdvisorCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceLifeAdvisorCtrl]);

 	function InsuranceLifeAdvisorCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter,$window){

        $scope.getRiskListForInsurance('Life');

        var w = angular.element($window);
          w.bind('resize', function () {         
          $scope.getRiskListForInsurance('Life');
        });
    }

})();
