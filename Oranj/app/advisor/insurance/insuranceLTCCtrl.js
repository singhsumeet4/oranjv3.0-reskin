(function (){

 	'use strict';

 	angular.module('app.advisor.insurance').controller('InsuranceLTCAdvisorCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceLTCAdvisorCtrl]);

 	function InsuranceLTCAdvisorCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter,$window){

        $scope.getRiskListForInsurance('LTC');

        var w = angular.element($window);
        w.bind('resize', function () {
			$scope.getRiskListForInsurance('LTC');
		});

     }

})();
