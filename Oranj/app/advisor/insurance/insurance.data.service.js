(function () {

	angular.module('app.advisor.insurance').factory('insuranceDataServiceAdvisor', ['OranjApiService', '$q','$stateParams', insuranceDataServiceAdvisor]);

	function insuranceDataServiceAdvisor(OranjApiService, $q,$stateParams) {

		    var _type='';
            _insuranceData = $q.defer();
            _userProfileByAdvisor = $q.defer();
    
		return {				
			 fetchInsuranceDataAdvisor: function (type) {
                if(type!=_type) {
                	_type=type;
                    OranjApiService('getRiskListForInsuranceAdvisor',null,{
                        userId: $stateParams.id
                    }).then(function (res) {
                        if (res.status == 'success') {                           
                            _insuranceData.resolve(res);                            
                        }
                    }, function () {
                       
                    });
                }

                return _insuranceData.promise;
            },
            fetchUserProfileByAdvisor: function () {
                    OranjApiService('getUserProfileData',null,{
                        userid: $stateParams.id
                    }).then(function (res) {
                        if (res.status == 'success') {
                            _userProfileByAdvisor.resolve(res.data);
                        }
                    }, function () {
                    });
                return _userProfileByAdvisor.promise;
            },
		}
	}
})();
