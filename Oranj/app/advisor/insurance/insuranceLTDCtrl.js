(function (){

 	'use strict';

 	angular.module('app.advisor.insurance').controller('InsuranceLTDAdvisorCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceLTDAdvisorCtrl]);

 	function InsuranceLTDAdvisorCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter,$window){

        $scope.getRiskListForInsurance('LTD');
       
        var w = angular.element($window);
        w.bind('resize', function () {
			$scope.getRiskListForInsurance('LTD');
		});

     }

})();
