(function (){

 	'use strict';

 	angular.module('app.advisor.insurance').controller('InsuranceAdvisorCtrl', ['$scope', '$state', '$rootScope', 'GlobalFactory', 'OranjApiService','$filter', '$window', InsuranceAdvisorCtrl]);

 	function InsuranceAdvisorCtrl($scope, $state, $rootScope, GlobalFactory, OranjApiService,$filter, $window){

        $scope.getRiskListForInsurance('All');

        var w = angular.element($window);
        w.bind('resize', function () {			
			$scope.getRiskListForInsurance('All');
		});
        
     }

})();
