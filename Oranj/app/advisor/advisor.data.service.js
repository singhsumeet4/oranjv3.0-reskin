﻿(function () {



    function advisorDataService(OranjApiService, $q) {
        var defer = $q.defer();
        return {
            fetchAllClientList: function () {
                OranjApiService('getAllClientList').then(function (res) {
                    if (res.status === 'success') {
                        var dataObj = res.data['client-summary'];
                        defer.resolve(dataObj);
                    }
                    else {
                        defer.reject(res);
                    }
                },
                    function () {

                    });

                return defer.promise;
            }

        };
    }

    angular.module('app.advisor').factory('advisorDataService', ['OranjApiService', '$q', advisorDataService]);
})();
