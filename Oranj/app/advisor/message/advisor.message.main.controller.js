var messageApp = angular.module('app.advisor.message', ['btford.socket-io']);

messageApp.controller('messageMainController', ['$scope', '$rootScope', '$stateParams', 'socket', '$sanitize', '$timeout', 'GlobalFactory', 'UserProfileService', messageMainController]);
function messageMainController($scope, $rootScope, $stateParams, socket, $sanitize, $timeout, GlobalFactory, UserProfileService) {
   
        var typing = false;
        var lastTypingTime;
        var TYPING_TIMER_LENGTH = 400;
        var USER_TYPES = ['ROLE_ADVISOR', 'ROLE_CLIENT', 'ROLE_PROSPECT'];
        var MESSAGES_LENGTH = 10;
        var CLIENT_CHAT_INDEX = 1;
        var userInfo;
        var chatCollection = {};
        var connected = false;
        $scope.global = GlobalFactory;
        $scope.global.clientList = {};
        // UMC : Unread message count map
        var umcMap = {};

        $scope.isAdvisor = true;
        $scope.typing = false;
        $scope.sender = true;
        $scope.today = new Date().toDateOnlyFormat();
        var advisorData;

        UserProfileService.fetchAdvisorProfile().then(function (data) {    
        console.log("data",data); 
        advisorData = data.advisorProfile;
        $scope = this; // socketprogram();
        init();
    }, function (error) {
        console.log('error', error);

    });

        //initializing messages 
        $rootScope.messages = {}

        socket.on('connect',function(){

            connected = true;   

            // On login display welcome message
            socket.on('login', function (data) {
                console.log('login called');
                CLIENT_CHAT_INDEX = 1;
                //Set the value of connected flag
                $rootScope.connected = true;

                $rootScope.number_message = message_string(data.numUsers.totAdvisors + data.numUsers.totClients);
                console.log('$rootScope.number_message : ', $rootScope.number_message);
                $rootScope.$apply();
            });

             // Whenever the server emits 'user joined', log it in the chat body
            socket.on('user joined', function (data) {
                addMessageToList("",false,data.username + " joined")
                addMessageToList("",false,message_string(data.numUsers))
            });

            // Whenever the server emits 'user left', log it in the chat body
            socket.on('user left', function (data) {
                addMessageToList("",false,data.username+" left")
                addMessageToList("",false,message_string(data.numUsers))
            });

            //Whenever the server emits 'typing', show the typing message
            socket.on('typing', function (data) {
                addChatTyping(data);
            });

            // Whenever the server emits 'stop typing', kill the typing message
            socket.on('stop typing', function (data) {
                removeChatTyping(data.username);
            });

            // Whenever the server emits 'userIsTyping',notify to advisor that client is typing
            socket.on('userIsTyping', function (msg,userId) {
                if($scope.isAdvisor && userId == $rootScope.currentSelectedUserId)
                    $scope.typing = true;
                $scope.typingText = msg;
                $timeout(stopTyping, 1000);            
            });

            // push client chat history from server
            socket.on('pushClientChatHistory', function (chatList) {
                populateChatListForClient(chatList);
            });

            // push advisor chat history from server
            socket.on('pushAdvisorChatHistory', function (chatList, clientId) {
                populateChatListForAdvisor(chatList, clientId);

                if($rootScope.isClientSelected && clientId == $rootScope.currentSelectedUserId)
                    updateUnreadMsgStatus(chatList);
            });


            socket.on('pushClientList', function (clientList) {
                console.log('pushClientList', clientList);
                var clientObj = {'recent': [] , 'clients': [], 'prospects': []}
                var recentDate = moment(new Date()).subtract(7, 'days').format("MM-DD-YYYY");               
               // console.log(clientObj);
                for (var i = 0; i < clientList.length; i++) {
                    console.log('in for loop');
                        var lastChatTime = moment(clientList[i].lastChatTime).format("MM-DD-YYYY");
                        if (moment(lastChatTime).isAfter(recentDate)) {                            
                            clientObj.recent.push(clientList[i]);
                        }
                        else if (clientList[i].role==='ROLE_CLIENT') {
                            clientObj.clients.push(clientList[i]);
                        }
                        else {
                            clientObj.prospects.push(clientList[i]);
                        }
                        //one more check reqiured once it will return client && Prospect Type                  
                   
                    }
                $rootScope.$broadcast("clientListChat", clientObj);
            });

            // on client is online
            socket.on('clientOnline', function(clientId){
                $rootScope.$broadcast("clientOnline", clientId);
            });

            // on client is offline
            socket.on('clientOffline', function(clientId){
                $rootScope.$broadcast("clientOffline", clientId);
            });

             // on updateUnreadMsgCount
            socket.on('updateUnreadMsgCount', function(numOfMsgUpdated, clientId){
                $rootScope.$broadcast("updateUnreadMsgCount", numOfMsgUpdated, clientId);
            });

            // increase UnreadMsgCount on new message push from server
            socket.on('increaseUnreadMsgCount', function(clientId){                
                $rootScope.$broadcast("increaseUnreadMsgCount", clientId);
            });          

        }); 

        function getUsername() {
            var advisorInfo = advisorData;
            if (!angular.isUndefinedOrNull(advisorInfo))
                return advisorInfo.firstName + advisorInfo.lastName;
        };

        function message_string(number_of_users){
            return number_of_users === 1 ? "there's 1 participant":"there are " + number_of_users + " participants"
        };
            // initial
        function init(){
            $rootScope.isClientSelected = false;            
            initUserInfo(); 
        };

        function initUserInfo(){
            //Add user
            userInfo = {
                userType: 'ROLE_ADVISOR',
                email: advisorData.email,
                username: getUsername()
            }

            userInfo.senderId =  advisorData.id;
            console.log("userInfo",userInfo);           
            socket.emit('joinChat', userInfo);
        };
}