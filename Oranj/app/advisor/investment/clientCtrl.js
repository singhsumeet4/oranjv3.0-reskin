(function () {
    'use strict';

    angular.module('app.advisor.investment')
        .controller('advisorInvestmentCtrl', ['$scope', 'OranjApiService', 'GlobalFactory', '$state', '$stateParams', '$filter','$rootScope', advisorInvestmentCtrl]);


    function advisorInvestmentCtrl($scope, OranjApiService, GlobalFactory, $state, $stateParams, $filter,$rootScope) {

        $scope.global = GlobalFactory;
        var vm = this;
        $scope.userId = $stateParams.id;
        vm.accountName = $stateParams.name;
        var endDate = $filter('date')(new Date(), 'MMMM dd yyyy');
        var startDate = moment(new Date()).subtract(30, 'days').format('MMMM DD YYYY');
        $scope.date = {'startDate':startDate,'endDate':endDate};
        $scope.maxDate = $filter('date')(new Date(), 'MMMM dd, yyyy');
        var cacheObjTrans = []; 

        $scope.showLabelsOnChart = {
            startDate: '',
            endDate: '',
            startBalance: '',
            endBalance: '',
            change: '',
            changePercentage: ''
        };
       
        $scope.getInvestementAccountHoldings = function(Acctype,id)
        {
        	$scope.global.investmentSpecificAccountHolding = [];
        	$scope.noHoldingText = "";
            if (cacheObjTrans.hasOwnProperty(id)) {                
                 var transPresent = true;
            }
            if (!transPresent) {
                if(Acctype == "HELD_AWAY"){
                    OranjApiService('getInvestementAccountHoldingsYodleeAdvisor',null,{
                            yodleeItemAccountId: id,
                            userId:$scope.userId
                        }).then(function(response){
                            if(response.status == "success"){
                                $scope.global.investmentSpecificAccountHolding = response.data.holdings;
                                $scope.specificAccountHolding = response.data.holdings;
                                if(response.data.holdings.length == 0) {
                                	$scope.noHoldingText = "No holdings info available";
                                }
                                if($scope.global.investmentAccountSideList != undefined){
                                    for( var i =0 ;i<$scope.global.investmentAccountSideList.length;i++)
                                    {
                                        if($scope.global.investmentAccountSideList[i] != undefined)
                                            if($scope.global.investmentAccountSideList[i].accountId == id){
                                                $scope.global.investmentAccountSideList[i].holdings = response.data.holdings;
                                                $scope.investmentAccountDetails[i].holdings = response.data.holdings;
                                            }                        
                                    }
                                }
                                cacheObjTrans[id] = response.data.holdings;
                            }
                        },function(response){
                            if(response.status == "error")
                                $scope.noHoldingText = "No holdings info available";
                         
                    });
                }
                else if(Acctype == "MANAGED")
                {
                    OranjApiService('getInvestementAccountHoldingsQuovoAdvisor',null,{
                        yodleeItemAccountId: id,
                        userId:$scope.userId
                        }).then(function(response){
                            if(response.status == "success"){
                                $scope.global.investmentSpecificAccountHolding = response.data.holdings;
                                $scope.specificAccountHolding = response.data.holdings;
                                if(response.data.holdings.length == 0)
                                	$scope.noHoldingText = "No holdings info available";

                                for( var i =0 ;i<$scope.global.investmentAccountSideList.length;i++)
                                {
                                    if($scope.global.investmentAccountSideList[i] != undefined)
                                        if($scope.global.investmentAccountSideList[i].accountId == id){
                                            $scope.global.investmentAccountSideList[i].holdings = response.data.holdings;  
                                            $scope.investmentAccountDetails[i].holdings = response.data.holdings;
                                        }                        
                                }
                                cacheObjTrans[id] = response.data.holdings;
                            }
                        },function(response){
                            if(response.status == "error")
                                $scope.noHoldingText = "No holdings info available";
                    });
                }
            }
        }

        $scope.getInvestmentHeader = function () {
            OranjApiService('getInvestmentHeaderAdvisor',null,{
                userId: $scope.userId
            }).then(function (response) {
                
                if (response.status == "success") {
                    $scope.numberOfAccount = response.data.numberOfAccount;
                    $scope.totalBalance = response.data.totalBalance;
                    $scope.goalCount = response.data.goalCount;
                    $scope.changedValue = response.data.totalChangedValue;
                    $scope.percentageChange = response.data.totalChangePercentage;
                    $scope.changeStatus = response.data.changePositive;
                    
                }
            }, function (response) {

            });
        };

        $scope.getInvestmentMainAssetChart = function () {
            OranjApiService('getInvestmentMainAssetChartAdvisor',null,{
                userId:$scope.userId
            }).then(function (response) {
                var newValue = 0;
                var combineValue = 0;
                var unknownAvailable = false;
                if (response.status == "success") {
                     var doughnutData = response.data.investmentPieChart['Asset Class'];
                     var finalDoughnutData=[];
                    for (var i = 0; i < doughnutData.length; i++) {

                        if(doughnutData[i].name == 'Unknown' || doughnutData[i].name == null || doughnutData[i].name == 'unknown')
                        {
                            unknownAvailable = true;
                            var newValue = combineValue + doughnutData[i].value;
                            combineValue = newValue;
                        }
                        else
                        {
                            var doughnutDataobj = {};
                            doughnutDataobj.label = doughnutData[i].name;
                            doughnutDataobj.value = doughnutData[i].value;
                            finalDoughnutData.push(doughnutDataobj);
                        }
                    }
                    if(unknownAvailable){
                        var doughnutDataobj = {};
                        doughnutDataobj.label = 'Unknown';
                        doughnutDataobj.value = combineValue;
                        finalDoughnutData.push(doughnutDataobj);
                    }
                     assetChartCreation(finalDoughnutData);
                }
            }, function (response) {

            });
        };

        $scope.getInvestmentMainPercent = function () {
            $scope.chartLabels = [];
            $scope.chartData = [];
            var startDate = $filter('date')(new Date($scope.date.startDate), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.endDate), 'MM-dd-yyyy');
            
            if(new Date($scope.date.startDate) <= new Date($scope.date.endDate)) {
            	OranjApiService('getInvestmentPerformaceChartAdvsior', null, {
                    'startDate': startDate,
                    'endDate': endDate,
                    userId: $scope.userId
                }).then(function (response) {
                    if(response.status == "success"){
                        for (var i = 0; i < response.data.values.length; i++) {
                            for (var key in response.data.values[i]) {
                                if (key == 'date')
                                    $scope.chartLabels.push(response.data.values[i][key]);
                                if (key == 'totalBalance')
                                    $scope.chartData.push(response.data.values[i][key]);
                                if(key == 'performanceChart')
                                    $scope.performanceChart = response.data.values[i][key];
                                $scope.length = $scope.chartLabels.length;
                            }
                        }

                        if ($scope.chartLabels.length > 0 && $scope.chartData.length > 0) {
                            $scope.showLabelsOnChart.startDate = $filter('date')($scope.chartLabels[0], 'M/d/yyyy');
                            $scope.showLabelsOnChart.startBalance = $scope.chartData[0];

                            $scope.showLabelsOnChart.endDate = $filter('date')($scope.chartLabels[$scope.chartLabels.length - 1], 'M/d/yyyy');
                            $scope.showLabelsOnChart.endBalance = $scope.chartData[$scope.chartLabels.length - 1];

                            $scope.showLabelsOnChart.change = $scope.showLabelsOnChart.endBalance - $scope.showLabelsOnChart.startBalance;
                            if ($scope.showLabelsOnChart.change > 0) {
                                $scope.showLabelsOnChart.changePercentage = ($scope.showLabelsOnChart.startBalance / $scope.showLabelsOnChart.change) * 100;
                            }
                            else {
                                $scope.showLabelsOnChart.changePercentage = 0;
                            }

                        }
                        createPerformanceChartData($scope.chartLabels,$scope.chartData,$scope.performanceChart);
                    }
                }, function (response) {

                });
            }else {
            	GlobalFactory.showErrorAlert(' Start date should not be older than the End date');
            }
            
        };

        $scope.getInvestmentAccounts = function () {
            OranjApiService('getInvestmentAccountsAdvisor',null,{
                userId: $scope.userId
            }).then(function (response) {
                if(response.status == "success"){
                    $scope.accData = response.data.accounts;
                    for(var i=0;i<$scope.accData;i++)
                    {
                        if($scope.accData[i].accountInfoType == "HELD_AWAY")
                        {
                             $scope.investmentAccountDetailsYODLEE = $scope.accData[i];                        
                        }
                        if($scope.accData[i].accountInfoType == "MANAGED")
                        {
                            $scope.investmentAccountDetailsQUOVO = $scope.accData[i];                        
                        }
                    }

                    $scope.investmentAccountDetails = $scope.accData;
                    if( $scope.investmentAccountDetails.length > 0){
                    	 $scope.investmentAccountDetails.sort(function(a, b){
                    		 	return a.name > b.name;
                	  	   });
                	}
                    $scope.global.investmentAccountSideList = $scope.accData;
                    
                    if ($scope.global.accountId) {
                        $scope.getInvestmentHoldingsAssetChart($scope.global.accountId);
                        $scope.getInvestmentHoldingsPercent($scope.global.accountId);
                        if ($scope.investmentAccountDetails != undefined) {
                            for (var i = 0; i < $scope.investmentAccountDetails.length; i++) {
                                if($scope.investmentAccountDetails[i].accountInfoType == "HELD_AWAY" && $scope.global.accountId == $scope.investmentAccountDetails[i].accountId){
                                    $scope.getInvestementAccountHoldings('HELD_AWAY', $scope.global.accountId);
                                    $scope.accountType = $scope.investmentAccountDetails[i].type;
                                    $scope.value = $scope.investmentAccountDetails[i].value;
                                    $scope.accountName = $scope.investmentAccountDetails[i].name;
                                    $scope.changePercent = $scope.investmentAccountDetails[i].changePercent;
                                    $scope.changePositive = $scope.investmentAccountDetails[i].changePositive;
                                    $scope.changeValue = $scope.investmentAccountDetails[i].changeValue;
                                }
                                else if($scope.investmentAccountDetails[i].accountInfoType == "MANAGED" && $scope.global.accountId == $scope.investmentAccountDetails[i].accountId) {
                                    $scope.getInvestementAccountHoldings('MANAGED', $scope.global.accountId);
                                    $scope.accountType = $scope.investmentAccountDetails[i].type;
                                    $scope.value = $scope.investmentAccountDetails[i].value;
                                    $scope.accountName = $scope.investmentAccountDetails[i].name;
                                    $scope.changePercent = $scope.investmentAccountDetails[i].changePercent;
                                    $scope.changePositive = $scope.investmentAccountDetails[i].changePositive;
                                    $scope.changeValue = $scope.investmentAccountDetails[i].changeValue;
                                }

                            }
                        }
                    }
                }
            }, function (response) {

            });
        };

        $scope.getInvestmentHoldingsAssetChart = function (id) {
            OranjApiService('getInvestmentHoldingsAssetChartAdvisor', null, {
                yodleeItemAccountId: id,
                userId:$scope.userId
            }).then(function (response) {
                var newValue = 0;
                var combineValue = 0;
                var unknownAvailable = false;
                if(response.status == "success"){
                    var doughnutData = response.data.investmentPieChart['Asset Class'];
                     var finalDoughnutData=[];
                    for (var i = 0; i < doughnutData.length; i++) {

                        if(doughnutData[i].name == 'Unknown' || doughnutData[i].name == null || doughnutData[i].name == 'unknown')
                        {
                            unknownAvailable = true;
                            var newValue = combineValue + doughnutData[i].value;
                            combineValue = newValue;
                        }
                        else
                        {
                            var doughnutDataobj = {};
                            doughnutDataobj.label = doughnutData[i].name;
                            doughnutDataobj.value = doughnutData[i].value;
                            finalDoughnutData.push(doughnutDataobj);
                        }
                    }
                    if(unknownAvailable){
                        var doughnutDataobj = {};
                        doughnutDataobj.label = 'Unknown';
                        doughnutDataobj.value = combineValue;
                        finalDoughnutData.push(doughnutDataobj);
                    }
                     assetChartCreation(finalDoughnutData);
                }
            }, function (response) {

            });
        };

        $scope.getInvestmentHoldingsPercent = function (id) {
            $scope.chartLabels = [];
            $scope.chartData = [];
            var startDate = $filter('date')(new Date($scope.date.startDate), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.endDate), 'MM-dd-yyyy');
            OranjApiService('getInvestmentHoldingsPercentAdvsior', null, {
                'yodleeItemAccountId': id,
                'startDate': startDate,
                'endDate': endDate,
                userId:$scope.userId
            }).then(function (response) {
                if (response.status == 'success') {
                    for (var i = 0; i < response.data.values.length; i++) {
                        for (var key in response.data.values[i]) {
                            if (key == 'date')
                                $scope.chartLabels.push(response.data.values[i][key]);
                            if (key == 'totalBalance')
                                $scope.chartData.push(response.data.values[i][key]);
                            if(key == 'performanceChart')
                                $scope.performanceChart = response.data.values[i][key];
                            $scope.length = $scope.chartLabels.length;
                        }
                    }

                    if ($scope.chartLabels.length > 0 && $scope.chartData.length > 0) {
                        $scope.showLabelsOnChart.startDate = $filter('date')($scope.chartLabels[0], 'M/d/yyyy');
                        $scope.showLabelsOnChart.startBalance = $scope.chartData[0];

                        $scope.showLabelsOnChart.endDate = $filter('date')($scope.chartLabels[$scope.chartLabels.length - 1], 'M/d/yyyy');
                        $scope.showLabelsOnChart.endBalance = $scope.chartData[$scope.chartLabels.length - 1];

                        $scope.showLabelsOnChart.change = $scope.showLabelsOnChart.endBalance - $scope.showLabelsOnChart.startBalance;
                        if ($scope.showLabelsOnChart.change > 0) {
                            $scope.showLabelsOnChart.changePercentage = ($scope.showLabelsOnChart.startBalance / $scope.showLabelsOnChart.change) * 100;
                        }
                        else {
                            $scope.showLabelsOnChart.changePercentage = 0;
                        }

                    }
                    createPerformanceChartData($scope.chartLabels,$scope.chartData,$scope.performanceChart);
                }

            }, function (response) {

            });
        };

        $scope.changeDatesMain = function () {
            $scope.getInvestmentMainPercent();
        };

        $scope.changeDatesSpecific = function () {
            $scope.getInvestmentHoldingsPercent($scope.global.accountId);
        };

        //Asset
        function assetChartCreation(doughnutDataArr) {

            for(var i=0; i<doughnutDataArr.length;i++){
                if(doughnutDataArr[i].label.toLowerCase() == 'us stock')
                    doughnutDataArr[i].color = '#fdce0b';
                if(doughnutDataArr[i].label.toLowerCase() == 'non us stock')
                    doughnutDataArr[i].color = '#a3cd39';
                if(doughnutDataArr[i].label.toLowerCase() == 'us bond')
                    doughnutDataArr[i].color = '#3eb649';
                if(doughnutDataArr[i].label.toLowerCase() == 'non us bond')
                    doughnutDataArr[i].color = '#23c1e2';
                if(doughnutDataArr[i].label.toLowerCase() == 'preferred')
                    doughnutDataArr[i].color = '#2e4ea1';
                if(doughnutDataArr[i].label.toLowerCase() == 'cash')
                    doughnutDataArr[i].color = '#fdb45c';
                if(doughnutDataArr[i].label.toLowerCase() == 'other')
                    doughnutDataArr[i].color = '#f7464a';
                if(doughnutDataArr[i].label.toLowerCase() == 'unknown')
                    doughnutDataArr[i].color = '#d1cdcd';
            }
            Chart.defaults.global.responsive = true;

            var doughnutData = doughnutDataArr;

            var Total = 0.0;
            doughnutData.forEach(function (element) {
                Total = Total + element.value;
            });

            var doughnutOptions = {
                tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' (' + $filter('currency')((value.value / Total) * 100, '', 2) + '%)' },
                percentageInnerCutout: 75,
                segmentShowStroke: false,
                responsive: true,
            };
            if (document.getElementById("investmentDoughnut") !== null) {
                var ctx1 = document.getElementById("investmentDoughnut").getContext("2d");
                var myDoughnut = new Chart(ctx1).Doughnut(doughnutData, doughnutOptions);

                document.getElementById('investment-legend').innerHTML = myDoughnut.generateLegend(); // for legend 
            }


        };
        //Asset Chart end

        //Performance Chart Starts
        function createPerformanceChartData(chartLabels,chartData,performanceChart) {
        	
            function dateRange(range1, range2) {
                
                var fstDate = new Date(range1);
                var sndDate = new Date(range2);
                var dateArray = new Array();
                var counterDate = fstDate.getDate();
                var evenFlag = false;
                if ((counterDate % 2) === 0) {
                    evenFlag = true;
                }
                var count = 0;
                while (fstDate <= sndDate) {

                    dateArray.push(angular.copy(fstDate));
                    counterDate = counterDate + 1;
                    if (counterDate > 31) {
                        count++;
                        fstDate = new Date(fstDate.setDate(1))
                        fstDate = new Date(fstDate.setMonth(fstDate.getMonth() + count));
                        counterDate = evenFlag ? 2 : 1;
                    }
                    fstDate = new Date(fstDate.setDate(counterDate))
                }
                
                return dateArray;

            };
            
                       
            var yAxisCount = 2;
            $(function () {
                $('#containerInvestment').highcharts({
                    chart: {
                        type: 'line',
                        backgroundColor: 'transparent'
                    },
                    title: {
                        text: null
                    },
                    // subtitle: {
                    //     text: 'Source: WorldClimate.com'
                    // },

                    xAxis: {
                        //  gridLineWidth: 0,
                        //  lineWidth: 0,
                        // minorGridLineWidth: 1,
                        // minorTickLength: 1,
                        // tickLength: 0,

                        // categories: ['2/1/2016', '2/3', '2/5', '2/7', '2/9', '2/11', '2/13', '2/15', '2/17', '2/19', '2/21', '2/23', '2/25', '2/27', '3/1'],
                        categories: dateRange(chartLabels[0],chartLabels[$scope.length - 1]),
                        
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            rotation: 0,
                            style: {
                                fontSize: '8px',
                                fontFamily: 'Open Sans',
                                fontStyle: 'italic',

                            },
                            formatter: function () {
                                var data = $filter('date')(this.value, 'M/d');
                                return data;
                            },

                        }


                    },
                    yAxis: {
                         // min: chartData[0],
                        // max: 10000000,
                         // tickInterval: chartData[yAxisCount],
                         gridLineWidth: 0,
                         lineWidth: 1,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        categories: chartData,
                        labels: {
                            // step:5,
                            rotation: 0,
                            style: {
                                fontSize: '8px',
                                fontFamily: 'Open Sans',
                                fontStyle: 'italic',

                            },
                            formatter: function () {
                                var data = $filter('currency')(this.value);
                                return data;
                            },

                        }

                    },
                    tooltip: {
                        backgroundColor: '#191919',
                        forecolor: '#ffffff',
                        borderColor: 'black',
                        borderRadius: 10,
                        borderWidth: 3,
                        valueSuffix: '$',
                        formatter: function () {
                            var Data = '';
                            var data = '<span style="color:#ffffff">' + $filter('date')(this.x, 'MM/dd/yyyy') + ': ' + $filter('currency')(this.y, '$', 2) + '<span>' + ' <br/>'; 
                            for(var i=0;i<performanceChart.length;i++)
                            {
                                var newData =  '<span style="color:#ffffff">'+(performanceChart[i].accountName)+ ':' +  $filter('currency')(performanceChart[i].value)  
                                Data = Data + '<br/>' + '<span style="color:#ffffff">' + newData;
                            }
                            
                            return data + Data;
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: false
                            },
                            enableMouseTracking: true,
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        showInLegend: false,
                        data: chartData,
                    }]
                });
            });
        }
        $scope.specificRoute = function (accountId) {
            $rootScope.$broadcast("specificRoute", { route: "InvestmentSpecific", accId: accountId});
            $scope.global.accountId = accountId;
        }
        if (!angular.isUndefinedOrNull($scope.global.accountId)) {
            $scope.getInvestmentAccounts();//specific
        }
        else {
            $scope.global.accountId=null;
            $scope.getInvestmentHeader();
            $scope.getInvestmentMainAssetChart();
            $scope.getInvestmentMainPercent();
            $scope.getInvestmentAccounts();
        }

        

    }



})(); 