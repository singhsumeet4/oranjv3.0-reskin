(function () {
    'use strict';

    angular.module('app.advisor.settings')
        .controller('advisorsettingsCtrl', ['$scope','$rootScope', 'OranjApiService', 'GlobalFactory', advisorsettingsCtrl]);
    
        
    function advisorsettingsCtrl ($scope , $rootScope, OranjApiService, GlobalFactory) {
			
    		$(function(){
				$("#avatar-upload").on('click', function(e){
				    e.preventDefault();
				    $("#upload-av:hidden").trigger('click');
				});
			});


               $scope.advisor = {};
               $scope.deleteStatus = false;
               $scope.fileErrorflag = false;
               $scope.isDisabled = false;
               
               var role = GlobalFactory.getRoleService();
               role.then(
         	            function (result) {
	         	            if(result.isAdmin || result.isAdvisor){
	         	            	
	         	            }else{
	         	            	GlobalFactory.signout();
	         	            }
         	            }, function () {
         	                GlobalFactory.signout();
         	            }
         	   	);
               
	           $scope.profileData = function(){
	        	   OranjApiService('getAdvisorProfileData').then(function (response) {
	          		 if(response.status === 'success'){
	          			 if(typeof response.data !== 'undefined' && response.data.advisorProfile){
	          				 $scope.advisor.profile = response.data.advisorProfile;
	          				 if($scope.advisor.profile.advisorAvatarCompleteUrl == null) {
	          					$scope.advisor.profile.advisorAvatarCompleteUrl =  'images/avatar.jpg';
	          				 }
	          			 }
	          		 }
	          	 }, function (response) {
	               });
	           };
	           
	           function validateFileSize(file) {
	               var sizeInMb = (file.size / 1000);
	               if (sizeInMb > 250)
	                   return false;
	               else
	                   return true;
	           };

	           function validateFileExtension(filename) {
	               var extn = filename.split(".").pop().toLowerCase();
	               var VALID_FILE_EXTNS = ['png', 'jpg', 'jpeg', 'bmp', 'gif'];

	               if (VALID_FILE_EXTNS.indexOf(extn) > -1)
	                   return true;
	               else
	                   return false;
	           };
	           
	           function readURL(input) {
	               if (input.files && input.files[0]) {
	            	   var file = input.files[0];
	            	   if (!validateFileExtension(file.name)) {
	                       GlobalFactory.showErrorAlert("You can't upload files of this type.");
	                       $scope.fileErrorflag = true;
	                       if($scope.advisor.profile.advisorAvatarCompleteUrl != null) {
	                    	   $('#adv-avatar').attr('src', $scope.advisor.profile.advisorAvatarCompleteUrl);
	                       }
	                       return false;
	                   }

	                   if (!validateFileSize(file)) {
	                       GlobalFactory.showErrorAlert('Maximum Upload size is 250 KB.');
	                       $scope.fileErrorflag = true;
	                       if($scope.advisor.profile.advisorAvatarCompleteUrl != null) {
	                    	   $('#adv-avatar').attr('src', $scope.advisor.profile.advisorAvatarCompleteUrl);
	                       }
	                       return false;
	                   }
	                   
	                   $scope.fileErrorflag = false;
	                   var reader = new FileReader();

	                   reader.onload = function (e) {
	                       $('#adv-avatar').attr('src', e.target.result);
	                   }

	                   reader.readAsDataURL(input.files[0]);
	               }
	           }

	           $scope.loadProfileLogo = function(input){
	               readURL(input);
	               $scope.deleteStatus = false;
	           };
	           
	           $scope.removeAvatar = function(){
	        	   
	        	   $('#adv-avatar').attr('src', 'images/avatar.jpg');
	        	   $scope.deleteStatus = true;
	           };
	           
	           
	        // POST :  Upload advisor image
	           $scope.updateAdvisorImage = function(){
	        	   if($scope.deleteStatus == false) {
	        		   var headers = {
		                       'Content-Type': undefined
		                   };
		        	   var input = angular.element('#upload-av');
		        	   $scope.newUserFile = input[0].files[0];
		        	  
			           	if(typeof  $scope.newUserFile != 'undefined' &&  $scope.newUserFile.size > 0 && $scope.fileErrorflag == false)
			           	{
			       			
			           		$scope.formdata = new FormData();
			           		$scope.formdata.append('avatarContent', $scope.newUserFile);
				               
				           	OranjApiService('updateAdvisorAvatar', {
				                    data: $scope.formdata,
				                    transformRequest: angular.identity,
				                    headers: headers
				                }).then(function (response) {
				                    
				                }, function (response) {
				                	GlobalFactory.showErrorAlert('Server error in updation.');
				                });
			           	}
	        	   }else {
	        		   OranjApiService('deleteAvatar').then(function (response) {
	  	          	 	}, function (response) {
	  	          	 		GlobalFactory.showErrorAlert('Server error in updation.');
	  	          	 	});
	        	   }
	        	   
	           };
	           
	           //$scope.profileData();
	           
	           $scope.saveAdvisorProfile = function(){
	        	   
	        	   if($scope.form_profile.$valid){
	        		   $scope.isDisabled = true;
	        		   $scope.updateAdvisorImage();
	        		   $scope.postData = angular.copy($scope.advisor.profile);
	        		   if($scope.postData.firmName){
	        			   delete $scope.postData.firmName;
	        		   }
	        		   
	        		   OranjApiService('updateAdvisorProfile', {
	                        data: $scope.postData
	                    }).then(function (response) {
	                        if(response.status === 'success'){
	                        	GlobalFactory.showSuccessAlert('Advisor profile has been successfully updated.');
	                        }else{
	                        	GlobalFactory.showErrorAlert('Server error in updation.');
	                        }
	                        $scope.isDisabled = false;
	                    }, function (response) {console.log(response);
	                    	GlobalFactory.showErrorAlert('Server error in updation.');
	                    	$scope.isDisabled = false;
	                    });
	        	   }
	           };
	           
            
    }
})(); 