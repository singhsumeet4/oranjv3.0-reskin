angular.module('app.advisor.clientDashes')
    .controller("ClientDashesController", ['$scope', '$stateParams', 'GlobalFactory', '$state','AdvisorClientDashesDataService',
        function ($scope, $stateParams, GlobalFactory, $state, AdvisorClientDashesDataService) {


            var vm = this;

            vm.global = GlobalFactory;


            // Set Header size to small or big depeding on summary or other dash.
            vm.summary = $state.includes('page.advisor.contacts.clientDash.summary');


            vm.data = {
                selectedIndex: 0
            };


            $scope.$on('specificRoute', function (event, details) {
                vm.includeClientDash(details.route);
            });

            vm.userId = $stateParams.id;
            $scope.userName= vm.userName = $stateParams.name;


            AdvisorClientDashesDataService.getUserProfileData(vm.userId).then(function(userData) {
                vm.userProfile = userData.profile;


                vm.age = moment.duration(moment().valueOf() - userData.profile.dob).asYears().toFixed(0);
            });

            AdvisorClientDashesDataService.getLastContactInfo(vm.userId).then(function(lastContactInfo) {
                vm.lastContactTime = lastContactInfo.lastContactTime;
            });

            AdvisorClientDashesDataService.getLastLogin(vm.userId).then(function(lastLogin) {
                vm.lastLogin = lastLogin.loginDate;
            });

            AdvisorClientDashesDataService.getNetworth(vm.userId).then(function(networthData) {
                vm.networth = networthData.networth;
            });

            //set the selected md-tab on page load.

            switch($state.current.name) {
                case 'page.advisor.contacts.clientDash.summary':
                    vm.data.selectedIndex = 0;
                    break;
                case 'page.advisor.contacts.clientDash.goals.dashboard':
                case 'page.advisor.contacts.clientDash.goals.id':
                    vm.data.selectedIndex = 1;
                    break;
                default:
                    vm.data.selectedIndex = 0;
            }

            /*vm.clientdashpage = "app/client/home/html/clientDash.html";
             vm.includeClientDash = function (param) {
             switch (param) {
             case 'Summary':
             vm.summary = true;
             vm.clientdashpage = "app/client/home/html/clientDash.html";
             break;

             case 'Goals':
             vm.summary = false;
             vm.clientdashpage = "app/advisor/goals/html/goalDash.html";
             break;

             case 'Investments':
             vm.global.accountId=null;
             vm.summary = false;
             vm.clientdashpage = "app/advisor/investment/html/clientInvestmentMain.html";
             break;

             case 'InvestmentSpecific':
             vm.summary = false;
             vm.clientdashpage = "app/advisor/investment/html/investment-specific.html";
             break;

             case 'Insurance':
             vm.summary = false;
             vm.clientdashpage = "app/client/insurance/html/insurance.html";
             break;

             case 'Net worth':
             vm.summary = false;
             vm.clientdashpage = "app/client/networth/html/networth.html";
             break;

             case 'Accounts':
             vm.summary = false;
             vm.clientdashpage = "app/advisor/Accounts/html/advisorAccountMain.html";
             break;

             case 'AccountSpecific':
             vm.summary = false;
             vm.clientdashpage = "app/advisor/Accounts/html/advisorAccountSpecific.html";
             break;

             default:
             vm.clientdashpage = "app/client/home/html/clientDash.html";
             }

             };*/

        }]);
