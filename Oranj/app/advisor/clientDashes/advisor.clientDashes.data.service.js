(function () {

    angular.module('app.advisor.clientDashes')
        .service('AdvisorClientDashesDataService', ['OranjApiService', 'UserProfileService', '$q', function (OranjApiService, UserProfileService, $q) {

            this.getUserProfileData = function (id) {
                var defer = $q.defer();
                
                OranjApiService('getUserProfileData', {}, {userid: id}).then(function (res) {
                    if (res.status === 'success') {
                        defer.resolve(res.data);
                    } else {
                        defer.reject();
                    }
                });

                return defer.promise;
            };

            this.getLastContactInfo = function (clientId) {
                var defer = $q.defer();

                UserProfileService.fetchAdvisorProfile().then(function (res) {

                    console.log(res);

                    //Chatkey format is advisorId_clientid_advisorId

                    OranjApiService('getAdvisorClientLastContactInfo', {}, {
                        chatKey: res.advisorProfile.id+'_'+clientId+'_'+res.advisorProfile.id
                    }).then(function (res) {
                        if (res.status === 'success') {
                            defer.resolve(res.data);
                        } else {
                            defer.reject();
                        }
                    });
                });

                return defer.promise;
            };

            this.getLastLogin = function(id) {
                var defer = $q.defer();

                OranjApiService('getUserLastLogin',{},{userId: id}).then(function(res) {
                    if(res.status === 'success') {
                        defer.resolve(res.data);
                    } else {
                        defer.reject();
                    }
                });

                return defer.promise;
            };


            this.getNetworth = function(id) {


                var defer = $q.defer();

                OranjApiService('getAdvisorClientNetworth',{},{userId: id}).then(function(res) {
                    if(res.status === 'success') {
                        defer.resolve(res.data);
                    } else {
                        defer.reject();
                    }
                });

                return defer.promise;
            }
        }]);
})();
