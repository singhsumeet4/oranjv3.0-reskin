﻿(function () {
    'use restrict';

    angular.module('app.advisor.account', [])
        .controller('advisorAccountCtrl', ['$scope', '$rootScope', 'jQlinechart', 'OranjApiService', '$filter', '$state', 'GlobalFactory','$stateParams', advisorAccountCtrl])


    function advisorAccountCtrl($scope, $rootScope, jQlinechartFactory, OranjApiService, $filter, $state, globalFactory,$stateParams) {
                  
        $scope.todayDate = new Date();
        $scope.userId = $stateParams.id;

        $scope.getListOfAccountAdvisor = function () {
            OranjApiService('getListOfAccountAdvisor',null,{
                userId: $scope.userId
            }).then(function (response) {
                if(response.status == "success"){
                    var networthsDetailsinv = response.data.accounts;
                    $rootScope.$broadcast("accountDetails", { sideAccountList: networthsDetailsinv });
                }
            }, function (response) {

            });
        };
        // $scope.getListOfAccountAdvisor();
    }


})();