(function () {
    'use restrict';

    var app= angular.module('app.advisor.accountSpecific', [])

    app.controller('advisorAccountSpecificCtrl', ['$scope', '$rootScope', 'jQlinechart', 'OranjApiService', '$filter', '$state', 'GlobalFactory','$stateParams', advisorAccountSpecificCtrl])
    function advisorAccountSpecificCtrl($scope, $rootScope, jQlinechartFactory, OranjApiService, $filter, $state, globalFactory, $stateParams) {
                
        var endDate = $filter('date')(new Date(), 'MMMM dd yyyy');
        var startDate = moment(new Date()).subtract(30, 'days').format('MMMM DD YYYY');
        $scope.date = { 'start': startDate, 'end': endDate };
        $scope.userId = $stateParams.id;
        $scope.userName = $stateParams.name;
        $scope.accountData = [];
        $scope.transcations = [];
       
        var vm = this;
        vm.accountName = $stateParams.accountName;
       
        $scope.getTranscationsAdvisor = function () {
            var startDate = $filter('date')(new Date($scope.date.start), 'yyyy-MM-dd');
            var endDate = $filter('date')(new Date($scope.date.end), 'yyyy-MM-dd');
            var TransData = { 'startDate': startDate, 'endDate': endDate, "yodleeItemAccountId":  $scope.global.accountId.accountId}
            OranjApiService('getTranscationsAdvisor', {
                data: JSON.stringify(TransData)
            }, {
                    userId: $scope.userId,
                }).then(function (response) {   
                if (response.status == "success") {
                    $scope.transcations = response.data.transactions.transactions;
                }
            }, function (error) {
               
            });
        };

        $scope.historyChart = function()
        {
            $scope.chartDate = [];
            $scope.chartData = [];
            var startDate = $filter('date')(new Date($scope.date.start), 'MM-dd-yyyy');
            var endDate = $filter('date')(new Date($scope.date.end), 'MM-dd-yyyy');
            OranjApiService('getTransactionHistoryChartAdvisor', null, {
                'yodleeItemAccountId': $scope.global.accountId.accountId,
                'startDateString': startDate,
                'endDateString': endDate,
                'userId': $scope.userId
            }).then(function (response) {   
                if (response.status == "success") {
                    for(key in response.data)
                    {                       
                        $scope.chartDate.push($filter('date')(new Date(key), 'MM/dd'));
                        $scope.chartData.push(response.data[key]);
                    }
                    createHistoryChartData();
                }
            }, function (error) {
               
            });
        }

        // $scope.getAccountSpecificGoalAdvisor = function () {         
        //     OranjApiService('getAccountSpecificGoalAdvisor', null, {
        //         'userId': $scope.userId,
        //         'yodleeItemAccountId': $scope.global.accountId               
        //     }).then(function (response) {
        //         if (response.status == 'success') {                    
        //             $scope.headers = response.data;               
        //         }                
        //     }, function (error) {

        //     });
        // };   

        $scope.changeDate = function () {
             if (new Date($scope.date.start) <= new Date($scope.date.end)) {
                $scope.getTranscationsAdvisor();
                $scope.historyChart();
            }
            else {
                globalFactory.showErrorAlert(' Start date should not be older than the End date');
            }  
        }  
       
        function createHistoryChartData() {
            window.$filter = $filter;
            var data = {
                labels: $scope.chartDate,             
                datasets: [
                    {
                        label: "Debit",
                        type: 'line',
                        fillColor: "transparent",
                        strokeColor: "#3E51AF",
                        pointColor: "#3E51AF",
                        pointStrokeColor: "#3E51AF",
                        pointHighlightFill: "#3E51AF",
                        pointHighlightStroke: "#3E51AF",
                        data: $scope.chartData,
                    }
                ]
            };

            var lineoptions = {
                scaleShowGridLines: false,
                pointDot: false,
                bezierCurve: false,
                showScale: true,
                multiTooltipTemplate: function (value) { return ' $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
                tooltipTemplate: '<%if (label){%><%=moment(new Date(label)).format("MM/DD")%> : <%}%><%=$filter("currency")(value, "$" , 0)%>',
                scaleFontSize: 10,
                legendTemplate: '<table class=\"box_table\"><tr>'
                + '<% for (var i=0; i<datasets.length; i++) { %>'
                + '<td><div class=\"boxx\" style=\"background-color:<%=datasets[i].strokeColor %>\"></div></td>'
                + '<% if (datasets[i].labelShow) { %><td class=\"box_bar\"><%= datasets[i].labelShow %></td><% } %>'
                + '<% } %>'
                + '</tr></table>',
            }
            jQlinechartFactory.jQLineChart("canvas", data, lineoptions);//parameter sequence 1 canvas ID,2 Data to build, 3 options to configure
        }
                      
        function init() {         
          
            $scope.headers = $scope.global.accountId;  

            $scope.getTranscationsAdvisor();
            $scope.historyChart();
            //$scope.getAccountSpecificGoalAdvisor();
        };
        init();
    }


})();