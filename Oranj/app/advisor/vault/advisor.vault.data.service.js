(function () {

    angular.module('app.advisor.vaults').factory('advisorVaultDataService', ['OranjApiService', '$q', 'UserProfileService', advisorVaultDataService]);

    function advisorVaultDataService(OranjApiService, $q,  UserProfileService) {
        
        return {
            fetchAllClientList: function () {   
            	var defer = $q.defer();
                OranjApiService('getAllClientList').then(function (res) {
                        var dataObj = res.data['client-summary'];                        
                        defer.resolve(dataObj);
                    },
                    function () {

                    });

                return defer.promise;
            },
            
           fetchVaultRootList: function (userId) {
				var defer = $q.defer();
				var promiseA = OranjApiService('getUserVaultFolderListAll', {}, {
					userId: userId,
					shared: true,
	            });  //Folder List
				var promiseB = OranjApiService('getUserVaultFileListAll', {}, {
					userId: userId,
	                shared: true,
	            }); //file List
	
				function onSuccess(res) {
					defer.resolve(res);
				}
	
				function onError(error) {
					defer.reject(error);
				}
	            
				$q.all([promiseA, promiseB]).then(onSuccess, onError);			   
				return defer.promise;
		},
		
		prepareVaultData: function (res) {
			var dataObj = {
				folderCount: 0,
				totalFileCount: 0,
				sideVaultList: ''
			};
			var folders = [];
			var files = [];
            
			if (res[0].status === 'success' && res[1].status === 'success') {
				folders = res[0].data.folders;
				files = res[1].data.userFiles;
			}
			else if (res[0].status === 'success') {
				folders = res[0].data.folders;					
			}
			if (res[1].status === 'success') {					
				files = res[1].data.userFiles;
			}

			dataObj.sideVaultList = folders;
			var folderCount = folders.length;
			var totalFileCount = 0;

			folders.map(function (item) {
				totalFileCount += item.totalFilesCount;
			});

			var vaultFiles = files;
			dataObj.totalFileCount = vaultFiles.length + totalFileCount + folderCount;
			dataObj.folderCount = folderCount;			

			return dataObj;
		},
		
		prepareVaultRootData: function (results) {
		    var dataObj = {
		        vaultFolders: '',
		        vaultFiles:'',
		        folderCount: 0,
		        sideVaultList: '',
		        totalFileCount:0
		    };
		
		    if (results[0].status == "success" && results[1].status == "success") {
		        dataObj.vaultFolders = results[0].data.folders;
		        dataObj.folderCount = dataObj.vaultFolders.length;
		        dataObj.sideVaultList = results[0].data.folders;

		        dataObj.totalFileCount = 0;

		        dataObj.vaultFolders.map(function (item) {
		            dataObj.totalFileCount += item.totalFilesCount;
		        });

		        dataObj.vaultFiles = results[1].data.userFiles;			     
		    }
		    else if (results[0].status == "success") {
		        dataObj.vaultFolders = results[0].data.folders;
		        dataObj.folderCount = $scope.vaultFolders.length;
		        dataObj.sideVaultList = results[0].data.folders;

		        dataObj.totalFileCount = 0;

		        dataObj.vaultFolders.map(function (item) {
		            dataObj.totalFileCount += item.totalFilesCount;
		        });

		        dataObj.vaultFiles = [];			       
		    }
		    else if (results[1].status == "success") {

		        dataObj.totalFileCount = 0;
		        dataObj.vaultFiles = results[1].data.userFiles;			        
		    }                
		    return dataObj;
		},
		
		fetchFolderData : function(userId,folderName){
			var defer = $q.defer();
            OranjApiService('getUserVaultFolderData', {}, {
            	userId: userId,
				shared: true,
				folderName: folderName,
            }).then(function (res) {
            	var dataObj = res;                        
                    defer.resolve(dataObj);
                    
                },
                function () {

                });

            return defer.promise;
		},
		
		fetchVaultSearchList: function (userId,searchString) {
		    var defer = $q.defer();
		    function onSuccess(res) {
		        defer.resolve(res);
		    }

		    function onError(error) {
		        defer.reject(error);
		    }

		    OranjApiService('getUserVaultSearchList', {
		    	data: {
                    "searchString": searchString,
                }
		    }, {
		    	userId: userId,
		    }).then(onSuccess, onError);
		   
		    return defer.promise;
		}
            
        };
    }
})();
