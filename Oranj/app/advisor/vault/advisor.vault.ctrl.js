(function () {
    'use strict';

    angular.module('app.advisor.vaults')
        .controller('AdvisorVaultCtrl', ['advisorVaultDataService', '$scope', '$state', '$rootScope', 'advisorUserVaultDataService', function (advisorVaultDataService, $scope, $state, $rootScope, advisorUserVaultDataService) {

            advisorVaultDataService.fetchAllClientList().then(function(clientListObject) {
                $scope.isLoading = false;
                $scope.advisorGridData = clientListObject;
                var data = clientListObject;
                var contactCount = {'clientCount': 0, 'prospectsCount': 0};
                if (angular.isArray(data)) {

                    data.sort(function (a, b) {
                        if (a.lastName.toLowerCase() < b.lastName.toLowerCase()) {
                            return -1;
                        }
                        if (a.lastName.toLowerCase() > b.lastName.toLowerCase()) {
                            return 1;
                        }
                        return 0;
                    });
                    var defaultClient = '';
                    var userInfo = advisorUserVaultDataService.getUserInfo();
                    if(userInfo.userId){
                    	//default data already there
                    	defaultClient = userInfo.userId;
                    }else{
                    	$rootScope.$broadcast("clientVaultId", {
                            item: data[0],
                        });
                    }
                    
                    
                    var tmp = {};
                    var clientTemp = {};
                    var prospectTemp = {};
                    
                    for (var i = 0; i < data.length; i++) {
                        $scope.advisorGridData[i].firstNameIndx = data[i].firstName[0].toUpperCase();
                        $scope.advisorGridData[i].lastNameIndx = data[i].lastName[0].toUpperCase();

                        var letter = data[i].lastName.toUpperCase().charAt(0);
                        if (tmp[letter] === undefined) {
                            tmp[letter] = [];
                        }
                        if (clientTemp[letter] === undefined) {
                        	clientTemp[letter] = [];
                        }
                        if (prospectTemp[letter] === undefined) {
                        	prospectTemp[letter] = [];
                        }
                        tmp[letter].push(data[i]);
                        if (angular.isArray(data[i].roles) && data[i].roles.length > 0) {
                            if (data[i].roles[0].name === 'ROLE_CLIENT') {
                            	if(defaultClient === ''){
                            		defaultClient = data[i].userId;
                            	}
                                contactCount.clientCount = contactCount.clientCount + 1;
                                clientTemp[letter].push(data[i]);
                            }
                            else if (data[i].roles[0].name === 'ROLE_PROSPECT') {
                                contactCount.prospectsCount = contactCount.prospectsCount + 1;
                                prospectTemp[letter].push(data[i]);
                            }
                        }
                    }
                    clientListObject.sort(function (a, b) {
                        if (a.firstName.toLowerCase() < b.firstName.toLowerCase()) {
                            return -1;
                        }
                        if (a.firstName.toLowerCase() > b.firstName.toLowerCase()) {
                            return 1;
                        }
                        return 0;
                    });
                    $scope.TotalContact = contactCount.clientCount + contactCount.prospectsCount;
                    $rootScope.$broadcast("vaultContactsList", {
                        contactsList: tmp,
                        contactCount,
                        allContactsList: clientListObject,
                        clientList:clientTemp,
                        prospectList:prospectTemp,
                        defaultClient:defaultClient
                    });
                }
            },function(){
            	$scope.isLoading = false;
                GlobalFactory.showErrorAlert(error.message || 'An error occurred');
            });
           
            //console.log('inside vault advisor');

        }]);
})();
