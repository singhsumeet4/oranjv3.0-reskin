(function () {
	angular.module('app.advisor.vaults')
	.service('advisorUserVaultDataService',[function() {
		
		  var userInfo = {};
		  
		  var getUserInfo = function(){
		      return userInfo;
		  };

		  var setUserInfo = function(data){
			  userInfo = data;
		  };
		  
		  return {
			  getUserInfo: getUserInfo,
			  setUserInfo: setUserInfo,
		  };
	}]);

})();