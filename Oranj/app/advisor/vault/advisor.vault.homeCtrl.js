(function () {
    'use strict';

    angular.module('app.advisor.vaults')
   .controller('AdvisorVaultHomeCtrl', ['$scope', '$state', '$rootScope', '$cookies', 'GlobalFactory', '$http', '$mdDialog', '$mdMedia', 'OranjApiService', '$window', '$q','$stateParams', 'advisorVaultDataService', 'advisorUserVaultDataService', AdvisorVaultHomeCtrl]);

    function AdvisorVaultHomeCtrl($scope, $state, $rootScope, $cookies, GlobalFactory, $http, $mdDialog, $mdMedia, OranjApiService, $window, $q,$stateParams, advisorVaultDataService, advisorUserVaultDataService) {

        $scope.showCreateFolderDiv = false;
        $scope.isChatWindowVisible = true;
        $scope.isPdfHeaderVisible = false;
        $scope.global = GlobalFactory;
        $scope.vaultHeader = 'My Vault';
        $scope.totalCount  = 0;
        $scope.global.vaultsFolderName = "";
        $scope.global.sharedData.isRootFolder = true;
        $scope.userData = advisorUserVaultDataService.getUserInfo();
        var vm = this;
        var role = GlobalFactory.getRole();

        role.then(
  	            function (result) {
  	                if (result.data) {
  	                    if (GlobalFactory.lookupRole('ROLE_USER', result.data)) {

  	                    } else if (GlobalFactory.lookupRole('ROLE_PROSPECT', result.data) || GlobalFactory.lookupRole('ROLE_CLIENT', result.data)) {

  	                    } else if (GlobalFactory.lookupRole('ROLE_ADVISOR', result.data)) {

  	                    } else {
  	                        GlobalFactory.signout();
  	                    }
  	                } else {
  	                    GlobalFactory.signout();
  	                }
  	            }, function () {
  	                GlobalFactory.signout();
  	            }
  	   	);
        
            $scope.busy = true;
            $scope.hideloader=false;
        
        
        $scope.$on('clientVaultId', function (event, val) {   
        	advisorUserVaultDataService.setUserInfo(val.item);
            $scope.userData = advisorUserVaultDataService.getUserInfo();
        	if ($state.current.name === 'page.advisor.vault-home.search') {
                var searchString = $stateParams.searchString;
                $scope.searchKeyword = searchString,
                $scope.searchUserVault(searchString);
            }
             else if ($state.current.name === 'page.advisor.vault-home.folder') {

                 if (!$stateParams.hasOwnProperty('folderName')) {
                 	if($scope.userData.userId){
                 		$scope.initVault($scope.userData.userId,$scope.userData);
                 	}
                 }
                 else {
                     $scope.vaultFiles = [];
                     $scope.global.fileCount = 0;
                     $scope.totalCount = 0;               
                     vm.FolderName = $stateParams.folderName;
                     $scope.getFolderData($stateParams.folderName);
                 }
             }else{
            	 $scope.initVault(val.item.userId,val.item);
             }
        });

        $scope.initVault = function (userId,userInfo) {
        	$scope.busy = true;
            $scope.hideloader=false;
            $scope.vaultHeader = userInfo.fullName+" Vault" || "My Vault";
            $scope.openFolderStat = "vault";
            $scope.global.vaultsFolderName = "";  
            $scope.folderCount = 0;
            $scope.global.fileCount = 0;
            $scope.isRecentActive = false;
            $scope.isVaultActive = true;
            $scope.isStarredActive = false;
            $scope.global.sharedData.isRootFolder = true;

            advisorVaultDataService.fetchVaultRootList(userId).then(function (res) {
            	$scope.busy = false;
                $scope.hideloader=true;
                var data = advisorVaultDataService.prepareVaultRootData(res);
                $scope.totalCount = data.folderCount + data.vaultFiles.length;
                $scope.vaultFolders = data.vaultFolders;
                $scope.vaultFiles = data.vaultFiles;
                var sideVaultList = data.sideVaultList;

	        }, function (error) {
	
	        });
        };

        $scope.assignFileData = function () {
            $scope.filePath = $scope.filePath;
            $scope.fileType = $scope.fileType;
        };

        $scope.files = [];
      
        $scope.isChatWindowVisible = false;
        $scope.showChatWindow = function () {
            $scope.isChatWindowVisible = true;
        };

        $scope.hideChatWindow = function () {
            $scope.isChatWindowVisible = false;
        };

        $scope.getFolderName = function (name) {
            GlobalFactory.vaultsFolderName = name;
        }

        $scope.getFolderData = function (folder) {          
            $scope.openFolderStat = folder;
            $scope.global.vaultsFolderName = folder;
            $scope.global.sharedData.isRootFolder = false;
            $scope.busy = true;
            $scope.hideloader=false;
            if($scope.userData.userId){
            	var userId = $scope.userData.userId;
                $scope.vaultFolders = [];            
                advisorVaultDataService.fetchFolderData(userId,folder).then(function (response) {
                	$scope.busy = false;
                    $scope.hideloader=true;
                    if (response.status == 'success') {
                        $scope.vaultFiles = response.data.userFiles;
                        $scope.totalCount = response.data.userFiles.length;
                    }
                    else {
                        $scope.vaultFiles = [];
                        $scope.totalCount = 0;
                    }
                }, function (response) {

                });
                $scope.vaultHeader = $scope.userData.fullName+" Vault / "+folder || "My Vault / "+folder;
            }else{
            	$state.go('page.advisor.vault-home.dashboard');
            }
            
        };

        $scope.deleteFolder = function (folder) {

        	var confirmation = $mdDialog.confirm({
                title: 'Are you sure?',
                htmlContent: '<div> This folder <b>'+folder+'</b> will be deleted immediately.',
                ok: 'Delete',
                cancel : 'Cancel'
            });
        	
        	$mdDialog.show(confirmation).then(function(answer) {
	            OranjApiService('vaultDeleteUserFolder', {
	                data: {
	                    "folder": folder,
	                }
	            }, {
	            	'userId': $scope.userData.userId
	            }
	 			).then(function (response) {
	 				
	 				if($state.current.name === 'page.advisor.vault-home.folder') {
	 					$scope.getFolderData($scope.openFolderStat);
	 				}else if($state.current.name === 'page.advisor.vault-home.search' && $stateParams.searchString){
	 					$scope.searchUserVault($stateParams.searchString);
	 				}else{
	 			        $scope.initVault($scope.userData.userId,$scope.userData);
	 			    }
	 			}, function (response) {
	
	 			});
        	});

        };

        $scope.deleteFile = function (id,name) {

        	var confirmation = $mdDialog.confirm({
                title: 'Are you sure?',
                htmlContent: '<div> This file <b>'+name+'</b> will be deleted immediately.',
                ok: 'Delete',
                cancel : 'Cancel'
            });
        	
        	$mdDialog.show(confirmation).then(function(answer) {
	            OranjApiService('vaultDeleteUserFile', null, {
	                'id': id,
	                'userId': $scope.userData.userId
	            }
	 			).then(function (response) {
	 				if($state.current.name === 'page.advisor.vault-home.folder') {
	 					$scope.getFolderData($scope.openFolderStat);
	 				}else if($state.current.name === 'page.advisor.vault-home.search' && $stateParams.searchString){
	 					$scope.searchUserVault($stateParams.searchString);
	 				}else{
	 			        $scope.initVault($scope.userData.userId,$scope.userData);
	 			    }
	 			}, function (response) {
	
	 			});
        	});

        };

        $scope.toggleChatWindow = function () {
            if ($scope.isChatWindowVisible)
                $scope.isChatWindowVisible = false;
            else
                $scope.isChatWindowVisible = true;
        };

        $scope.showAdvanced = function (ev, fileUrl, fileName, fileDate, fileSize, fileId) {
        	
        	$scope.global.vaultsfilePath = '';
        	$scope.global.vaultsName = '';
        	OranjApiService('getUserVaultFilePath', null, {
                'id': fileId,
                'userId': $scope.userData.userId
            }
            ).then(function (response) {
                if (response.status == 'success') {
                	 $scope.filePath = response.data.vaultFile;
                	 $scope.global.vaultsfilePath= $scope.filePath;
                	 $scope.global.vaultsName = fileName;
                	 
                	 var fileArray = fileUrl.split('.');
                     
                     
                     $scope.global.vaultsfileType=  "text/" + fileArray[fileArray.length - 1];            
                     $scope.fileType = "text/" + fileArray[fileArray.length - 1];
                     $scope.filePath1 = $scope.filePath;
                     $scope.fileName = fileName;
                     $scope.fileDate = fileDate;
                     $scope.fileSize = fileSize;
                     $scope.fileId   = fileId;
                     $scope.global.vaultsfileId=fileId;
                     $scope.isChatWindowVisible = true;

                     var filetypelist = ["jpg", "jpeg", "png", "gif", "pdf", "bmp"];
                     var typeindex = filetypelist.indexOf(fileArray[fileArray.length - 1]);
                     if (typeindex >= 0) {

                         var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
                         $scope.isPdfHeaderVisible = true;

                         $mdDialog.show({
                             templateUrl: 'dialog1.tmpl.html',
                             parent: angular.element(document.body),
                             targetEvent: ev,
                             controller: 'AdvisorVaultHomeCtrl',                   
                             //clickOutsideToClose:true,
                             fullscreen: useFullScreen,
                             preserveScope: true,
                             onComplete: function () {

                             }
                         })
         			    .then(function (answer) {
         			        console.log('test');
         			        $scope.status = 'You said the information was "' + answer + '".';
         			    }, function () {
         			        $scope.status = 'You cancelled the dialog.';
         			    });
                     }
                     else {
                         var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
                         $scope.isPdfHeaderVisible = true;

                         $mdDialog.show({
                             templateUrl: 'dialog2.tmpl.html',
                             parent: angular.element(document.body),
                             targetEvent: ev,
                             controller: 'AdvisorVaultHomeCtrl',                    
                             //clickOutsideToClose:true,
                             fullscreen: useFullScreen,
                             preserveScope: true,
                             onComplete: function () {

                             }
                         })
                     }
                }
                else {
                   
                }
            }, function (response) {

            });
        	
            
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };

            $scope.$watch(function () {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function (wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        $scope.cancel = function () {
            $scope.isPdfHeaderVisible = false;
            $mdDialog.cancel();
        };

        
        $scope.downloadFromList = function (e, item) {
        	OranjApiService('getUserVaultFilePath', null, {
                'id': item.id,
                'userId': $scope.userData.userId
            }
            ).then(function (response) {
                if (response.status == 'success') {
                	 $scope.filePath = response.data.vaultFile;
                	 $scope.global.vaultsfilePath= $scope.filePath;
                	 $scope.global.vaultsName = item.name;
                	 $scope.fileName = item.name;
                     $scope.downloadfile(e,item.id,$scope.fileName,$scope.global.vaultsfilePath);
                }
                else {
                   
                }
            }, function (response) {

            });
            
           
        }

        $scope.downloadfile = function (e, id, fileName, filePath) {
            e = e || window.event;
            e.preventDefault();
            var fileURL = filePath;
            var fileName = fileName;
            if (!window.ActiveXObject) {
                var save = document.createElement('a');
                save.href = fileURL;
                save.target = '_blank';
                save.download = fileName || 'unknown';

                try {
                    var evt = new MouseEvent('click', {
                        'view': window,
                        'bubbles': true,
                        'cancelable': false
                    });
                    save.dispatchEvent(evt);
                    (window.URL || window.webkitURL).revokeObjectURL(save.href);
                } catch (e) {
                    console.log('catch');
                    window.open(fileURL, fileName);
                }
            }

                // for IE < 11
            else if (!!window.ActiveXObject && document.execCommand) {
                var _window = window.open(fileURL, '_blank');
                _window.document.close();
                _window.document.execCommand('SaveAs', true, fileName || fileURL)
                _window.close();
            }
            $scope.increaseCount(id);
        };
        
        $scope.increaseCount = function (id) {
        	OranjApiService('downloadUserFrequency', null, {
                'id': id,
                'userId': $scope.userData.userId
            }).then(function (response) {
                
            }, function (response) {

            });
        };

        $scope.showHideDiv = function () {

            if ($scope.showCreateFolderDiv == true) {
                $scope.showCreateFolderDiv = false;
            } else {
                $scope.showCreateFolderDiv = true;
            }

            if ($scope.showCreateFolderDiv == undefined) {
                $scope.showCreateFolderDiv = true;
            }
            if ($scope.showCreateFolderDiv == true) {
                $scope.add = !$scope.add;

                setTimeout(function () {
                    var element = $window.document.getElementById('newName');
                    if (element)
                        element.focus();
                }, 0);
            }
        };

        $scope.createFolder = function () {
        	myDropzone.disable();
        	if($scope.newName != null){
                OranjApiService('vaultCreateUserFolder', {
                    data: {
                        "folder": $scope.newName,
                        "shared": true,
                        "type": "folder"
                    }
                }, {
                	'userId': $scope.userData.userId
                }).then(function (response) {
                	myDropzone.enable();
                    $scope.newName = null;
                    if (response.status == "success") {
                        $scope.showCreateFolderDiv = false;
                        $scope.initVault($scope.userData.userId,$scope.userData);
                    } else {
                    	if(response.message){
                    		GlobalFactory.showErrorAlert(response.message);
                    	}else{
                    		GlobalFactory.showErrorAlert("Unabled to create Folder.");
                    	}
                        $scope.showCreateFolderDiv = false;
                    }

                }, function (response) {
                	myDropzone.enable();
                });
        	}else{
        		GlobalFactory.showErrorAlert("Folder Name is required. Please try again.");
        		$scope.showCreateFolderDiv = false;
        		setTimeout(function () {
        			myDropzone.enable();
                }, 100);
        	}
        };


        //For sorting
        $scope.propertyFolder = 'folder';
        $scope.propertyName = 'name';
        $scope.reverse = false;

        $scope.sortBy = function (propertyFolder, propertyName) {
            $scope.reverse = ($scope.propertyFolder === propertyFolder) ? !$scope.reverse : false;
            $scope.propertyFolder = propertyFolder;
            $scope.propertyName = propertyName;
        };

        $scope.sortByDynamic = function (propertyFolder, propertyName) {
            var flag = 0;

            if ($scope.vaultFolders != undefined && $scope.vaultFolders.length > 0) {
                flag = flag + 1;
            }
            if ($scope.vaultFiles != undefined && $scope.vaultFiles.length > 0) {
                flag = flag + 1;
            }
            if (flag == 1) {
                $scope.reverse = ($scope.propertyFolder === propertyFolder) ? !$scope.reverse : false;
                $scope.propertyFolder = propertyFolder;
                $scope.propertyName = propertyName;
            }
        };

        $scope.vaultUpload = function (elem, event) {

            var folderName = "";
            var vaultUploadUrl = 'vaultUploadRoot';                 

            if ($scope.global.vaultsFolderName != "") {
                folderName = $scope.global.vaultsFolderName;
                vaultUploadUrl = 'vaultUploadUserFolder';
            } else {
                folderName = "";
                vaultUploadUrl = 'vaultUploadUserRoot';
            }

            var files = elem.files;
            fileUploadHelper(files, vaultUploadUrl, folderName, 0, function (totalUploadedFiles) {
                if (!angular.isUndefinedOrNull(totalUploadedFiles)) {
                    if (files.length == totalUploadedFiles) {
                        elem.value = null;
                        $scope.global.showSuccessUploadFile();
                        if ($scope.openFolderStat == "vault") {
                            $scope.initVault($scope.userData.userId,$scope.userData);
                        }
                        else {
                            $scope.getFolderData($scope.openFolderStat);
                        }
                    }
                }
            });
        }                 

        function validateFileSize(file) {
            var sizeInMb = (file.size / (1024 * 1024));

            if (sizeInMb > 10)
                return false;
            else
                return true;
        };

        function validateFileExtension(filename) {
            var extn = filename.split(".").pop().toLowerCase();
            var INVALID_VALID_FILE_EXTNS = ['bin', 'exe'];

            if (INVALID_VALID_FILE_EXTNS.indexOf(extn) > -1)
                return false;
            else
                return true;
        };
                
        function fileUploadHelper(files, vaultUploadUrl, folderName, index, cb) {
            var file = files[index];
            if (angular.isUndefinedOrNull(file)) return;

            var formData = new FormData();
            formData.append('fileMult', file);

            if (!validateFileExtension(file.name)) {
                GlobalFactory.showErrorAlert("You can't upload files of this type.");
                return cb();
            }

            if (!validateFileSize(file)) {
                GlobalFactory.showErrorAlert('Maximum Upload size is 10 MB.');
                return cb();
            }              

            function success(res) {
                if (res.status == 'success') {
                    index++;
                    if (files.length == index) {
                        return cb(index)
                    }
                    else {
                        fileUploadHelper(files, vaultUploadUrl, folderName, index, cb)
                    }
                }
                else {
                    GlobalFactory.showErrorAlert(res.message || 'An error occurred with the file upload');
                    return cb();
                }
            }

            function failed(error) {
                GlobalFactory.showErrorAlert(error.message || 'An error occurred with the file upload');
                return cb();
            }         

            var headers = {'Content-Type': undefined }
            OranjApiService(vaultUploadUrl, {
                data: formData,
                transformRequest: angular.identity,
                headers: headers
            }, { 
            	'userId': $scope.userData.userId,
            	'folder': folderName 
            }).then(function (response) {
                success(response);
            }, function (response) {
                failed(response);
            });
        };

        $scope.$on('vaultAdvisorClick', function (event, val) {  
        	if ($scope.userData.userId) {
	        	if (val.vaultName == 'newFolder') {
	            		$scope.showHideDiv();
	            }
	            else if (val.vaultName == 'vaultupload') {                
	                setTimeout(function () {
	                    angular.element('#upload').trigger('click');
	                }, 0);
	            }
        	} else {
            	GlobalFactory.showErrorAlert("Select a contact first.")
            }
        });   
        $scope.searchUserVault = function(searchKey){
        		if (!angular.isUndefinedOrNull(searchKey)){
        			if($scope.userData.userId){
        				$scope.busy = true;
        	            $scope.hideloader=false;
        				$scope.vaultHeader = "Search";
        	            $scope.openFolderStat = "search";
        				if ($state.current.name === 'page.advisor.vault-home.search') {
        					advisorVaultDataService.fetchVaultSearchList($scope.userData.userId,searchKey).then(function (results) {
        						$scope.busy = false;
             	            	$scope.hideloader=true;
        					   $scope.vaultFolders = [];
             	               $scope.vaultFiles = [];
             	               if (results.status === "success") {
             	                   $scope.vaultFolders = results.data.folders;
             	                   $scope.vaultFiles = results.data.files;
             	                   $scope.totalCount = $scope.vaultFiles.length + $scope.vaultFolders.length;
             	               }
             	               else {                  
             	                   $scope.totalCount = 0;
             	               }
             	            }, function (error) {
             	            	$scope.busy = false;
             	            	$scope.hideloader=true;
             	                $scope.vaultFolders = [];
             	                $scope.vaultFiles = [];
             	                $scope.totalCount = 0;
             	            });
        				}else{
        					$state.go('page.advisor.vault-home.search', { searchString: searchKey });
        				}
        			
        			}
            	}else{
            		if($scope.userData.userId){
            			$scope.initVault($scope.userData.userId,$scope.userData);
            		}
            	}
        }

       if ($state.current.name === 'page.advisor.vault-home.dashboard') {
        	if($scope.userData.userId){
        		$scope.initVault($scope.userData.userId,$scope.userData);
        	}
        }
       else if ($state.current.name === 'page.advisor.vault-home.search') {
           var searchString = $stateParams.searchString;
           $scope.searchKeyword = searchString,
           $scope.searchUserVault(searchString);
       }
        else if ($state.current.name === 'page.advisor.vault-home.folder') {

            if (!$stateParams.hasOwnProperty('folderName')) {
            	if($scope.userData.userId){
            		$scope.initVault($scope.userData.userId,$scope.userData);
            	}
            }
            else {
                $scope.vaultFiles = [];
                $scope.global.fileCount = 0;
                $scope.totalCount = 0;               
                vm.FolderName = $stateParams.folderName;
                $scope.getFolderData($stateParams.folderName);
            }
        }        
    };

})();


