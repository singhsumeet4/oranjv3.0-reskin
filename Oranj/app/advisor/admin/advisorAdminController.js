(function () {
    'use strict';

    angular.module('app.advisor')

        .controller("advisorController", ['$scope', function ($scope) {


            $scope.advisorGridData = [
                {
                    firstName: "Donna",
                    lastName: "Langford",
                    title: "Advisor",
                    email: "dlang@mfirm.com",
                    phone: "312-878-1100"
                },
                {
                    firstName: "Kristopher",
                    lastName: "Schnell",
                    title: "CFP",
                    email: "Kschnell@cogentadvisors.com",
                    phone: "310-225-0009"
                },
                {
                    firstName: "Emily Lurene",
                    lastName: "Hartnell",
                    title: "RIA",
                    email: "Ihartnell@runoranj.com",
                    phone: "773-321-0984"
                },
                {
                    firstName: "Thomas",
                    lastName: "Fox",
                    title: "RIA",
                    email: "tfox@runoranj.com",
                    phone: "865-736-9945"
                },
                {
                    firstName: "John Christian",
                    lastName: "McKinley",
                    title: "Advisor",
                    email: "jc@cogentadvisors.com",
                    phone: "227-098-4462"
                }

            ];


        }]);

})();
