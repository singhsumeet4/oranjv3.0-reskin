(function () {
  'use strict';

  angular.module('app.portfolio', [])
    .controller('matchedPortfolioController', ['$scope', '$mdDialog', '$mdMedia', '$filter', matchedPortfolioController])


  function matchedPortfolioController($scope, $mdDialog, $mdMedia, $filter) {


    // matchedPortfolio doughnut chart start

    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
    $scope.cancel = function () {
      $mdDialog.cancel();
    };

    $scope.percentages = percentages;

    $scope.activebox = true;

    $scope.setPercentage = function (ev) {
      $scope.percentages = undefined;
      $scope.edit = !$scope.edit;
      $scope.percentages = percentages;

    }


    $scope.showDelete = function (ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/advisor/admin/delete-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        //   fullscreen: useFullScreen
      })
        .then(function (answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
          $scope.status = 'You cancelled the dialog.';
        });
      $scope.$watch(function () {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function (wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === false);
      });
    }


    $scope.showMemo = function (ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/advisor/admin/dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        //   fullscreen: useFullScreen
      })
        .then(function (answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
          $scope.status = 'You cancelled the dialog.';
        });
      $scope.$watch(function () {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function (wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === false);
      });
    }


    $scope.Tolerance = {}
    //$scope.Tolerance.PortfolioName="Portfolio Awesome";
    $scope.Tolerance.memo = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old";
    $scope.coverageTypeData = ('Very Conservative').split(',').map(function (Risk) {
      return { abbrev: Risk };
    })
    $scope.Tolerance.Risk = 'Very Conservative';



    Chart.defaults.global.responsive = true;
    $scope.assetPercentages = {};



    // sliders start

    var percentages = {};
    percentages.low1 = 6;
    percentages.low2 = 11;
    percentages.low3 = 28;
    percentages.low4 = 19;
    percentages.low5 = 7;
    percentages.low6 = 21;
    percentages.low7 = 2;
    percentages.low8 = 6;


    //Asset Percentage initialization value
    $scope.assetPercentages = angular.copy(percentages);

    // savePercentages
    $scope.activeUser = "Active";
    $scope.activeUserColor = 'activeUser';
    $scope.PortfolioName = "Smith Financial Very Conservative";
    $scope.PortfolioNameHeading = "Smith Financial Very Conservative";
    $scope.savePercentages = function (percentages) {

      $scope.assetPercentages = angular.copy(percentages);
      $scope.PortfolioNameHeading = angular.copy($scope.PortfolioName);
      percentages = angular.copy(percentages);
      assetAllocationChart();

      if (percentages.active === true) {
        $scope.activeUser = "Active";

        $scope.activeUserColor = 'activeUser';
      }
      else {
        $scope.activeUser = "Active";
        $scope.activeUserColor = 'activeUser';

      }

    };
    //portfolio name

    $scope.PortfolioName = "Smith Financial Very Conservative";
    function getDonutData() {
      var doughnutData = [

        {
          label: 'US Stock',
          value: percentages.low1,
          name: 'US Stock',
          color: '#fdce0b',

        },

        {
          label: 'Non US Stock',
          value: percentages.low2,
          name: 'Non US Stock',
          color: '#a3cd39',

        },

        {
          label: 'US Bond',
          value: percentages.low3,
          name: 'US Bond',
          color: '#3eb649',

        },

        {
          label: 'Non US Bond',
          value: percentages.low4,
          name: 'Non US Bond',
          color: '#23c1e2',

        },

        {
          label: 'Preferred',
          value: percentages.low5,
          name: 'Preferred',
          color: '#2e4ea1',

        },

        {
          label: 'Convertible',
          value: percentages.low6,
          name: 'Convertible',
          color: '#000285',

        },
        {
          label: 'Cash',
          value: percentages.low7,
          name: 'Cash',
          color: '#fdb45c',

        },

        {
          label: 'Other',
          value: percentages.low8,
          name: 'Other',
          color: '#f7464a',

        }
      ];
      // console.log(doughnutData);

      return doughnutData;
    }

    // var recommendedPortfolioTotal = 0.0;
    //   getDonutData().forEach(function (element) {
    //       recommendedPortfolioTotal = recommendedPortfolioTotal + element.value;
    //   }, this);


    var doughnutOptions = {
      tooltipTemplate: function (value) { return value.label + ': ' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%'; },
      //  tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' (' + $filter('currency')((value.value / recommendedPortfolioTotal) * 100, '', 2) + '%)' },
      percentageInnerCutout: 80,
      segmentShowStroke: false,
      responsive: true
      // onAnimationComplete: innerTextFunction

    };

    function assetAllocationChart() {
      var element = document.getElementById("matchedPortfolioDoughnut"); // notice the change
      if (element != undefined)
        element.parentNode.removeChild(element);

      var canvas = document.createElement("CANVAS");
      canvas.id = "matchedPortfolioDoughnut";
      canvas.className="canvas-matchedPortfolio-circle";
      var ctxStep4 = canvas.getContext("2d");
      var canvasDiv = document.getElementById('canvasDiv');
      canvasDiv.appendChild(canvas);


      var myDoughnut = new Chart(ctxStep4).Doughnut(getDonutData(), doughnutOptions);
      document.getElementById('matchedPortfolio-legend').innerHTML = myDoughnut.generateLegend(); // for legend 

    }




    //function loading on run time
    assetAllocationChart();
    //________________________________________

    // slider 1 start
    var formatToPercentage1 = function (value) {
      return value + '%';
    };

    percentages.options1 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 6,
      translate: formatToPercentage1,
      hidePointerLabels: true,
      showSelectionBar: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 1 end


    // slider 2 start
    var formatToPercentage2 = function (value) {
      return value + '%';
    };

    percentages.options2 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 1,
      translate: formatToPercentage2,
      hidePointerLabels: true,
      showSelectionBar: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 2 end


    // slider 3 start
    var formatToPercentage3 = function (value) {
      return value + '%';
    };

    percentages.options3 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 1,
      translate: formatToPercentage3,
      hidePointerLabels: true,
      showSelectionBar: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 3 end


    // slider 4 start
    var formatToPercentage4 = function (value) {
      return value + '%';
    };

    percentages.options4 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 1,
      translate: formatToPercentage4,
      hidePointerLabels: true,
      showSelectionBar: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 4 end


    // slider 5 start
    var formatToPercentage5 = function (value) {
      return value + '%';
    };

    percentages.options5 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 1,
      translate: formatToPercentage5,
      hidePointerLabels: true,
      showSelectionBar: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 5 end



    // slider 6 start
    var formatToPercentage6 = function (value) {
      return value + '%';
    };

    percentages.options6 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 1,
      translate: formatToPercentage6,
      hidePointerLabels: true,
      showSelectionBar: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 6 end



    // slider 7 start
    var formatToPercentage7 = function (value) {
      return value + '%';
    };

    percentages.options7 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 1,
      translate: formatToPercentage7,
      showSelectionBar: true,
      hidePointerLabels: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 7 end


    // slider 8 start
    var formatToPercentage8 = function (value) {
      return value + '%';
    };

    percentages.options8 = {
      floor: 0,
      ceil: 100,
      step: 1,
      // precision: 1,
      translate: formatToPercentage8,
      hidePointerLabels: true,
      showSelectionBar: true,
      hideLimitLabels: true,
      getSelectionBarColor: function (value) { return '#999'; },
      getPointerColor: function (value) { return '#999'; },

    };
    // slider 8 end
    $scope.percentages = percentages;

    $scope.Memo = {
      name: 'Lorem ipsum dolor sit amet',
    }

  }
})();

function DialogController($scope, $mdDialog) {
  $scope.hide = function () {
    $mdDialog.hide();
  };
  $scope.cancel = function () {
    $mdDialog.cancel();
  };
  $scope.answer = function (answer) {
    $mdDialog.hide(answer);
  };
}