(function () {
    'use strict';

    angular.module('app.questionnaire_admin', [ ])
        .controller('questionnaireCtrl', ['$scope', 'Questionaries', questionnaireCtrl]);

    function questionnaireCtrl($scope, Questionaries) {

        $scope.addDymCLass = function (params) {
            return 'qradgroup' + params;
        };

        // for save data locally start
        $scope.questions = Questionaries.questions;

        $scope.save = function () {
            $log.debug($scope.questions);
            console.log($scope.questions);
            // window.localStorage.set('selectedQues', [$scope.questions]);
            window.sessionStorage.setItem("savselectedQuesed", JSON.stringify($scope.questions));
        };

        $scope.selectedQuestion = JSON.parse(window.sessionStorage.getItem("savselectedQuesed"));

    }

})();
