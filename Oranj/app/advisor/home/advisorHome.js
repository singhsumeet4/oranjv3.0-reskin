(function () {
    'use strict';

    angular.module('app.advisor.home', [])
        .controller('advisorHomeCtrl', ['$scope', '$timeout', 'linechartObjArray', '$rootScope', '$filter', advisorHomeCtrl])

    function advisorHomeCtrl($scope, $timeout, linechartObjArray, $rootScope, $filter) {


        //  asset allocation chart code start

        // Advisor doughnut Under Management chart start

        Chart.defaults.global.responsive = true;

        var advisorDoughnutDataUnderManagement = [

            {
                label: 'US Stock',
                value: 31540000,
                name: 'US Stock',
                color: '#fdce0b',

            },
            {
                label: 'Non US Stock',
                value: 9130000,
                name: 'Non US Stock',
                color: '#a3cd39',

            },
            {
                label: 'US Bond',
                value: 38180000,
                name: 'US Bond',
                color: '#3eb649',

            },
            {
                label: 'Non US Bond',
                value: 830000,
                name: 'Non US Bond',
                color: '#23c1e2',

            },
            {
                label: 'Preferred ',
                value: 0,
                name: 'Cash',
                color: '#2e4ea1',

            },
            {
                label: 'Cash',
                value: 3320000,
                name: 'Cash',
                color: '#fdb45c',

            },
            {
                label: 'Other ',
                value: 0,
                name: 'Cash',
                color: '#f7464a',

            },

        ];

        var TotalUnderManagement = 0.0;
        advisorDoughnutDataUnderManagement.forEach(function (element) {
            TotalUnderManagement = TotalUnderManagement + element.value;
        }, this);

        var advisorDoughnutOptionsUnderManagement = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' (' + $filter('currency')((value.value / TotalUnderManagement) * 100, '', 2) + '%)' },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,
            scaleShowLabels: true,
            scaleOverlay: true,
            // scaleFontColor : "#666",

        }

        var ctx2 = document.getElementById("advisorDoughnutUnderManagement").getContext("2d");

        var myDoughnut2 = new Chart(ctx2).Doughnut(advisorDoughnutDataUnderManagement, advisorDoughnutOptionsUnderManagement);

        document.getElementById('advisor-legendUnderManagement').innerHTML = myDoughnut2.generateLegend(); // for legend 

        // $scope.midValueUnderManagement = assetAllocationTotal(advisorDoughnutDataUnderManagement, 'value');
        $scope.midValueUnderManagement = TotalUnderManagement.toString().replace('000000', "");


        // Advisor doughnut Under Management chart end


        // Advisor doughnut  Held Away chart start


        Chart.defaults.global.responsive = true;

        var advisorDoughnutDataHeldAway = [

            {
                label: 'US Stock',
                value: 113160000,
                name: 'US Stock',
                color: '#fdce0b',

            },
            {
                label: 'Non US Stock',
                value: 14760000,
                name: 'Non US Stock',
                color: '#a3cd39',

            },
            {
                label: 'US Bond',
                value: 68880000,
                name: 'US Bond',
                color: '#3eb649',

            },
            {
                label: 'Non US Bond',
                value: 9840000,
                name: 'Non US Bond',
                color: '#23c1e2',

            },
            {
                label: 'Preferred ',
                value: 0,
                name: 'Cash',
                color: '#2e4ea1',

            },
            {
                label: 'Cash',
                value: 9840000,
                name: 'Cash',
                color: '#fdb45c',

            },
            {
                label: 'Other ',
                value: 29520000,
                name: 'Cash',
                color: '#f7464a',

            },

        ];

        var TotalHeldAway = 0.0;
        advisorDoughnutDataHeldAway.forEach(function (element) {
            TotalHeldAway = TotalHeldAway + element.value;
        }, this);

        var advisorDoughnutOptionsHeldAway = {
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' (' + $filter('currency')((value.value / TotalHeldAway) * 100, '', 2) + '%)' },
            percentageInnerCutout: 80,
            segmentShowStroke: false,
            responsive: true,
            scaleShowLabels: true,
            scaleOverlay: true,
            // scaleFontColor : "#666",

        }

        var ctx2 = document.getElementById("advisorDoughnutHeldAway").getContext("2d");

        var myDoughnut2 = new Chart(ctx2).Doughnut(advisorDoughnutDataHeldAway, advisorDoughnutOptionsHeldAway);

        document.getElementById('advisor-legendHeldAway').innerHTML = myDoughnut2.generateLegend(); // for legend 

        // $scope.midValueHeldAway = assetAllocationTotal(advisorDoughnutDataHeldAway, 'value');
        $scope.midValueHeldAway = TotalHeldAway.toString().replace('000000', "");

        // Advisor doughnut  Held Away chart end

        // asset allocation chart code end






        // activity feed code start

        var imagePath = 'images/g1.jpg';

        $scope.messages = [
            {
                face: imagePath,
                notes: "Leonardo DiCaprio added two new documents to his vault tagged #ChaseStatement."
            },
            {
                face: imagePath,
                notes: " Albus Dumbledore won the lottery yesterday in Illinois."
            },
            {
                face: imagePath,
                notes: "Albus Dumbledore sent you a message:'Hey David, I'd like to start planning for my new mansion. Let's talk soon.'"
            },
            {
                face: imagePath,
                notes: "Harry created a new goal to retire when he is 65 years old and another goal of buying a home next May."
            },
        ];

        // activity feed code end


        //  net worth chart code start

        $scope.date = {
            date1: 'March 21,2016',
            date2: 'April 21,2016'
        }

        $scope.showLabelsOnChart = {

            balance_2_1_2016: '900000',
            balance_3_1_2016: '1000000',
            change_performance: '100000',
            change_percentage: '11.1',
            header_balance: '1355045.00',
            totalDayChange: '11450.13'

        }

        $rootScope.breadcrum = 'Home / Investment';

        var linechartObj = {}
        linechartObj.labels = ["January 2016", "February 2016", "March 2016", "April 2016", "May 2016", "June 2016", "July 2016", "August 2016", "September 2016"];
        linechartObj.series = [''];
       
         linechartObj.data = [
            [462000, 425000, 519000, 530500, 520800, 540300, 599000, 610000, 629012],
        ];
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        linechartObj.options = {
            showScale: false,
            scaleShowGridLines: false,
            pointDot: false,
            bezierCurve: false,
            tooltipCaretSize: 0,
            tooltipCornerRadius: 0,
            tooltipTemplate: function (value) { return value.label + ': $' + value.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
            pointHitDetectionRadius: 2,
        };

        linechartObj.colours = [{
            "fillColor": "#e0f0f1",
            "strokeColor": "#36b8b7",

        }];

        linechartObjArray.reset();
        linechartObjArray.chartDataArray.push(angular.copy(linechartObj));

        linechartObj.data = [
            [99330000, 91375000, 111585000, 114057500, 111972000, 116164500, 128785000, 131150000, 135237580],
        ];
        linechartObjArray.chartDataArray.push(angular.copy(linechartObj));

        // net worth chart code end





        // goals section code start

        // engagment pie charts for "addGoal today" start
        $(function () {
            var advisorhomepietoday = [{ name: "42", y: 42 }, { name: " ", y: 58, color: "#eee" }];
            var donutChartOptions = {
                chart: {
                    backgroundColor: '#ffffff',
                    margin: [0, 0, 0, 0],
                    renderTo: 'advisorHomePieToday',
                    spacing: [0, 0, 0, 0],
                    type: 'pie'
                },
                colors: ['#36b8b7'],
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            color: 'red'
                        }
                    },
                    pie: {
                        borderWidth: 0,
                        allowPointSelect: false,
                        dataLabels: {
                            connectorWidth: 0,
                            enabled: false
                        },
                        shadow: false,
                        states: {
                            hover: {
                                enabled: false
                            }
                        },
                    },
                },
                tooltip: {
                    enabled: false
                },
                series: [{
                    data: advisorhomepietoday,
                    name: "",
                    size: 95,
                    innerSize: 85,
                    pointPadding: 0,
                    groupPadding: 0
                }],
                template: 'donut',
                title: {
                    align: 'center',
                    style: {
                        color: '#36b8b7',
                        fontFamily: 'Arial, Helvetica, sans',
                        fontSize: '15px',
                        // fontWeight: 'bold'
                    },
                    text: advisorhomepietoday[0].name + "%",
                    verticalAlign: 'middle',
                    y: 5
                }
            };

            try {
                var temp = new Highcharts.Chart(donutChartOptions);
            }
            catch (e) {

            }

        });
        // engagment pie charts for "addGoal today" end


        // engagment pie charts for "addGoal future" start
        $(function () {
            var advisorhomepiefuture = [{ name: "89", y: 89 }, { name: " ", y: 11, color: "#eee" }];
            var donutChartOptions = {
                chart: {
                    backgroundColor: '#ffffff',
                    margin: [0, 0, 0, 0],
                    renderTo: 'advisorHomePieFuture',
                    spacing: [0, 0, 0, 0],
                    type: 'pie'
                },
                colors: ['#36b8b7'],
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            color: 'red'
                        }
                    },
                    pie: {
                        borderWidth: 0,
                        allowPointSelect: false,
                        dataLabels: {
                            connectorWidth: 0,
                            enabled: false
                        },
                        shadow: false,
                        states: {
                            hover: {
                                enabled: false
                            }
                        },
                    },
                },
                tooltip: {
                    enabled: false
                },
                series: [{
                    data: advisorhomepiefuture,
                    name: "",
                    size: 95,
                    innerSize: 85,
                    pointPadding: 0,
                    groupPadding: 0
                }],
                template: 'donut',
                title: {
                    align: 'center',
                    style: {
                        color: '#36b8b7',
                        fontFamily: 'Arial, Helvetica, sans',
                        fontSize: '15px',
                        // fontWeight: 'bold'
                    },
                    text: advisorhomepiefuture[0].name + "%",
                    verticalAlign: 'middle',
                    y: 5
                }
            };

            try {
                var temp = new Highcharts.Chart(donutChartOptions);
            }
            catch (e) {

            }

        });
        // engagment pie charts for "addGoal future" end

        //  goals section code end



    }
})();