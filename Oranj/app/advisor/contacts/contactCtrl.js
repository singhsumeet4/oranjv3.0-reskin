(function () {
    'use strict';

    angular.module('app.advisor.contact')
        .controller('contactCtrl', ['$scope', '$rootScope', 'world', 'OranjApiService', 'GlobalFactory', 'advisorDataService','$timeout', contactCtrl]);


    function contactCtrl($scope, $rootScope, world, OranjApiService, GlobalFactory, advisorDataService,$timeout) {
        var role = GlobalFactory.getRoleService();
        role.then(
            function (result) {
                if (result.isAdmin || result.isAdvisor) {

                } else {
                    GlobalFactory.signout();
                }
            }, function () {
                GlobalFactory.signout();
            }
        );
        $scope.advisorGridData = [];
        $scope.TotalContact = 0;
        $scope.isLoading = true;
        $scope.pageSize= 20;
        $scope.busy = true;
        $scope.hideloader=false;
        
        advisorDataService.fetchAllClientList().then(function (clientListObject) {
            $scope.isLoading = false;
            $scope.advisorGridData = clientListObject;
            var data = clientListObject;
            var contactCount = {'clientCount': 0, 'prospectsCount': 0};
            if (angular.isArray(data)) {

                data.sort(function (a, b) {
                    if (a.lastName.toLowerCase() < b.lastName.toLowerCase()) {
                        return -1;
                    }
                    if (a.lastName.toLowerCase() > b.lastName.toLowerCase()) {
                        return 1;
                    }
                    return 0;
                });

                var tmp = {};
                for (var i = 0; i < data.length; i++) {
                    $scope.advisorGridData[i].firstNameIndx = data[i].firstName[0].toUpperCase();
                    $scope.advisorGridData[i].lastNameIndx = data[i].lastName[0].toUpperCase();

                    var letter = data[i].lastName.toUpperCase().charAt(0);
                    if (tmp[letter] === undefined) {
                        tmp[letter] = [];
                    }
                    tmp[letter].push(data[i]);
                    if (angular.isArray(data[i].roles) && data[i].roles.length > 0) {
                        if (data[i].roles[0].name === 'ROLE_CLIENT') {
                            contactCount.clientCount = contactCount.clientCount + 1;
                        }
                        else if (data[i].roles[0].name === 'ROLE_PROSPECT') {
                            contactCount.prospectsCount = contactCount.prospectsCount + 1;
                        }
                    }
                }
                clientListObject.sort(function (a, b) {
                    if (a.firstName.toLowerCase() < b.firstName.toLowerCase()) {
                        return -1;
                    }
                    if (a.firstName.toLowerCase() > b.firstName.toLowerCase()) {
                        return 1;
                    }
                    return 0;
                });
                $scope.busy = false;
                $scope.hideloader=true;
                $scope.TotalContact = contactCount.clientCount + contactCount.prospectsCount;
                $rootScope.$broadcast("clientContactsList", {
                    contactsList: tmp,
                    contactCount: contactCount,
                    allContactsList: clientListObject
                });
            }
        }, function (error) {
            $scope.isLoading = false;
            GlobalFactory.showErrorAlert(error.message || 'An error occurred');

        });

        $scope.sendCredentialsToClient = function (userName, userId) {
            var fullName = userName.split("[");
            var email = fullName[1] && fullName[1].replace("]", "");
            OranjApiService('sendCredentialsToClient', {
                data: {'email': email}
            }).then(function (response) {
                if (response.status === "success") {
                    GlobalFactory.showSuccessAlert("Login Credentials Successfully Sent!");
                    for (var i = 0; i < $scope.advisorGridData.length; i++) {
                        if ($scope.advisorGridData[i].userId === userId) {
                            $scope.advisorGridData[i].inviteSent = true;
                            return;
                        }
                    }
                }
            }, function (response) {
                GlobalFactory.showErrorAlert("Sorry Error in Sending Login Credentials!");
            });
        };
        
        $scope.increaeContactLimit = function(){
        	//console.log('trigger contact limit');
        	if ($scope.busy === true) return;
  	   		$scope.busy = true;
  	   		if($scope.TotalContact > $scope.pageSize){
  	   			$scope.pageSize = $scope.pageSize + 20;
  	   			//console.log('$scope.pageSize',$scope.pageSize);
	  	   		$timeout(function () {
	          		$scope.busy = false;
	            }, 500);
  	   		}
        }

        $scope.firminfo = {
            name: 'Matt Frost Firm',
            phone: '224-0098',
            address1: '333 W. Wacker Dr. Suite',
            address2: 'Address2',
            city: 'Mountain View',
            zip: '9387660',
            disclaimer: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia",
            roi: '2%',
            ror: '5%',
            lifeexp: '85',
        };

        $scope.advisorindv = {
            firstname: 'Thomos',
            lastname: 'Fox',
            tittle: 'RIA',
            email: 'tfox@runoranj.com',
            phone: '865-736-9945',
            cell: '773-762-1010',
            fax: '111-982-9737',
            emailsign: "Thomos Fox",
            state: "CA",
            bio: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore",
        };
        $scope.user = {
            state: "CA",
        };


        $scope.firminfo.country = 'US';
        $scope.count = world.countries;

        $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
        'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
        'WY').split(' ').map(function (state) {
            return {abbrev: state};
        });


        $scope.contact = {
            firstname: 'John Christian',
            lastname: 'Mckinley',
            email: 'johnchristian@cogent.com',
            phone: '650-555-1234',
            income: '$507,088',
            dateofbirth: '10/19/1988',
            childdateofbirth: '06/21/1999',
            childfirstname: 'Kate',
            childlastname: 'McKinley',


        };
        $scope.addChild = false;
        $scope.contact.type = 'Lorem',
            $scope.types = ('Lorem ipsum').split(' ').map(function (type) {
                return {abbrev: type};
            });
        $scope.contact.maritalstatus = 'Single',
            $scope.maritalstatus = ('Single Married').split(' ').map(function (maritalstatus) {
                return {abbrev: maritalstatus};
            });

        $scope.invite = {
            fullname: 'John Christian Mckinley',
            email: 'johnchristian@cogent.com',
            message: "Hi,\n\ \n\Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia\n\ \n\Regards\n\Phil",


        };
    }
})();
