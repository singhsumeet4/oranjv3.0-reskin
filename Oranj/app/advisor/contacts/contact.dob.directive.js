(function() {
   
    angular.module('app.advisor.contact')
        .directive('ageCheck', function (){ 
		   return {
		      require: 'ngModel',
		      link: function(scope, elem, attr, ngModel) {
		          //For DOM -> model validation
		          ngModel.$parsers.unshift(function(value) {
		        	  if(value){
		        		  var now = Date.now();
			        	  var dob = new Date(value).getTime();
			        	  var ageDifMs = now - dob;
			        	  var age = 0;
			        	  if(ageDifMs > 0){
			        		  var ageDate = new Date(ageDifMs); // miliseconds from epoch
				        	      age = Math.abs(ageDate.getUTCFullYear() - 1970);
			        	  }
			        	  if(age < 18){
			        		  ngModel.$setValidity('dob', false);
			        	  }else{
			        		  ngModel.$setValidity('dob', null);
			        	  }
		        	  }
		        	  return value;
		          });
		
		          //For model -> DOM validation
		          ngModel.$formatters.unshift(function(value) {
		        	  if(value){
		        		  var now = Date.now();
			        	  var dob = new Date(value).getTime();
			        	  var ageDifMs = now - dob;
			        	  var age = 0;
			        	  if(ageDifMs > 0){
			        		  var ageDate = new Date(ageDifMs); // miliseconds from epoch
				        	      age = Math.abs(ageDate.getUTCFullYear() - 1970);
			        	  }
			        	  if(age < 18){
			        		  ngModel.$setValidity('dob', false);
			        	  }else{
			        		  ngModel.$setValidity('dob', null);
			        	  }
		        	  }
		        	  return value;
		          });
		      }
		   };
		});
    
    angular.module('app.advisor.contact')
    .directive('childAge', function (){ 
	   return {
	      require: 'ngModel',
	      link: function(scope, elem, attr, ngModel) {
	          //For DOM -> model validation
	          ngModel.$parsers.unshift(function(value) {
	        	  if(value){
	        		  var now = Date.now();
		        	  var dob = new Date(value).getTime();
		        	  var ageDifMs = now - dob;
		        	  if(ageDifMs > 0){
		        		  ngModel.$setValidity('dob', null);
		        	  }else{
		        		  ngModel.$setValidity('dob', false);
		        	  }
	        	  }
	        	  return value;
	          });
	
	          //For model -> DOM validation
	          ngModel.$formatters.unshift(function(value) {
	        	  if(value){
	        		  var now = Date.now();
		        	  var dob = new Date(value).getTime();
		        	  var ageDifMs = now - dob;
		        	  if(ageDifMs > 0){
		        		  ngModel.$setValidity('dob', null);
		        	  }else{
		        		  ngModel.$setValidity('dob', false);
		        	  }
	        	  }
	        	  return value;
	          });
	      }
	   };
	});
})();