(function () {
    'use strict';

    angular.module('app.advisor.contact')
        .controller('createContactCtrl', ['$scope','$rootScope', 'world', 'OranjApiService', 'GlobalFactory', '$state', createContactCtrl]);
    
        
         function createContactCtrl ($scope , $rootScope , world, OranjApiService, GlobalFactory, $state) {
    	
	    	var role = GlobalFactory.getRoleService();
	        role.then(
	  	            function (result) {
	      	            if(result.isAdmin || result.isAdvisor){
	      	            	
	      	            }else{
	      	            	GlobalFactory.signout();
	      	            }
	  	            }, function () {
	  	                GlobalFactory.signout();
	  	            }
	  	   	);
	        
	        $scope.contact = {};
	        
	        $scope.$watch('switchData.children', function (n, o) {
	        	if(n === true && n !== o){
	        		$scope.contact.children = [{
	        			name: "",
	        			last: "",
	        			dob: ""
		        	}];
	        	}else{
	        		$scope.contact.children = [];
	        	}
	        });
	        
	        $scope.$watch('contact.maritalStatus', function (n, o) {
                if(n === 'single' && n !== o){
                	$scope.spouseStat = false;
                	if($scope.contact.spouse){
                		delete $scope.contact.spouse;
                	}
                }else{
                	$scope.spouseStat = true;
                	$scope.contact.spouse = {
                			name: "",
                			last: "",
                			dob: "",
                			income: ""
                	};
                }
            });
	        
	        
	        $scope.contact_type = 'Client',
	        $scope.types = ('Client Prospect').split(' ').map(function(type) {
		        return {abbrev: type};
		    });
	        $scope.contact.maritalStatus = 'single',
	        $scope.maritalstatus = ('single married').split(' ').map(function(maritalstatus) {
		        return {abbrev: maritalstatus};
		    });
           
	        $scope.saveContact = function () {
	        	if($scope.form_contact.$valid){
	        		console.log($scope.contact);
	        		OranjApiService('createContact', {
	        			data: $scope.contact
	        		}, {
	        			type: $scope.contact_type,
                    }).then(function(res) {
                        if(res.status === 'success'){
                        	var newContactId = res.data.id;console.log(newContactId);
                        	$state.go('page.advisor.contacts.createsuccess');
                        }
                    });
	        	}
	        };
	        
	        $scope.removeChild = function ($index){
	        	$scope.contact.children.splice($index,1);
	        	if($scope.contact.children.length === 0 ){
	        		$scope.switchData.children = false;
	        	}
	        };
	        
	        $scope.addChidren = function () {
	        	if($scope.contact.children.length >0){
	        		var index = $scope.contact.children.length-1;
	        		if($scope.form_contact['children_name_'+index].$invalid || $scope.form_contact['children_last_'+index].$invalid || $scope.form_contact['children_dob_'+index].$invalid){
	        			$scope.form_contact['children_name_'+index].$setTouched();
		        		$scope.form_contact['children_last_'+index].$setTouched();
		        		$scope.form_contact['children_dob_'+index].$setTouched();
		        		return;
	        		}
	        	}
        		$scope.contact.children.push({
        			name: "",
        			last: "",
        			dob: ""
	        	});
	        };
    	}
    })(); 