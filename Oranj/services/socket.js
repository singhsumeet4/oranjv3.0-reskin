angular.module('app').factory('socket', ['socketFactory', function (socketFactory) {

     var myIoSocket = io.connect('/');
	// var myIoSocket = io.connect(socketConfig.host + ':' + socketConfig.port + socketConfig.path);

	var mySocket = socketFactory({
		ioSocket: myIoSocket
	});

	console.log('In socket.js', myIoSocket);

    return mySocket;
}]);
