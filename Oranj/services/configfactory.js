(function () {
    'use strict';

    angular.module('app').factory('configfactory',['$cookies',configfactory]);


    	function configfactory($cookies) {
    		var origin = '/oranj/';
            var pathArray = window.location.hostname.split( '.' );
            var nodeApiHost = ''; // here put the node api origin
            var host = origin ;
            var firmName = pathArray[0];
            var host = host+firmName;
            var origin = origin;
            var auth = $cookies.get('auth');
            var token = (auth == undefined?"":JSON.parse(auth).access_token);
            var userCredential = {
                              "access_token": 'Bearer ' + token,
                              "token_type": "bearer"
                           };

    		return {
    			origin : origin,
    			nodeApiHost : nodeApiHost,
    			host : host,
    			firmName : firmName,
    			auth : auth,
    			token : token,
    			userCredential : userCredential
             }
        }

})();
