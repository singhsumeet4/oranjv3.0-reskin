(function () {
    'use strict';

    angular.module('app').factory('GlobalFactory', ['$rootScope', '$cookies', '$cookieStore', 'OranjApiService','$uibModal','UserProfileService','$timeout',globalfactory]);


    function globalfactory($rootScope, $cookies, $cookieStore, OranjApiService, $uibModal, UserProfileService, $timeout) {
        var globalfactoryArray = {};


       globalfactoryArray.lookupRole = function ( role, arr ) {
        		for(var i = 0, len = arr.length; i < len; i++) {
        			if( arr[ i ].name === role ) {
        				globalfactoryArray.initUserData();
        				return true;
        			}

        		}
        		return false;
            };

        globalfactoryArray.getRole = function () {
            return OranjApiService('getUserRole');
        };

        globalfactoryArray.getRoleService = function (){
        	return UserProfileService.fetchUserRole();
        }

        globalfactoryArray.getProfileService = function (){
        	return UserProfileService.fetchUserProfile();
        }

		globalfactoryArray.initUserData = function (){
		    if (angular.isUndefinedOrNull($rootScope.avatarUrl)) {
		        OranjApiService('getUserProfileData',null,{'userid':''}).then(function (response) {
		            var avatar = '../../../images/avatar.jpg';
		            if (response.status == "success") {		               
		                if (response.data.profile.avatarCompleteUrl != null) {
		                    //var parts = response.data.profile.avatarFile.fileName.split('.');
		                    //var avatar = 'data:image/' + parts[parts.length - 1] + ';base64,' + response.data.profile.avatarFile.file;
		                    avatar = response.data.profile.avatarCompleteUrl;
		                }
		            }
		            $rootScope.avatarUrl = avatar;
		        }, function (response) {

		        });
		    }
        };

        globalfactoryArray.signout = function () {
            $cookies.put('auth', '', {path: '/'});
            $cookieStore.remove('auth');
            $cookies.remove("auth");
            //localStorageService.clearAll();
            location.href = '/';
        };

        globalfactoryArray.showSuccessUploadFile = function () {
            var modalInstance = $uibModal.open({
                template: '<h4 class="modal-body"><span><i class="fa fa-check-circle"></i></span><span> File is successfully added.</span> </h4>',
                windowClass: 'modalcls',
                animation: true
                //backdrop: 'static'
            });
        }

        globalfactoryArray.showSuccessAlert = function (message) {
            var modalInstance = $uibModal.open({
                template: '<h4 class="modal-body"><span><i class="fa fa-check-circle"></i></span><span> '+message+'</span> </h4>',
                windowClass: 'modalcls',
                size: 'lg',
                animation: true
                //backdrop: 'static'
            });
        }

  //-----------for Error alert st-----------------------

            globalfactoryArray.showErrorAlert = function (message) {
            var modalInstance = $uibModal.open({
                template: '<h4 class="modal-body"><span><i class="fa fa-exclamation-triangle"></i></span><span> '+message+'</span> </h4>',
                windowClass: 'modalcls-err',
                size: 'lg',
                animation: true
                //backdrop: 'static'
            });

            $timeout(function () {
              modalInstance.close();
            }, 3000);
        }

  //-----------for Error alert end-----------------------

        //-----------for shared global data-----------------------

        globalfactoryArray.sharedData = {
            yodleeItemAccountId: 0,
            yodleeItemAccountName: '',
            currentState: '',
            sideVaultList: '',
            isRootFolder : true
        };

        //--------------------------------------------------------------------------------
        //  for Home/Account whenever change the state empty account and for current state
        //--------------------------------------------------------------------------------
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            globalfactoryArray.sharedData.currentState = toState.name;
            broadcastGlobalEvents(toState.name);
        });

        function broadcastGlobalEvents(currentState) {
            $rootScope.$broadcast("currentStateStatus", currentState);
        };
        //---------------------------------------------------------


        return globalfactoryArray;
    }

})();

angular.module('app').directive('dropZone', function (GlobalFactory, OranjApiService, $rootScope, $cookies, vaultDataService, advisorVaultDataService) {
    return function (scope, element, attrs) {

    	if((scope.global.vaultsFolderName != "vault") && (scope.global.vaultsFolderName != "recent") && (scope.global.vaultsFolderName != "starred")) {
    	    var apiUrl = OranjApiService.host + '/' + OranjApiService.firmname + '/vault/file-upload?folder=' + scope.global.vaultsFolderName;
    	}else {
    	    var apiUrl = OranjApiService.host + '/' + OranjApiService.firmname + '/vault/file-upload';
    	}
    	var uploadFileCount = 1;
        var INVALID_VALID_FILE_EXTNS = ['bin', 'exe'];
        element.dropzone({
            autoProcessQueue: true,
            url: apiUrl,            
            // acceptedFiles: ".gif, .jpeg,.bmp,.jpg,.gif, .png, .jpe,.psd,.pdf,.doc,.docx, .xls,.xlsx,.txt,.csv,.ppt,.pptx,.rtf,.mp3,.wav,.3gp,.flv,.mp4,.mpg,.avi,",
            maxFilesize: 10,
            paramName: "fileMult",
            addRemoveLinks: false,
            dictFileTooBig: "Maximum Upload size is 10 MB",
            clickable: "#vaultUpload",
            previewTemplate: '<div style="display:none"></div>',
            headers: {
                "Authorization": 'Bearer ' +  $cookies.getObject('auth').access_token
            },
            init: function () {
            	var myDropzone = window.myDropzone = this;
                var drop = this;
                var successFiles = 0;
                var errorFiles = 0;
                var errorMessage = 0;
                if(typeof scope.files == 'undefined'){
                    scope.files = [];
                }
                scope.files.push({file: 'added'}); // here works
                drop.on('then', function (file, json) {
                });
                drop.on("queuecomplete", function () {
                    window.NProgress.done();
                	if (errorFiles > 0 && successFiles === 0 && uploadFileCount === 1) {
                        GlobalFactory.showErrorAlert(errorMessage || 'An error occurred with the file upload');
                        errorFiles = 0;
                        errorMessage = 0;
                        successFiles = 0;
                    }else if (errorFiles > 0 && successFiles === 0 && uploadFileCount !== 1) {
                        GlobalFactory.showErrorAlert(errorMessage || 'An error occurred in bulk file upload');
                        errorFiles = 0;
                        errorMessage = 0;
                        successFiles = 0;
                    }
                    else if (successFiles > 0) {
                    	
                    	 if(attrs.dropZone === 'advisor'){
                    		 if(scope.global.vaultsFolderName != ""){
                    			 scope.getFolderData(scope.global.vaultsFolderName);
                    		 }else{
                    			 scope.initVault(scope.userData.userId,scope.userData);
                    		 }
                    		 if(errorFiles === 0){
                    			 GlobalFactory.showSuccessUploadFile();
                    		 }else if (errorFiles > 0 && uploadFileCount !== 1) {
                                 GlobalFactory.showErrorAlert(errorMessage || 'An error occurred in bulk file upload');
                    		 }else{
                    			 GlobalFactory.showErrorAlert(errorMessage || 'An error occurred with the file upload');
                    		 }
                    	 }else{
                    		 if ((scope.global.vaultsFolderName != "vault") && (scope.global.vaultsFolderName != "recent") && (scope.global.vaultsFolderName != "starred")) {
                                 scope.getFolderData(scope.global.vaultsFolderName);

                                 vaultDataService.fetchVaultRootList().then(onSuccess, function (err) { console.log(error) });
                                 function onSuccess(res) {
                                     var data = vaultDataService.prepareVaultData(res);
                                     scope.global.fileCount = data.totalFileCount;
                                 }
                             } else {
                                 scope.initVault();
                             }
                    		 if(errorFiles === 0){
                    			 GlobalFactory.showSuccessUploadFile();
                    		 }else if (errorFiles > 0 && uploadFileCount !== 1) {
                                 GlobalFactory.showErrorAlert(errorMessage || 'An error occurred in bulk file upload');
                    		 }else{
                    			 GlobalFactory.showErrorAlert(errorMessage || 'An error occurred with the file upload');
                    		 }
                    	 }
                    	 errorFiles = 0;
                         errorMessage = 0;
                     	 successFiles = 0;
                    }
                });
                drop.on("processing", function (file) {
                    window.NProgress.start();
                    if(attrs.dropZone === 'advisor'){
                    	//console.log('scope.userData',scope.userData);
                    	if(scope.userData.userId){
                    		if(scope.global.vaultsFolderName != ""){
                        		drop.options.url = OranjApiService.host +'/'+ OranjApiService.firmname + '/vault/file-upload?userId='+ scope.userData.userId +'&folder=' + scope.global.vaultsFolderName;
                        	}else{
                        		drop.options.url = OranjApiService.host +'/'+ OranjApiService.firmname + '/vault/file-upload?userId='+ scope.userData.userId;
                        	}
                    	}else{
                    		drop.removeAllFiles(true);
                    		GlobalFactory.showErrorAlert("Select a contact first.");
                    	}
                    	
                    }else{
                    	if((scope.global.vaultsFolderName != "vault") && (scope.global.vaultsFolderName != "recent") && (scope.global.vaultsFolderName != "starred")) {
                            drop.options.url = OranjApiService.host +'/'+ OranjApiService.firmname +  '/vault/file-upload?folder=' + scope.global.vaultsFolderName;
                        }else {
                            drop.options.url = OranjApiService.host + '/' + OranjApiService.firmname + '/vault/file-upload';
                        }
                    }
                });
                $rootScope.change_FileName = function (fileName) {
                    var file_name = fileName;
                    var files = drop.getQueuedFiles();
                    var index = parseInt(files.length - 1);
                    var current_file = files[index];
                    current_file.custom_name = file_name;
                    var selector = "div.dz-preview:eq(" + index + ")";
                    $(selector).find('.dz-details .dz-filename span').text(file_name);
                    //GlobalFactory.closeModal()
                };
                $('#upload-file-btn').click(function () {
                    if (scope.global.vaultsFolderName != '')
                        drop.processQueue();
                    else
                        alert('Select a Folder First');//GlobalFactory.showthenModal('Select a Folder First');
                });
                $("#file-upload-cancel").click(function () {
                    drop.removeAllFiles(true);
                })
                drop.on("sending", function (file, xhr, formData) {
                    if (!file.custom_name) {
                        file.custom_name = file.name;
                    }
                    formData.append("name", file.custom_name);
                });
                drop.on('addedfile', function (file,fileCount) {
                	uploadFileCount = fileCount;
                	if(attrs.dropZone === 'advisor'){
                    	if(!scope.userData.userId){
                    		this.removeFile(file);
                    		GlobalFactory.showErrorAlert("Select a contact first.")
                    	}
                	}
                     var extn = file.name.split(".").pop().toLowerCase();
                     var sizeInMb = (file.size / (1024 * 1024));
                     
                    if (INVALID_VALID_FILE_EXTNS.indexOf(extn) > -1)
                    {
                        GlobalFactory.showErrorAlert('Invalid File Type');
                        this.removeFile(file);
                    }else if (sizeInMb > 10){
                    	this.removeFile(file);
                    }
                    else{                       
                    scope.$apply(function () {
                        scope.files.push({file: 'added'});
                        });
                    }
                });
                drop.on('drop', function (file) {
                    // drop.processQueue();
                });

                drop.on('success', function (file) {
                    successFiles++;
                });

                drop.on('error', function (file, error) {
                	if(typeof error === 'object' && error.message){
                		errorMessage = error.message;
                	}else{
                		errorMessage = error;  
                	}
                    errorFiles++;
                });


            }

        });

    }
});
