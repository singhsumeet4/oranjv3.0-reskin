var gulp = require('gulp');
var args = require('yargs').argv;
var connect = require('connect'),
    http = require('http'),
    serveStatic = require('serve-static');
var config = require('./gulp.config')();
var del = require('del');
var $ = require('gulp-load-plugins')({lazy: true});
var karmaServer = require('karma').Server;
var proxy = require('http-proxy-middleware'),
    htmlhint = require("gulp-htmlhint"),
    zip = require('gulp-vinyl-zip').zip,
    ngTemplates = require('gulp-ng-templates'),
    htmlmin = require('htmlmin'),
    path = require('path');
var apiProxy;

//
//
//
// var socketProxy = proxy('/socket.io/',
//     {
//         target: config.socketUrl,
//         changeOrigin: 'true',
//         logLevel: 'debug',
//         pathRewrite: {
//             'ws://localhost:3000/socket.io/': config.wsSocketUrl
//         }
//     });

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('fixjs', function () {
    log('Analyzing source with JSHint and JSCS');

    return gulp
        .src(config.alljs)
       // .pipe($.if(args.verbose, $.print()))
       // .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-html-reporter', {reporterOutput: 'js-report.html'}))
       // .pipe($.jshint.reporter('default'))
        .pipe($.jshint.reporter('fail'));
});

gulp.task('fixhtml', function (done) {
    log('Analyzing html source with html hint');

    console.log([config.index, config.allTemplatesHtml]);
    return gulp.src([config.index, config.allTemplatesHtml])
        .pipe(htmlhint('.htmlhintrc'))
        .pipe(htmlhint.reporter('htmlhint-stylish'))
        .pipe(htmlhint.failReporter({
            supress: true
        }));
    //.pipe(htmlLint.format())
    //.pipe(htmlLint.failOnError())
});

gulp.task('genNgTemplates', function() {
    console.log(config.allTemplatesHtml);
    return gulp.src(config.allTemplatesHtml,{base: path.join(process.cwd(),'Oranj/')})
        //.pipe(htmlmin({collapseWhitespace: true}))
        .pipe(ngTemplates({
            filename: 'templates.js',
            module:'app.templates',
            standalone: true
        }))
        .pipe(gulp.dest(config.client));
});

gulp.task('cleanup', function () {
    gulp.start('fixjs', 'fixhtml');
})

gulp.task('clean-tmp', function (done) {
    var files = config.tmp;
    clean(files, done);
});

gulp.task('clean', function (done) {
    var delconfig = [].concat(config.dist, config.tmp);
    log('Cleaning ' + $.util.colors.blue(delconfig));
    del(delconfig, done);
});

gulp.task('clean-all', function (done) {
    var delconfig = config.allToClean;
    log('Cleaning ' + $.util.colors.blue(delconfig));
    clean(delconfig, done);
});

gulp.task('less', function () {
    log('Compiling Less --> CSS');

    return gulp
        .src(config.less)
        .pipe($.plumber({errorHandler: swallowError}))
        .pipe($.less())
        .pipe($.autoprefixer())
        .pipe(gulp.dest(config.tmp));
});

gulp.task('less-watcher', function () {
    gulp.watch([config.less], ['less']);
});

gulp.task('sass', function () {
    log('Compiling Sass --> CSS');

    var sassOptions = {
        outputStyle: 'nested' // nested, expanded, compact, compressed
    };

    return gulp
        .src(config.sass)
        .pipe($.plumber({errorHandler: swallowError}))
        .pipe($.sourcemaps.init())
        .pipe($.sass(sassOptions))
        .pipe($.autoprefixer())
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(config.tmp + '/styles'));
});

gulp.task('sass-min', function () {
    log('Compiling Sass --> minified CSS');

    var sassOptions = {
        outputStyle: 'compressed' // nested, expanded, compact, compressed
    };

    return gulp
        .src(config.sass)
        .pipe($.plumber({errorHandler: swallowError}))
        .pipe($.sass(sassOptions))
        .pipe($.autoprefixer())
        .pipe(gulp.dest(config.tmp + '/styles'));
})

gulp.task('sass-watcher', function () {
    gulp.watch([config.sass], ['sass']);
});

gulp.task('html-watcher', function() {
    gulp.watch([config.allTemplatesHtml], ['genNgTemplates']);
})

gulp.task('inject', function () {
    // log('Injecting custom scripts to index.html');

    // return gulp
    //     .src(config.index)
    //     .pipe( $.inject(gulp.src(config.js), {relative: true}) )
    //     .pipe(gulp.dest(config.client));
});

gulp.task('copy', function () {
    log('Copying assets');

    return gulp
        .src(config.assets, {base: config.client})
        .pipe(gulp.dest(config.dist + '/'));
});

gulp.task('optimize', ['inject', 'sass-min'], function () {
    log('Optimizing the js, css, html');

    return gulp
        .src(config.index)
        .pipe($.plumber({errorHandler: swallowError}))
        .pipe($.useref())
        //.pipe($.if('scripts/app.js', $.uglify()))
        .pipe(gulp.dest('temp2'));
});


gulp.task('serve', ['inject', 'sass', 'genNgTemplates', 'html-watcher'], function () {
    apiProxy = proxy('/', {
        target: 'http://localhost:3000',
        changeOrigin: 'true',
        ws: true,
        // onProxyRes: function (proxyRes, req, res) {
        //     delete proxyRes.headers['www-authenticate'];
        // },
        pathRewrite: {
            '^/oranj/localhost': '/oranj/' + config.apiFirm,
        },
        router: {
            '/oranj': config.apiUrl,
            '/socket.io': config.socketUrl,
            '/admin_tool_dash.html' : config.apiUrl,
            '/msa-app' : config.apiUrl
        }
    });
    startBrowserSync('serve');
});

gulp.task('build', ['optimize', 'copy'], function () {
    startBrowserSync('dist');
})

gulp.task('serve-dist', function () {
    gulp.run('build');
})


gulp.task('unit-test', ['optimize'], function (done) {
    new karmaServer({
        configFile: __dirname + '/tests/unit.karma.conf.js',
        singleRun: true,
    }, function (exitCode) {
        done();
        process.exit(exitCode);
    }).start();
});

gulp.task('tdd', ['optimize'], function (done) {
    new karmaServer({
        configFile: __dirname + '/tests/unit.karma.conf.js',
        singleRun: false,
    }, function (exitCode) {
        done();
        process.exit(exitCode);
    }).start();
})

gulp.task('package',['genNgTemplates'],function (done) {
    gulp.src([config.client+'/**/*.*'], {base: config.client})
        .pipe(zip('reskin.zip'))
        .pipe(gulp.dest('./package/'))
        .on('end', done);
});

function clean(path, done) {
    log('Cleaning: ' + $.util.colors.blue(path));
    del(path, done);
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.green(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.green(msg));
    }
}

function swallowError(error) {
    // If you want details of the error in the console
    console.log(error.toString());

    this.emit('end');
}

function startBrowserSync(opt) {


    var options = {
        port: 3000,
        ghostMode: {
            clicks: false,
            location: false,
            forms: false,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 0, //1000,
        online: false
    };

    switch (opt) {
        case 'dist':
            log('Serving dist app');
            serveDistApp();
            break;
        default:
            log('Serving app');
            serveApp();
            break;
    }

    function serveApp() {

        var staticMiddleware = serveStatic(config.client);

        var connectApp = connect();

        connectApp.use(staticMiddleware);
        connectApp.use(apiProxy);


        http.createServer(connectApp).listen(3000);

    }

    function serveDistApp() {

        var staticMiddleware = serveStatic(config.dist);

        var connectApp = connect();

        connectApp.use(staticMiddleware);
        connectApp.use(apiProxy);

        http.createServer(connectApp).listen(3000);
    }
}


